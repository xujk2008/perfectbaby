
package com.perfectbaby.EasterEggs;
/**
 * Created by KiteXu.
 */
import java.util.ArrayList;
import java.util.Random;

import android.animation.TimeAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bebeanan.perfectbaby.R;

@SuppressLint("NewApi")
public class FlyingObjectsActivity extends Activity {

	public final static String RESOURCE = "resource"; 
	private boolean isArray;
	
    public static class Board extends FrameLayout {
        public static final boolean FIXED_STARS = true;

        // 控制数量
        public static final int NUM_OBJECTS = 100;

        static Random sRNG = new Random();

        private TimeAnimator mAnim;

        private Context mContext;
        
        private int resource;
        
        private ArrayList<Integer> resourceArray;

        public Board(Context context, AttributeSet as, int resource) {
            super(context, as);
            this.mContext = context;
            this.resource = resource;
        }
        
        public Board(Context context, AttributeSet as, ArrayList<Integer> resourceArray) {
            super(context, as);
            this.mContext = context;
            this.resourceArray = resourceArray;
        }
        
        static float getMiddle(float a, float b, float f) {
            return (b - a) * f + a;
        }

        static float randRange(float a, float b) {
            return getMiddle(a, b, sRNG.nextFloat());
        }

        public class FlyingObject extends ImageView {
            public static final float VMAX = 110.0f;
            public static final float VMIN = 40.0f;
            
            public static final float DMAX = 23.0f;
            public static final float DMIN = 6.0f;

            public float v, w;

            public float z;

            public ComponentName component;

            public FlyingObject(Context context, AttributeSet as, int resource) {
                super(context, as);
                
                if(resource != -1)
                {
                	this.setImageResource(resource);
                }
                else
                {
                	this.setImageResource(R.drawable.ic_launcher);
                }
            }
            
            public FlyingObject(Context context, AttributeSet as, ArrayList<Integer> resourceArray) {
                super(context, as);
                
                int index = sRNG.nextInt(resourceArray.size());
                
                this.setImageResource(resourceArray.get(index));
            }

            public void init() {
                final float scale = getMiddle(1.5f, 2.5f, z);
                this.setScaleX(scale);
                this.setScaleY(scale);
                this.setY(randRange(0, Board.this.getHeight() - scale * Board.this.getHeight()));
                v = getMiddle(VMIN, VMAX, z);
                w = getMiddle(DMIN, DMAX, 1-z);
            }

            public void update(float dt) {
                this.setY(getY() + v * dt);
                this.setRotation(getRotation() + (w*dt)%360);
            }
        }

        private void reset() {
            removeAllViews();
            final ViewGroup.LayoutParams wrap = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            for (int i = 0; i < NUM_OBJECTS; i++) {
            	
            	FlyingObject flyingObject;
            	if(resourceArray == null)
            	{
            		flyingObject = new FlyingObject(getContext(), null, resource);
            	}
            	else
            	{
            		flyingObject = new FlyingObject(getContext(), null, resourceArray);
            	}
            	
                addView(flyingObject, wrap);
                flyingObject.z = ((float) i / NUM_OBJECTS);
                flyingObject.z *= flyingObject.z;
                flyingObject.init();
                flyingObject.setX(randRange(0, Board.this.getWidth()));
            }
            if (mAnim != null) {
                mAnim.cancel();
            }
            mAnim = new TimeAnimator();
            mAnim.setTimeListener(new TimeAnimator.TimeListener() {
                public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
                    for (int i = 0; i < getChildCount(); i++) {
                        View v = getChildAt(i);
                        if (!(v instanceof FlyingObject))
                            continue;
                        FlyingObject nv = (FlyingObject) v;
                        nv.update(deltaTime / 200f);
                    }
                }
            });
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            post(new Runnable() {
                public void run() {
                    reset();
                    mAnim.start();
                }
            });
        }

        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            mAnim.cancel();
        }

        @Override
        public boolean isOpaque() {
            return true;
        }
    }

    private Board mBoard;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        isArray = FlyingObjectsActivity.this.getIntent().getBooleanExtra("isArray", false);
        if(isArray)
        {
        	mBoard = new Board(this, null, FlyingObjectsActivity.this.getIntent().getIntegerArrayListExtra(RESOURCE));
        }
        else
        {
        	mBoard = new Board(this, null, FlyingObjectsActivity.this.getIntent().getIntExtra(RESOURCE, -1));
        }
        
        setContentView(mBoard);
        mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				finish();
			}
		}, 9000);
	}
}
