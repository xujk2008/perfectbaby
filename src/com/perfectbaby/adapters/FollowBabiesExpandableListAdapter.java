package com.perfectbaby.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.AddBabyActivity;
import com.bebeanan.perfectbaby.BabyActivity;
import com.bebeanan.perfectbaby.MainActivity;
import com.bebeanan.perfectbaby.MemoryHandler;
import com.bebeanan.perfectbaby.R;
import com.bebeanan.perfectbaby.StoreHandler;
import com.bebeanan.perfectbaby.common.Utils;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
/**
 * Created by KiteXu.
 */
public class FollowBabiesExpandableListAdapter extends BaseExpandableListAdapter{

	Context context;
	ArrayList<String> mainData;
	ArrayList<ArrayList<Map<String, Object>>> childData;
	
	private OnAddBabyListener onAddBabyListener;
	
	public FollowBabiesExpandableListAdapter(Context context, ArrayList<String> mainData, ArrayList<ArrayList<Map<String, Object>>> childData)
	{
		this.context = context;
		this.mainData = mainData;
		this.childData = childData;
	}
	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return mainData.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		
		if(childData.get(groupPosition).size() == 0)
		{
			return 1;
		}
		
		return childData.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return mainData.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childData.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	TextView getTextView() {
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT, 64);
        TextView textView = new TextView(
                context);
        textView.setTypeface(Utils.typeface);
        textView.setLayoutParams(lp);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(36, 0, 0, 0);
        textView.setTextSize(20);
        textView.setTextColor(Color.BLACK);
        return textView;
    }
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LinearLayout groupView = new LinearLayout(
                context);
		groupView.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout relativeLayout = new RelativeLayout(context);
		
		TextView textView = getTextView();
        textView.setTypeface(Utils.typeface);
        textView.setText(mainData.get(groupPosition));
        relativeLayout.addView(textView);
        
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)textView.getLayoutParams();
        params.leftMargin = (int)(20*Utils.density);
        textView.setLayoutParams(params);
        
        if(groupPosition == 0)
        {
        	ImageView addImage = new ImageView(context);
            addImage.setImageResource(R.drawable.follow_babies_list_add_selector);
            addImage.setClickable(true);
            relativeLayout.addView(addImage);
            
            params = (RelativeLayout.LayoutParams)addImage.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            params.rightMargin = (int)(20*Utils.density);
            addImage.setLayoutParams(params);
            
            StoreHandler.getAndSetBabies();
    		String baby = MemoryHandler.getInstance().getKey("baby");
    		try {
    			JSONArray babyArray = new JSONArray(baby);
//    			if (babyArray.length() > 2) {
//    				addImage.setImageResource(R.drawable.follow_babies_list_add_selector);
//    	            addImage.setClickable(true);
//    	            addImage.setOnClickListener(new OnClickListener(){
//
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method stub
//							Toast.makeText(context, "亲，最多只能添加3个宝宝哦~", Toast.LENGTH_LONG).show();
//						}
//    	            	
//    	            });
//    			} 
//    			else {
    				addImage.setImageResource(R.drawable.follow_babies_list_add_selector);
    	            addImage.setClickable(true);
    				addImage.setOnClickListener(new OnClickListener(){

    					@Override
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						if(onAddBabyListener != null)
    						{
    							onAddBabyListener.onAddBaby();
    						}
    					}
    	            	
    	            });
//    			}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
        
        groupView.addView(relativeLayout);
        
        ImageView dividerImage = new ImageView(context);
        dividerImage.setImageResource(R.drawable.follow_babies_list_divider);
        groupView.addView(dividerImage);
        
        LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams)relativeLayout.getLayoutParams();
        linearLayoutParams.gravity = Gravity.CENTER_VERTICAL;
        linearLayoutParams.topMargin = (int)(20*Utils.density);
        relativeLayout.setLayoutParams(linearLayoutParams);
        
        linearLayoutParams = (LinearLayout.LayoutParams)dividerImage.getLayoutParams();
        linearLayoutParams.width = RelativeLayout.LayoutParams.FILL_PARENT;
        linearLayoutParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        linearLayoutParams.topMargin = (int)(10*Utils.density);
        dividerImage.setLayoutParams(linearLayoutParams);

        return groupView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(childData.get(groupPosition).size()==0)
		{
			TextView emptyView = new TextView(context);
			emptyView.setTypeface(Utils.typeface);
			emptyView.setTextSize(18);
			emptyView.setPadding(0, (int)(20*Utils.density), 0, (int)(20*Utils.density));
			emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
			
			emptyView.setTag(R.id.group_position, -1);
			emptyView.setTag(R.id.child_position, -1);
			
			switch(groupPosition)
			{
			case 0:
				
				emptyView.setText("添加宝宝\n记录TA的成长点滴O(∩_∩)O");
				return emptyView;
				
			case 1:
				
				emptyView.setText("添加小伙伴\n关注他们宝宝的成长O(∩_∩)O");
				return emptyView;
				
			}
		}
		
		ChildViewHolder holder;
		
		if(convertView == null || convertView.getTag() == null)
		{
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_follow_babies_list, null);
			
			holder = new ChildViewHolder();
			
			holder.follow_babies_list_avatar = (ImageView) convertView.findViewById(R.id.follow_babies_list_avatar);
			holder.follow_babies_list_nickname = (TextView) convertView.findViewById(R.id.follow_babies_list_nickname);
			holder.follow_babies_list_gender = (ImageView) convertView.findViewById(R.id.follow_babies_list_gender);
			holder.follow_babies_list_age = (TextView) convertView.findViewById(R.id.follow_babies_list_age);
		}
		else
		{
			holder = (ChildViewHolder) convertView.getTag();
		}
		
		Map<String, Object> cur = childData.get(groupPosition).get(childPosition);

		String avatarUrl = (String) cur.get("avatarUrl");
		if (!avatarUrl.equals("")) {
			new UrlImageViewHelper().setUrlDrawable(
					holder.follow_babies_list_avatar, avatarUrl);
		} else {
			
			if(groupPosition == 0)
			{
				holder.follow_babies_list_avatar
				.setImageResource(R.drawable.baby_default_pic);
			}
			else
			{
				holder.follow_babies_list_avatar
				.setImageResource(R.drawable.user_info_default_avatar);
			}
		}
		
		String nickName = (String) cur.get("nickName");
		holder.follow_babies_list_nickname.setTypeface(Utils.typeface);
		holder.follow_babies_list_nickname.setText(nickName);
		
		int gender = (Integer) cur.get("gender");
		if(gender == 1)
		{
			holder.follow_babies_list_gender.setImageResource(R.drawable.user_info_male);
		}
		else
		{
			holder.follow_babies_list_gender.setImageResource(R.drawable.user_info_female);
		}
		
		if(groupPosition == 0)
		{
			holder.follow_babies_list_age.setVisibility(View.VISIBLE);
			holder.follow_babies_list_age.setTypeface(Utils.typeface);
			String birthday = (String)cur.get("age");
			holder.follow_babies_list_age.setText(birthday);
		}
		else
		{
			holder.follow_babies_list_age.setVisibility(View.GONE);
		}
		
		convertView.setTag(holder);
		convertView.setTag(R.id.group_position, groupPosition);
		convertView.setTag(R.id.child_position, childPosition);
		
		return convertView;
	}

	private class ChildViewHolder{
		ImageView follow_babies_list_avatar;
		TextView follow_babies_list_nickname;
		ImageView follow_babies_list_gender;
		TextView follow_babies_list_age;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	public interface OnAddBabyListener{
		public void onAddBaby();
	};

	public void setOnAddBabyListener(OnAddBabyListener onAddBabyListener)
	{
		this.onAddBabyListener = onAddBabyListener;
	}
}
