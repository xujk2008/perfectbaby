package com.perfectbaby.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
/**
 * Created by KiteXu.
 */
public class StartWelcomePagerAdapter extends PagerAdapter{

	private Context context;
	private ArrayList<ImageView> mData;
	
	public StartWelcomePagerAdapter(Context context, ArrayList<ImageView> mData)
	{
		this.context = context;
		this.mData = mData;
	}
	
	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Object instantiateItem(View arg0, int arg1) {
		
		((ViewPager) arg0).addView(mData.get(arg1));
		
		return mData.get(arg1);
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}
}
