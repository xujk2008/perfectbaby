package com.perfectbaby.adapters;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bebeanan.perfectbaby.R;
import com.bebeanan.perfectbaby.common.Utils;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
/**
 * Created by KiteXu.
 */
public class FollowerExpandableListAdapter extends BaseExpandableListAdapter{

	private Context context;
	private ArrayList<Map<String, String>> mainData;
	private ArrayList<ArrayList<Map<String, Object>>> childData;
	
	private OnInviteClickListener onInviteClickListener;
	
	public FollowerExpandableListAdapter(Context context, ArrayList<Map<String, String>> mainData, ArrayList<ArrayList<Map<String, Object>>> childData)
	{
		this.context = context;
		this.mainData = mainData;
		this.childData = childData;
	}
	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return mainData.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		
		if(childData.get(groupPosition).size() == 0)
		{
			return 1;
		}
		
		return childData.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return mainData.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childData.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	TextView getTextView() {
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT, 64);
        TextView textView = new TextView(
                context);
        textView.setTypeface(Utils.typeface);
        textView.setLayoutParams(lp);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(36, 0, 0, 0);
        textView.setTextSize(20);
        textView.setTextColor(Color.BLACK);
        return textView;
    }
	
	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		GroupViewHolder holder;
		
		if(convertView == null)
		{
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_follower_list_group, null);
			
			holder = new GroupViewHolder();
			holder.follower_list_group_title = (TextView) convertView.findViewById(R.id.follower_list_group_title);
			holder.follower_list_group_title.setTypeface(Utils.typeface);
			holder.follower_list_group_describe = (TextView) convertView.findViewById(R.id.follower_list_group_describe);
			holder.follower_list_group_describe.setTypeface(Utils.typeface);
			holder.follower_list_group_invite = (ImageView) convertView.findViewById(R.id.follower_list_group_invite);
		}
		else
		{
			holder = (GroupViewHolder) convertView.getTag();
		}
		
		holder.follower_list_group_title.setText(mainData.get(groupPosition).get("group_title"));
		holder.follower_list_group_describe.setText(mainData.get(groupPosition).get("group_describe"));
		holder.follower_list_group_invite.setImageResource(R.drawable.follower_list_invite_selector);
		holder.follower_list_group_invite.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(onInviteClickListener != null)
				{
					onInviteClickListener.onInviteClick(groupPosition);
				}
			}
			
		});
		
		convertView.setTag(holder);
		
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(childData.get(groupPosition).size()==0)
		{
			TextView emptyView = new TextView(context);
			emptyView.setTypeface(Utils.typeface);
			emptyView.setTextSize(18);
			emptyView.setPadding(0, (int)(20*Utils.density), 0, (int)(20*Utils.density));
			emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
			
			emptyView.setTag(R.id.group_position, -1);
			emptyView.setTag(R.id.child_position, -1);
			
			switch(groupPosition)
			{
			case 0:
				
				emptyView.setText("添加家人\n一起管理你的宝宝");
				
				return emptyView;
				
			case 1:
				
				emptyView.setText("添加粉丝团\n让更多人关注你的宝宝");
				
				return emptyView;
			}
		}
		
		ChildViewHolder holder;
		
		if(convertView == null || convertView.getTag() == null)
		{
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_follower_list, null);
			
			holder = new ChildViewHolder();
			
			holder.follower_list_avatar = (ImageView) convertView.findViewById(R.id.follower_list_avatar);
			holder.follower_list_nickname = (TextView) convertView.findViewById(R.id.follower_list_nickname);
			holder.follower_list_gender = (ImageView) convertView.findViewById(R.id.follower_list_gender);
		}
		else
		{
			holder = (ChildViewHolder) convertView.getTag();
		}
		
		Map<String, Object> cur = childData.get(groupPosition).get(childPosition);

		String avatarUrl = (String) cur.get("avatarUrl");
		if (!avatarUrl.equals("")) {
			new UrlImageViewHelper().setUrlDrawable(
					holder.follower_list_avatar, avatarUrl);
		} else {
			
			holder.follower_list_avatar
					.setImageResource(R.drawable.user_info_default_avatar);
		}
		
		String nickName = (String) cur.get("nickName");
		holder.follower_list_nickname.setTypeface(Utils.typeface);
		holder.follower_list_nickname.setText(nickName);
		
		int gender = (Integer) cur.get("gender");
		if(gender == 1)
		{
			holder.follower_list_gender.setImageResource(R.drawable.user_info_male);
		}
		else
		{
			holder.follower_list_gender.setImageResource(R.drawable.user_info_female);
		}
		
		convertView.setTag(holder);
		convertView.setTag(R.id.group_position, groupPosition);
		convertView.setTag(R.id.child_position, childPosition);
		
		return convertView;
	}
	
	private class GroupViewHolder{
		TextView follower_list_group_title;
		TextView follower_list_group_describe;
		ImageView follower_list_group_invite;
	}

	private class ChildViewHolder{
		ImageView follower_list_avatar;
		TextView follower_list_nickname;
		ImageView follower_list_gender;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	public interface OnInviteClickListener
	{
		public void onInviteClick(int groupPosition);
	}

	public void setOnInviteClickListener(OnInviteClickListener onInviteClickListener)
	{
		this.onInviteClickListener = onInviteClickListener;
	}
}
