package com.perfectbaby.adapters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

import com.bebeanan.perfectbaby.BabyActivity;
import com.bebeanan.perfectbaby.BrowserActivity;
import com.bebeanan.perfectbaby.DetailVoiceCheckbox;
import com.bebeanan.perfectbaby.MainActivity;
import com.bebeanan.perfectbaby.MemoryHandler;
import com.bebeanan.perfectbaby.NetHandler;
import com.bebeanan.perfectbaby.PersonalDetailActivity;
import com.bebeanan.perfectbaby.R;
import com.bebeanan.perfectbaby.common.CustomMediaPlayer;
import com.bebeanan.perfectbaby.common.FeedsOperations;
import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.SpannableFixFocusTextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
/**
 * Created by KiteXu.
 */
public class PersonalZoneAdapter extends BaseAdapter {
	private Context context;
	private FeedsOperations feedsOperations;
	private String followeeId;
	private String babyId = "";
	private List<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
	private int[] imageIds = { R.id.zone_list_image_single,
			R.id.zone_list_image1, R.id.zone_list_image2,
			R.id.zone_list_image3, R.id.zone_list_image4,
			R.id.zone_list_image5, R.id.zone_list_image6,
			R.id.zone_list_image7, R.id.zone_list_image8, R.id.zone_list_image9 };
	private Typeface typeFace;

	private int itemIndex, imageIndex;

	private String upId;

	public static final String HEAD_BACKGROUND = "head_background",
			HEAD_BACKGROUND_URL = "head_background_url",
			HEAD_NICKNAME = "head_nickname", HEAD_AVATAR = "head_avatar",
			AVATAR = "avatar", NICKNAME = "nickname",
			FEEDS_OWNER_ID = "feedsOwnerId", USER_TYPE = "user_type",
			TIME = "time", LOCATION = "location", EVENT_TAG = "tag",
			CONTENT = "content", SHARE_NUMBER = "share_number",
			UP_NUMBER = "up_number", COMMENT_NUMBER = "comment_number",
			UP_LIST = "up_list", UP_USER_NAME = "up_user_name",
			UP_USER_ID = "up_user_id", UP_ID = "up_id", COMMENTS = "comments",
			COMMENT_NAME = "comment_name", COMMENT_CONTENT = "comment_content",
			IMAGE_LIST = "image_list", TYPE = "type", VIDEO_URL = "video_url",
			VOICE_URL = "voice_url";

	private boolean triangleListShown = false;
	private View zone_list_triangle_layout;

	ViewHolder[] holderList;
	String[] fileList;
	List<Integer> loadingItems = new ArrayList<Integer>();

	private OnTouchingZoomInListener onTouchingZoomInListener;
	private OnVideoClickListener onVideoClickListener;
	private OnShareImageClickListener onShareImageClickListener;
	private OnCommentImageClickListener onCommentImageClickListener;
	private OnUpImageClickListener onUpImageClickListener;

	private CustomMediaPlayer mediaPlayer = new CustomMediaPlayer();

	private Handler voiceHandler = new Handler();
	private Runnable voiceRunnable = null;
	private Runnable voiceTimmerRunnable = null;
	
	private Handler thirdLoginHandler = null;

	private AlertDialog dialog;
	private AlertDialog.Builder builder;

	private SharedPreferences userPreferences = Utils.application
			.getSharedPreferences("userInfo", Context.MODE_PRIVATE);;

//	public PersonalZoneAdapter(Context context,
//			ArrayList<Map<String, Object>> mData) {
//		this.context = context;
//		this.feedsOperations = new FeedsOperations(context);
//		this.mData = mData;
//		this.typeFace = Typeface.createFromAsset(context.getAssets(),
//				"fonts/handwriting.fon");
//		holderList = new ViewHolder[mData.size()];
//		fileList = new String[mData.size()];
//
//		followeeId = null;
//	}

	public PersonalZoneAdapter(Context context,
			ArrayList<Map<String, Object>> mData, String followeeId) {
		this.context = context;
		this.feedsOperations = new FeedsOperations(context);
		this.mData = mData;
		this.typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/handwriting.fon");
		holderList = new ViewHolder[mData.size()];
		fileList = new String[mData.size()];

		this.followeeId = followeeId;
	}

	public PersonalZoneAdapter(Context context,
			ArrayList<Map<String, Object>> mData, String followeeId, String babyId) {
		this.context = context;
		this.feedsOperations = new FeedsOperations(context);
		this.mData = mData;
		this.typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/handwriting.fon");
		holderList = new ViewHolder[mData.size()];
		fileList = new String[mData.size()];

		this.followeeId = null;
		this.babyId = babyId;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub

		if (position == 0) {
			return 0;
		}

		else
		{
			String feedId = (String) mData.get(position).get("feedId");
			
			if(feedId.equals("empty"))
			{
				return 2;
			}
			else
			{
				return 1;
			}
		}
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	public void stopVoice()
	{
		mediaPlayer.release();
		mediaPlayer.setReleased(true);
		mediaPlayer = new CustomMediaPlayer();
	}

	public int getItemIndex() {
		return itemIndex;
	}

	public int getImageIndex() {
		return imageIndex;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		int type = getItemViewType(position);

		if (type == 0) {
			ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.adapter_head_zone_list, null);

				holder.zone_head_background = (ImageView) convertView
						.findViewById(R.id.zone_head_background);
				
				holder.zone_head_avatar = (ImageView) convertView
						.findViewById(R.id.zone_head_avatar);

				holder.zone_head_nickname = (TextView) convertView
						.findViewById(R.id.zone_head_nickname);
				holder.zone_head_nickname.setTypeface(Utils.typeface);
				
				holder.zone_head_fans_count = (TextView) convertView.findViewById(R.id.zone_head_fans_count);
				holder.zone_head_fans_count.setTypeface(Utils.typeface);
				holder.zone_head_followees_count = (TextView) convertView.findViewById(R.id.zone_head_followees_count);
				holder.zone_head_followees_count.setTypeface(Utils.typeface);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Map<String, Object> cur = mData.get(position);

			// holder.zone_head_background.setImageResource((Integer) cur
			// .get(HEAD_BACKGROUND));
			new UrlImageViewHelper().setUrlDrawable(
					holder.zone_head_background,
					(String) cur.get(HEAD_BACKGROUND));
			final String head_background_url = (String) (cur
					.get(HEAD_BACKGROUND_URL));
			holder.zone_head_background
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							if (head_background_url.equals(""))
								return;
							Intent intent = new Intent(context,
									BrowserActivity.class);
							intent.putExtra("url", head_background_url);
							context.startActivity(intent);
						}

					});

			String avatarUrl = (String) cur.get(HEAD_AVATAR);
			if (avatarUrl.equals("")) {
				holder.zone_head_avatar
						.setImageResource(R.drawable.baby_default_pic);
			} else {
				String fileName = avatarUrl.substring(avatarUrl.indexOf("/"));
				if (!fileName.contains(".")) {
					avatarUrl += ".jpg";
				}
				new UrlImageViewHelper().setUrlDrawable(
						holder.zone_head_avatar, avatarUrl);
			}

			holder.zone_head_avatar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if(followeeId == null || followeeId.equals(userPreferences.getString("userId", "")))
					{
						String baby = MemoryHandler.getInstance().getKey("baby");
						if (baby == null) {
							Intent intent = new Intent(context, BabyActivity.class);
							intent.putExtra("next", true);
							intent.putExtra("addBaby", true);

							context.startActivity(intent);

							return;
						}
						JSONArray babyArray;
						try {
							babyArray = new JSONArray(baby);
							if (babyArray.length() == 0) {
								Intent intent = new Intent(context,
										BabyActivity.class);
								intent.putExtra("next", true);
								intent.putExtra("addBaby", true);

								context.startActivity(intent);

								return;
							}
							
							int currentBabyIndex = 0;
							for(int i=0; i<babyArray.length(); i++)
							{
								if(babyId.equals(babyArray.getJSONObject(i).getString("id")))
								{
									currentBabyIndex = i;
									break;
								}
							}
							
							Intent intent = new Intent(context,
									BabyActivity.class);

							intent.putExtra("currentBaby", currentBabyIndex);
							context.startActivity(intent);
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
//					
				}

			});
			
			holder.zone_head_nickname.setText((String)cur.get(MainListAdapter.HEAD_NICKNAME));

//			if (followeeId == null) {
//				holder.zone_head_follow.setVisibility(View.INVISIBLE);
//			} else {
//				holder.zone_head_follow.setVisibility(View.VISIBLE);
				holder.zone_head_fans_count.setVisibility(View.GONE);
				holder.zone_head_followees_count.setVisibility(View.GONE);
//				holder.zone_head_follow
//						.setOnClickListener(new OnClickListener() {
//
//							@Override
//							public void onClick(View v) {
//								// TODO Auto-generated method stub
//								JSONObject followeeObject = new JSONObject();
//								try {
//									followeeObject.put("userId", userPreferences.getString("userId", ""));
//									followeeObject.put("followee", followeeId);
//									
//									NetHandler handler = new NetHandler(context, 
//											NetHandler.METHOD_POST, "/followee",
//											new LinkedList<BasicNameValuePair>(),
//											followeeObject) {
//
//										@Override
//										public void handleRsp(Message msg) {
//											// TODO Auto-generated method stub
//											Bundle bundle = msg.getData();
//											
//											int code = bundle.getInt("code");
//											String data = bundle.getString("data");
//											if(code == 200)
//											{
//												Toast.makeText(context, "关注成功", Toast.LENGTH_LONG).show();
//												
//												Log.v("Kite", "follow success");
//											}
//											else
//											{
//												Toast.makeText(context, "关注失败", Toast.LENGTH_LONG).show();
//												
//												Log.v("Kite", "follow fail because" + data);
//											}
//										}
//
//									};
//									
//									handler.start();
//								} catch (JSONException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//
//						});
//			}

			convertView.setTag(holder);
		}

		else if(type == 1){
			ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.adapter_zone_list, null);

				holder.zone_list_date = (TextView) convertView
						.findViewById(R.id.zone_list_date);
				holder.zone_list_date.setTypeface(typeFace);
				holder.zone_list_date.getPaint().setFakeBoldText(true);
				holder.zone_list_role = (TextView) convertView
						.findViewById(R.id.zone_list_role);
				holder.zone_list_role.setTypeface(typeFace);
				holder.zone_list_role.getPaint().setFakeBoldText(true);

				holder.zone_list_time = (TextView) convertView
						.findViewById(R.id.zone_list_time);
				holder.zone_list_time.setTypeface(typeFace);
				holder.zone_list_time.getPaint().setFakeBoldText(true);

				holder.zone_list_locator_image = (ImageView) convertView
						.findViewById(R.id.zone_list_locator_image);
				holder.zone_list_location = (TextView) convertView
						.findViewById(R.id.zone_list_location);
				holder.zone_list_location.setTypeface(typeFace);
				holder.zone_list_location.getPaint().setFakeBoldText(true);

				holder.zone_list_triangle_layout = (LinearLayout) convertView
						.findViewById(R.id.zone_list_triangle_layout);
				LinearLayout zone_list_triangle_option_layout = (LinearLayout) convertView
						.findViewById(R.id.zone_list_triangle_option_layout);
				zone_list_triangle_option_layout.setVisibility(View.GONE);
				holder.zone_list_triangle_layout
						.setTag(zone_list_triangle_option_layout);

				holder.zone_list_event = (ImageView) convertView
						.findViewById(R.id.zone_list_event);
				holder.zone_list_content = (TextView) convertView
						.findViewById(R.id.zone_list_content);
				// holder.zone_list_content.setTypeface(typeFace);
				// holder.zone_list_content.getPaint().setFakeBoldText(true);

				holder.zone_list_share_image = (ImageView) convertView
						.findViewById(R.id.zone_list_share_image);
				holder.zone_list_up_image = (ImageView) convertView
						.findViewById(R.id.zone_list_up_image);
				holder.zone_list_comment_image = (ImageView) convertView
						.findViewById(R.id.zone_list_comment_image);
				holder.zone_list_share_number = (TextView) convertView
						.findViewById(R.id.zone_list_share_number);
				holder.zone_list_share_number.setTypeface(typeFace);
				holder.zone_list_share_number.getPaint().setFakeBoldText(true);

				holder.zone_list_up_number = (TextView) convertView
						.findViewById(R.id.zone_list_up_number);
				holder.zone_list_up_number.setTypeface(typeFace);
				holder.zone_list_up_number.getPaint().setFakeBoldText(true);

				holder.zone_list_comment_number = (TextView) convertView
						.findViewById(R.id.zone_list_comment_number);
				holder.zone_list_comment_number.setTypeface(typeFace);
				holder.zone_list_comment_number.getPaint()
						.setFakeBoldText(true);

				holder.zone_list_up_filled = (ImageView) convertView
						.findViewById(R.id.zone_list_up_filled);
				holder.zone_list_up_names = (SpannableFixFocusTextView) convertView
						.findViewById(R.id.zone_list_up_names);
				holder.zone_list_up_names.setTypeface(typeFace);
				holder.zone_list_up_names.getPaint().setFakeBoldText(true);

				holder.zone_list_system_photo = (ImageView) convertView
						.findViewById(R.id.zone_list_system_photo);

				holder.zone_list_image_layout = (LinearLayout) convertView
						.findViewById(R.id.zone_list_image_layout);

				holder.zone_list_video_layout = (RelativeLayout) convertView
						.findViewById(R.id.zone_list_video_layout);

				holder.zone_list_video_thumb = (ImageView) holder.zone_list_video_layout
						.findViewById(R.id.zone_list_video_thumb);
				// holder.zone_list_video.setZOrderOnTop(true);

				holder.zone_list_voice_layout = (RelativeLayout) convertView
						.findViewById(R.id.zone_list_voice_layout);

				holder.zone_list_voice_button = (DetailVoiceCheckbox) holder.zone_list_voice_layout
						.findViewById(R.id.zone_list_voice_button);

				holder.zone_list_voice_background = (ImageView) holder.zone_list_voice_layout
						.findViewById(R.id.zone_list_voice_background);

				holder.zone_list_voice_seekbar = (SeekBar) holder.zone_list_voice_layout
						.findViewById(R.id.zone_list_voice_seekbar);
				holder.zone_list_voice_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			        int originalProgress;

			        @Override
			        public void onStopTrackingTouch(SeekBar seekBar) {
			        }

			        @Override
			        public void onStartTrackingTouch(SeekBar seekBar) {
			            originalProgress = seekBar.getProgress();
			        }

			        @Override
			        public void onProgressChanged(SeekBar seekBar, int arg1, boolean fromUser) {
			            if(fromUser == true){
			                seekBar.setProgress(originalProgress);
			            }               
			        }
			    });

				holder.zone_list_voice_time = (TextView) holder.zone_list_voice_layout
						.findViewById(R.id.zone_list_voice_time);

				holder.zone_list_comment_layout = (LinearLayout) convertView
						.findViewById(R.id.zone_list_comment_layout);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Map<String, Object> cur = mData.get(position);

//			if(followeeId == null)
//			{
				String zone_list_date = (String) cur
						.get(MainListAdapter.ZONE_LIST_DATE);
				if (!zone_list_date.equals("")) {
					holder.zone_list_date.setVisibility(View.VISIBLE);
					holder.zone_list_date.setText(zone_list_date);
				} else {
					holder.zone_list_date.setVisibility(View.INVISIBLE);
					Log.v("Kite", "pos is " + position + "cur is " + cur);
				}
//			}
//			else
//			{
//				holder.zone_list_date.setVisibility(View.INVISIBLE);
//			}

			holder.zone_list_role.setVisibility(View.GONE);

			holder.zone_list_time.setText((String) cur.get(TIME));

			String location = (String) cur.get(LOCATION);
			if (location.equals("")) {
				holder.zone_list_locator_image.setVisibility(View.GONE);
			} else {
				holder.zone_list_locator_image.setVisibility(View.VISIBLE);
			}
			holder.zone_list_location.setText((String) cur.get(LOCATION));
			holder.zone_list_location.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((TextView) v).requestFocus();
					((TextView) v).setSelected(!v.isSelected());
				}

			});

			holder.zone_list_triangle_layout
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							// ImageView zone_list_triangle = (ImageView) v
							// .findViewById(R.id.zone_list_triangle);

							zone_list_triangle_layout = v;
							if (!triangleListShown) {
								// zone_list_triangle
								// .setImageResource(R.drawable.zone_list_triangle_down);
								//
								// Animation anim = new RotateAnimation(90, 0,
								// zone_list_triangle.getWidth() / 2,
								// zone_list_triangle.getHeight() / 2);
								// anim.setDuration(500);
								// anim.setFillAfter(true);
								// zone_list_triangle.startAnimation(anim);
								//
								// ((LinearLayout) v.getTag())
								// .setVisibility(View.VISIBLE);
								// anim = new AlphaAnimation(0, 100);
								// anim.setDuration(5000);
								// anim.setFillAfter(false);
								// ((LinearLayout) v.getTag())
								// .startAnimation(anim);
								//
								// triangleListShown = true;
								showTriangleLayout();
							} else {
								// zone_list_triangle
								// .setImageResource(R.drawable.zone_list_triangle_left);
								//
								// Animation anim = new RotateAnimation(-90, 0,
								// zone_list_triangle.getWidth() / 2,
								// zone_list_triangle.getHeight() / 2);
								// anim.setDuration(500);
								// anim.setFillAfter(true);
								// zone_list_triangle.startAnimation(anim);
								//
								// ((LinearLayout) v.getTag())
								// .setVisibility(View.GONE);
								// anim = new AlphaAnimation(100, 0);
								// anim.setDuration(500);
								// anim.setFillAfter(false);
								// ((LinearLayout) v.getTag())
								// .startAnimation(anim);
								//
								// triangleListShown = false;
								hideTriangleLayout();
							}

						}

					});

			View zone_list_triangle_option_layout = (View) holder.zone_list_triangle_layout
					.getTag();
			ImageView zone_list_triangle_option_favorite = (ImageView) zone_list_triangle_option_layout
					.findViewById(R.id.zone_list_triangle_option_favorite);
			zone_list_triangle_option_favorite
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();

							Object isFavObject = mData.get(position).get(MainListAdapter.IS_FAVOR);
							if(isFavObject != null && ((Boolean)isFavObject))
							{
								Toast.makeText(context, "已收藏", Toast.LENGTH_LONG).show();
								
								return;
							}
							
							feedsOperations.addFavFeeds(
									(String) mData.get(position).get("feedId"),
									userPreferences.getString("userId", ""),
									userPreferences.getString("nickName", ""),
									"user", (String)mData.get(position).get(MainListAdapter.FEEDS_OWNER_ID));
						}

					});
			ImageView zone_list_triangle_option_shield = (ImageView) zone_list_triangle_option_layout
					.findViewById(R.id.zone_list_triangle_option_shield);
			if (followeeId == null || followeeId.equals(userPreferences.getString("userId", ""))) {
				zone_list_triangle_option_shield.setVisibility(View.GONE);
			} else {
				zone_list_triangle_option_shield.setVisibility(View.VISIBLE);

				zone_list_triangle_option_shield
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								hideTriangleLayout();

								feedsOperations.addShield(
										userPreferences.getString("userId", ""),
										(String) mData.get(position).get(
												FEEDS_OWNER_ID));
							}

						});
			}

			ImageView zone_list_triangle_option_report = (ImageView) zone_list_triangle_option_layout
					.findViewById(R.id.zone_list_triangle_option_report);
			zone_list_triangle_option_report
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();

							feedsOperations.reportFeeds((String) mData.get(
									position).get("feedId"));
						}

					});
			ImageView zone_list_triangle_option_cancel = (ImageView) zone_list_triangle_option_layout
					.findViewById(R.id.zone_list_triangle_option_cancel);
			zone_list_triangle_option_cancel
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();
						}

					});

			int eventTag = (Integer) cur.get(EVENT_TAG);
			switch (eventTag) {
			case 0:

				holder.zone_list_event.setVisibility(View.GONE);

				break;

			case 1:

				holder.zone_list_event.setVisibility(View.VISIBLE);
				holder.zone_list_event
						.setImageResource(R.drawable.event_milk_pic);

				break;

			case 2:

				holder.zone_list_event.setVisibility(View.VISIBLE);
				holder.zone_list_event
						.setImageResource(R.drawable.event_feed_pic);

				break;

			case 4:

				holder.zone_list_event.setVisibility(View.VISIBLE);
				holder.zone_list_event
						.setImageResource(R.drawable.event_liquid_pic);

				break;

			case 8:

				holder.zone_list_event.setVisibility(View.VISIBLE);
				holder.zone_list_event
						.setImageResource(R.drawable.event_napkin_pic);

				break;

			case 16:

				holder.zone_list_event.setVisibility(View.VISIBLE);
				holder.zone_list_event
						.setImageResource(R.drawable.event_sleep_pic);

				break;
			}

			holder.zone_list_content.setText((String) cur.get(CONTENT));

			holder.zone_list_share_image
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							if (thirdLoginHandler == null) {
								thirdLoginHandler = new Handler() {
									@Override
									public void handleMessage(Message msg) {

										int id = msg.arg1;
										if (id == 1) {
											Toast.makeText(context, "分享成功",
													Toast.LENGTH_LONG).show();
											if (onShareImageClickListener != null) {
												onShareImageClickListener
														.onShareImageClick();
											}
										} else if (id == 2) {
											Toast.makeText(context, "分享失败",
													Toast.LENGTH_LONG).show();
										}
									}
								};
							}

							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(context);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											String shareUrl = "";
											String feedId = (String) mData.get(
													position).get("feedId");
											if (i == 0) {

												NetHandler netHandler = new NetHandler(context, 
														NetHandler.METHOD_GET,
														"/share/generate?feedId="
																+ feedId,
														new LinkedList<BasicNameValuePair>(),
														null) {

													@Override
													public void handleRsp(
															Message msg) {
														// TODO Auto-generated
														// method stub
														Bundle bundle = msg
																.getData();
														int code = bundle
																.getInt("code");
														Log.v("Kite",
																"share data is "
																		+ bundle.getString("data"));

														if (code == 200) {
															try {
																JSONObject shareObject = new JSONObject(
																		bundle.getString("data"));
																String shareUrl = shareObject
																		.getString("url");

																SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
																sp.setText("我在”完美宝贝“发现好东西了！【"
																		+ shareUrl
																		+ "】");

																Platform weibo = ShareSDK
																		.getPlatform(
																				context,
																				SinaWeibo.NAME);
																weibo.setPlatformActionListener(new PlatformActionListener() {
																	@Override
																	public void onComplete(
																			Platform platform,
																			int i,
																			HashMap<String, Object> stringObjectHashMap) {
																		Message msg = new Message();
																		msg.arg1 = 1;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"WEIBO SHARE DONE");
																	}

																	@Override
																	public void onError(
																			Platform platform,
																			int i,
																			Throwable throwable) {
																		throwable
																				.printStackTrace();
																		Message msg = new Message();
																		msg.arg1 = 2;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"WEIBO SHARE ERROR");
																	}

																	@Override
																	public void onCancel(
																			Platform platform,
																			int i) {
																		Log.v("DBG",
																				"WEIBO SHARE CANCEL");
																	}
																});
																weibo.share(sp);

															} catch (JSONException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}
														} else {

														}
													}

												};
												netHandler.start();

											} else if (i == 1) {

												NetHandler netHandler = new NetHandler(context, 
														NetHandler.METHOD_GET,
														"/share/generate?feedId="
																+ feedId,
														new LinkedList<BasicNameValuePair>(),
														null) {

													@Override
													public void handleRsp(
															Message msg) {
														// TODO Auto-generated
														// method stub
														Bundle bundle = msg
																.getData();
														int code = bundle
																.getInt("code");
														if (code == 200) {

															try {
																JSONObject shareObject = new JSONObject(
																		bundle.getString("data"));
																String shareUrl = shareObject
																		.getString("url");

																QZone.ShareParams sp = new QZone.ShareParams();
																sp.setTitle("我在”完美宝贝“发现好东西了！【"
																		+ shareUrl
																		+ "】");
																sp.setText("我在”完美宝贝“发现好东西了！【"
																		+ shareUrl
																		+ "】");

																sp.setTitleUrl(shareUrl);
																sp.setSite(shareUrl);
																sp.setSiteUrl(shareUrl);

																Platform qzone = ShareSDK
																		.getPlatform(
																				context,
																				QZone.NAME);
																qzone.setPlatformActionListener(new PlatformActionListener() {
																	@Override
																	public void onComplete(
																			Platform platform,
																			int i,
																			HashMap<String, Object> stringObjectHashMap) {
																		Message msg = new Message();
																		msg.arg1 = 1;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"QZONE SHARE DONE");
																	}

																	@Override
																	public void onError(
																			Platform platform,
																			int i,
																			Throwable throwable) {
																		throwable
																				.printStackTrace();
																		Message msg = new Message();
																		msg.arg1 = 2;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"QZONE SHARE ERROR");
																	}

																	@Override
																	public void onCancel(
																			Platform platform,
																			int i) {
																		Log.v("DBG",
																				"QZONE SHARE CANCEL");
																	}
																});
																qzone.share(sp);
															} catch (JSONException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}

														} else {

														}
													}
												};
												netHandler.start();

											} else if (i == 2) {

												NetHandler netHandler = new NetHandler(context, 
														NetHandler.METHOD_GET,
														"/share/generate?feedId="
																+ feedId,
														new LinkedList<BasicNameValuePair>(),
														null) {

													@Override
													public void handleRsp(
															Message msg) {
														// TODO Auto-generated
														// method stub
														Bundle bundle = msg
																.getData();
														int code = bundle
																.getInt("code");
														if (code == 200) {

															try {
																JSONObject shareObject = new JSONObject(
																		bundle.getString("data"));
																String shareUrl = shareObject
																		.getString("url");

																Wechat.ShareParams sp = new Wechat.ShareParams();
																sp.setText("我在”完美宝贝“发现好东西了！【"
																		+ shareUrl
																		+ "】");
																/*
																 * HashMap<String
																 * , Object>
																 * IpInfo =
																 * MemoryHandler
																 * .getInstance
																 * ().getHost();
																 * if
																 * (IpInfo.get
																 * ("IP") ==
																 * null) { sp
																 * .setTitleUrl
																 * (NetHandler
																 * .BAKIP +
																 * "/system-event/"
																 * + id); } else
																 * {
																 * sp.setTitleUrl
																 * (IpInfo.get
																 * ("IP"
																 * ).toString()
																 * +
																 * "/system-event/"
																 * + id); }
																 */
																sp.shareType = Platform.SHARE_TEXT;
																Platform wechat = ShareSDK
																		.getPlatform(
																				context,
																				WechatMoments.NAME);
																wechat.setPlatformActionListener(new PlatformActionListener() {
																	@Override
																	public void onComplete(
																			Platform platform,
																			int i,
																			HashMap<String, Object> stringObjectHashMap) {
																		Message msg = new Message();
																		msg.arg1 = 1;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"WECHAT SHARE DONE");
																	}

																	@Override
																	public void onError(
																			Platform platform,
																			int i,
																			Throwable throwable) {
																		throwable
																				.printStackTrace();
																		Message msg = new Message();
																		msg.arg1 = 2;
																		msg.arg2 = i;
																		msg.obj = platform;
																		thirdLoginHandler
																				.sendMessage(msg);
																		Log.v("DBG",
																				"WECHAT SHARE ERROR");
																	}

																	@Override
																	public void onCancel(
																			Platform platform,
																			int i) {
																		Log.v("DBG",
																				"WECHAT SHARE CANCEL");
																	}
																});
																wechat.share(sp);
															} catch (JSONException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}

														} else {

														}
													}
												};
												netHandler.start();

											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();

						}

					});

			holder.zone_list_up_image.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					itemIndex = position;

					int upListIndex = (Integer) v.getTag();
					if (onUpImageClickListener != null) {
						onUpImageClickListener.onUpImageClick(upListIndex);
					}
				}

			});

			holder.zone_list_comment_image
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							itemIndex = position;

							if (onCommentImageClickListener != null) {
								onCommentImageClickListener
										.onCommentImageClick();
							}
						}

					});

			holder.zone_list_share_number.setText("("
					+ (Integer) cur.get(SHARE_NUMBER) + ")");
			holder.zone_list_up_number.setText("("
					+ (Integer) cur.get(UP_NUMBER) + ")");
			holder.zone_list_comment_number.setText("("
					+ (Integer) cur.get(COMMENT_NUMBER) + ")");

			// holder.zone_list_up_names.setText((String) cur.get(UP_NAMES));
			List<Map<String, String>> upList = (List) cur.get(UP_LIST);

			holder.zone_list_up_image.setImageResource(R.drawable.main_list_up);
			holder.zone_list_up_image.setTag(-1);

			if (upList.isEmpty()) {
				holder.zone_list_up_filled.setVisibility(View.GONE);
				holder.zone_list_up_names.setVisibility(View.GONE);
			} else {
				holder.zone_list_up_filled.setVisibility(View.VISIBLE);
				holder.zone_list_up_names.setVisibility(View.VISIBLE);

				StringBuffer namesStringBuffer = new StringBuffer();
				int maxLength = (upList.size() > 5) ? 5 : upList.size();
				for (int i = 0; i < maxLength; i++) {
					String upName = upList.get(i).get(UP_USER_NAME);
					if (upName
							.equals(userPreferences.getString("nickName", ""))) {
						holder.zone_list_up_image
								.setImageResource(R.drawable.main_list_up_filled);
						holder.zone_list_up_image.setTag(i);
					}

					if (i < maxLength) {
						namesStringBuffer.append(upName);
						if (i != maxLength - 1) {
							namesStringBuffer.append(",");
						}
					}
				}
				namesStringBuffer.append("等" + upList.size() + "人");

				SpannableString sp = new SpannableString(namesStringBuffer);
				int spanStart = 0;
				for (int i = 0; i < maxLength; i++) {
					String upName = upList.get(i).get(UP_USER_NAME);
					String upId = upList.get(i).get(UP_USER_ID);
					sp.setSpan(
							new com.bebeanan.perfectbaby.common.NameClickableSpan(
									context, upId, upName), spanStart,
							spanStart + upName.length(),
							Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

					if (i != maxLength - 1) {
						spanStart = spanStart + upName.length() + 1;
					}
				}
				holder.zone_list_up_names.setText(sp);
				holder.zone_list_up_names
						.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod
								.getInstance());
			}

			int event_type = (Integer) cur.get(TYPE);
			Log.v("Kite", "pos is " + position + " type is " + event_type);
			switch (event_type) {

			case MainActivity.EVENT_NOTE:

				holder.zone_list_system_photo.setVisibility(View.GONE);
				holder.zone_list_video_layout.setVisibility(View.GONE);
				holder.zone_list_voice_layout.setVisibility(View.GONE);
				holder.zone_list_image_layout.setVisibility(View.GONE);

				break;

			case MainActivity.EVENT_SYSTEM:

				holder.zone_list_system_photo.setVisibility(View.VISIBLE);
				holder.zone_list_video_layout.setVisibility(View.GONE);
				holder.zone_list_voice_layout.setVisibility(View.GONE);
				holder.zone_list_image_layout.setVisibility(View.GONE);

				String system_url = (String) cur
						.get(MainListAdapter.SYSTEM_URL);
				final String system_id = (String) cur.get("feedId");
				
				new UrlImageViewHelper(true, Utils.density, 100)
				.setUrlDrawable(holder.zone_list_system_photo,
						system_url);

//				holder.zone_list_system_photo
//						.setOnClickListener(new OnClickListener() {
//
//							@Override
//							public void onClick(View v) {
//								// TODO Auto-generated method stub
//								Intent intent = new Intent();
//								intent.putExtra("id", system_id);
//								intent.setClass(context,
//										DetailSystemActivity.class);
//								context.startActivity(intent);
//								((Activity) context).overridePendingTransition(
//										R.anim.activity_open_in_anim,
//										R.anim.activity_open_out_anim);
//							}
//
//						});

				break;

			case MainActivity.EVENT_PHOTO:

				holder.zone_list_system_photo.setVisibility(View.GONE);
				holder.zone_list_video_layout.setVisibility(View.GONE);
				holder.zone_list_voice_layout.setVisibility(View.GONE);
				holder.zone_list_image_layout.setVisibility(View.VISIBLE);

				List<String> imageResourceList = (List) cur.get(IMAGE_LIST);

				List<ImageView> imageViewList = new ArrayList<ImageView>();
				for (int i = 0; i < 10; i++) {
					ImageView curImageView = (ImageView) holder.zone_list_image_layout
							.findViewById(imageIds[i]);
					curImageView.setVisibility(View.GONE);
					imageViewList.add(curImageView);
				}

				if (imageResourceList.size() == 1) {
					imageViewList.get(0).setVisibility(View.VISIBLE);
					imageViewList.get(0).setTag(0);
					imageViewList.get(0).setOnClickListener(
							new OnClickListener() {

								@Override
								public void onClick(View view) {
									// TODO Auto-generated method stub
									itemIndex = position;
									imageIndex = (Integer) view.getTag();

									if (onTouchingZoomInListener != null) {
										onTouchingZoomInListener
												.onTouchingZoomIn();
									}
								}
							});

					new UrlImageViewHelper(true, Utils.density, 100)
							.setUrlDrawable(imageViewList.get(0),
									imageResourceList.get(0));
				} else {
					for (int i = 0, j = 1; i < imageResourceList.size()
							&& j < imageViewList.size(); i++, j++) {
						imageViewList.get(j).setVisibility(View.VISIBLE);
						imageViewList.get(j).setTag(i);
						imageViewList.get(j).setOnClickListener(
								new OnClickListener() {

									@Override
									public void onClick(View view) {
										// TODO Auto-generated method stub
										itemIndex = position;
										imageIndex = (Integer) view.getTag();

										if (onTouchingZoomInListener != null) {
											onTouchingZoomInListener
													.onTouchingZoomIn();
										}
									}
								});

						new UrlImageViewHelper().setUrlDrawable(
								imageViewList.get(j), imageResourceList.get(i));
					}
				}

				break;

			case MainActivity.EVENT_VIDEO:

				holder.zone_list_system_photo.setVisibility(View.GONE);
				holder.zone_list_video_layout.setVisibility(View.VISIBLE);
				holder.zone_list_voice_layout.setVisibility(View.GONE);
				holder.zone_list_image_layout.setVisibility(View.GONE);

				final String videoUrl = (String) cur.get(VIDEO_URL);

				holder.zone_list_video_layout
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub

								if (onVideoClickListener != null) {
									onVideoClickListener
											.onVideoClickListener(videoUrl);
								}
							}
						});

				break;

			case MainActivity.EVENT_VOICE:

//				holder.zone_list_system_photo.setVisibility(View.GONE);
//				holder.zone_list_video_layout.setVisibility(View.GONE);
//				holder.zone_list_voice_layout.setVisibility(View.VISIBLE);
//				holder.zone_list_image_layout.setVisibility(View.GONE);
//
//				final String voiceUrl = (String) cur.get(VOICE_URL);
//				holderList[position] = holder;
//				Log.v("Kite", "current voice is " + position + " "
//						+ fileList[position]);
//
//				holder.zone_list_voice_time.setText("");
//
//				holder.zone_list_voice_button
//						.setOnClickListener(new View.OnClickListener() {
//							@Override
//							public void onClick(View view) {
//
//								DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) view;
//
//								if (fileList[position] != null) {
//
//									voiceButton.sourceFile = fileList[position];
//									voiceButton.setVisibility(View.VISIBLE);
//
//									if (!voiceButton.isChecked()) {
//										voiceButton.mediaPlayer = new CustomMediaPlayer();
//										try {
//											voiceButton.mediaPlayer
//													.setDataSource(getOutputMediaFile(
//															voiceButton.sourceFile)
//															.getPath());
//											voiceButton.mediaPlayer.prepare();
//											voiceButton.mediaPlayer.start();
//											voiceButton.mediaPlayer
//													.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//														@Override
//														public void onCompletion(
//																MediaPlayer mediaPlayer) {
//															mediaPlayer
//																	.release();
//															mediaPlayer = null;
//														}
//													});
//										} catch (IOException e) {
//											e.printStackTrace();
//											Log.e("DBG", "播放失败");
//										}
//										voiceButton.setChecked(false);
//									} else {
//										if (voiceButton.mediaPlayer != null) {
//											voiceButton.mediaPlayer.release();
//										}
//										voiceButton.mediaPlayer = null;
//										voiceButton.setChecked(true);
//									}
//
//								} else if (!loadingItems.contains(position)) {
//									loadingItems.add(position);
//									downloadVoice(position, voiceUrl);
//								}
//							}
//						});
//
//				break;
				holder.zone_list_system_photo.setVisibility(View.GONE);
				holder.zone_list_video_layout.setVisibility(View.GONE);
				holder.zone_list_voice_layout.setVisibility(View.VISIBLE);
				holder.zone_list_image_layout.setVisibility(View.GONE);

				final String voiceUrl = (String) cur.get(VOICE_URL);
				holderList[position] = holder;
				Log.v("Kite", "current voice is " + position + " "
						+ fileList[position]);

				holder.zone_list_voice_time.setText("");

				holder.zone_list_voice_button
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {

								if (isTriangleListShown()) {

									hideTriangleLayout();

									return;
								}

								DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) view;

//								if (fileList[position] != null) {
//
//									voiceButton.sourceFile = fileList[position];
//									voiceButton.setVisibility(View.VISIBLE);
//
//									if (!voiceButton.isChecked()) {
//										voiceButton.mediaPlayer = new MediaPlayer();
//										try {
//											voiceButton.mediaPlayer
//													.setDataSource(getOutputMediaFile(
//															voiceButton.sourceFile)
//															.getPath());
//											voiceButton.mediaPlayer.prepare();
//											voiceButton.mediaPlayer.start();
//											voiceButton.mediaPlayer
//													.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//														@Override
//														public void onCompletion(
//																MediaPlayer mediaPlayer) {
//															mediaPlayer
//																	.release();
//															mediaPlayer = null;
//														}
//													});
//										} catch (IOException e) {
//											e.printStackTrace();
//											Log.e("DBG", "播放失败");
//										}
//										voiceButton.setChecked(false);
//									} else {
//										if (voiceButton.mediaPlayer != null) {
//											voiceButton.mediaPlayer.release();
//										}
//										voiceButton.mediaPlayer = null;
//										voiceButton.setChecked(true);
//									}
//
//								} else if (!loadingItems.contains(position)) {
									loadingItems.add(position);
									downloadVoice(voiceButton, position, voiceUrl);
//								}
							}
						});

				break;

			default:

				holder.zone_list_video_layout.setVisibility(View.GONE);
				holder.zone_list_voice_layout.setVisibility(View.GONE);
				holder.zone_list_image_layout.setVisibility(View.GONE);

				break;
			}

			holder.zone_list_comment_layout.removeAllViews();
			List<Map<String, String>> comments = (List) cur.get(COMMENTS);
			int maxLength = (comments.size() > 2) ? 2 : comments.size();
			for (int i = 0; i < maxLength; i++) {

				Map<String, String> curComment = comments.get(i);
				String comment_name = curComment.get(COMMENT_NAME);
				String comment_content = curComment.get(COMMENT_CONTENT);

				LinearLayout commentView = new LinearLayout(context);
				commentView.setOrientation(LinearLayout.HORIZONTAL);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 20;
				commentView.setLayoutParams(params);

				TextView commentNameView = new TextView(context);
				commentNameView.setTypeface(typeFace);
				commentNameView.getPaint().setFakeBoldText(true);
				commentNameView.setTextColor(0xFF23B183);
				commentNameView.setText(comment_name + ":");
				commentView.addView(commentNameView);

				TextView commentContentView = new TextView(context);
				commentContentView.setTypeface(typeFace);
				commentContentView.getPaint().setFakeBoldText(true);
				commentContentView.setText(comment_content);
				commentView.addView(commentContentView);

				holder.zone_list_comment_layout.addView(commentView);
			}

			// convertView.setTag(holder);
		}
		else
		{

			ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.adapter_empty_zone_list, null);

				holder.zone_empty_date = (TextView) convertView.findViewById(R.id.zone_empty_list_date);
				holder.zone_empty_date.setTypeface(Utils.typeface);
				holder.zone_empty_date.getPaint().setFakeBoldText(true);
				holder.zone_empty_content = (TextView) convertView.findViewById(R.id.zone_empty_list_content);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Map<String, Object> cur = mData.get(position);
			holder.zone_empty_date.setText((String) cur
					.get(MainListAdapter.ZONE_LIST_DATE));

			convertView.setTag(holder);
		}

		return convertView;

	}

	private class ViewHolder {

		private ImageView zone_head_background;
		private ImageView zone_head_avatar;
		private TextView zone_head_nickname;
		
		private TextView zone_head_fans_count;
		private TextView zone_head_followees_count;
		
		private TextView zone_empty_date;
		private TextView zone_empty_content;

		private TextView zone_list_date;
		private TextView zone_list_role;
		private TextView zone_list_time;
		private ImageView zone_list_locator_image;
		private TextView zone_list_location;
		private LinearLayout zone_list_triangle_layout;
		private ImageView zone_list_event;
		private TextView zone_list_content;

		private ImageView zone_list_share_image, zone_list_up_image,
				zone_list_comment_image;
		private TextView zone_list_share_number, zone_list_up_number,
				zone_list_comment_number;

		private ImageView zone_list_up_filled;
		private SpannableFixFocusTextView zone_list_up_names;

		private ImageView zone_list_system_photo;

		private LinearLayout zone_list_image_layout;

		private RelativeLayout zone_list_video_layout;
		private ImageView zone_list_video_thumb;
		private MediaController zone_list_video_controller;

		private RelativeLayout zone_list_voice_layout;
		private DetailVoiceCheckbox zone_list_voice_button;
		private ImageView zone_list_voice_background;
		private SeekBar zone_list_voice_seekbar;
		private TextView zone_list_voice_time;

		private LinearLayout zone_list_comment_layout;
	}

	private void updateTime(TextView recordTimeView, int recordTime) {
		int minute = recordTime / 60;
		int second = recordTime % 60;
		recordTimeView.setText(String.format("%02d:%02d", minute, second));
	}

//	private void downloadVoice(final int position, String fileUrl) {
//
//		if (fileUrl == null) {
//			Toast.makeText(context, "播放失败", Toast.LENGTH_LONG).show();
//		}
//
//		NetHandler handler = new NetHandler(context, NetHandler.METHOD_DOWNLOAD_FILE,
//				fileUrl, null, null) {
//			@Override
//			public void handleRsp(Message msg) {
//				Bundle bundle = msg.getData();
//				int code = bundle.getInt("code");
//				if (code == 200) {
//					String data = bundle.getString("data");
//					String fileName = bundle.getString("uri");
//					fileName = fileName
//							.substring(fileName.lastIndexOf('/') + 1);
//
//					try {
//						File file = getOutputMediaFile(fileName);
//						FileOutputStream fop = new FileOutputStream(file);
//						fop.write(data.getBytes("ISO-8859-1"), 0,
//								data.getBytes("ISO-8859-1").length);
//						fop.flush();
//						fop.close();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//					Log.v("Kite", "file path is "
//							+ getOutputMediaFile(fileName).getAbsolutePath());
//
//					final DetailVoiceCheckbox voiceButton = holderList[position].zone_list_voice_button;
//					final SeekBar voiceSeekBar = holderList[position].zone_list_voice_seekbar;
//					final TextView voiceTime = holderList[position].zone_list_voice_time;
//					updateTime(voiceTime, 0);
//
//					voiceButton.sourceFile = fileName;
//					{
//						voiceButton.mediaPlayer = new CustomMediaPlayer();
//						try {
//							voiceButton.mediaPlayer
//									.setDataSource(getOutputMediaFile(
//											voiceButton.sourceFile).getPath());
//							voiceButton.mediaPlayer.prepare();
//							voiceSeekBar.setMax(voiceButton.mediaPlayer
//									.getDuration());
//							final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
//							voiceHandler = new Handler();
//							voiceRunnable = new Runnable() {
//
//								int count = 10;
//								int currentTime = 0;
//
//								@Override
//								public void run() {
//									// TODO Auto-generated method stub
//									count--;
//									if (count == 0) {
//										currentTime++;
//										updateTime(voiceTime, currentTime);
//
//										count = 10;
//									}
//
//									voiceSeekBar.setProgress(voiceMediaPlayer
//											.getCurrentPosition());
//									voiceHandler.postDelayed(this, 100);
//								}
//							};
//
//							voiceHandler.removeCallbacks(voiceRunnable);
//							voiceHandler.postDelayed(voiceRunnable, 100);
//
//							voiceButton.mediaPlayer.start();
//							voiceButton.mediaPlayer
//									.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//										@Override
//										public void onCompletion(
//												MediaPlayer mediaPlayer) {
//
//											voiceSeekBar.setProgress(0);
//											voiceHandler
//													.removeCallbacks(voiceRunnable);
//											updateTime(voiceTime, 0);
//											voiceButton.setChecked(true);
//
//											mediaPlayer.release();
//											mediaPlayer = null;
//										}
//									});
//						} catch (IOException e) {
//							e.printStackTrace();
//							Log.e("DBG", "播放失败");
//						}
//						voiceButton.setChecked(false);
//
//					}
//
//					voiceButton.setOnClickListener(new View.OnClickListener() {
//						@Override
//						public void onClick(View view) {
//							if (!voiceButton.isChecked()) {
//								voiceButton.mediaPlayer = new CustomMediaPlayer();
//								try {
//									voiceButton.mediaPlayer
//											.setDataSource(getOutputMediaFile(
//													voiceButton.sourceFile)
//													.getPath());
//									voiceButton.mediaPlayer.prepare();
//									voiceSeekBar.setMax(voiceButton.mediaPlayer
//											.getDuration());
//									final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
//									voiceHandler = new Handler();
//									voiceRunnable = new Runnable() {
//
//										int count = 10;
//										int currentTime = 0;
//
//										@Override
//										public void run() {
//											// TODO Auto-generated method stub
//											count--;
//											if (count == 0) {
//												currentTime++;
//												updateTime(voiceTime,
//														currentTime);
//
//												count = 10;
//											}
//
//											voiceSeekBar
//													.setProgress(voiceMediaPlayer
//															.getCurrentPosition());
//											voiceHandler.postDelayed(this, 100);
//										}
//									};
//
//									voiceHandler.removeCallbacks(voiceRunnable);
//									voiceHandler
//											.postDelayed(voiceRunnable, 100);
//
//									voiceButton.mediaPlayer.start();
//									voiceButton.mediaPlayer
//											.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//												@Override
//												public void onCompletion(
//														MediaPlayer mediaPlayer) {
//
//													voiceSeekBar.setProgress(0);
//													voiceHandler
//															.removeCallbacks(voiceRunnable);
//													updateTime(voiceTime, 0);
//													voiceButton
//															.setChecked(true);
//
//													mediaPlayer.release();
//													mediaPlayer = null;
//												}
//											});
//								} catch (IOException e) {
//									e.printStackTrace();
//									Log.e("DBG", "播放失败");
//								}
//								voiceButton.setChecked(false);
//
//							} else {
//
//								voiceHandler.removeCallbacks(voiceRunnable);
//								voiceSeekBar.setProgress(0);
//								updateTime(voiceTime, 0);
//
//								if (voiceButton.mediaPlayer != null) {
//									voiceButton.mediaPlayer.release();
//								}
//								voiceButton.mediaPlayer = null;
//								voiceButton.setChecked(true);
//							}
//						}
//					});
//
//				} else {
//					Log.v("DBG", "Download " + bundle.getString("uri")
//							+ " failed");
//				}
//
//			}
//		};
//		handler.start();
//	}
	private void downloadVoice(final DetailVoiceCheckbox voiceButton, final int position, final String fileUrl) {

		if (fileUrl == null) {
			Toast toast = Toast.makeText(context, "播放失败", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
		
		final SeekBar voiceSeekBar = holderList[position].zone_list_voice_seekbar;
		final TextView voiceTime = holderList[position].zone_list_voice_time;
		updateTime(voiceTime, 0);
		
//		if (!voiceButton.mediaPlayer.getReleased()) {
//			voiceButton.mediaPlayer.start();
//			return;
//		}
		
		mediaPlayer.release();
		mediaPlayer.setReleased(true);
		mediaPlayer = new CustomMediaPlayer();
		
		voiceButton.mediaPlayer = mediaPlayer;
		voiceButton.mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
		// 监听错误事件
		voiceButton.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.v("Kite", "Error on Listener, what: " + what + "extra: " + extra);
				return false;
			}
		});
		// 监听缓冲事件
		voiceButton.mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				
				if(percent == 100)
				{
					return;
				}
				Log.v("Kite", "MediaPlayer Update buffer: " + Integer.toString(percent) + "%");
			}
		});
		// 监听播放完毕事件
		voiceButton.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Listener Completed");
				
				voiceSeekBar.setProgress(0);
				voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
				updateTime(voiceTime, 0);
				voiceButton.setChecked(true);
				voiceButton.mediaPlayer.pause();
				voiceButton.mediaPlayer.setPaused(true);

				mp.pause();
			}
		});
		// 监听准备事件
		voiceButton.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Prepared Listener");
			}
		});
		
		voiceRunnable = new Runnable() {
			
			@Override
			public void run() {
				if(!voiceButton.mediaPlayer.getReleased() && !voiceButton.mediaPlayer.getPaused())
				{

					try {
						voiceButton.mediaPlayer.setDataSource(
								context, Uri.parse(fileUrl));
						voiceButton.mediaPlayer.prepare();
						voiceSeekBar.setMax(voiceButton.mediaPlayer
								.getDuration());
						Log.v("Kite",
								"Duration: "
										+ voiceButton.mediaPlayer.getDuration());

						final CustomMediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;

						voiceTimmerRunnable = new Runnable() {

							int count = 10;

							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								if(voiceMediaPlayer.getReleased())
								{
									voiceButton.setChecked(true);
									voiceSeekBar.setProgress(0);
									updateTime(voiceTime,0);
									
									voiceHandler.removeCallbacks(voiceTimmerRunnable);
								}
								else
								{
									count--;
									if (count == 0) {
										updateTime(
												voiceTime,
												voiceMediaPlayer
														.getCurrentPosition() / 1000);

										count = 10;
									}

									voiceSeekBar.setProgress(voiceMediaPlayer
											.getCurrentPosition());
									voiceHandler.postDelayed(this, 100);
								}
							}
						};

						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);

						voiceButton.mediaPlayer.start();
					} catch (Exception e) {
					}
				
				}
			}
		};
		
		new Thread(voiceRunnable).start();
		
		voiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(!voiceButton.mediaPlayer.getReleased())
				{
					if (!voiceButton.isChecked()) 
					{
						voiceButton.mediaPlayer.start();
						voiceButton.mediaPlayer.setPaused(false);
						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);
					} 
					else {
						voiceButton.mediaPlayer.pause();
						voiceButton.mediaPlayer.setPaused(true);
						voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
					}
				}
				else
				{
					loadingItems.add(position);
					downloadVoice(voiceButton, position, fileUrl);
				}
			}
		});
	
	}

	private static File getOutputMediaFile(String filename) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ filename);

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	public boolean isTriangleListShown() {
		return triangleListShown;
	}

	public void showTriangleLayout() {
		ImageView zone_list_triangle = (ImageView) zone_list_triangle_layout
				.findViewById(R.id.zone_list_triangle);
		zone_list_triangle.setImageResource(R.drawable.main_list_triangle_down);

		Animation anim = new RotateAnimation(90, 0,
				zone_list_triangle.getWidth() / 2,
				zone_list_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		zone_list_triangle.startAnimation(anim);

		((LinearLayout) zone_list_triangle_layout.getTag())
				.setVisibility(View.VISIBLE);
		anim = new AlphaAnimation(0, 100);
		anim.setDuration(5000);
		anim.setFillAfter(false);
		((LinearLayout) zone_list_triangle_layout.getTag())
				.startAnimation(anim);

		triangleListShown = true;
	}

	public void hideTriangleLayout() {
		ImageView zone_list_triangle = (ImageView) zone_list_triangle_layout
				.findViewById(R.id.zone_list_triangle);
		zone_list_triangle.setImageResource(R.drawable.main_list_triangle_left);

		Animation anim = new RotateAnimation(-90, 0,
				zone_list_triangle.getWidth() / 2,
				zone_list_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		zone_list_triangle.startAnimation(anim);

		((LinearLayout) zone_list_triangle_layout.getTag())
				.setVisibility(View.GONE);
		anim = new AlphaAnimation(100, 0);
		anim.setDuration(500);
		anim.setFillAfter(false);
		((LinearLayout) zone_list_triangle_layout.getTag())
				.startAnimation(anim);

		triangleListShown = false;
	}

	public void setOnTouchingZoomInListener(OnTouchingZoomInListener listener) {
		this.onTouchingZoomInListener = listener;
	}

	public interface OnTouchingZoomInListener {
		public void onTouchingZoomIn();
	};

	public void setOnVideoClickListener(OnVideoClickListener listener) {
		this.onVideoClickListener = listener;
	}

	public interface OnVideoClickListener {
		public void onVideoClickListener(String videoUrl);
	};

	public void setOnShareImageClickListener(OnShareImageClickListener listener) {
		this.onShareImageClickListener = listener;
	}

	public interface OnShareImageClickListener {
		public void onShareImageClick();
	};

	public void setOnCommentImageClickListener(
			OnCommentImageClickListener listener) {
		this.onCommentImageClickListener = listener;
	}

	public interface OnCommentImageClickListener {
		public void onCommentImageClick();
	};

	public void setOnUpImageClickListener(OnUpImageClickListener listener) {
		this.onUpImageClickListener = listener;
	}

	public interface OnUpImageClickListener {
		public void onUpImageClick(int upListIndex);
	};
}
