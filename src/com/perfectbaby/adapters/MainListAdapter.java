package com.perfectbaby.adapters;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.BabyActivity;
import com.bebeanan.perfectbaby.BrowserActivity;
import com.bebeanan.perfectbaby.DetailVoiceCheckbox;
import com.bebeanan.perfectbaby.FeedDetailActivity;
import com.bebeanan.perfectbaby.MainActivity;
import com.bebeanan.perfectbaby.MemoryHandler;
import com.bebeanan.perfectbaby.NetHandler;
import com.bebeanan.perfectbaby.PersonalDetailActivity;
import com.bebeanan.perfectbaby.PostActivity;
import com.bebeanan.perfectbaby.R;
import com.bebeanan.perfectbaby.common.CustomMediaPlayer;
import com.bebeanan.perfectbaby.common.FeedsOperations;
import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.SpannableFixFocusTextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
/**
 * Created by KiteXu.
 */
public class MainListAdapter extends BaseAdapter {

	private Context context;
	private boolean showBanner = true;
	private FeedsOperations feedsOperations;
	private List<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
	private int[] imageIds = { R.id.main_list_image_single,
			R.id.main_list_image1, R.id.main_list_image2,
			R.id.main_list_image3, R.id.main_list_image4,
			R.id.main_list_image5, R.id.main_list_image6,
			R.id.main_list_image7, R.id.main_list_image8, R.id.main_list_image9 };
	private Typeface typeFace;

	private int itemIndex, imageIndex;

	private String upId;
	private boolean upStatusChanging = false;

	public static final String HEAD_BACKGROUND = "head_background",
			HEAD_BACKGROUND_URL = "head_background_url",
			HEAD_NICKNAME = "head_nickname", HEAD_AVATAR = "head_avatar",
			HEAD_MESSAGE = "head_message", AVATAR = "avatar",
			NICKNAME = "nickname", FEEDS_OWNER_ID = "feedsOwnerId",
			USER_TYPE = "user_type", TIME = "time", LOCATION = "location",
			EVENT_TAG = "tag", CONTENT = "content",
			SHARE_NUMBER = "share_number", UP_NUMBER = "up_number",
			COMMENT_NUMBER = "comment_number", UP_LIST = "up_list",
			UP_USER_NAME = "up_user_name", UP_USER_ID = "up_user_id",
			UP_ID = "up_id", COMMENTS = "comments",
			COMMENT_FROM_USER_NAME = "comment_from_user_name",
			COMMENT_FROM_USER_ID = "comment_from_user_id",
			COMMENT_TO_USER_NAME = "comment_to_user_name",
			COMMENT_TO_USER_ID = "comment_to_user_id",
			COMMENT_CONTENT = "comment_content", COMMENT_ID = "comment_id",
			SYSTEM_URL = "system_url", SYSTEM_ID = "system_id",
			ZONE_LIST_DATE = "zone_list_date", IMAGE_LIST = "image_list",
			TYPE = "type", VIDEO_URL = "video_url", VOICE_URL = "voice_url",
			IS_FAVOR = "isFavor";

	private boolean simpleStyle = false;
	
	private boolean deleteStyle = false;

	private boolean triangleListShown = false;
	private View main_list_triangle_layout;

	ViewHolder[] holderList;
	String[] fileList;
	List<Integer> loadingItems = new ArrayList<Integer>();

	private OnTouchingZoomInListener onTouchingZoomInListener;
	private OnVideoClickListener onVideoClickListener;
	private OnShareImageClickListener onShareImageClickListener;
	private OnCommentImageClickListener onCommentImageClickListener;
	private OnUpImageClickListener onUpImageClickListener;
	private OnDeleteImageClickListener onDeleteImageClickListener;

	private CustomMediaPlayer mediaPlayer = new CustomMediaPlayer();
	
	private Handler voiceHandler = new Handler();
	private Runnable voiceRunnable = null;
	private Runnable voiceTimmerRunnable = null;

	private Handler thirdLoginHandler = null;

	private AlertDialog dialog;
	private AlertDialog.Builder builder;

	private static SharedPreferences userPreferences = Utils.application
			.getSharedPreferences("userInfo", Context.MODE_PRIVATE);

	public MainListAdapter(Context context, List<Map<String, Object>> mData) {
		this.context = context;
		this.feedsOperations = new FeedsOperations(context);
		this.mData = mData;
		this.typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/handwriting.fon");
		holderList = new ViewHolder[mData.size()];
		fileList = new String[mData.size()];
		
		showBanner = true;
	}

	public void hideBanner()
	{
		this.showBanner = false;
	}
	
	public void setSimpleStyle(boolean isSimple) {
		simpleStyle = isSimple;
	}

	public void setDeleteStyle(boolean isDeleteStyle) {
		deleteStyle = isDeleteStyle;
	}
	
	public void stopVoice()
	{
		mediaPlayer.release();
		mediaPlayer.setReleased(true);
		mediaPlayer = new CustomMediaPlayer();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub

		if (position == 0 && showBanner) {
			return 0;
		}

		return 1;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	public int getItemIndex() {
		return itemIndex;
	}

	public int getImageIndex() {
		return imageIndex;
	}

	public void setUpStatus(boolean changing) {
		this.upStatusChanging = changing;
	}

	public boolean getUpStatus() {
		return this.upStatusChanging;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		int type = getItemViewType(position);

		if (type == 0) {
			ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.adapter_head_main_list, null);

				holder.main_head_background = (ImageView) convertView
						.findViewById(R.id.main_head_background);
				holder.main_head_nickname = (TextView) convertView
						.findViewById(R.id.main_head_nickname);
				holder.main_head_nickname.setTypeface(typeFace);
				holder.main_head_nickname.getPaint().setFakeBoldText(true);

				holder.main_head_avatar = (ImageView) convertView
						.findViewById(R.id.main_head_avatar);
				
				holder.main_head_message = (TextView) convertView.findViewById(R.id.main_head_message);
//				holder.main_head_message.setTypeface(Utils.typeface);
				
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Map<String, Object> cur = mData.get(position);

			// holder.main_head_background.setImageResource((Integer) cur
			// .get(HEAD_BACKGROUND));
			new UrlImageViewHelper().setUrlDrawable(
					holder.main_head_background,
					(String) cur.get(HEAD_BACKGROUND));
			final String head_background_url = (String) (cur
					.get(HEAD_BACKGROUND_URL));
			holder.main_head_background
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							if (isTriangleListShown()) {

								hideTriangleLayout();

								return;
							}

							if (head_background_url.equals(""))
								return;
							Intent intent = new Intent(context,
									BrowserActivity.class);
							intent.putExtra("url", head_background_url);
							context.startActivity(intent);
						}

					});
			holder.main_head_nickname.setVisibility(View.GONE);
//			holder.main_head_nickname.setText((String) cur.get(HEAD_NICKNAME));
//			holder.main_head_nickname.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//
//					if (isTriangleListShown()) {
//
//						hideTriangleLayout();
//
//						return;
//					}
//
//					Intent intent = new Intent(context, TreeActivity.class);
//					context.startActivity(intent);
//				}
//
//			});

			String avatarUrl = (String) cur.get(HEAD_AVATAR);
			if (avatarUrl == null) {
				holder.main_head_avatar
						.setImageResource(R.drawable.user_info_default_avatar);
			} else {
				String fileName = avatarUrl.substring(avatarUrl.indexOf("/"));
				if (!fileName.contains(".")) {
					avatarUrl += ".jpg";
				}
				new UrlImageViewHelper().setUrlDrawable(
						holder.main_head_avatar, avatarUrl);
			}

			holder.main_head_avatar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (isTriangleListShown()) {

						hideTriangleLayout();

						return;
					}

					String baby = MemoryHandler.getInstance().getKey("baby");
					if (baby == null) {
						Intent intent = new Intent(context, BabyActivity.class);
						intent.putExtra("next", true);
						intent.putExtra("addBaby", true);

						context.startActivity(intent);

						return;
					}
					JSONArray babyArray;
					try {
						babyArray = new JSONArray(baby);
						if (babyArray.length() == 0) {
							Intent intent = new Intent(context,
									BabyActivity.class);
							intent.putExtra("next", true);
							intent.putExtra("addBaby", true);

							context.startActivity(intent);

							return;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

//					Intent intent = new Intent(context, TreeActivity.class);
//					context.startActivity(intent);
					Intent intent = new Intent(
							context,
							PersonalDetailActivity.class);
					intent.putExtra(
							"followeeId",
							userPreferences.getString("userId", ""));
					intent.putExtra(
							"followeeNickName",
							userPreferences.getString("nickName", ""));
					intent.putExtra(
							"followeeAvatarUrl",
							userPreferences.getString("avatarUrl", ""));

					context.startActivity(intent);
				}

			});
			
			if(cur.get(HEAD_MESSAGE) == null)
			{
				holder.main_head_message.setVisibility(View.GONE);
				holder.main_head_message.setClickable(false);
				holder.main_head_message.setOnClickListener(null);
			}
			else
			{
				holder.main_head_message.setVisibility(View.VISIBLE);
				holder.main_head_message.setText((String)cur.get(HEAD_MESSAGE));
				holder.main_head_message.setClickable(true);
				holder.main_head_message.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(context, PostActivity.class);
						context.startActivity(intent);
						
						mData.get(position).put(HEAD_MESSAGE, null);
						MainListAdapter.this.notifyDataSetChanged();
					}
					
				});
			}

			convertView.setTag(holder);
		}

		else {
			ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.adapter_main_list, null);

				holder.main_list_avatar = (ImageView) convertView
						.findViewById(R.id.main_list_avatar);
				holder.main_list_nickname = (TextView) convertView
						.findViewById(R.id.main_list_nickname);
				holder.main_list_nickname.setTypeface(typeFace);
				holder.main_list_nickname.getPaint().setFakeBoldText(true);

				holder.main_list_time = (TextView) convertView
						.findViewById(R.id.main_list_time);
				holder.main_list_time.setTypeface(typeFace);
				holder.main_list_time.getPaint().setFakeBoldText(true);

				holder.main_list_locator_image = (ImageView) convertView
						.findViewById(R.id.main_list_locator_image);
				holder.main_list_location = (TextView) convertView
						.findViewById(R.id.main_list_location);
				holder.main_list_location.setTypeface(typeFace);
				holder.main_list_location.getPaint().setFakeBoldText(true);

				holder.main_list_triangle_layout = (LinearLayout) convertView
						.findViewById(R.id.main_list_triangle_layout);
				if (simpleStyle && !deleteStyle) {
					holder.main_list_triangle_layout.setVisibility(View.GONE);
				}
				LinearLayout main_list_triangle_option_layout = (LinearLayout) convertView
						.findViewById(R.id.main_list_triangle_option_layout);
				main_list_triangle_option_layout.setVisibility(View.GONE);
				holder.main_list_triangle_layout
						.setTag(main_list_triangle_option_layout);

				holder.main_list_event = (ImageView) convertView
						.findViewById(R.id.main_list_event);
				holder.main_list_content = (SpannableFixFocusTextView) convertView
						.findViewById(R.id.main_list_content);
				// holder.main_list_content.setTypeface(typeFace);
				// holder.main_list_content.getPaint().setFakeBoldText(true);

				holder.main_list_action_layout = (LinearLayout) convertView
						.findViewById(R.id.main_list_action_layout);
				if (simpleStyle) {
					holder.main_list_action_layout.setVisibility(View.GONE);
				}
				holder.main_list_share_image = (ImageView) convertView
						.findViewById(R.id.main_list_share_image);
				holder.main_list_up_image = (ImageView) convertView
						.findViewById(R.id.main_list_up_image);
				holder.main_list_comment_image = (ImageView) convertView
						.findViewById(R.id.main_list_comment_image);
				holder.main_list_share_number = (TextView) convertView
						.findViewById(R.id.main_list_share_number);
				holder.main_list_share_number.setTypeface(typeFace);
				holder.main_list_share_number.getPaint().setFakeBoldText(true);

				holder.main_list_up_number = (TextView) convertView
						.findViewById(R.id.main_list_up_number);
				holder.main_list_up_number.setTypeface(typeFace);
				holder.main_list_up_number.getPaint().setFakeBoldText(true);

				holder.main_list_comment_number = (TextView) convertView
						.findViewById(R.id.main_list_comment_number);
				holder.main_list_comment_number.setTypeface(typeFace);
				holder.main_list_comment_number.getPaint()
						.setFakeBoldText(true);

				holder.main_list_up_filled = (ImageView) convertView
						.findViewById(R.id.main_list_up_filled);
				holder.main_list_up_names = (SpannableFixFocusTextView) convertView
						.findViewById(R.id.main_list_up_names);
//				holder.main_list_up_names.setTypeface(typeFace);
//				holder.main_list_up_names.getPaint().setFakeBoldText(true);

				holder.main_list_image_layout = (LinearLayout) convertView
						.findViewById(R.id.main_list_image_layout);

				holder.main_list_video_layout = (RelativeLayout) convertView
						.findViewById(R.id.main_list_video_layout);

				holder.main_list_video_thumb = (ImageView) holder.main_list_video_layout
						.findViewById(R.id.main_list_video_thumb);
				// holder.main_list_video.setZOrderOnTop(true);

				holder.main_list_voice_layout = (RelativeLayout) convertView
						.findViewById(R.id.main_list_voice_layout);

				holder.main_list_voice_button = (DetailVoiceCheckbox) holder.main_list_voice_layout
						.findViewById(R.id.main_list_voice_button);

				holder.main_list_voice_background = (ImageView) holder.main_list_voice_layout
						.findViewById(R.id.main_list_voice_background);

				holder.main_list_voice_seekbar = (SeekBar) holder.main_list_voice_layout
						.findViewById(R.id.main_list_voice_seekbar);
				holder.main_list_voice_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			        int originalProgress;

			        @Override
			        public void onStopTrackingTouch(SeekBar seekBar) {
			        }

			        @Override
			        public void onStartTrackingTouch(SeekBar seekBar) {
			            originalProgress = seekBar.getProgress();
			        }

			        @Override
			        public void onProgressChanged(SeekBar seekBar, int arg1, boolean fromUser) {
			            if(fromUser == true){
			                seekBar.setProgress(originalProgress);
			            }               
			        }
			    });

				holder.main_list_voice_time = (TextView) holder.main_list_voice_layout
						.findViewById(R.id.main_list_voice_time);

				holder.main_list_comment_layout = (LinearLayout) convertView
						.findViewById(R.id.main_list_comment_layout);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			Map<String, Object> cur = mData.get(position);

			String avatarUrl = (String) cur.get(AVATAR);
			Log.v("Kite", "pos is " + position + " url is " + avatarUrl);
			if (avatarUrl == null || !avatarUrl.equals("")) {
				new UrlImageViewHelper().setUrlDrawable(
						holder.main_list_avatar, avatarUrl);
			} else {
				holder.main_list_avatar
						.setImageResource(R.drawable.user_info_default_avatar);
			}

			holder.main_list_avatar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (isTriangleListShown()) {

						hideTriangleLayout();

						return;
					}

					String userId = (String) mData.get(position).get(
							FEEDS_OWNER_ID);
					if (userId.equals(userPreferences.getString("userId", ""))) {
//						Intent intent = new Intent(context, TreeActivity.class);
//						context.startActivity(intent);
						Intent intent = new Intent(
								context,
								PersonalDetailActivity.class);
						intent.putExtra(
								"followeeId",
								userPreferences.getString("userId", ""));
						intent.putExtra(
								"followeeNickName",
								userPreferences.getString("nickName", ""));
						intent.putExtra(
								"followeeAvatarUrl",
								userPreferences.getString("avatarUrl", ""));

						context.startActivity(intent);
					} else {
						Intent intent = new Intent(context,
								PersonalDetailActivity.class);
						
						intent.putExtra("followeeId",
								(String) mData.get(position)
										.get(FEEDS_OWNER_ID));
						
						context.startActivity(intent);
					}

				}

			});

			holder.main_list_nickname.setText((String) cur.get(NICKNAME));
			holder.main_list_time.setText((String) cur.get(TIME));

			String location = (String) cur.get(LOCATION);
			if (location.equals("")) {
				holder.main_list_locator_image.setVisibility(View.GONE);
			} else {
				holder.main_list_locator_image.setVisibility(View.VISIBLE);
			}
			holder.main_list_location.setText((String) cur.get(LOCATION));
			holder.main_list_location.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (isTriangleListShown()) {

						hideTriangleLayout();

						return;
					}

					((TextView) v).requestFocus();
					((TextView) v).setSelected(!v.isSelected());
				}

			});

			final String userId = (String) cur.get(FEEDS_OWNER_ID);
			ImageView triangleImage = (ImageView)holder.main_list_triangle_layout.findViewById(R.id.main_list_triangle);
			
			if(deleteStyle || userId.equals(userPreferences.getString("userId", ""))) {
				
				triangleImage.setImageResource(R.drawable.main_list_delete);
				
				holder.main_list_triangle_layout
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(onDeleteImageClickListener != null)
								{
									onDeleteImageClickListener.onDeleteImageClick(position);
								}
							}

						});
			}
			else {
				
				triangleImage.setImageResource(R.drawable.main_list_triangle_left);
				
				holder.main_list_triangle_layout
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								// ImageView main_list_triangle = (ImageView) v
								// .findViewById(R.id.main_list_triangle);

								main_list_triangle_layout = v;
								if (!triangleListShown) {
									
									showTriangleLayout();
									
								} else {
									
									hideTriangleLayout();
									
								}

							}

						});
			} 

			View main_list_triangle_option_layout = (View) holder.main_list_triangle_layout
					.getTag();
			ImageView main_list_triangle_option_favorite = (ImageView) main_list_triangle_option_layout
					.findViewById(R.id.main_list_triangle_option_favorite);
			main_list_triangle_option_favorite
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();

							Object isFavObject = mData.get(position).get(
									MainListAdapter.IS_FAVOR);
							if (isFavObject != null && ((Boolean) isFavObject)) {
								Toast.makeText(context, "已收藏",
										Toast.LENGTH_LONG).show();

								return;
							}

							feedsOperations.addFavFeeds(
									(String) mData.get(position).get("feedId"),
									userPreferences.getString("userId", ""),
									userPreferences.getString("nickName", ""),
									"user",
									(String) mData.get(position).get(
											FEEDS_OWNER_ID));
						}

					});
			ImageView main_list_triangle_option_shield = (ImageView) main_list_triangle_option_layout
					.findViewById(R.id.main_list_triangle_option_shield);
			int userType = (Integer) cur.get(USER_TYPE);
			// String userId = (String) cur.get(FEEDS_OWNER_ID);
			if (userType == -1
					|| userId.equals(userPreferences.getString("userId", ""))) {
				main_list_triangle_option_shield.setVisibility(View.GONE);
			} else {
				main_list_triangle_option_shield.setVisibility(View.VISIBLE);

				main_list_triangle_option_shield
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								hideTriangleLayout();

								feedsOperations.addShield(
										userPreferences.getString("userId", ""),
										(String) mData.get(position).get(
												FEEDS_OWNER_ID));
							}

						});
			}

			ImageView main_list_triangle_option_report = (ImageView) main_list_triangle_option_layout
					.findViewById(R.id.main_list_triangle_option_report);
			main_list_triangle_option_report
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();

							feedsOperations.reportFeeds((String) mData.get(
									position).get("feedId"));
						}

					});
			ImageView main_list_triangle_option_cancel = (ImageView) main_list_triangle_option_layout
					.findViewById(R.id.main_list_triangle_option_cancel);
			main_list_triangle_option_cancel
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							hideTriangleLayout();
						}

					});

			int eventTag = (Integer) cur.get(EVENT_TAG);
			switch (eventTag) {
			case 0:

				holder.main_list_event.setVisibility(View.GONE);

				break;

			case 1:

				holder.main_list_event.setVisibility(View.VISIBLE);
				holder.main_list_event
						.setImageResource(R.drawable.event_milk_pic);

				break;

			case 2:

				holder.main_list_event.setVisibility(View.VISIBLE);
				holder.main_list_event
						.setImageResource(R.drawable.event_feed_pic);

				break;

			case 4:

				holder.main_list_event.setVisibility(View.VISIBLE);
				holder.main_list_event
						.setImageResource(R.drawable.event_liquid_pic);

				break;

			case 8:

				holder.main_list_event.setVisibility(View.VISIBLE);
				holder.main_list_event
						.setImageResource(R.drawable.event_napkin_pic);

				break;

			case 16:

				holder.main_list_event.setVisibility(View.VISIBLE);
				holder.main_list_event
						.setImageResource(R.drawable.event_sleep_pic);

				break;
			}

			String content = (String) cur.get(CONTENT);
			List<Map<String, Integer>> urlList = Utils.getUrl(content);
			if(urlList.size() == 0)
			{
				holder.main_list_content.setText((String) cur.get(CONTENT));
			}
			else
			{
				SpannableString sp = new SpannableString(content);
				for(int i=0; i<urlList.size(); i++)
				{
					Map<String, Integer> urlMap = urlList.get(i);
					int urlStart = urlMap.get("urlStart");
					int urlEnd = urlMap.get("urlEnd");
					Log.v("Kite", "url String is " + content.substring(urlStart, urlEnd));
					Log.v("Kite", "urlStart is " + urlStart + " urlEnd is " + urlEnd);
					sp.setSpan(new URLSpan(content.substring(urlStart, urlEnd)), urlStart, urlEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				holder.main_list_content.setText(sp);
				holder.main_list_content.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod.getInstance());
			}
			
			holder.main_list_share_image
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							if (isTriangleListShown()) {

								hideTriangleLayout();

								return;
							}

							itemIndex = position;
							
							if (onShareImageClickListener != null) {
								List<String> imageResourceList = (List) mData
										.get(position).get(IMAGE_LIST);
								if (imageResourceList != null) {
									onShareImageClickListener
											.onShareImageClick(
													(String) mData
															.get(position).get(
																	"feedId"),
													"完美宝贝",
													(String) mData
															.get(position).get(
																	CONTENT),
													imageResourceList.get(0));
								} else {
									onShareImageClickListener
											.onShareImageClick(
													(String) mData
															.get(position).get(
																	"feedId"),
													"完美宝贝",
													(String) mData
															.get(position).get(
																	CONTENT),
													null);
								}

							}
						}

						// @Override
						// public void onClick(View v) {
						//
						// if (thirdLoginHandler == null) {
						// thirdLoginHandler = new Handler() {
						// @Override
						// public void handleMessage(Message msg) {
						//
						// int id = msg.arg1;
						// if (id == 1) {
						// Toast.makeText(context, "分享成功",
						// Toast.LENGTH_LONG).show();
						// if (onShareImageClickListener != null) {
						// onShareImageClickListener
						// .onShareImageClick((String) mData
						// .get(position)
						// .get("feedId"));
						// }
						// } else if (id == 2) {
						// Toast.makeText(context, "分享失败",
						// Toast.LENGTH_LONG).show();
						// }
						// }
						// };
						// }
						//
						// final CharSequence[] items = { "新浪微博", "QQ空间",
						// "微信", "取消" };
						// builder = new AlertDialog.Builder(context);
						// builder.setTitle("分享");
						// builder.setItems(items,
						// new DialogInterface.OnClickListener() {
						// @Override
						// public void onClick(
						// DialogInterface dialogInterface,
						// int i) {
						// String shareUrl = "";
						// String feedId = (String) mData.get(
						// position).get("feedId");
						// if (i == 0) {
						//
						// NetHandler netHandler = new NetHandler(
						// NetHandler.METHOD_GET,
						// "/share/generate?feedId="
						// + feedId,
						// new LinkedList<BasicNameValuePair>(),
						// null) {
						//
						// @Override
						// public void handleRsp(
						// Message msg) {
						// // TODO Auto-generated
						// // method stub
						// Bundle bundle = msg
						// .getData();
						// int code = bundle
						// .getInt("code");
						// Log.v("Kite",
						// "share data is "
						// + bundle.getString("data"));
						//
						// if (code == 200) {
						// try {
						// JSONObject shareObject = new JSONObject(
						// bundle.getString("data"));
						// String shareUrl = shareObject
						// .getString("url");
						//
						// SinaWeibo.ShareParams sp = new
						// SinaWeibo.ShareParams();
						// sp.setText((String)mData.get(position).get(CONTENT));
						// sp.setTitle("完美宝贝");
						// sp.setTitleUrl(shareUrl);
						//
						// List<String> imageResourceList = (List)
						// mData.get(position).get(IMAGE_LIST);
						// if(imageResourceList.size() > 0)
						// {
						// sp.setImageUrl(imageResourceList.get(0));
						// }
						//
						// Platform weibo = ShareSDK
						// .getPlatform(
						// context,
						// SinaWeibo.NAME);
						// weibo.setPlatformActionListener(new
						// PlatformActionListener() {
						// @Override
						// public void onComplete(
						// Platform platform,
						// int i,
						// HashMap<String, Object> stringObjectHashMap) {
						// Message msg = new Message();
						// msg.arg1 = 1;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "WEIBO SHARE DONE");
						// }
						//
						// @Override
						// public void onError(
						// Platform platform,
						// int i,
						// Throwable throwable) {
						// throwable
						// .printStackTrace();
						// Message msg = new Message();
						// msg.arg1 = 2;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "WEIBO SHARE ERROR");
						// }
						//
						// @Override
						// public void onCancel(
						// Platform platform,
						// int i) {
						// Log.v("DBG",
						// "WEIBO SHARE CANCEL");
						// }
						// });
						// weibo.share(sp);
						//
						// } catch (JSONException e) {
						// // TODO
						// // Auto-generated
						// // catch block
						// e.printStackTrace();
						// }
						// } else {
						// Toast.makeText(
						// context,
						// "分享失败",
						// Toast.LENGTH_LONG)
						// .show();
						// }
						// }
						//
						// };
						// netHandler.start();
						//
						// } else if (i == 1) {
						//
						// NetHandler netHandler = new NetHandler(
						// NetHandler.METHOD_GET,
						// "/share/generate?feedId="
						// + feedId,
						// new LinkedList<BasicNameValuePair>(),
						// null) {
						//
						// @Override
						// public void handleRsp(
						// Message msg) {
						// // TODO Auto-generated
						// // method stub
						// Bundle bundle = msg
						// .getData();
						// int code = bundle
						// .getInt("code");
						// if (code == 200) {
						//
						// try {
						// JSONObject shareObject = new JSONObject(
						// bundle.getString("data"));
						// String shareUrl = shareObject
						// .getString("url");
						//
						// QZone.ShareParams sp = new QZone.ShareParams();
						// sp.setText((String)mData.get(position).get(CONTENT));
						// sp.setTitle("完美宝贝");
						// sp.setTitleUrl(shareUrl);
						//
						// List<String> imageResourceList = (List)
						// mData.get(position).get(IMAGE_LIST);
						// if(imageResourceList.size() > 0)
						// {
						// sp.setImageUrl(imageResourceList.get(0));
						// }
						//
						// Platform qzone = ShareSDK
						// .getPlatform(
						// context,
						// QZone.NAME);
						// qzone.setPlatformActionListener(new
						// PlatformActionListener() {
						// @Override
						// public void onComplete(
						// Platform platform,
						// int i,
						// HashMap<String, Object> stringObjectHashMap) {
						// Message msg = new Message();
						// msg.arg1 = 1;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "QZONE SHARE DONE");
						// }
						//
						// @Override
						// public void onError(
						// Platform platform,
						// int i,
						// Throwable throwable) {
						// throwable
						// .printStackTrace();
						// Message msg = new Message();
						// msg.arg1 = 2;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "QZONE SHARE ERROR");
						// Log.v("Kite", throwable.toString());
						// }
						//
						// @Override
						// public void onCancel(
						// Platform platform,
						// int i) {
						// Log.v("DBG",
						// "QZONE SHARE CANCEL");
						// }
						// });
						// qzone.share(sp);
						// } catch (JSONException e) {
						// // TODO
						// // Auto-generated
						// // catch block
						// e.printStackTrace();
						// }
						//
						// } else {
						// Toast.makeText(
						// context,
						// "分享失败",
						// Toast.LENGTH_LONG)
						// .show();
						// }
						// }
						// };
						// netHandler.start();
						//
						// } else if (i == 2) {
						//
						// NetHandler netHandler = new NetHandler(
						// NetHandler.METHOD_GET,
						// "/share/generate?feedId="
						// + feedId,
						// new LinkedList<BasicNameValuePair>(),
						// null) {
						//
						// @Override
						// public void handleRsp(
						// Message msg) {
						// // TODO Auto-generated
						// // method stub
						// Bundle bundle = msg
						// .getData();
						// int code = bundle
						// .getInt("code");
						// if (code == 200) {
						//
						// try {
						// JSONObject shareObject = new JSONObject(
						// bundle.getString("data"));
						// String shareUrl = shareObject
						// .getString("url");
						//
						// /*
						// * HashMap<String
						// * , Object>
						// * IpInfo =
						// * MemoryHandler
						// * .getInstance
						// * ().getHost();
						// * if
						// * (IpInfo.get
						// * ("IP") ==
						// * null) { sp
						// * .setTitleUrl
						// * (NetHandler
						// * .BAKIP +
						// * "/system-event/"
						// * + id); } else
						// * {
						// * sp.setTitleUrl
						// * (IpInfo.get
						// * ("IP"
						// * ).toString()
						// * +
						// * "/system-event/"
						// * + id); }
						// */
						// Platform plat = ShareSDK
						// .getPlatform("WechatMoments");
						//
						// plat.setPlatformActionListener(new
						// PlatformActionListener() {
						// @Override
						// public void onComplete(
						// Platform platform,
						// int i,
						// HashMap<String, Object> stringObjectHashMap) {
						// Message msg = new Message();
						// msg.arg1 = 1;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "WECHAT SHARE DONE");
						// }
						//
						// @Override
						// public void onError(
						// Platform platform,
						// int i,
						// Throwable throwable) {
						// throwable
						// .printStackTrace();
						// Message msg = new Message();
						// msg.arg1 = 2;
						// msg.arg2 = i;
						// msg.obj = platform;
						// thirdLoginHandler
						// .sendMessage(msg);
						// Log.v("DBG",
						// "WECHAT SHARE ERROR");
						// }
						//
						// @Override
						// public void onCancel(
						// Platform platform,
						// int i) {
						// Log.v("DBG",
						// "WECHAT SHARE CANCEL");
						// }
						// });
						// ShareParams sp = new ShareParams();
						// sp.setText((String)mData.get(position).get(CONTENT));
						// sp.setTitle("完美宝贝");
						// sp.setTitleUrl(shareUrl);
						//
						// List<String> imageResourceList = (List)
						// mData.get(position).get(IMAGE_LIST);
						// if(imageResourceList.size() > 0)
						// {
						// sp.setImageUrl(imageResourceList.get(0));
						// }
						// plat.share(sp);
						// } catch (JSONException e) {
						// // TODO
						// // Auto-generated
						// // catch block
						// e.printStackTrace();
						// }
						//
						// } else {
						// Toast.makeText(
						// context,
						// "分享失败",
						// Toast.LENGTH_LONG)
						// .show();
						// }
						// }
						// };
						// netHandler.start();
						//
						// } else {
						// dialog.dismiss();
						// }
						// }
						// });
						// dialog = builder.show();
						//
						// }

					});

			holder.main_list_up_image.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (isTriangleListShown()) {

						hideTriangleLayout();

						return;
					}

					itemIndex = position;

					int upListIndex = (Integer) v.getTag();
					if (onUpImageClickListener != null && !upStatusChanging) {
						if (upListIndex == -1) {
							((ImageView) v)
									.setImageResource(R.drawable.main_list_up_filled);
						} else {
							((ImageView) v)
									.setImageResource(R.drawable.main_list_up);
						}

						onUpImageClickListener.onUpImageClick(upListIndex);
					}
				}

			});

			holder.main_list_comment_image
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							if (isTriangleListShown()) {

								hideTriangleLayout();

								return;
							}

							itemIndex = position;

							if (onCommentImageClickListener != null) {
								onCommentImageClickListener
										.onCommentImageClick();
							}
						}

					});

			int share_num = (Integer) cur.get(SHARE_NUMBER);
			if (share_num != 0) {
				holder.main_list_share_number.setText("(" + share_num + ")");
			} else {
				holder.main_list_share_number.setText("");
			}

			int up_num = (Integer) cur.get(UP_NUMBER);
			if (up_num != 0) {
				holder.main_list_up_number.setText("(" + up_num + ")");
			} else {
				holder.main_list_up_number.setText("");
			}

			int comment_num = (Integer) cur.get(COMMENT_NUMBER);
			if (comment_num != 0) {
				holder.main_list_comment_number
						.setText("(" + comment_num + ")");
			} else {
				holder.main_list_comment_number.setText("");
			}

			// holder.main_list_up_names.setText((String) cur.get(UP_NAMES));
			List<Map<String, String>> upList = (List) cur.get(UP_LIST);

			holder.main_list_up_image.setImageResource(R.drawable.main_list_up);
			holder.main_list_up_image.setTag(-1);

			if (upList.isEmpty()) {
				holder.main_list_up_filled.setVisibility(View.INVISIBLE);
				holder.main_list_up_names.setVisibility(View.GONE);
			} else {
				holder.main_list_up_filled.setVisibility(View.VISIBLE);
				holder.main_list_up_names.setVisibility(View.VISIBLE);

				StringBuffer namesStringBuffer = new StringBuffer();
				int maxLength = (upList.size() > 5) ? 5 : upList.size();
				for (int i = 0; i < upList.size(); i++) {
					String upName = upList.get(i).get(UP_USER_NAME);
					String upId = upList.get(i).get(UP_USER_ID);
					if (upId.equals(userPreferences.getString("userId", ""))) {
						holder.main_list_up_image
								.setImageResource(R.drawable.main_list_up_filled);
						holder.main_list_up_image.setTag(i);
					}

					if (i < maxLength) {
						namesStringBuffer.append(upName);
						if (i != maxLength - 1) {
							namesStringBuffer.append(",");
						}
					}
				}
				namesStringBuffer.append("等" + (Integer)mData.get(position).get(UP_NUMBER) + "人");
				
				SpannableString sp = new SpannableString(namesStringBuffer);
				int spanStart = 0;
				for (int i = 0; i < maxLength; i++) {
					String upName = upList.get(i).get(UP_USER_NAME);
					String upId = upList.get(i).get(UP_USER_ID);
					sp.setSpan(
							new com.bebeanan.perfectbaby.common.NameClickableSpan(
									context, upId, upName), spanStart,
							spanStart + upName.length(),
							Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

					if (i != maxLength - 1) {
						spanStart = spanStart + upName.length() + 1;
					}
				}
				holder.main_list_up_names.setText(sp);
				holder.main_list_up_names
						.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod
								.getInstance());
			}

			int event_type = (Integer) cur.get(TYPE);
			Log.v("Kite", "pos is " + position + " type is " + event_type);
			switch (event_type) {
			case MainActivity.EVENT_NOTE:

				holder.main_list_video_layout.setVisibility(View.GONE);
				holder.main_list_voice_layout.setVisibility(View.GONE);
				holder.main_list_image_layout.setVisibility(View.GONE);

				break;

			case MainActivity.EVENT_PHOTO:

				holder.main_list_video_layout.setVisibility(View.GONE);
				holder.main_list_voice_layout.setVisibility(View.GONE);
				holder.main_list_image_layout.setVisibility(View.VISIBLE);

				List<String> imageResourceList = (List) cur.get(IMAGE_LIST);

				List<ImageView> imageViewList = new ArrayList<ImageView>();
				for (int i = 0; i < 10; i++) {
					ImageView curImageView = (ImageView) holder.main_list_image_layout
							.findViewById(imageIds[i]);
					curImageView.setVisibility(View.GONE);
					imageViewList.add(curImageView);
				}

				if (imageResourceList.size() == 1) {
					imageViewList.get(0).setVisibility(View.VISIBLE);
					imageViewList.get(0).setTag(0);
					imageViewList.get(0).setOnClickListener(
							new OnClickListener() {

								@Override
								public void onClick(View view) {
									// TODO Auto-generated method stub

									if (isTriangleListShown()) {

										hideTriangleLayout();

										return;
									}

									itemIndex = position;
									imageIndex = (Integer) view.getTag();

									if (onTouchingZoomInListener != null) {
										onTouchingZoomInListener
												.onTouchingZoomIn();
									}
								}
							});

					new UrlImageViewHelper(true, Utils.density, 100)
							.setUrlDrawable(imageViewList.get(0),
									imageResourceList.get(0));
				} else {
					for (int i = 0, j = 1; i < imageResourceList.size()
							&& j < imageViewList.size(); i++, j++) {
						imageViewList.get(j).setVisibility(View.VISIBLE);
						imageViewList.get(j).setTag(i);
						imageViewList.get(j).setOnClickListener(
								new OnClickListener() {

									@Override
									public void onClick(View view) {
										// TODO Auto-generated method stub

										if (isTriangleListShown()) {

											hideTriangleLayout();

											return;
										}

										itemIndex = position;
										imageIndex = (Integer) view.getTag();

										if (onTouchingZoomInListener != null) {
											onTouchingZoomInListener
													.onTouchingZoomIn();
										}
									}
								});

						new UrlImageViewHelper().setUrlDrawable(
								imageViewList.get(j), imageResourceList.get(i));
					}
				}

				break;

			case MainActivity.EVENT_VIDEO:

				holder.main_list_video_layout.setVisibility(View.VISIBLE);
				holder.main_list_voice_layout.setVisibility(View.GONE);
				holder.main_list_image_layout.setVisibility(View.GONE);

				final String videoUrl = (String) cur.get(VIDEO_URL);

				holder.main_list_video_layout
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub

								if (isTriangleListShown()) {

									hideTriangleLayout();

									return;
								}

								if (onVideoClickListener != null) {
									onVideoClickListener
											.onVideoClickListener(videoUrl);
								}
							}
						});

				break;

			case MainActivity.EVENT_VOICE:

				holder.main_list_video_layout.setVisibility(View.GONE);
				holder.main_list_voice_layout.setVisibility(View.VISIBLE);
				holder.main_list_image_layout.setVisibility(View.GONE);

				final String voiceUrl = (String) cur.get(VOICE_URL);
				holderList[position] = holder;
				Log.v("Kite", "current voice is " + position + " "
						+ fileList[position]);

				holder.main_list_voice_time.setText("");

				holder.main_list_voice_button
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {

								if (isTriangleListShown()) {

									hideTriangleLayout();

									return;
								}

								DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) view;

//								if (fileList[position] != null) {
//
//									voiceButton.sourceFile = fileList[position];
//									voiceButton.setVisibility(View.VISIBLE);
//
//									if (!voiceButton.isChecked()) {
//										voiceButton.mediaPlayer = new MediaPlayer();
//										try {
//											voiceButton.mediaPlayer
//													.setDataSource(getOutputMediaFile(
//															voiceButton.sourceFile)
//															.getPath());
//											voiceButton.mediaPlayer.prepare();
//											voiceButton.mediaPlayer.start();
//											voiceButton.mediaPlayer
//													.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//														@Override
//														public void onCompletion(
//																MediaPlayer mediaPlayer) {
//															mediaPlayer
//																	.release();
//															mediaPlayer = null;
//														}
//													});
//										} catch (IOException e) {
//											e.printStackTrace();
//											Log.e("DBG", "播放失败");
//										}
//										voiceButton.setChecked(false);
//									} else {
//										if (voiceButton.mediaPlayer != null) {
//											voiceButton.mediaPlayer.release();
//										}
//										voiceButton.mediaPlayer = null;
//										voiceButton.setChecked(true);
//									}
//
//								} else if (!loadingItems.contains(position)) {
									loadingItems.add(position);
									downloadVoice(voiceButton, position, voiceUrl);
//								}
							}
						});

				break;

			default:

				holder.main_list_video_layout.setVisibility(View.GONE);
				holder.main_list_voice_layout.setVisibility(View.GONE);
				holder.main_list_image_layout.setVisibility(View.GONE);

				break;
			}

			holder.main_list_comment_layout.removeAllViews();
			List<Map<String, String>> comments = (List) cur.get(COMMENTS);
			int maxLength = (comments.size() > 2) ? 2 : comments.size();
			for (int i = 0; i < maxLength; i++) {

				Map<String, String> curComment = comments.get(i);
				String comment_from_name = curComment
						.get(COMMENT_FROM_USER_NAME);
				final String comment_from_id = curComment.get(COMMENT_FROM_USER_ID);
				String comment_to_name = curComment.get(COMMENT_TO_USER_NAME);
				String comment_to_id = curComment.get(COMMENT_TO_USER_ID);
				String comment_content = curComment.get(COMMENT_CONTENT);

//				LinearLayout commentView = new LinearLayout(context);
//				commentView.setOrientation(LinearLayout.HORIZONTAL);
//				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//				params.topMargin = 20;
//				commentView.setLayoutParams(params);
//
//				TextView commentFromNameView = new TextView(context);
////				commentFromNameView.setTypeface(typeFace);
////				commentFromNameView.getPaint().setFakeBoldText(true);
//				commentFromNameView.setTextColor(0xFF23B183);
//				commentFromNameView.setText(comment_from_name);
//				commentFromNameView.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						if (comment_from_id.equals(userPreferences.getString("userId", ""))) {
////							Intent intent = new Intent(context, TreeActivity.class);
////							context.startActivity(intent);
//							Intent intent = new Intent(
//									context,
//									PersonalDetailActivity.class);
//							intent.putExtra(
//									"followeeId",
//									userPreferences.getString("userId", ""));
//							intent.putExtra(
//									"followeeNickName",
//									userPreferences.getString("nickName", ""));
//							intent.putExtra(
//									"followeeAvatarUrl",
//									userPreferences.getString("avatarUrl", ""));
//
//							context.startActivity(intent);
//						} else {
//							
//							Intent intent = new Intent(context, PersonalDetailActivity.class);
//							intent.putExtra("followeeId", comment_from_id);
//							
//							context.startActivity(intent);
//						}
//					}
//					
//				});
//				commentView.addView(commentFromNameView);
//
//				TextView commentContentView = new TextView(context);
////				commentContentView.setTypeface(typeFace);
////				commentContentView.getPaint().setFakeBoldText(true);
//				commentContentView.setText(":" + comment_content);
//				commentView.addView(commentContentView);
				
				SpannableFixFocusTextView commentView = new SpannableFixFocusTextView(context);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 20;
				commentView.setLayoutParams(params);
				
				StringBuffer commentStringBuffer = new StringBuffer();
				commentStringBuffer.append(comment_from_name);

//				commentFromNameView.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						if (comment_from_id.equals(userPreferences.getString("userId", ""))) {
////							Intent intent = new Intent(context, TreeActivity.class);
////							context.startActivity(intent);
//							Intent intent = new Intent(
//									context,
//									PersonalDetailActivity.class);
//							intent.putExtra(
//									"followeeId",
//									userPreferences.getString("userId", ""));
//							intent.putExtra(
//									"followeeNickName",
//									userPreferences.getString("nickName", ""));
//							intent.putExtra(
//									"followeeAvatarUrl",
//									userPreferences.getString("avatarUrl", ""));
//
//							context.startActivity(intent);
//						} else {
//							
//							Intent intent = new Intent(context, PersonalDetailActivity.class);
//							intent.putExtra("followeeId", comment_from_id);
//							
//							context.startActivity(intent);
//						}
//					}
//					
//				});

				commentStringBuffer.append(":" + comment_content);
				SpannableString sp = new SpannableString(commentStringBuffer);
				
				int spanStart = 0;
				sp.setSpan(
						new com.bebeanan.perfectbaby.common.NameClickableSpan(
								context, comment_from_id, comment_from_name), spanStart,
								spanStart + comment_from_name.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				spanStart = spanStart + comment_from_name.length() + 1;
				
				sp.setSpan(
						new MyClickableSpan(position), spanStart,
								spanStart + comment_content.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				
				commentView.setText(sp);
				commentView
						.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod
								.getInstance());

				holder.main_list_comment_layout.addView(commentView);
			}

//			holder.main_list_comment_layout
//					.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method stub
//
//							if (isTriangleListShown()) {
//
//								hideTriangleLayout();
//
//								return;
//							}
//
//							Intent intent = new Intent(context,
//									FeedDetailActivity.class);
//							intent.putExtra("feedId",
//									(String) mData.get(position).get("feedId"));
//
//							context.startActivity(intent);
//						}
//
//					});

			// convertView.setTag(holder);
		}

		return convertView;
	}

	private class MyClickableSpan extends ClickableSpan
	{

		private int position;
		
		public MyClickableSpan(int position)
		{
			this.position = position;
		}
		
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub

			if (isTriangleListShown()) {

				hideTriangleLayout();

				return;
			}

			Intent intent = new Intent(context,
					FeedDetailActivity.class);
			intent.putExtra("feedId",
					(String) mData.get(position).get("feedId"));

			context.startActivity(intent);
		}
		
		@Override
		public void updateDrawState(TextPaint ds) {
			// TODO Auto-generated method stub
			super.updateDrawState(ds);
			
			ds.setUnderlineText(false);
			ds.setARGB(255, 0, 0, 0);
		}
	}
	
	private class ViewHolder {

		private ImageView main_head_background;
		private TextView main_head_nickname;
		private ImageView main_head_avatar;
		private TextView main_head_message;

		private ImageView main_list_avatar;
		private TextView main_list_nickname;
		private TextView main_list_time;
		private ImageView main_list_locator_image;
		private TextView main_list_location;
		private LinearLayout main_list_triangle_layout;
		private ImageView main_list_event;
		private SpannableFixFocusTextView main_list_content;

		private LinearLayout main_list_action_layout;
		private ImageView main_list_share_image, main_list_up_image,
				main_list_comment_image;
		private TextView main_list_share_number, main_list_up_number,
				main_list_comment_number;

		private ImageView main_list_up_filled;
		private SpannableFixFocusTextView main_list_up_names;

		private LinearLayout main_list_image_layout;

		private RelativeLayout main_list_video_layout;
		private ImageView main_list_video_thumb;
		private MediaController main_list_video_controller;

		private RelativeLayout main_list_voice_layout;
		private DetailVoiceCheckbox main_list_voice_button;
		private ImageView main_list_voice_background;
		private SeekBar main_list_voice_seekbar;
		private TextView main_list_voice_time;

		private LinearLayout main_list_comment_layout;
	}

	private void downloadVideo(final int position, String fileUrl) {

		NetHandler handler = new NetHandler(context, NetHandler.METHOD_DOWNLOAD_FILE,
				fileUrl, null, null) {
			@Override
			public void handleRsp(Message msg) {
				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				if (code == 200) {
					String data = bundle.getString("data");
					String fileName = bundle.getString("uri");
					fileName = fileName
							.substring(fileName.lastIndexOf('/') + 1);

					try {
						File file = getOutputMediaFile(fileName);
						FileOutputStream fop = new FileOutputStream(file);
						fop.write(data.getBytes("ISO-8859-1"), 0,
								data.getBytes("ISO-8859-1").length);
						fop.flush();
						fop.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					ViewHolder holder = holderList[position];

					Log.v("Kite", "file path is "
							+ getOutputMediaFile(fileName).getAbsolutePath());

					fileList[position] = fileName;

				} else {
					Log.v("DBG", "Download " + bundle.getString("uri")
							+ " failed");
				}

			}
		};
		handler.start();
	}

	private void updateTime(TextView recordTimeView, int recordTime) {
		int minute = recordTime / 60;
		int second = recordTime % 60;
		recordTimeView.setText(String.format("%02d:%02d", minute, second));
	}

	private void downloadVoice(final DetailVoiceCheckbox voiceButton, final int position, final String fileUrl) {

		if (fileUrl == null) {
			Toast toast = Toast.makeText(context, "播放失败", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
		
		final SeekBar voiceSeekBar = holderList[position].main_list_voice_seekbar;
		final TextView voiceTime = holderList[position].main_list_voice_time;
		updateTime(voiceTime, 0);
		
//		if (!voiceButton.mediaPlayer.getReleased()) {
//			voiceButton.mediaPlayer.start();
//			return;
//		}
		
		mediaPlayer.release();
		mediaPlayer.setReleased(true);
		mediaPlayer = new CustomMediaPlayer();
		
		voiceButton.mediaPlayer = mediaPlayer;
		voiceButton.mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
		// 监听错误事件
		voiceButton.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.v("Kite", "Error on Listener, what: " + what + "extra: " + extra);
				return false;
			}
		});
		// 监听缓冲事件
		voiceButton.mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				
				if(percent == 100)
				{
					return;
				}
				Log.v("Kite", "MediaPlayer Update buffer: " + Integer.toString(percent) + "%");
			}
		});
		// 监听播放完毕事件
		voiceButton.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Listener Completed");
				
				voiceSeekBar.setProgress(0);
				voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
				updateTime(voiceTime, 0);
				voiceButton.setChecked(true);
				voiceButton.mediaPlayer.pause();
				voiceButton.mediaPlayer.setPaused(true);

				mp.pause();
			}
		});
		// 监听准备事件
		voiceButton.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Prepared Listener");
			}
		});
		
		voiceRunnable = new Runnable() {
			
			@Override
			public void run() {
				if(!voiceButton.mediaPlayer.getReleased() && !voiceButton.mediaPlayer.getPaused())
				{

					try {
						voiceButton.mediaPlayer.setDataSource(
								context, Uri.parse(fileUrl));
						voiceButton.mediaPlayer.prepare();
						voiceSeekBar.setMax(voiceButton.mediaPlayer
								.getDuration());
						Log.v("Kite",
								"Duration: "
										+ voiceButton.mediaPlayer.getDuration());

						final CustomMediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;

						voiceTimmerRunnable = new Runnable() {

							int count = 10;

							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								if(voiceMediaPlayer.getReleased())
								{
									voiceButton.setChecked(true);
									voiceSeekBar.setProgress(0);
									updateTime(voiceTime,0);
									
									voiceHandler.removeCallbacks(voiceTimmerRunnable);
								}
								else
								{
									count--;
									if (count == 0) {
										updateTime(
												voiceTime,
												voiceMediaPlayer
														.getCurrentPosition() / 1000);

										count = 10;
									}

									voiceSeekBar.setProgress(voiceMediaPlayer
											.getCurrentPosition());
									voiceHandler.postDelayed(this, 100);
								}
							}
						};

						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);

						voiceButton.mediaPlayer.start();
					} catch (Exception e) {
					}
				
				}
			}
		};
		
		new Thread(voiceRunnable).start();
		
		voiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(!voiceButton.mediaPlayer.getReleased())
				{
					if (!voiceButton.isChecked()) 
					{
						voiceButton.mediaPlayer.start();
						voiceButton.mediaPlayer.setPaused(false);
						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);
					} 
					else {
						voiceButton.mediaPlayer.pause();
						voiceButton.mediaPlayer.setPaused(true);
						voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
					}
				}
				else
				{
					loadingItems.add(position);
					downloadVoice(voiceButton, position, fileUrl);
				}
			}
		});
	
	}

	private static File getOutputMediaFile(String filename) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ filename);

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	public boolean isTriangleListShown() {
		return triangleListShown;
	}

	public void showTriangleLayout() {
		ImageView main_list_triangle = (ImageView) main_list_triangle_layout
				.findViewById(R.id.main_list_triangle);
		main_list_triangle.setImageResource(R.drawable.main_list_triangle_down);

		Animation anim = new RotateAnimation(90, 0,
				main_list_triangle.getWidth() / 2,
				main_list_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		main_list_triangle.startAnimation(anim);

		((LinearLayout) main_list_triangle_layout.getTag())
				.setVisibility(View.VISIBLE);
		anim = new AlphaAnimation(0, 100);
		anim.setDuration(5000);
		anim.setFillAfter(false);
		((LinearLayout) main_list_triangle_layout.getTag())
				.startAnimation(anim);

		triangleListShown = true;
	}

	public void hideTriangleLayout() {
		ImageView main_list_triangle = (ImageView) main_list_triangle_layout
				.findViewById(R.id.main_list_triangle);
		main_list_triangle.setImageResource(R.drawable.main_list_triangle_left);

		Animation anim = new RotateAnimation(-90, 0,
				main_list_triangle.getWidth() / 2,
				main_list_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		main_list_triangle.startAnimation(anim);

		((LinearLayout) main_list_triangle_layout.getTag())
				.setVisibility(View.GONE);
		anim = new AlphaAnimation(100, 0);
		anim.setDuration(500);
		anim.setFillAfter(false);
		((LinearLayout) main_list_triangle_layout.getTag())
				.startAnimation(anim);

		triangleListShown = false;
	}

	public void setOnTouchingZoomInListener(OnTouchingZoomInListener listener) {
		this.onTouchingZoomInListener = listener;
	}

	public interface OnTouchingZoomInListener {
		public void onTouchingZoomIn();
	};

	public void setOnVideoClickListener(OnVideoClickListener listener) {
		this.onVideoClickListener = listener;
	}

	public interface OnVideoClickListener {
		public void onVideoClickListener(String videoUrl);
	};

	public void setOnShareImageClickListener(OnShareImageClickListener listener) {
		this.onShareImageClickListener = listener;
	}

	public interface OnShareImageClickListener {
		public void onShareImageClick(String feedId, String title, String text,
				String imageUrl);
	};

	public void setOnCommentImageClickListener(
			OnCommentImageClickListener listener) {
		this.onCommentImageClickListener = listener;
	}

	public interface OnCommentImageClickListener {
		public void onCommentImageClick();
	};

	public void setOnUpImageClickListener(OnUpImageClickListener listener) {
		this.onUpImageClickListener = listener;
	}

	public interface OnUpImageClickListener {
		public void onUpImageClick(int upListIndex);
	};
	
	public void setOnDeleteImageClickListener(OnDeleteImageClickListener listener) {
		this.onDeleteImageClickListener = listener;
	}
	
	public interface OnDeleteImageClickListener {
		public void onDeleteImageClick(int position);
	};
}
