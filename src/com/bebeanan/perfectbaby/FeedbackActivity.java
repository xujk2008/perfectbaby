package com.bebeanan.perfectbaby;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout.IMEStateListener;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class FeedbackActivity extends Activity {

    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;
    
    private IMELinearLayout feedbackLayout;
    
    private EditText feedbackEditText;
    private ImageButton feedbackButton;
    
    private void CheckPass() {
        String feedback = feedbackEditText.getText().toString();
        if (!feedback.isEmpty()) {
            feedbackButton.setImageResource(R.drawable.feedback_submit_enable);
            feedbackButton.setClickable(true);
        } else {
            feedbackButton.setImageResource(R.drawable.feedback_submit_disable);
            feedbackButton.setClickable(false);
        }

        /* 这里需要添加每一个edittext的判断，这里还需要判断软键盘消失的时候 */
    }

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(FeedbackActivity.this);
        builder.setCancelable(false);

        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.drawable.title_back_pic);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.feedback_title);
        titleView.setTypeface(Utils.typeface);
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);

        feedbackLayout = (IMELinearLayout) findViewById(R.id.feedback_layout);
        feedbackLayout.setIMEChangeListener(new IMEStateListener() {
			
			@Override
			public void onChange() {
				// TODO Auto-generated method stub
				CheckPass();
			}
		});
        
        feedbackEditText = (EditText) findViewById(R.id.feedback_edittext);
//        feedbackEditText.setTypeface(Utils.typeface);
        
        feedbackButton = (ImageButton) findViewById(R.id.more_feedback_button);
        feedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedback = feedbackEditText.getText().toString();
                if (feedback.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "反馈意见不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                else if(feedback.length()<5)
                {
                	toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "亲，多给点意见吧··~0~谢谢",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }

                JSONObject data = new JSONObject();
                try {
                    data.put("content", feedback);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                builder.setMessage("登录中");
                dialog = builder.show();
                showBuilder = true;

                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                NetHandler handler = new NetHandler(FeedbackActivity.this, NetHandler.METHOD_POST,
                        "/feedback", param, data) {
                    @Override
                    public void handleRsp(Message msg) {
                        dialog.dismiss();
                        showBuilder = false;
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "反馈成功",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                    overridePendingTransition(R.anim.activity_close_in_anim,
                                            R.anim.activity_close_out_anim);
                                }
                            }.start();
                            return;
                        } else if (code == -100) {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "系统异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "其他异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            Log.v("Kite", "feedBack fail because: " + bundle.getString("data"));
                            return;
                        }
                    }
                };
                handler.start();

            }
        });
        feedbackButton.setClickable(false);

        feedbackEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    CheckPass();
                }
            }
        });
        feedbackEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CheckPass();
                    return false;
                }
                return false;
            }
        });
        feedbackEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CheckPass();
                return false;
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (showBuilder) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        } else {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    this.finish();
                    overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}