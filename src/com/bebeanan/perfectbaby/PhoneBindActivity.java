package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by KiteXu.
 */
public class PhoneBindActivity extends Activity {

    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;

    private String code;
    JSONObject object;
    private EditText phoneBindNumberEdit;
    private EditText phoneBindCodeEdit;
    private ImageButton phoneSendButton;
    private ImageButton phoneBindDoneButton;
    
    private static SharedPreferences userPreferences;

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_bind);

        if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
        
        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(PhoneBindActivity.this);
        builder.setCancelable(false);

        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.drawable.title_back_pic);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.phone_bind_title);
        titleView.setTypeface(Utils.typeface);
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);

        TextView phone_bind_text = (TextView) findViewById(R.id.phone_bind_text);
        phone_bind_text.setTypeface(Utils.typeface);
        
        TextView phone_bind_title_text = (TextView) findViewById(R.id.phone_bind_title_text);
        phone_bind_title_text.setTypeface(Utils.typeface);
        
        TextView phone_bind_code_title = (TextView) findViewById(R.id.phone_bind_code_title);
        phone_bind_code_title.setTypeface(Utils.typeface);
        
        TextView phone_bind_wait_again_text = (TextView) findViewById(R.id.phone_bind_wait_again_text);
        phone_bind_wait_again_text.setTypeface(Utils.typeface);
        
        phoneSendButton = (ImageButton) findViewById(R.id.phone_bind_send_button);
        phoneSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = phoneBindNumberEdit.getEditableText().toString();
                if (phone.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "手机号为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {

                    builder.setMessage("发送中");
                    dialog = builder.show();
                    showBuilder = true;

                    JSONObject data = new JSONObject();
                    try {
                        data.put("phone", phone);
                        Random randGen = new Random();
                        char[]numbersAndLetters = ("0123456789").toCharArray();
                        char [] randBuffer = new char[5];
                        for (int i=0; i<randBuffer.length; i++) {
                            randBuffer[i] = numbersAndLetters[randGen.nextInt(9)];
                        }
                        code = new String(randBuffer);
                        data.put("code", code);
                        Log.v("DBG", "code:" + code);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                    NetHandler handler = new NetHandler(PhoneBindActivity.this, NetHandler.METHOD_POST,
                            "/phoneverify", param, data) {
                        @Override
                        public void handleRsp(Message msg) {
                            dialog.dismiss();
                            showBuilder = false;
                            Bundle bundle = msg.getData();
                            int code = bundle.getInt("code");
                            if (code == 200) {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "发送成功",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            } else if (code == -100) {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "系统异常",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            } else {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "其他异常",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                
                                Log.v("Kite", "phoneBind fail because " + bundle.getString("data"));
                                
                                return;
                            }
                        }
                    };
                    handler.start();
                }
            }
        });
        phoneSendButton.setClickable(false);
        
        phoneBindNumberEdit = (EditText) findViewById(R.id.phone_bind_number_edittext);
//        phoneBindNumberEdit.setTypeface(Utils.typeface);
        phoneBindCodeEdit = (EditText) findViewById(R.id.phone_bind_code_edittext);
//        phoneBindCodeEdit.setTypeface(Utils.typeface);

//        String users = MemoryHandler.getInstance().getKey("users");
//        try {
//            object = new JSONObject(users);
//            phoneBindNumberEdit.setText(object.getString("phone"));
//            
//            if(phoneBindNumberEdit.getText().length() == 11)
//			{
//				phoneSendButton.setClickable(true);
//				phoneSendButton.setImageResource(R.drawable.bind_send_selector);
//			}
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        
        String phoneNumber = PhoneBindActivity.this.getIntent().getStringExtra("phone");
        
        if(phoneNumber != null)
        {
        	phoneBindNumberEdit.setText(phoneNumber);
            
            if(phoneBindNumberEdit.getText().length() == 11)
    		{
    			phoneSendButton.setClickable(true);
    			phoneSendButton.setImageResource(R.drawable.bind_send_selector);
    		}
        }

        TextView leftText = (TextView) findViewById(R.id.phone_bind_wait_again_text);
        leftText.setVisibility(View.INVISIBLE);

        phoneBindNumberEdit.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable edit) {
				// TODO Auto-generated method stub
				if(edit.length() == 11)
				{
					phoneSendButton.setClickable(true);
					phoneSendButton.setImageResource(R.drawable.bind_send_selector);
				}
				else
				{
					phoneSendButton.setClickable(false);
					phoneSendButton.setImageResource(R.drawable.bind_send_disable);
					
					phoneBindDoneButton.setClickable(false);
					phoneBindDoneButton.setImageResource(R.drawable.bind_send_disable);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        

        phoneBindDoneButton = (ImageButton) findViewById(R.id.phone_bind_done_button);
        phoneBindDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = phoneBindNumberEdit.getEditableText().toString();
                String phoneCode = phoneBindCodeEdit.getEditableText().toString();
                Log.v("DBG", "phonecode:" + phoneCode);
                if (phoneCode.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "验证码为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (!phoneCode.equals(code)) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "验证码错误",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {

                    builder.setMessage("修改中");
                    dialog = builder.show();
                    showBuilder = true;

                    JSONObject data = new JSONObject();
                    try {
                        data.put("phone", phone);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                    String uid = "";
                    try {
//                        uid = object.getString("id");
                        uid = userPreferences.getString("userId", "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    NetHandler handler = new NetHandler(PhoneBindActivity.this, NetHandler.METHOD_POST,
                            "/users/" + uid, param, data) {
                        @Override
                        public void handleRsp(Message msg) {
                            dialog.dismiss();
                            showBuilder = false;
                            Bundle bundle = msg.getData();
                            int code = bundle.getInt("code");
                            if (code == 200) {
                            	
                            	PhoneBindActivity.this.setResult(RESULT_OK);
                            	
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "修改成功",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                String users = MemoryHandler.getInstance().getKey("users");
                                try {
                                    JSONObject usersObject = new JSONObject(users);
                                    
                                    if(usersObject.has("usertype") && usersObject.getInt("usertype") == 2)
                		            {
                		            	Intent intent = new Intent(PhoneBindActivity.this,
                		            			ChangePasswordActivity.class);
                		            	intent.putExtra("firstSetPassword", true);

                		            	PhoneBindActivity.this.startActivity(intent);
                		            }
                                    
                                    usersObject.put("phone", phoneBindNumberEdit.getEditableText().toString());
                                    MemoryHandler.getInstance().setKey("users", usersObject.toString());
                                    Log.v("Kite", "users data in PhoneBindActivity is " + MemoryHandler.getInstance().getKey("users"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                
                                new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        finish();
                                        overridePendingTransition(R.anim.activity_close_in_anim,
                                                R.anim.activity_close_out_anim);
                                    }
                                }.start();
                                return;
                            } else if (code == -100) {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "系统异常",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            } else {
                                toast.cancel();
                                if(bundle.getString("data").contains("已绑定其他"))
                                {
                                	toast = Toast.makeText(getApplicationContext(), "此号码已绑定其他账号",
                                            Toast.LENGTH_SHORT);
                                }
                                else
                                {
                                	toast = Toast.makeText(getApplicationContext(), "其他异常",
                                            Toast.LENGTH_SHORT);
                                }
                                
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            }
                        }
                    };
                    handler.start();
                }
            }
        });
        
        phoneBindDoneButton.setClickable(false);
        
        phoneBindCodeEdit.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(phoneSendButton.isClickable() && s.length() == 5)
				{
					phoneBindDoneButton.setClickable(true);
					phoneBindDoneButton.setImageResource(R.drawable.bind_send_selector);
				}
				else
				{
					phoneBindDoneButton.setClickable(false);
					phoneBindDoneButton.setImageResource(R.drawable.bind_send_disable);
				}
			}
        	
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}