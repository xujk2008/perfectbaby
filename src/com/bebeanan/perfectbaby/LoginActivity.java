package com.bebeanan.perfectbaby;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
//import java.util.Objects;


import java.util.Set;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;

import com.bebeanan.perfectbaby.common.Utils;
import com.perfectbaby.EasterEggs.FlyingObjectsActivity;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;

/**
 * Created by KiteXu.
 */
public class LoginActivity extends Activity {

    private EditText usernameEditText;
    private EditText passwordEditText;
    private ProgressBar login_progress;
    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;
    private ImageView loginButton;
    private ImageView rememberMe;
    private TextView login_thirdparty_text;
    private Handler thirdLoginHandler;
    
    private Typeface typeFace;
    
    private long exitTime = 0;
    
    private SharedPreferences userPreferences;
    
    private final String SHEEP_USERNAME = "SheepIsGood";
    private final String SHEEP_PASSWORD = "HorseAgrees";
    private final int SHEEP_TYPE = 1000;
    private final int SHEEP_RESOURCE = R.drawable.eastern_egg_sheep;
    
    private final String SNOW_USERNAME = "WinterIsComing";
    private final String SNOW_PASSWORD = "Stark";
    private final int SNOW_TYPE = 1001;
    private final int[] SNOW_RESOURCE = {R.drawable.eastern_egg_snow1, R.drawable.eastern_egg_snow2};

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ShareSDK.initSDK(LoginActivity.this, "1bf31aa5fde1");
        
        setContentView(R.layout.activity_login);

        UmengUpdateAgent.update(this);
        
        if(Utils.application == null)
		{
			Utils.init(LoginActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
        
        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setCancelable(false);
        
        typeFace = Typeface.createFromAsset(LoginActivity.this.getAssets(),"fonts/handwriting.fon");

        usernameEditText = (EditText) findViewById(R.id.login_username_edit);
        passwordEditText = (EditText) findViewById(R.id.login_password_edit);
        rememberMe = (ImageView) findViewById(R.id.login_remeber_checkBox);
        rememberMe.setImageResource(R.drawable.login_remember_checked);
		rememberMe.setTag(1);
        rememberMe.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int tag = (Integer)rememberMe.getTag();
				
				if(tag == 0)
				{
					rememberMe.setImageResource(R.drawable.login_remember_checked);
					rememberMe.setTag(1);
				}
				else if(tag == 1)
				{
					rememberMe.setImageResource(R.drawable.login_remember_unchecked);
					rememberMe.setTag(0);
				}
			}
        	
        });

        TextView rememberText = (TextView) findViewById(R.id.login_remember_text);
        rememberText.setTypeface(typeFace);
        rememberText.getPaint().setFakeBoldText(true);
        
        {
            String data = StoreHandler.GetStoreFile(LoginActivity.this);
            if (!data.isEmpty()) {
                try {
                    JSONObject object = new JSONObject(data);
                    if (object.has("username")) {
                        usernameEditText.setText(object.getString("username"));
                    }
                    if (object.has("password")) {
                        passwordEditText.setText(object.getString("password"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
        login_thirdparty_text = (TextView) findViewById(R.id.login_thirdparty_text);
        login_thirdparty_text.setTypeface(Utils.typeface);
        
        login_progress = (ProgressBar) findViewById(R.id.login_progress);
        login_progress.setVisibility(View.GONE);

        usernameEditText.addTextChangedListener(new TextWatcher(){

//        	String passwordString = passwordEditText.getText().toString();
        	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
				if(s.length() == 0)
				{
					passwordEditText.setText("");
				}
				
				if(s.length() != 0 && !passwordEditText.getText().toString().equals(""))
				{
					loginButton.setImageResource(R.drawable.login_login_selector);
				}
				else
				{
					loginButton.setImageResource(R.drawable.login_login_disable);
//					loginButton.setClickable(false);
				}
			}
        	
        });
        
        passwordEditText.addTextChangedListener(new TextWatcher(){

//        	String usernameString = usernameEditText.getText().toString();
        	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.length() != 0 && !usernameEditText.getText().toString().equals(""))
				{
					loginButton.setImageResource(R.drawable.login_login_selector);
				}
				else
				{
					loginButton.setImageResource(R.drawable.login_login_disable);
//					loginButton.setClickable(false);
				}
			}
        	
        });
        
        thirdLoginHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                int id = msg.arg1;
                // int action = msg.arg2;
                Platform platform = (Platform) msg.obj;

                JSONObject data = new JSONObject();
                try {
                    data.put("uid", platform.getDb().getUserId());
                    data.put("token", platform.getDb().getToken());
                    switch(id)
                    {
                    case 1:
                    	
                    	data.put("type", "weibo");
                    	break;
                    	
                    case 3:
                    	
                    	data.put("type", "qq");
                    	break;
                    	
                    case 4:
                    	
                    	data.put("type", "weixin");
                    	break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.v("Kite", "login exception is " + e);
                }

//                builder.setMessage("登录中");
//                dialog = builder.show();
                showBuilder = true;
                login_progress.setVisibility(View.VISIBLE);

                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                NetHandler handler = new NetHandler(LoginActivity.this, NetHandler.METHOD_POST,
                        "/oauth", param, data) {
                    @Override
                    public void handleRsp(Message msg) {
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {
                            /*
                            if (rememberMe.isChecked()) {
                                JSONObject data = new JSONObject();
                                try {
                                    data.put("username", usernameEditText.getText().toString());
                                    data.put("password", passwordEditText.getText().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                StoreHandler.SetStoreFile(LoginActivity.this, data.toString());
                            } else {
                                StoreHandler.SetStoreFile(LoginActivity.this, "");
                            }
                            */
                            {
                                String userId = "";
                                try {
                                    JSONObject object = new JSONObject(bundle.getString("data"));
                                    userId = object.getString("uid");
                                    Editor editor = userPreferences.edit();
                                    editor.putString("userId", userId);
                                    editor.commit();
//                                    Utils.userId = userId;
                                    
                                    JPushInterface.setAlias(LoginActivity.this, userId, new TagAliasCallback(){

                        				@Override
                        				public void gotResult(int arg0, String arg1, Set<String> arg2) {
                        					// TODO Auto-generated method stub
                        					Log.v("Kite", "JPUSH " + arg0 + " " + arg1 + " " + arg2);
                        				}
                        				
                        			});
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                                NetHandler handler1 = new NetHandler(LoginActivity.this, NetHandler.METHOD_GET,
                                        "/users/" + userId, param, null) {
                                    @Override
                                    public void handleRsp(Message msg) {
                                        Bundle bundle1 = msg.getData();
                                        int code = bundle1.getInt("code");
                                        if (code == 200) {
//                                            MemoryHandler.getInstance().setKey("users", bundle1.getString("data"));
//                                            try {
//                                                JSONObject users = new JSONObject(bundle1.getString("data"));
//                                                Log.v("Kite", "user is " + bundle1.getString("data"));
//                                                String nickname = users.getString("nickname");
////                                                Utils.nickName = nickname;
//                                                Editor editor = userPreferences.edit();
//                                                editor.putString("nickName", nickname);
////                                                Utils.city = users.getString("city");
//                                                editor.putString("city", users.getString("city"));
//                                                if(users.has("avatar"))
//                                                {
////                                                	Utils.avatarUrl = users.getString("avatar");
//                                                	editor.putString("avatarUrl", users.getString("avatar"));
//                                                }
//                                                
////                                                Utils.gender = -1;
//                                                editor.putInt("gender", -1);
//                                                if(users.has("gender"))
//                                                {
////                                                	Utils.gender = users.getInt("gender");
//                                                	editor.putInt("gender", users.getInt("gender"));
//                                                }
//                                                
//                                                editor.commit();
//                                                
//                                                if (nickname.isEmpty()) {
////                                                    dialog.dismiss();
//                                                	showBuilder = false;
//                                                    login_progress.setVisibility(View.GONE);
//                                                    Intent intent = new Intent();
//                                                    intent.setClass(LoginActivity.this, RegisterFullActivity.class);
//                                                    startActivity(intent);
//                                                    finish();
//                                                    return;
//                                                }
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//                                            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//                                            NetHandler handler2 = new NetHandler(NetHandler.METHOD_GET,
//                                                    "/baby", param, null) {
//                                                @Override
//                                                public void handleRsp(Message msg) {
//                                                    Bundle bundle2 = msg.getData();
//                                                    int code = bundle2.getInt("code");
//                                                    if (code == 200) {
////                                                        dialog.dismiss();
//                                                    	showBuilder = false;
//                                                        login_progress.setVisibility(View.GONE);
//                                                        try {
//                                                            JSONArray baby = new JSONArray(bundle2.getString("data"));
//                                                            if (baby.length() == 0) {
//                                                                Log.v("DBG", "no baby");
//                                                                MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
//                                                                StoreHandler.storeBabies(bundle2.getString("data"));
//                                                                Intent intent = new Intent();
//                                                                intent.putExtra("next", true);
//                                                                intent.putExtra("addBaby", true);
//                                                                intent.setClass(LoginActivity.this, BabyActivity.class);
//                                                                startActivity(intent);
//                                                                finish();
//                                                                /* 这里需要跳到增加baby的Activity */
//                                                            } else {
//                                                                MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
//                                                                StoreHandler.storeBabies(bundle2.getString("data"));
//                                                                Intent intent = new Intent();
//                                                                intent.setClass(LoginActivity.this, MainActivity.class);
//                                                                startActivity(intent);
//                                                                finish();
//                                                            }
//                                                        } catch (Exception e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
////                                                        dialog.dismiss();
//                                                    	showBuilder = false;
//                                                        login_progress.setVisibility(View.GONE);
//                                                        toast.cancel();
//                                                        toast = Toast.makeText(getApplicationContext(), "系统异常",
//                                                                Toast.LENGTH_SHORT);
//                                                        toast.show();
//                                                        return;
//                                                    }
//                                                }
//                                            };
//                                            handler2.start();

                                            MemoryHandler.getInstance().setKey("users", bundle1.getString("data"));
                                            try {
                                                JSONObject users = new JSONObject(bundle1.getString("data"));
                                                Log.v("Kite", "users data is " + users.toString());
                                                String nickName = users.getString("nickname");
                                                Editor editor=userPreferences.edit();
                                            	editor.putString("nickName", nickName);
                                            	editor.putInt("usertype", users.getInt("usertype"));
//                                                Utils.nickName = nickname;
                                                if(users.has("avatar"))
                                                {
//                                                	Utils.avatarUrl = users.getString("avatar");
                                                	editor.putString("avatarUrl", users.getString("avatar"));
                                                }
                                                
//                                                Utils.gender = -1;
                                                editor.putInt("gender", -1);
                                                if(users.has("gender"))
                                                {
                                                	editor.putInt("gender", users.getInt("gender"));
                                                }
                                                
                                                if(users.has("city"))
                                                {
                                                	editor.putString("city", users.getString("city"));
                                                }
                                                
                                                editor.commit();
                                                
                                                if (nickName.isEmpty()) {
//                                                    dialog.dismiss();
                                                	showBuilder = false;
                                                    login_progress.setVisibility(View.GONE);
                                                    Intent intent = new Intent();
                                                    intent.setClass(LoginActivity.this, RegisterFullActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                    return;
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                                            NetHandler handler2 = new NetHandler(LoginActivity.this, NetHandler.METHOD_GET,
                                            		"/baby?owner=" + userPreferences.getString("userId", ""), param, null) {
                                                @Override
                                                public void handleRsp(Message msg) {
                                                    Bundle bundle2 = msg.getData();
                                                    int code = bundle2.getInt("code");
                                                    if (code == 200) {
//                                                        dialog.dismiss();
                                                        showBuilder = false;
                                                        login_progress.setVisibility(View.GONE);

                                                        try {
                                                            JSONArray baby = new JSONArray(bundle2.getString("data"));
                                                            if (baby.length() == 0) {
                                                                Log.v("DBG", "no baby");
                                                                MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
                                                                StoreHandler.storeBabies(bundle2.getString("data"));
                                                                Intent intent = new Intent();
                                                                intent.putExtra("next", true);
                                                                intent.putExtra("addBaby", true);
                                                                intent.setClass(LoginActivity.this, BabyActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                                /* 这里需要跳到增加baby的Activity */
                                                            } else {
                                                            	boolean loginRequired = LoginActivity.this.getIntent().getBooleanExtra("loginRequired", false);
                                                            	
                                                            	if(!loginRequired)
                                                            	{
                                                            		MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
                                                                    StoreHandler.storeBabies(bundle2.getString("data"));
                                                                    Intent intent = new Intent();
                                                                    intent.setClass(LoginActivity.this, MainActivity.class);
                                                                    startActivity(intent);
                                                            	}
                                                                
                                                                finish();
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
//                                                        dialog.dismiss();
                                                    	Log.v("Kite", "oauth baby code is " + code + " error is " + bundle2.getString("data"));
                                                        showBuilder = false;
                                                        login_progress.setVisibility(View.GONE);
                                                        toast.cancel();
                                                        toast = Toast.makeText(getApplicationContext(), "系统异常",
                                                                Toast.LENGTH_SHORT);
                                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                                        toast.show();
                                                        return;
                                                    }
                                                }
                                            };
                                            handler2.start();
                                        
                                        } else {
//                                            dialog.dismiss();
                                        	Log.v("Kite", "oauth user code is " + code + " error is " + bundle1.getString("data"));
                                        	showBuilder = false;
                                            login_progress.setVisibility(View.GONE);
                                            toast.cancel();
                                            toast = Toast.makeText(getApplicationContext(), "系统异常",
                                                    Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                            toast.show();
                                            return;
                                        }
                                    }
                                };
                                handler1.start();
                            }
                            return;
                        } else if (code == -100) {
//                            dialog.dismiss();
                        	Log.v("Kite", "oauth code is " + code + " error is " + bundle.getString("data"));
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                        	toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "系统异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else if (code == 401) {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "登录失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else if (code == -200) {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), bundle.getString("data"),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "其他异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }
                };
                handler.start();

                super.handleMessage(msg);
            }
        };

        ImageView sinaButton = (ImageView) findViewById(R.id.login_sina);
        sinaButton.setOnClickListener(new View.OnClickListener() {
        	@Override
            public void onClick(View view) {
//                Platform weibo = ShareSDK.getPlatform(LoginActivity.this, SinaWeibo.NAME);
                Platform weibo = new SinaWeibo(LoginActivity.this);
        		
                HashMap<String,Object> hashMap = new HashMap<String, Object>();
                hashMap.put("Id","1");
                hashMap.put("SortId","1");
                hashMap.put("AppKey","4060819693");
                hashMap.put("AppSecret","5EDACA8B93E91127C53CACCF480E3DA6");
                hashMap.put("RedirectUrl","https://api.weibo.com/oauth2/default.html");
                hashMap.put("ShareByAppClient","true");
                hashMap.put("Enable","true");
                ShareSDK.setPlatformDevInfo(SinaWeibo.NAME,hashMap);
                
                if(weibo.isValid()) {
        			String userId = weibo.getDb().getUserId();
        			if (!TextUtils.isEmpty(userId)) {
        				
        				Message msg = new Message();
        		        msg.arg1 = 1;
        		        msg.obj = weibo;
        		        thirdLoginHandler.sendMessage(msg);
        				
        		        return;
        				
        			}
        		}
//                weibo.SSOSetting(true);
                weibo.showUser(null);
                weibo.setPlatformActionListener(new PlatformActionListener() {
                    @Override
                    public void onComplete(Platform platform, int i, HashMap<String, Object> stringObjectHashMap) {
                        Log.v("DBG", platform.getDb().getToken() + " " + platform.getDb().getUserId());

                        Message msg = new Message();
                        msg.arg1 = 1;
                        msg.arg2 = i;
                        msg.obj = platform;
                        thirdLoginHandler.sendMessage(msg);
                    }

                    @Override
                    public void onError(Platform platform, int i, Throwable throwable) {
                        Log.v("DBG", "WEIBO ERROR");
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onCancel(Platform platform, int i) {
                        Log.v("DBG", "Weibo Cancel");
                    }
                });
            }
        });

        ImageView qqButton = (ImageView) findViewById(R.id.login_qq);
        qqButton.setOnClickListener(new View.OnClickListener() {
        	@Override
            public void onClick(View view) {
                Platform qzone = new QZone(LoginActivity.this);
                
                HashMap<String,Object> hashMap = new HashMap<String, Object>();
                hashMap.put("Id","3");
                hashMap.put("SortId","3");
                hashMap.put("AppKey","101084830");
                hashMap.put("AppSecret","71a9cf47a645ce4e306839e0e976c1e5");
                hashMap.put("ShareByAppClient","true");
                hashMap.put("Enable","true");
                ShareSDK.setPlatformDevInfo(QZone.NAME,hashMap);
        		
                if(qzone.isValid()) {
        			String userId = qzone.getDb().getUserId();
        			if (!TextUtils.isEmpty(userId)) {
        				
        				Message msg = new Message();
        		        msg.arg1 = 3;
        		        msg.obj = qzone;
        		        thirdLoginHandler.sendMessage(msg);
        				
        		        return;
        				
        			}
        		}
//                qzone.SSOSetting(true);
                qzone.showUser(null);
                qzone.setPlatformActionListener(new PlatformActionListener() {
                    @Override
                    public void onComplete(Platform platform, int i, HashMap<String, Object> stringObjectHashMap) {
                        Log.v("DBG", platform.getDb().getToken() + " " + platform.getDb().getUserId());

                        Message msg = new Message();
                        msg.arg1 = 3;
                        msg.arg2 = i;
                        msg.obj = platform;
                        thirdLoginHandler.sendMessage(msg);
                    }

                    @Override
                    public void onError(Platform platform, int i, Throwable throwable) {
                        Log.v("DBG", "QZone ERROR");
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onCancel(Platform platform, int i) {
                        Log.v("DBG", "QZone Cancel");
                    }
                });
            }
        });
        
        ImageView wechatButton = (ImageView) findViewById(R.id.login_wechat);
        wechatButton.setOnClickListener(new View.OnClickListener() {
        	@Override
            public void onClick(View view) {
//                Platform weibo = ShareSDK.getPlatform(LoginActivity.this, SinaWeibo.NAME);
        		Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
        		
                HashMap<String,Object> hashMap = new HashMap<String, Object>();
                hashMap.put("Id","4");
                hashMap.put("SortId","4");
                hashMap.put("AppId","wx68db34add2f5b9ad");
                hashMap.put("AppSecret","03e09c4b16134e0a86453e3553795a67");
                hashMap.put("Enable","true");
                ShareSDK.setPlatformDevInfo(Wechat.NAME,hashMap);
                Log.v("DBG", "wechat " + wechat.isValid());
                
                if(wechat.isValid()) {
                	Log.v("DBG", "wechat isValid");
        			String userId = wechat.getDb().getUserId();
        			if (!TextUtils.isEmpty(userId)) {
        				Message msg = new Message();
        		        msg.arg1 = 4;
        		        msg.obj = wechat;
        		        thirdLoginHandler.sendMessage(msg);
        		        Log.v("Kite", "wechat login success.");
        				
        		        return;
        				
        			}
        		}
                wechat.SSOSetting(true);
                wechat.setPlatformActionListener(new PlatformActionListener() {
                    @Override
                    public void onComplete(Platform platform, int i, HashMap<String, Object> stringObjectHashMap) {
                    	Log.v("DBG", "wechat success");
                        Log.v("DBG", "wechat " + platform.getDb().getToken() + " " + platform.getDb().getUserId());

                        Message msg = new Message();
                        msg.arg1 = 4;
                        msg.arg2 = i;
                        msg.obj = platform;
                        thirdLoginHandler.sendMessage(msg);
                        Log.v("Kite", "wechat login success.");
                    }

                    @Override
                    public void onError(Platform platform, int i, Throwable throwable) {
                        Log.v("DBG", "Wechat ERROR");
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onCancel(Platform platform, int i) {
                        Log.v("DBG", "Wechat Cancel");
                    }
                });
                wechat.showUser(null);
            }
        });

        ImageView registerButton = (ImageView) findViewById(R.id.login_register);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                if (!IpInfo.get("Status").toString().equals("0")) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), IpInfo.get("Message").toString(),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterItemActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_in_anim, R.anim.activity_open_out_anim);
            }
        });

        TextView fetchPasswordButton = (TextView) findViewById(R.id.login_refetch);
        fetchPasswordButton.setTypeface(typeFace);
        fetchPasswordButton.getPaint().setFakeBoldText(true);
        fetchPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                if (!IpInfo.get("Status").toString().equals("0")) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), IpInfo.get("Message").toString(),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_in_anim, R.anim.activity_open_out_anim);
            }
        });

        loginButton = (ImageView) findViewById(R.id.login_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            	InputMethodManager imm = (InputMethodManager) LoginActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            	imm.hideSoftInputFromWindow(usernameEditText.getWindowToken(), 0);
            	imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 0);
            	
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                
                int easternType = getEasternEggType(username, password);
                
                if(easternType != -1)
                {
                	showEasternEggs(easternType);
                	
                	return;
                }
                
                if (username.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "用户名不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                
                if (password.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "密码不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }

                JSONObject data = new JSONObject();
                try {
                    data.put("username", username);
                    data.put("password", password);
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                builder.setMessage("登录中");
//                dialog = builder.show();
                showBuilder = true;
                login_progress.setVisibility(View.VISIBLE);

                JSONObject fileData = new JSONObject();
                try {
                    fileData.put("username", usernameEditText.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                StoreHandler.SetStoreFile(LoginActivity.this, fileData.toString());

                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                NetHandler handler = new NetHandler(LoginActivity.this, NetHandler.METHOD_POST,
                        "/users/login", param, data) {
                    @Override
                    public void handleRsp(Message msg) {
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {
                        	int tag = (Integer)rememberMe.getTag();
                            if (tag == 1) {
                                JSONObject data = new JSONObject();
                                try {
                                    data.put("username", usernameEditText.getText().toString());
                                    data.put("password", passwordEditText.getText().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                StoreHandler.SetStoreFile(LoginActivity.this, data.toString());
                            }
                            {
                                String userId = "";
                                try {
                                    JSONObject object = new JSONObject(bundle.getString("data"));
                                    userId = object.getString("uid");
                                	Editor editor=userPreferences.edit();
                                	editor.putString("userId", userId);
                                	editor.commit();
//                                    Utils.userId = userId;
                                	
                                	JPushInterface.setAlias(LoginActivity.this, userId, new TagAliasCallback(){

                        				@Override
                        				public void gotResult(int arg0, String arg1, Set<String> arg2) {
                        					// TODO Auto-generated method stub
                        					Log.v("Kite", "JPUSH " + arg0 + " " + arg1 + " " + arg2);
                        				}
                        				
                        			});
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                                NetHandler handler1 = new NetHandler(LoginActivity.this, NetHandler.METHOD_GET,
                                        "/users/me", param, null) {
                                    @Override
                                    public void handleRsp(Message msg) {
                                        Bundle bundle1 = msg.getData();
                                        int code = bundle1.getInt("code");
                                        if (code == 200) {
                                            MemoryHandler.getInstance().setKey("users", bundle1.getString("data"));
                                            try {
                                                JSONObject users = new JSONObject(bundle1.getString("data"));
                                                Log.v("Kite", "users data is " + users.toString());
                                                
                                                if (!users.has("nickname")) {
                    								Intent intent = new Intent();
                    								intent.setClass(LoginActivity.this,
                    										RegisterFullActivity.class);
                    								startActivity(intent);
                    								finish();
                    								return;
                    							}
                                                
                                                String nickName = users.getString("nickname");
                                                Editor editor=userPreferences.edit();
                                            	editor.putString("nickName", nickName);
                                            	editor.putInt("usertype", users.getInt("usertype"));
//                                                Utils.nickName = nickname;
                                                if(users.has("avatar"))
                                                {
//                                                	Utils.avatarUrl = users.getString("avatar");
                                                	editor.putString("avatarUrl", users.getString("avatar"));
                                                }
                                                
//                                                Utils.gender = -1;
                                                editor.putInt("gender", -1);
                                                if(users.has("gender"))
                                                {
                                                	editor.putInt("gender", users.getInt("gender"));
                                                }
                                                
                                                if(users.has("city"))
                                                {
                                                	editor.putString("city", users.getString("city"));
                                                }
                                                
                                                editor.commit();
                                                
                                                if (nickName.isEmpty()) {
//                                                    dialog.dismiss();
                                                	showBuilder = false;
                                                    login_progress.setVisibility(View.GONE);
                                                    Intent intent = new Intent();
                                                    intent.setClass(LoginActivity.this, RegisterFullActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                    return;
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                                            NetHandler handler2 = new NetHandler(LoginActivity.this, NetHandler.METHOD_GET,
                                                    "/baby?owner=" + userPreferences.getString("userId", ""), param, null) {
                                                @Override
                                                public void handleRsp(Message msg) {
                                                    Bundle bundle2 = msg.getData();
                                                    int code = bundle2.getInt("code");
                                                    if (code == 200) {
//                                                        dialog.dismiss();
                                                        showBuilder = false;
                                                        login_progress.setVisibility(View.GONE);

                                                        try {
                                                            JSONArray baby = new JSONArray(bundle2.getString("data"));
                                                            if (baby.length() == 0) {
                                                                Log.v("DBG", "no baby");
                                                                MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
                                                                StoreHandler.storeBabies(bundle2.getString("data"));
                                                                Intent intent = new Intent();
                                                                intent.putExtra("next", true);
                                                                intent.putExtra("addBaby", true);
                                                                intent.setClass(LoginActivity.this, BabyActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                                /* 这里需要跳到增加baby的Activity */
                                                            } else {
                                                            	
																boolean loginRequired = LoginActivity.this
																		.getIntent()
																		.getBooleanExtra(
																				"loginRequired",
																				false);

																if (!loginRequired) {
																	MemoryHandler.getInstance().setKey("baby", bundle2.getString("data"));
	                                                                StoreHandler.storeBabies(bundle2.getString("data"));
	                                                                Intent intent = new Intent();
	                                                                intent.setClass(LoginActivity.this, MainActivity.class);
	                                                                startActivity(intent);
																}

                                                                finish();
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
//                                                        dialog.dismiss();
                                                        showBuilder = false;
                                                        login_progress.setVisibility(View.GONE);
                                                        toast.cancel();
                                                        toast = Toast.makeText(getApplicationContext(), "系统异常",
                                                                Toast.LENGTH_SHORT);
                                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                                        toast.show();
                                                        return;
                                                    }
                                                }
                                            };
                                            handler2.start();
                                        } else {
//                                            dialog.dismiss();
                                        	showBuilder = false;
                                            login_progress.setVisibility(View.GONE);
                                        	toast.cancel();
                                            toast = Toast.makeText(getApplicationContext(), "系统异常",
                                                    Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                            toast.show();
                                            return;
                                        }
                                    }
                                };
                                handler1.start();
                            }
                            return;
                        } else if (code == -100) {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "系统异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else if (code == 401) {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "手机号/邮箱或密码不正确",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else if (code == -200) {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                        	toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), bundle.getString("data"),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else {
//                            dialog.dismiss();
                        	showBuilder = false;
                            login_progress.setVisibility(View.GONE);
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "其他异常",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }
                };
                handler.start();

            }
        });
        
        if(!usernameEditText.getText().toString().equals("") && !passwordEditText.getText().toString().equals(""))
		{
			loginButton.setImageResource(R.drawable.login_login_selector);
		}
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (showBuilder) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        }
        else if(keyCode == KeyEvent.KEYCODE_BACK)
		{
        	long curTime = System.currentTimeMillis();
			if( (curTime-exitTime) > 2000)
			{
				Toast toast = Toast.makeText(LoginActivity.this, "再按一次退出", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
				exitTime = curTime;
			}
			else
			{
				LoginActivity.this.finish();
				
				System.exit(0);
			}
			
			return true;
		}
        
        
        return super.onKeyDown(keyCode, event);
    }

    private boolean isEmail(String input)
    {
    	if(!input.matches("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$")){

    		   return false;

    		}else{

    		    return true;

    		}
    }
    
    private void showEasternEggs(int easternType)
    {
    	switch(easternType)
    	{
    	
    	case SHEEP_TYPE:
    	{
    		Intent intent = new Intent(LoginActivity.this, FlyingObjectsActivity.class);
    		intent.putExtra("isArray", false);
        	intent.putExtra(FlyingObjectsActivity.RESOURCE, R.drawable.eastern_egg_sheep);
        	LoginActivity.this.startActivity(intent);
        	
        	break;
    	}
    	case SNOW_TYPE:
    	{
    		Intent intent = new Intent(LoginActivity.this, FlyingObjectsActivity.class);
    		intent.putExtra("isArray", true);
    		
    		ArrayList<Integer> snowResource = new ArrayList<Integer>();
    		for(int i=0; i<SNOW_RESOURCE.length; i++)
    		{
    			snowResource.add(SNOW_RESOURCE[i]);
    		}
    		
        	intent.putIntegerArrayListExtra(FlyingObjectsActivity.RESOURCE, snowResource);
        	LoginActivity.this.startActivity(intent);
    		
    		break;
    	}
    	
    	}
    }
    
    private int getEasternEggType(String username, String password)
    {
    	if(username.equalsIgnoreCase(SHEEP_USERNAME) && password.equalsIgnoreCase(SHEEP_PASSWORD))
    	{
    		return SHEEP_TYPE;
    	}
    	else if(username.equalsIgnoreCase(SNOW_USERNAME) && password.equalsIgnoreCase(SNOW_PASSWORD))
    	{
    		return SNOW_TYPE;
    	}
    	
    	return -1;
    }
    
    @Override
    protected void onDestroy() {
        /*
        JPushInterface.stopPush(getApplicationContext());
        ShareSDK.stopSDK(getApplicationContext());
        */
        super.onDestroy();
    }
}