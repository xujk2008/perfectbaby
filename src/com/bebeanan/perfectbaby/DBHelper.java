package com.bebeanan.perfectbaby;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by guomoyi on 14-6-2.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;//版本
    private static final String DB_NAME = "perfectbaby.db";//数据库名
    public static final String STUDENT_TABLE = "BabyMessage";//表名
    private static final String CREATE_TABLE = "CREATE TABLE BabyMessage ( id INTEGER PRIMARY KEY, uid VARCHAR(64) DEFAULT NULL, isread TINYINT DEFAULT 0, text VARCHAR(256) DEFAULT NULL, isid TINYINT DEFAULT 0, url TEXT, date TIMESTAMP DEFAULT CURRENT_TIMESTAMP );";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

}
