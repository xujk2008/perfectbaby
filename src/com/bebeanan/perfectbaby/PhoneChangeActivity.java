package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class PhoneChangeActivity extends Activity{

	private TextView phone_change_number_text; 
	private EditText phone_change_number_edit;
	private ImageButton phone_change_change_button;
	
	private String phoneNumber;
	
	private int PHONE_CHANGE_REQUEST_CODE = 1;
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phone_change);
		
		ImageView leftButton = (ImageView) findViewById(R.id.title_left_button);
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setImageResource(R.drawable.title_back_pic);
		leftButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PhoneChangeActivity.this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
			}
			
		});
		
		TextView titleText = (TextView) findViewById(R.id.title_text);
		titleText.setTypeface(Utils.typeface);
		titleText.setText("绑定手机");
		
		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);
		
		phone_change_number_text = (TextView) findViewById(R.id.phone_change_number_text);
		phone_change_number_text.setTypeface(Utils.typeface);
		
		phone_change_number_edit = (EditText) findViewById(R.id.phone_change_number_edittext);
//		phone_change_number_edit.setTypeface(Utils.typeface);
		phoneNumber = PhoneChangeActivity.this.getIntent().getStringExtra("phone");
		phone_change_number_edit.setText(phoneNumber);
		
		phone_change_change_button = (ImageButton) findViewById(R.id.phone_change_change_button);
		phone_change_change_button.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PhoneChangeActivity.this, PhoneVerifyActivity.class);
				intent.putExtra("phone", phoneNumber);
				PhoneChangeActivity.this.startActivityForResult(intent, PHONE_CHANGE_REQUEST_CODE);
			}
			
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == PHONE_CHANGE_REQUEST_CODE)
		{
			if(resultCode == RESULT_OK)
			{
				PhoneChangeActivity.this.finish();
			}
		}
	}

	
}
