package com.bebeanan.perfectbaby;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class PersonalDetailActivity extends Activity {

	private ImageView personal_detail_avatar;
	private TextView personal_detail_nickname;
	private ImageView personal_detail_gender;

	private TextView personal_detail_location, personal_detail_location_text;

	private TextView personal_detail_babies;
	private ImageView personal_detail_baby_avatar1,
			personal_detail_baby_avatar2, personal_detail_baby_avatar3;
	private ImageView[] babies_avatar = new ImageView[3];
	private JSONObject currentBaby;

	private ProgressBar personal_detail_progress;

	private ImageView personal_detail_bottom_button;

	private ImageView dialog_follow_cancel, dialog_follow_complete;

	private String followeeId, avatarUrl, nickName;

	private static SharedPreferences userPreferences;
	
	private boolean isOwn;
	private boolean shouldRefresh = false;

	@Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  		
  		shouldRefresh = true;
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  		
  		if(shouldRefresh && isOwn)
  		{
  			getOwnData();
  		}
  	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_detail);

		if (Utils.application == null) {
			Utils.init(PersonalDetailActivity.this);
		}
		if (userPreferences == null) {
			userPreferences = Utils.application.getSharedPreferences(
					"userInfo", Context.MODE_PRIVATE);
		}

		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PersonalDetailActivity.this.finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}

		});

		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("个人详情");

		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);

		followeeId = PersonalDetailActivity.this.getIntent().getStringExtra(
				"followeeId");
//		avatarUrl = PersonalDetailActivity.this.getIntent().getStringExtra(
//				"followeeAvatarUrl");
//		nickName = PersonalDetailActivity.this.getIntent().getStringExtra(
//				"followeeNickName");

		personal_detail_avatar = (ImageView) findViewById(R.id.personal_detail_avatar);
		
		personal_detail_nickname = (TextView) findViewById(R.id.personal_detail_nickname);
		personal_detail_nickname.setTypeface(Utils.typeface);
		personal_detail_gender = (ImageView) findViewById(R.id.personal_detail_gender);

		personal_detail_location = (TextView) findViewById(R.id.personal_detail_location);
		personal_detail_location.setTypeface(Utils.typeface);
		personal_detail_location_text = (TextView) findViewById(R.id.personal_detail_location_text);
		personal_detail_location_text.setTypeface(Utils.typeface);

		personal_detail_babies = (TextView) findViewById(R.id.personal_detail_babies);
		personal_detail_babies.setTypeface(Utils.typeface);
		personal_detail_baby_avatar1 = (ImageView) findViewById(R.id.personal_detail_baby_avatar1);
		babies_avatar[0] = personal_detail_baby_avatar1;
		personal_detail_baby_avatar2 = (ImageView) findViewById(R.id.personal_detail_baby_avatar2);
		babies_avatar[1] = personal_detail_baby_avatar2;
		personal_detail_baby_avatar3 = (ImageView) findViewById(R.id.personal_detail_baby_avatar3);
		babies_avatar[2] = personal_detail_baby_avatar3;

		personal_detail_progress = (ProgressBar) findViewById(R.id.personal_detail_progress);

		personal_detail_bottom_button = (ImageView) findViewById(R.id.personal_detail_bottom_button);
		personal_detail_bottom_button.setClickable(false);
		
		isOwn = followeeId.equals(userPreferences.getString("userId", ""));
		if (isOwn) 
		{
			personal_detail_bottom_button.setVisibility(View.GONE);
			
			getOwnData();
//			personal_detail_bottom_button
//					.setImageResource(R.drawable.personal_detail_enter_selector);
//			personal_detail_bottom_button
//					.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method
//							// stub
//
//							try {
//								String baby = MemoryHandler.getInstance()
//										.getKey("baby");
//								JSONArray babyArray = new JSONArray(baby);
//								String babyId = babyArray.getJSONObject(0)
//										.getString("id");
//
//								SimpleDateFormat sdf = new SimpleDateFormat(
//										"yyyy-MM-dd");
//								Calendar calendar = Calendar.getInstance();
//								calendar.set(Calendar.HOUR_OF_DAY, 0);
//								calendar.set(Calendar.MINUTE, 0);
//								calendar.set(Calendar.SECOND, 0);
//								calendar.set(Calendar.MILLISECOND, 0);
//								calendar.setFirstDayOfWeek(Calendar.SUNDAY);
//
//								String uri = "/feeds?";
//								try {
//									uri += URLEncoder.encode(
//											"{\"baby\":\""
//													+ babyId
//													+ "\",\"day\":\""
//													+ sdf.format(calendar
//															.getTime()) + "\"}",
//											"UTF-8");
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
//
//								Intent intent = new Intent();
//								intent.putExtra("babyid", babyId);
//								intent.putExtra("uri", uri);
//								intent.putExtra("date",
//										sdf.format(calendar.getTime()));
//								Log.v("Kite",
//										"date in intent is "
//												+ sdf.format(calendar.getTime()));
//								String babyName = currentBaby.getString("name");
//								if (babyName.equals("") || babyName.equals(" ")) {
//									intent.putExtra("nickName",
//											personal_detail_nickname.getText()
//													.toString() + "家宝宝");
//								} else {
//									intent.putExtra("nickName",
//											currentBaby.getString("name"));
//								}
//								intent.putExtra("avatarUrl",
//										currentBaby.getString("avatar"));
//								intent.putExtra("birthday",
//										currentBaby.getString("birthday"));
//								// intent.setClass(TreeActivity.this,
//								// DetailActivity.class);
//								intent.setClass(PersonalDetailActivity.this,
//										PersonalZoneActivity.class);
//								startActivity(intent);
//							} catch (JSONException e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
//						}
//
//					});
		}
		else
		{
			personal_detail_bottom_button.setVisibility(View.VISIBLE);
			personal_detail_bottom_button
					.setImageResource(R.drawable.personal_detail_follow_pressed);
			personal_detail_bottom_button
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							applyFollow();
						}

					});

			getData();
		}
	}

	private void applyFollow()
	{

		// TODO Auto-generated method stub

		final Dialog personal_detail_follow_dialog = new Dialog(
				PersonalDetailActivity.this);
		personal_detail_follow_dialog.show();

		Window window = personal_detail_follow_dialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(0));
		window.setContentView(R.layout.dialog_follow);

		dialog_follow_cancel = (ImageView) window
				.findViewById(R.id.dialog_follow_cancel);
		dialog_follow_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				personal_detail_follow_dialog.cancel();
			}

		});
		dialog_follow_complete = (ImageView) window
				.findViewById(R.id.dialog_follow_complete);
		dialog_follow_complete
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						// TODO Auto-generated method stub
						
						personal_detail_follow_dialog.cancel();
						
						JSONObject followeeObject = new JSONObject();
						try {
							 followeeObject.put("userId",
							 userPreferences.getString("userId", ""));
							 followeeObject.put("followee",
							 followeeId);
							//
							// NetHandler handler = new NetHandler(
							// NetHandler.METHOD_POST, "/followee",
							// new LinkedList<BasicNameValuePair>(),
							// followeeObject) {
							//
							// @Override
							// public void handleRsp(Message msg) {
							// // TODO Auto-generated method stub
							// personal_detail_follow_dialog.cancel();
							//
							// Bundle bundle = msg.getData();
							//
							// int code = bundle.getInt("code");
							// String data = bundle.getString("data");
							// if(code == 200)
							// {
							// Toast.makeText(PersonalDetailActivity.this,
							// "关注成功", Toast.LENGTH_SHORT).show();
							//
							// Log.v("Kite", "follow success");
							// }
							// else
							// {
							// Toast.makeText(PersonalDetailActivity.this,
							// "关注失败", Toast.LENGTH_SHORT).show();
							//
							// Log.v("Kite", "follow fail because" +
							// data);
							// }
							// }
							//
							// };

							NetHandler handler = new NetHandler(PersonalDetailActivity.this, 
									NetHandler.METHOD_POST,
									"/functions/applyFollow?who="
											+ followeeId,
									new LinkedList<BasicNameValuePair>(),
									null) {

								@Override
								public void handleRsp(Message msg) {
									// TODO Auto-generated method stub
									
									Bundle bundle = msg.getData();
									int code = bundle.getInt("code");
									String data = bundle.getString("data");
									
									if (code == 200) 
									{
										Toast toast = Toast.makeText(PersonalDetailActivity.this, "您的关注验证已发送成功，请等待对方验证", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										Log.v("Kite", "applyFollow success");
									} 
									else if(code == -100)
									{
										Toast toast = Toast.makeText(PersonalDetailActivity.this, "您的网络连接有问题，请稍后再试。", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
									}
									else
									{
										Toast toast = Toast.makeText(PersonalDetailActivity.this, "服务器繁忙，小贝正在紧急修复中，非常抱歉！", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										Log.v("Kite",
												"applyFollow fail because"
														+ data);
									}
								}

							};

							handler.start();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				});

	
	}
	
	private void getOwnData()
	{
		personal_detail_progress.setVisibility(View.GONE);
		
		avatarUrl = userPreferences.getString("avatarUrl",
					null);
		if (avatarUrl != null && !avatarUrl.equals("")) {
			new UrlImageViewHelper().setUrlDrawable(personal_detail_avatar,
					avatarUrl);
		} else {

			personal_detail_avatar
					.setImageResource(R.drawable.user_info_default_avatar);
		}
		
		nickName = userPreferences.getString("nickName",
				"");
		personal_detail_nickname.setText(nickName);
		
		int gender = userPreferences.getInt("gender", -1);
		if (gender == 2) {
			personal_detail_gender
					.setImageResource(R.drawable.user_info_female);
		} else {
			personal_detail_gender
					.setImageResource(R.drawable.user_info_male);
		}

		personal_detail_location_text.setText(userPreferences.getString("city", ""));

		StoreHandler.getAndSetBabies();
		String baby = MemoryHandler.getInstance().getKey("baby");
		JSONArray babiesArray;
		try {
			babiesArray = new JSONArray(baby);
			
			setBabies(babiesArray);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
	}
	
	private void getData() {
		personal_detail_progress.setVisibility(View.VISIBLE);

		LinkedList<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("include", "baby"));
		NetHandler handler = new NetHandler(PersonalDetailActivity.this, NetHandler.METHOD_GET, "/users/"
				+ followeeId, params, null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub

				personal_detail_progress.setVisibility(View.GONE);

				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				String data = bundle.getString("data");
				Log.v("Kite", "PersonalDetail data is " + data);

				if (code == 200) {
					try {
						JSONObject object = new JSONObject(data);

						if(object.has("avatar"))
						{
							avatarUrl = object.getString("avatar");
						}
						else
						{
							avatarUrl = "";
						}
						if (!avatarUrl.equals("")) {
							new UrlImageViewHelper().setUrlDrawable(personal_detail_avatar,
									avatarUrl);
						} else {

							personal_detail_avatar
									.setImageResource(R.drawable.user_info_default_avatar);
						}
						
						nickName = object.getString("nickname");
						personal_detail_nickname.setText(nickName);
						
						int gender = object.has("gender") ? object
								.getInt("gender") : -1;
						if (gender == 2) {
							personal_detail_gender
									.setImageResource(R.drawable.user_info_female);
						} else {
							personal_detail_gender
									.setImageResource(R.drawable.user_info_male);
						}

						if(object.has("city"))
						{
							personal_detail_location_text.setText(object
									.getString("city"));
						}

						final JSONArray babiesArray = object.getJSONArray("babies");
						
						setBabies(babiesArray);
//						for (int i = 0; i < 3; i++) {
//							babies_avatar[i].setVisibility(View.INVISIBLE);
//						}
//						for (int i = 0; i < babiesArray.length(); i++) {
//							
//							final int currentBabyIndex = i;
//							
//							if (i == 3) {
//								break;
//							}
//							final JSONObject babyObject = babiesArray
//									.getJSONObject(i);
//							
//							if(i == 0)
//							{
//								currentBaby = babyObject;
//							}
//							
//							babies_avatar[i].setVisibility(View.VISIBLE);
//
//							String babyAvatar = babyObject.has("avatar") ? babyObject
//									.getString("avatar") : "";
//							if (!babyAvatar.equals("")) {
//								new UrlImageViewHelper().setUrlDrawable(
//										babies_avatar[i], babyAvatar);
//							} else {
//
//								babies_avatar[i]
//										.setImageResource(R.drawable.baby_default_pic);
//							}
//							
//							babies_avatar[i].setOnClickListener(new OnClickListener(){
//
//								@Override
//								public void onClick(View v) {
//									// TODO Auto-generated method stub
//									
//									try {
//										String babyId = babyObject.getString("id");
//										
//										SimpleDateFormat sdf = new SimpleDateFormat(
//												"yyyy-MM-dd");
//										Calendar calendar = Calendar
//												.getInstance();
//										calendar.set(Calendar.HOUR_OF_DAY,
//												0);
//										calendar.set(Calendar.MINUTE, 0);
//										calendar.set(Calendar.SECOND, 0);
//										calendar.set(Calendar.MILLISECOND,
//												0);
//										calendar
//												.setFirstDayOfWeek(Calendar.SUNDAY);
//										
//										String uri = "/feeds?";
//										try {
//											uri += URLEncoder
//													.encode("{\"baby\":\""
//															+ babyId
//															+ "\",\"day\":\""
//															+ sdf.format(calendar
//																	.getTime())
//															+ "\"}",
//															"UTF-8");
//										} catch (Exception e) {
//											e.printStackTrace();
//										}
//
//										Intent intent = new Intent();
//										if(!followeeId.equals(userPreferences.getString("userId", "")))
//										{
//											intent.putExtra("followeeId", followeeId);
//										}
//										intent.putExtra("babyid", babyId);
//										intent.putExtra("uri", uri);
//										intent.putExtra("date",
//												sdf.format(calendar
//														.getTime()));
//										Log.v("Kite", "date in intent is " + sdf.format(calendar
//												.getTime()));
//										String babyName = babyObject.getString("name");
//										if(babyName.equals("") || babyName.equals(" "))
//										{
//											intent.putExtra("nickName", personal_detail_nickname.getText().toString() + "家宝宝");
//										}
//										else
//										{
//											intent.putExtra("nickName", babyObject.getString("name"));
//										}
//										
//										String babyAvatar = babyObject.has("avatar") ? babyObject
//												.getString("avatar") : "";
//										intent.putExtra("avatarUrl", babyAvatar);
//										intent.putExtra("birthday", babyObject.getString("birthday"));
//										// intent.setClass(TreeActivity.this,
//										// DetailActivity.class);
//										intent.setClass(
//												PersonalDetailActivity.this,
//												PersonalZoneActivity.class);
//										startActivity(intent);
//									} catch (JSONException e1) {
//										// TODO Auto-generated catch block
//										e1.printStackTrace();
//									}
//								}
//								
//							});
//						}
						
						NetHandler followeeHandler = new NetHandler(PersonalDetailActivity.this, NetHandler.METHOD_GET,
								"/followee?userId=" + userPreferences.getString("userId", "")+"&$limit=0", new LinkedList<BasicNameValuePair>(), null) {

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub

								Bundle followeeBundle = msg.getData();
								int code = followeeBundle.getInt("code");
								if (code == 200) {
									Log.v("Kite",
											"followee data is "
													+ followeeBundle.getString("data"));
									try {
										JSONArray followeeArray = new JSONArray(
												followeeBundle.getString("data"));

										if(!isOwn)
										{
											personal_detail_bottom_button
											.setImageResource(R.drawable.personal_detail_follow_selector);
										}
											
										boolean hasFollowed = false;
										
										for (int i = 0; i < followeeArray.length(); i++) {
											
											if(isOwn)
											{
												break;
											}
											
											JSONObject curObject = followeeArray
													.getJSONObject(i);
											JSONObject followeeObject = curObject
													.getJSONObject("followee");

											if (followeeId.equals(followeeObject
													.getString("id"))) {
												
												hasFollowed = true;
												
												personal_detail_bottom_button
														.setImageResource(R.drawable.personal_detail_enter_selector);
												personal_detail_bottom_button
														.setOnClickListener(new OnClickListener() {

															@Override
															public void onClick(View v) {
																// TODO Auto-generated method
																// stub
//																Intent intent = new Intent(
//																		PersonalDetailActivity.this,
//																		PersonalZoneActivity.class);
//																intent.putExtra("followeeId",
//																		followeeId);
//																intent.putExtra(
//																		"followeeNickName",
//																		nickName);
//																intent.putExtra(
//																		"followeeAvatarUrl",
//																		avatarUrl);
				//
//																PersonalDetailActivity.this
//																		.startActivity(intent);
																
																if(currentBaby == null)
																{
																	Toast toast = Toast.makeText(PersonalDetailActivity.this, "TA还没有添加宝宝哦~", Toast.LENGTH_SHORT);
																	toast.setGravity(Gravity.CENTER, 0, 0);
																	toast.show();
																	
																	return;
																}
																
																try {
																	String babyId = currentBaby.getString("id");
																	
																	SimpleDateFormat sdf = new SimpleDateFormat(
																			"yyyy-MM-dd");
																	Calendar calendar = Calendar
																			.getInstance();
																	calendar.set(Calendar.HOUR_OF_DAY,
																			0);
																	calendar.set(Calendar.MINUTE, 0);
																	calendar.set(Calendar.SECOND, 0);
																	calendar.set(Calendar.MILLISECOND,
																			0);
																	calendar
																			.setFirstDayOfWeek(Calendar.SUNDAY);
																	
																	String uri = "/feeds?";
																	try {
																		uri += URLEncoder
																				.encode("{\"baby\":\""
																						+ babyId
																						+ "\",\"day\":\""
																						+ sdf.format(calendar
																								.getTime())
																						+ "\"}",
																						"UTF-8");
																	} catch (Exception e) {
																		e.printStackTrace();
																	}

																	Intent intent = new Intent();
																	intent.putExtra("followeeId", followeeId);
																	intent.putExtra("babyid", babyId);
																	intent.putExtra("uri", uri);
																	intent.putExtra("date",
																			sdf.format(calendar
																					.getTime()));
																	Log.v("Kite", "date in intent is " + sdf.format(calendar
																			.getTime()));
																	String babyName = currentBaby.getString("name");
																	if(babyName.equals("") || babyName.equals(" "))
																	{
																		intent.putExtra("nickName", personal_detail_nickname.getText().toString() + "家宝宝");
																	}
																	else
																	{
																		intent.putExtra("nickName", currentBaby.getString("name"));
																	}
																	intent.putExtra("avatarUrl", currentBaby.getString("avatar"));
																	intent.putExtra("birthday", currentBaby.getString("birthday"));
																	// intent.setClass(TreeActivity.this,
																	// DetailActivity.class);
																	intent.setClass(
																			PersonalDetailActivity.this,
																			PersonalZoneActivity.class);
																	startActivity(intent);
																} catch (JSONException e1) {
																	// TODO Auto-generated catch block
																	e1.printStackTrace();
																}
															}

														});

												break;
											}
										}

										personal_detail_bottom_button.setClickable(true);
										
										if(!isOwn && !hasFollowed)
										{
											for (int i = 0; i < babiesArray.length(); i++) {
												if (i == 3) {
													break;
												}
												
												babies_avatar[i].setOnClickListener(new OnClickListener(){

													@Override
													public void onClick(View v) {
														// TODO Auto-generated method stub
														
														applyFollow();
													}
													
												});
											}
										}

									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {

								}
							}

						};

						followeeHandler.start();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					Toast toast = Toast.makeText(PersonalDetailActivity.this, "加载数据失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		};

		handler.start();
	}
	
	private void setBabies(JSONArray babiesArray) throws JSONException
	{
		for (int i = 0; i < 3; i++) {
			babies_avatar[i].setVisibility(View.INVISIBLE);
		}
		for (int i = 0; i < babiesArray.length(); i++) {
			
			final int currentBabyIndex = i;
			
			if (i == 3) {
				break;
			}
			final JSONObject babyObject = babiesArray
					.getJSONObject(i);
			
			if(i == 0)
			{
				currentBaby = babyObject;
			}
			
			babies_avatar[i].setVisibility(View.VISIBLE);

			String babyAvatar = babyObject.has("avatar") ? babyObject
					.getString("avatar") : "";
			if (!babyAvatar.equals("")) {
				new UrlImageViewHelper().setUrlDrawable(
						babies_avatar[i], babyAvatar);
			} else {

				babies_avatar[i]
						.setImageResource(R.drawable.baby_default_pic);
			}
			
			babies_avatar[i].setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					try {
						String babyId = babyObject.getString("id");
						
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Calendar calendar = Calendar
								.getInstance();
						calendar.set(Calendar.HOUR_OF_DAY,
								0);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND,
								0);
						calendar
								.setFirstDayOfWeek(Calendar.SUNDAY);
						
						String uri = "/feeds?";
						try {
							uri += URLEncoder
									.encode("{\"baby\":\""
											+ babyId
											+ "\",\"day\":\""
											+ sdf.format(calendar
													.getTime())
											+ "\"}",
											"UTF-8");
						} catch (Exception e) {
							e.printStackTrace();
						}

						Intent intent = new Intent();
						if(!followeeId.equals(userPreferences.getString("userId", "")))
						{
							intent.putExtra("followeeId", followeeId);
						}
						intent.putExtra("babyid", babyId);
						intent.putExtra("uri", uri);
						intent.putExtra("date",
								sdf.format(calendar
										.getTime()));
						Log.v("Kite", "date in intent is " + sdf.format(calendar
								.getTime()));
						String babyName = babyObject.getString("name");
						if(babyName.equals("") || babyName.equals(" "))
						{
							intent.putExtra("nickName", personal_detail_nickname.getText().toString() + "家宝宝");
						}
						else
						{
							intent.putExtra("nickName", babyObject.getString("name"));
						}
						
						String babyAvatar = babyObject.has("avatar") ? babyObject
								.getString("avatar") : "";
						intent.putExtra("avatarUrl", babyAvatar);
						intent.putExtra("birthday", babyObject.getString("birthday"));
						// intent.setClass(TreeActivity.this,
						// DetailActivity.class);
						intent.setClass(
								PersonalDetailActivity.this,
								PersonalZoneActivity.class);
						startActivity(intent);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			});
		}
	}
}
