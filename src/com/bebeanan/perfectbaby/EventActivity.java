package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bebeanan.perfectbaby.common.CustomMediaPlayer;
import com.bebeanan.perfectbaby.common.MultipartEntityWithProgressBar.WriteListener;
import com.bebeanan.perfectbaby.common.RotatePhoto;
import com.bebeanan.perfectbaby.common.Utils;
import com.luminous.pick.Action;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class EventActivity extends Activity {

    public static final int EVENT_VOICE = 1;
    public static final int EVENT_NOTE = 2;
    public static final int EVENT_PHOTO = 3;
    public static final int EVENT_VIDEO = 4;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_VOICE = 3;


    public static final int VOICE_INIT = 1;
    private int miVoiceState = VOICE_INIT;
    public static final int VOICE_RECORD = 2;
    public static final int VOICE_RECORD_END = 3;
    public static final int VOICE_START = 4;
    public static final int VOICE_STOP = 5;
    public static final int VOICE_UPLOADING = 6;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE = 2000;
    private static final int GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE = 2001;
    private static final int GALLERY_ACTIVITY_REQUEST_CODE = 3001;
    private static final int OPEN_GPS = 3000;
    private static final int MAX_TEXT_TO_INPUT = 300;
    private static final int VOICE_RECORD_MAX_LENGTH = 61000;
    int weight = 100;
    int height = 80;
    int marginTop = 20;
    private static final String POSITION_TEXT = "插入位置";
    
    private ImageView event_guide;
    
    private ImageView babyAvatar;
    private TextView otherBaby1;
    private TextView otherBaby2;
    private boolean otherBabyShown = false;
    private List<TextView> otherBabyView;
    private List<String> otherBabyName;
    private List<Integer> otherBabyIndex;
    private LinearLayout photoLayout;
    private RelativeLayout videoLayout;
    private RelativeLayout voiceLayout;
    private VideoView videoView;
    private ImageView photoView;
    private Uri fileUri;
    private int eventType;
    private int miNumber = 1;
    private int maxNumber = 1;
    private ArrayList<String> imageResourceList;
    private MediaPlayer mPlayer = null;
    private MediaRecorder mRecorder = null;
    private ImageView reRecord, useVoice;
    private SeekBar voiceSeekBar;
    private ImageButton record;
    private ImageButton recordButton;
    private LocationManager locationManager = null;
    private ImageButton positionButton;
    private TextView positionText;
    private TextView restText;
    private Location location;
    private LocationListener locationListener;
    private List<String> urlList;
    private TextView recordTimeView;

    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;

    private String babyId;
    private int currentBaby;

    private EditText noteEditText;
    private ImageView modeImage;
    private TextView modeText;
    private RadioGroup tagGroup;

    private boolean hasEditted = false;
    
    private Handler handler = null;
    private Runnable runnable = null;

    int recordTime;
    
    private Dialog upload_progress_dialog;
	private TextView upload_progress_percentage;

    private Handler uploadProgressHandler = new Handler()
    {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			
			Bundle bundle = msg.getData();
			if(bundle.getInt("result") == RESULT_OK)
			{
				long amountOfBytesWritten = bundle.getLong("amountOfBytesWritten");
				long totalByteOfFile = bundle.getLong("totalByteOfFile");
				
				double percentage = amountOfBytesWritten*1.0/totalByteOfFile;
				Log.v("Kite", "percentage: " + percentage + " " + amountOfBytesWritten + "/" + totalByteOfFile);
				
//				dialog.dismiss();
//				builder.setMessage(String.format("%.2f", percentage*100));
//				dialog = builder.show();
				
				upload_progress_percentage.setText(String.format("%.2f", percentage*100)+"%");
			}
		}
    	
    };
    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type, int number) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PerfectBaby");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.


        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PerfectBaby", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + String.valueOf(number) + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + String.valueOf(number) + ".mp4");
        } else if (type == MEDIA_TYPE_VOICE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VOI_" + String.valueOf(number) + ".aac");
        } else {
            return null;
        }

        Log.v("DBG", mediaFile.getPath());

        return mediaFile;

    }

    private void UpdateTime() {
    	
    	if(mRecorder == null)
    	{
    		return;
    	}
    	
    	if(recordTime >= VOICE_RECORD_MAX_LENGTH/1000)
    	{
    		Toast toast = Toast.makeText(EventActivity.this, "录音时间已达上限", Toast.LENGTH_SHORT);
    		toast.setGravity(Gravity.CENTER, 0, 0);
    		toast.show();
    		
    		if(mRecorder != null)
    		{
    			mRecorder.stop();
                mRecorder.release();
    		}
    		
            record.clearAnimation();
            handler.removeCallbacks(runnable);

            mRecorder = null;
            recordButton.setImageResource(R.drawable.event_voice_record_start_pic);
            
            showVoiceOption();
            
            miVoiceState = VOICE_INIT;
            record.setClickable(false);
    		
    		return;
    	}
    	
        int minute = recordTime / 60;
        int second = recordTime % 60;
        recordTimeView.setText(String.format("%02d:%02d", minute, second));
    }

    private void voiceClickEvent() {
        switch (miVoiceState) {
            case VOICE_INIT: {
            	
            	recordTime = 0;
                mRecorder = new MediaRecorder();
//                mRecorder.setMaxDuration(VOICE_RECORD_MAX_LENGTH);
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                File fileName = getOutputMediaFile(MEDIA_TYPE_VOICE, 0);
                mRecorder.setOutputFile(fileName.getPath());
                try {
                    mRecorder.prepare();
                } catch (IOException e) {
                    Log.e("DBG", "prepare() failed");
                }
                mRecorder.start();
                RotateAnimation rotateAnimation = new RotateAnimation(0f, 360f,
                        Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
                rotateAnimation.setDuration(1000);
                rotateAnimation.setRepeatCount(Animation.INFINITE);
                record.setAnimation(rotateAnimation);

                handler = new Handler();
                runnable = new Runnable() {
                    public void run () {
                        recordTime++;
                        UpdateTime();
                        handler.postDelayed(this,1000);
                    }
                };
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable,1000);

                miVoiceState = VOICE_RECORD;
                miNumber = 1;
                maxNumber = 1;
                recordButton.setImageResource(R.drawable.event_voice_record_stop_pic);
                break;
            }
            case VOICE_RECORD: {

            	mRecorder.stop();
                mRecorder.release();
                record.clearAnimation();
                handler.removeCallbacks(runnable);

                mRecorder = null;
                recordButton.setImageResource(R.drawable.event_voice_record_start_pic);
                
                showVoiceOption();
                
                miVoiceState = VOICE_INIT;
                record.setClickable(false);
                
                break;
            }
//            case VOICE_RECORD_END:
//            case VOICE_STOP: {
//                mPlayer = new MediaPlayer();
//                try {
//                    File fileName = getOutputMediaFile(MEDIA_TYPE_VOICE, 0);
//                    mPlayer.setDataSource(fileName.getPath());
//                    mPlayer.prepare();
//                    mPlayer.start();
//                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mediaPlayer) {
//                            mPlayer.release();
//                            mPlayer = null;
//                            miVoiceState = VOICE_STOP;
//                            recordButton.setImageResource(R.drawable.event_voice_record_start_pic);
//                        }
//                    });
//                } catch (IOException e) {
//                    Log.e("DBG", "播放失败");
//                }
//                miVoiceState = VOICE_START;
//                recordButton.setImageResource(R.drawable.event_voice_record_stop_pic);
//                break;
//            }
//            case VOICE_START: {
//                mPlayer.release();
//                mPlayer = null;
//                miVoiceState = VOICE_STOP;
//                recordButton.setImageResource(R.drawable.event_voice_record_start_pic);
//                break;
//            }
//            case VOICE_UPLOADING: {
//                break;
//            }
        }
    }

    private void showVoiceOption()
    {
    	record.setClickable(false);
    	
    	AnimationSet reRecordAnimationSet = new AnimationSet(true);
        {
            AlphaAnimation myAnimation1 = new AlphaAnimation(0, 100);
            myAnimation1.setDuration(300);
            myAnimation1.setFillAfter(false);
            reRecordAnimationSet.addAnimation(myAnimation1);
            
            int width = reRecord.getWidth();
            int height = reRecord.getHeight();
            reRecord.setTranslationX(- 1*width);
            reRecord.setTranslationY(- 1*height);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            reRecordAnimationSet.addAnimation(myAnimation);
        }
        AnimationSet useVoiceAnimationSet = new AnimationSet(true);
        {
            AlphaAnimation myAnimation1 = new AlphaAnimation(0, 100);
            myAnimation1.setDuration(300);
            myAnimation1.setFillAfter(false);
            useVoiceAnimationSet.addAnimation(myAnimation1);
            
            int width = useVoice.getWidth();
            int height = useVoice.getHeight();
            useVoice.setTranslationX(1*width);
            useVoice.setTranslationY(- 1*height);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            useVoiceAnimationSet.addAnimation(myAnimation);
        }
        
        reRecord.startAnimation(reRecordAnimationSet);
        useVoice.startAnimation(useVoiceAnimationSet);
        
    }
    
    private void hideVoiceOption()
    {
    	record.setClickable(false);
    	
    	AnimationSet reRecordAnimationSet = new AnimationSet(true);
        {
            AlphaAnimation myAnimation1 = new AlphaAnimation(100, 0);
            myAnimation1.setDuration(300);
            myAnimation1.setFillAfter(false);
            reRecordAnimationSet.addAnimation(myAnimation1);
            
            int width = reRecord.getWidth();
            int height = reRecord.getHeight();
            reRecord.setTranslationX(0);
            reRecord.setTranslationY(0);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            reRecordAnimationSet.addAnimation(myAnimation);
        }
        AnimationSet useVoiceAnimationSet = new AnimationSet(true);
        {
            AlphaAnimation myAnimation1 = new AlphaAnimation(100, 0);
            myAnimation1.setDuration(300);
            myAnimation1.setFillAfter(false);
            useVoiceAnimationSet.addAnimation(myAnimation1);
            
            int width = useVoice.getWidth();
            int height = useVoice.getHeight();
            useVoice.setTranslationX(0);
            useVoice.setTranslationY(0);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            useVoiceAnimationSet.addAnimation(myAnimation);
        }
        
        reRecord.startAnimation(reRecordAnimationSet);
        useVoice.startAnimation(useVoiceAnimationSet);
        
        record.setClickable(true);
    }
    
    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        /*
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.city_title);
        */

        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(EventActivity.this);
        builder.setCancelable(false);

        upload_progress_dialog = new Dialog(
				EventActivity.this);
		Window window = upload_progress_dialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(0));
		window.setContentView(R.layout.dialog_upload_progress);
		upload_progress_percentage = (TextView) window
				.findViewById(R.id.dialog_upload_progress_percentage);
        
        urlList = new LinkedList<String>();

        Intent intent = this.getIntent();
        fileUri = intent.getData();
        eventType = intent.getIntExtra("EventType", EVENT_NOTE);
        
        currentBaby = intent.getIntExtra("currentBaby", 0);
        StoreHandler.getAndSetBabies();
        String baby = MemoryHandler.getInstance().getKey("baby");
		
        babyId = "";
        try {
            JSONArray babyArray = new JSONArray(baby);
            babyId = babyArray.getJSONObject(currentBaby).getString("id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v("DBG", "babyId " + babyId);
        
        event_guide = (ImageView) findViewById(R.id.event_guide);
        
        babyAvatar = (ImageView) findViewById(R.id.event_baby_select_avatar);
        otherBaby1 = (TextView) findViewById(R.id.event_baby_select_other1);
        otherBaby2 = (TextView) findViewById(R.id.event_baby_select_other2);
        videoLayout = (RelativeLayout) findViewById(R.id.event_video_layout);
        photoLayout = (LinearLayout) findViewById(R.id.event_photo_layout);
        voiceLayout = (RelativeLayout) findViewById(R.id.event_voice_layout);

        noteEditText = (EditText) findViewById(R.id.event_note_edittext);
//        noteEditText.setTypeface(Utils.typeface);
        noteEditText.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				int restLength = MAX_TEXT_TO_INPUT - s.length();
				if(restLength > 10)
				{
					restText.setVisibility(View.GONE);
				}
				else
				{
					restText.setVisibility(View.VISIBLE);
					restText.setText(restLength+"");
					if(restLength>=0)
					{
						restText.setTextColor(0xff000000);
					}
					else
					{
						restText.setTextColor(0xffff0000);
					}
				}
			}
        	
        });
        modeImage = (ImageView) findViewById(R.id.event_mode_image);
        modeImage.setTag(0);
        modeText = (TextView) findViewById(R.id.event_mode_text);
        modeText.setTypeface(Utils.typeface);
        modeText.setText("公开");
        
        modeImage.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int tag = (Integer)modeImage.getTag();
				
				if(tag == 0)
				{
					modeImage.setImageResource(R.drawable.event_private);
					modeImage.setTag(1);
					modeText.setText("私密");
				}
				else if(tag == 1)
				{
					modeImage.setImageResource(R.drawable.event_public);
					modeImage.setTag(0);
					modeText.setText("公开");
				}
			}
        	
        });
        
        tagGroup = (RadioGroup) findViewById(R.id.event_tag_radio);
        RadioButton event_tag_milk = (RadioButton) tagGroup.findViewById(R.id.event_tag_milk);
        event_tag_milk.setTypeface(Utils.typeface);
        RadioButton event_tag_feed = (RadioButton) tagGroup.findViewById(R.id.event_tag_feed);
        event_tag_feed.setTypeface(Utils.typeface);
        RadioButton event_tag_liquid = (RadioButton) tagGroup.findViewById(R.id.event_tag_liquid);
        event_tag_liquid.setTypeface(Utils.typeface);
        RadioButton event_tag_napkin = (RadioButton) tagGroup.findViewById(R.id.event_tag_napkin);
        event_tag_napkin.setTypeface(Utils.typeface);
        RadioButton event_tag_sleep = (RadioButton) tagGroup.findViewById(R.id.event_tag_sleep);
        event_tag_sleep.setTypeface(Utils.typeface);


        switch (eventType) {
            case EVENT_VOICE: {
                videoLayout.setVisibility(View.GONE);
                photoLayout.setVisibility(View.GONE);
                recordTimeView = (TextView) findViewById(R.id.event_voice_time);
                recordTimeView.setTypeface(Utils.typeface);
                UpdateTime();
                miNumber = 0;
                maxNumber = 0;
                
                final RelativeLayout voicePlayerLayout = (RelativeLayout) findViewById(R.id.event_voice_player_layout);
                voicePlayerLayout.setVisibility(View.GONE);
                final RelativeLayout voiceRecordLayout = (RelativeLayout) findViewById(R.id.event_voice_record_layout);
                
                final DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) findViewById(R.id.event_voice_player__button);
                
                voiceSeekBar = (SeekBar) findViewById(R.id.event_voice_player_seekbar);
                
                record = (ImageButton) findViewById(R.id.event_voice_record);
                record.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        voiceClickEvent();
                    }
                });
                reRecord = (ImageView) findViewById(R.id.event_voice_rerecord);
                reRecord.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						recordTime = 0;
						UpdateTime();
						
						hideVoiceOption();
						
						if(mRecorder != null)
						{
							mRecorder.release();
						}
						mRecorder = new MediaRecorder();
//						mRecorder.setMaxDuration(VOICE_RECORD_MAX_LENGTH);
		                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
		                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
		                File fileName = getOutputMediaFile(MEDIA_TYPE_VOICE, 0);
		                mRecorder.setOutputFile(fileName.getPath());
		                try {
		                    mRecorder.prepare();
		                } catch (IOException e) {
		                    Log.e("DBG", "prepare() failed");
		                }
		                mRecorder.start();
		                RotateAnimation rotateAnimation = new RotateAnimation(0f, 360f,
		                        Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
		                rotateAnimation.setDuration(1000);
		                rotateAnimation.setRepeatCount(Animation.INFINITE);
		                record.setAnimation(rotateAnimation);

		                handler = new Handler();
		                runnable = new Runnable() {
		                    public void run () {
		                        recordTime++;
		                        UpdateTime();
		                        handler.postDelayed(this,1000);
		                    }
		                };
		                handler.removeCallbacks(runnable);
		                handler.postDelayed(runnable,1000);

		                miVoiceState = VOICE_RECORD;
		                miNumber = 1;
		                maxNumber = 1;
		                recordButton.setImageResource(R.drawable.event_voice_record_stop_pic);
					}
                	
                });
                useVoice = (ImageView) findViewById(R.id.event_voice_use);
                useVoice.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						hideVoiceOption();
						
						voicePlayerLayout.setVisibility(View.VISIBLE);
						voiceRecordLayout.setVisibility(View.GONE);
						
						voiceButton.setVisibility(View.VISIBLE);
						voiceButton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								final DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) view;
								if (!voiceButton.isChecked()) {
									voiceButton.mediaPlayer = new CustomMediaPlayer();
									try {
										voiceButton.mediaPlayer
												.setDataSource(getOutputMediaFile(MEDIA_TYPE_VOICE, 0).getAbsolutePath());
										voiceButton.mediaPlayer.prepare();
										voiceSeekBar.setMax(voiceButton.mediaPlayer.getDuration());
										final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
//										
										recordTime = 0;
										UpdateTime();
										handler = new Handler();
						                runnable = new Runnable() {
						                	int count = 10;
						                    public void run () {
						                    	count--;
						                    	if(count == 0)
						                    	{
						                    		recordTime++;
						                    		UpdateTime();
						                    		count = 10;
						                        }
						                        voiceSeekBar.setProgress(voiceMediaPlayer.getCurrentPosition());
						                        handler.postDelayed(this,100);
						                    }
						                };
						                handler.removeCallbacks(runnable);
						                handler.postDelayed(runnable,100);
							            
										voiceButton.mediaPlayer.start();
										voiceButton.mediaPlayer
												.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
													@Override
													public void onCompletion(
															MediaPlayer mediaPlayer) {
														
														handler.removeCallbacks(runnable);
														recordTime = 0;
														UpdateTime();
														voiceSeekBar.setProgress(0);
														voiceButton.setChecked(true);
														
														mediaPlayer.release();
														mediaPlayer = null;
													}
												});
									} catch (IOException e) {
										e.printStackTrace();
										Log.e("DBG", "播放失败");
									}
									voiceButton.setChecked(false);
								} else {
									
									handler.removeCallbacks(runnable);
									voiceSeekBar.setProgress(0);
									recordTime = 0;
									UpdateTime();
									
									if (voiceButton.mediaPlayer != null) {
										voiceButton.mediaPlayer.release();
									}
									voiceButton.mediaPlayer = null;
									voiceButton.setChecked(true);
								}
							}
						});
					
					}
                	
                });
                recordButton = (ImageButton) findViewById(R.id.event_voice_record_button);
                recordButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        voiceClickEvent();
                    }
                });
                break;
            }
            case EVENT_NOTE: {
                videoLayout.setVisibility(View.GONE);
                photoLayout.setVisibility(View.GONE);
                voiceLayout.setVisibility(View.GONE);
                break;
            }
            case EVENT_PHOTO: {
                float density = getResources().getDisplayMetrics().density;
                int finalWeight = (int) (weight * density);
                int finalHeight = (int) (height * density);
                int finalMarginTop = (int) (marginTop * density);

                miNumber = EventActivity.this.getIntent().getIntExtra("miNumber", 1);
                maxNumber = EventActivity.this.getIntent().getIntExtra("miNumber", 1);
                
                imageResourceList = new ArrayList<String>();
               
            	for(int i=0; i<miNumber; i++)
            	{
            		//addedThings
            		File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
            		imageResourceList.add(fileName.getPath());
            	}
                
            	updateImage();
                
                videoLayout.setVisibility(View.GONE);
                voiceLayout.setVisibility(View.GONE);
                break;
            }
            case EVENT_VIDEO: {
                photoLayout.setVisibility(View.GONE);
                voiceLayout.setVisibility(View.GONE);
                videoView = (VideoView) findViewById(R.id.event_video_id);
                videoView.setVideoPath(intent.getData().getPath());
                videoView.seekTo(100);
                videoView.setMediaController(new MediaController(this));
                videoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        videoView.setClickable(false);
                        videoView.start();
                    }
                });
                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        videoView.setClickable(true);
                    }
                });
                break;
            }
        }

        locationListener = new LocationListener(){
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                // TODO Auto-generatedmethod stub
            }

            @Override
            public void onProviderDisabled(String provider) {
                // TODO Auto-generatedmethod stub
            }

            @Override
            public void onLocationChanged(Location location) {
                return;
            }
        };

        positionText = (TextView) findViewById(R.id.event_postion_text);
        positionText.setTypeface(Utils.typeface);
        positionText.setText(POSITION_TEXT);
        positionText.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				positionText.requestFocus();
			}
        	
        });
        positionButton = (ImageButton) findViewById(R.id.event_position_button);
        positionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	
            	if(!positionText.getText().toString().equals(POSITION_TEXT))
            	{
            		positionText.setText(POSITION_TEXT);
            	}
            	else
            	{
            		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        toast.cancel();
                        toast = Toast.makeText(getApplicationContext(), "请开启GPS",
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, OPEN_GPS); //此为设置完成后返回到获取界面
                        return ;
                    }

                    // 查找到服务信息
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_FINE); // 高精度
                    criteria.setAltitudeRequired(false);
                    criteria.setBearingRequired(false);
                    criteria.setCostAllowed(true);
                    criteria.setPowerRequirement(Criteria.POWER_LOW); // 低功耗

                    String provider = locationManager.getBestProvider(criteria, true); // 获取GPS信息
                    Location location = locationManager.getLastKnownLocation(provider); // 通过GPS获取位置
                    updateToNewLocation(location);
                    // 设置监听器，自动更新的最小时间为间隔N秒(1秒为1*1000，这样写主要为了方便)或最小位移变化超过N米
                    locationManager.requestLocationUpdates(provider, 100 * 1000, 500,
                            locationListener);

                    positionButton.setClickable(false);
            	}
            	
            }
        });

        restText = (TextView) findViewById(R.id.event_rest_text);
//        restText.setTypeface(Utils.typeface);
        restText.setVisibility(View.GONE);
        
        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				// TODO Auto-generated method stub
            	if( eventType==EVENT_NOTE && (noteEditText.getText().toString()).equals("") )
            	{
            		Intent intent = new Intent();
                    setResult(999, intent);
                    finish();
                    
                    return;
            	}
            	
				AlertDialog.Builder builder = new Builder(EventActivity.this);

				builder.setMessage("退出此次编辑？");
				builder.setTitle("提示");

				builder.setPositiveButton("退出", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent intent = new Intent();
		                setResult(999, intent);
		                finish();

					}
				});

				builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});

				builder.create().show();
			
            }
        });
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setImageResource(R.drawable.baby_done);
        title_right.setVisibility(View.VISIBLE);
        title_right.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String noteText = noteEditText.getText().toString();
				if(noteText.equals(""))
				{
					InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

					noteEditText.requestFocus();
					inputMethodManager.showSoftInput(noteEditText,InputMethodManager.SHOW_FORCED);
					
					toast = Toast.makeText(EventActivity.this, "亲，还需要输入文字，才能发布噢~", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, (int)(-20*Utils.density));
					toast.show();
					
					return;
				}
				else if(noteText.length() > MAX_TEXT_TO_INPUT)
				{
					Toast.makeText(EventActivity.this, "内容太长了哟~", Toast.LENGTH_SHORT).show();
					return;
				}
				
				InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(noteEditText.getWindowToken(), 0);
				
				switch(eventType)
				{
				case EVENT_VOICE:
				{
					
					builder = new AlertDialog.Builder(EventActivity.this);
	                builder.setMessage("上传中");
	                showBuilder = true;
	                dialog = builder.show();
	                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
	                File fileUri = getOutputMediaFile(MEDIA_TYPE_VOICE, 0);
	                param.add(new BasicNameValuePair("filename", fileUri.getAbsolutePath()));
	                param.add(new BasicNameValuePair("filetype", "image"));
	                NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_UPLOAD_FILE,
	                        "/file", param, null) {
	                    @Override
	                    public void handleRsp(Message msg) {
	                        showBuilder = false;
	                        dialog.dismiss();
	                        String url = "";
	
	                        Bundle bundle = msg.getData();
	                        final int code = bundle.getInt("code");
	                        if (code == 200) {
	
	                            String data = bundle.getString("data");
	                            try {
	                                JSONArray array = new JSONArray(data);
	                                url = array.getJSONObject(0).getString("url");
	                            } catch (Exception e) {
	                                e.printStackTrace();
	                            }
	
	                            urlList.add(url);
	                            
	                            try {

	                                JSONObject feeds = new JSONObject();
	                                feeds.put("baby", babyId);
	                                feeds.put("text", noteEditText.getText().toString());
	                                if (!positionText.getText().toString().equals(POSITION_TEXT)) {
	                                    feeds.put("location", positionText.getText());
	                                }
	                                int type = 0;
	                                
	                                type = 1 << 3;
	                                   
	                                feeds.put("type", type);
	                                boolean eventMode;
	                                int modeTag = (Integer)modeImage.getTag();
	                                eventMode = (modeTag==1)?true:false;
	                                feeds.put("private", eventMode);
	                                int tag = 0;
	                                int radioButtonId = tagGroup.getCheckedRadioButtonId();
	                                switch (radioButtonId) {
	                                    case R.id.event_tag_milk:
	                                        tag = 1;
	                                        break;
	                                    case R.id.event_tag_feed:
	                                        tag = 1 << 1;
	                                        break;
	                                    case R.id.event_tag_liquid:
	                                        tag = 1 << 2;
	                                        break;
	                                    case R.id.event_tag_napkin:
	                                        tag = 1 << 3;
	                                        break;
	                                    case R.id.event_tag_sleep:
	                                        tag = 1 << 4;
	                                        break;
	                                }
	                                feeds.put("tag", tag);
	                                if (urlList.size() > 0) {
	                                    JSONArray urlArray = new JSONArray();
	                                    for(int i = 0; i < urlList.size(); i++) {
	                                        urlArray.put(i, urlList.get(i));
	                                    }
	                                    feeds.put("urls", urlArray);
	                                }

	                                NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_POST,
	                                        "/feeds", new LinkedList<BasicNameValuePair>(), feeds) {
	                                    @Override
	                                    public void handleRsp(Message msg) {
	                                        dialog.dismiss();
	                                        showBuilder = false;
	                                        Bundle bundle = msg.getData();
	                                        int code = bundle.getInt("code");
	                                        Log.v("Kite", "post feed code is " + code);
	                                        if (code == 200) {
	                                            Intent intent = new Intent();
	                                            setResult(RESULT_OK, intent);
	                                            EventActivity.this.finish();
	                                            toast.cancel();
	                                            toast = Toast.makeText(getApplicationContext(), "保存成功",
	                                                    Toast.LENGTH_SHORT);
	                                            toast.setGravity(Gravity.CENTER, 0, 0);
	                                            toast.show();
	                                        } else {
	                                            toast.cancel();
	                                            Log.v("Kite", "feed error: " + bundle);
	                                            toast = Toast.makeText(getApplicationContext(), "保存失败",
	                                                    Toast.LENGTH_SHORT);
	                                            toast.setGravity(Gravity.CENTER, 0, 0);
	                                            toast.show();
	                                        }
	                                    }
	                                };
	                                handler.start();
	                            } catch (Exception e) {
	                                e.printStackTrace();
	                            }
	
	                        } else {
	                            toast.cancel();
	                            toast = Toast.makeText(getApplicationContext(), "上传失败",
	                                    Toast.LENGTH_SHORT);
	                            toast.setGravity(Gravity.CENTER, 0, 0);
	                            toast.show();
	                            miVoiceState = VOICE_INIT;
	                        }
	                    }
	                };
	                handler.start();
					
					break;
				}
					
				case EVENT_VIDEO:
				{
					upload_progress_dialog.show();
//					builder = new AlertDialog.Builder(EventActivity.this);
//	                builder.setMessage("上传中");
//	                dialog = builder.show();
	                showBuilder = true;
	                // Video captured and saved to fileUri specified in the Intent
	                Log.v("DBG", fileUri.getPath().toString());
	                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
	                param.add(new BasicNameValuePair("filename", getOutputMediaFile(MEDIA_TYPE_VIDEO, 0).getAbsolutePath()));
	                param.add(new BasicNameValuePair("filetype", "image"));
	                NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_UPLOAD_FILE,
	                        "/file", param, null) {
	                    @Override
	                    public void handleRsp(Message msg) {
	                    	upload_progress_dialog.dismiss();
//	                        dialog.dismiss();
	                        showBuilder = false;
	                        String url = "";

	                        Bundle bundle = msg.getData();
	                        final int code = bundle.getInt("code");
	                        if (code == 200) {

	                            String data = bundle.getString("data");
	                            try {
	                                JSONArray array = new JSONArray(data);
	                                url = array.getJSONObject(0).getString("url");
	                                urlList.add(url);
	                                
	                                try {
	                                  JSONObject feeds = new JSONObject();
	                                  feeds.put("baby", babyId);
	                                  feeds.put("text", noteEditText.getText().toString());
	                                  if (!positionText.getText().toString().equals(POSITION_TEXT)) {
	                                      feeds.put("location", positionText.getText());
	                                  }
	                                  int type = 0;
	                                  type = 1 << 2;
	                                  
	                                  feeds.put("type", type);
	                                  boolean eventMode;
	                                  int modeTag = (Integer)modeImage.getTag();
	                                  eventMode = (modeTag==1)?true:false;
	                                  feeds.put("private", eventMode);
	                                  int tag = 0;
	                                  int radioButtonId = tagGroup.getCheckedRadioButtonId();
	                                  switch (radioButtonId) {
	                                      case R.id.event_tag_milk:
	                                          tag = 1;
	                                          break;
	                                      case R.id.event_tag_feed:
	                                          tag = 1 << 1;
	                                          break;
	                                      case R.id.event_tag_liquid:
	                                          tag = 1 << 2;
	                                          break;
	                                      case R.id.event_tag_napkin:
	                                          tag = 1 << 3;
	                                          break;
	                                      case R.id.event_tag_sleep:
	                                          tag = 1 << 4;
	                                          break;
	                                  }
	                                  feeds.put("tag", tag);
	                                  if (urlList.size() > 0) {
	                                      JSONArray urlArray = new JSONArray();
	                                      for(int i = 0; i < urlList.size(); i++) {
	                                          urlArray.put(i, urlList.get(i));
	                                      }
	                                      feeds.put("urls", urlArray);
	                                  }
	              
	                                builder = new AlertDialog.Builder(EventActivity.this);
	              	                builder.setMessage("处理中");
	              	                dialog = builder.show();
	              	                showBuilder = true;
	                                  NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_POST,
	                                          "/feeds", new LinkedList<BasicNameValuePair>(), feeds) {
	                                      @Override
	                                      public void handleRsp(Message msg) {
	                                          dialog.dismiss();
	                                          showBuilder = false;
	                                          Bundle bundle = msg.getData();
	                                          int code = bundle.getInt("code");
	                                          Log.v("Kite", "post feed code is " + code);
												if (code == 200) {
													Intent intent = new Intent();
													setResult(RESULT_OK, intent);
													EventActivity.this.finish();
													toast.cancel();
													toast = Toast
															.makeText(
																	getApplicationContext(),
																	"保存成功",
																	Toast.LENGTH_SHORT);
													toast.setGravity(Gravity.CENTER, 0, 0);
													toast.show();
												} else {
	                                              toast.cancel();
	                                              Log.v("Kite", "feed error: " + bundle);
	                                              toast = Toast.makeText(getApplicationContext(), "保存失败",
	                                                      Toast.LENGTH_SHORT);
	                                              toast.setGravity(Gravity.CENTER, 0, 0);
	                                              toast.show();
	                                          }
	                                      }
	                                  };
	                                  handler.start();
	                              } catch (Exception e) {
	                                  e.printStackTrace();
	                              }
	                                
	                            } catch (Exception e) {
	                                e.printStackTrace();
	                            }

	                        } else {
	                        	
	                        	Log.v("Kite", "upload fail because: " + bundle.getString("data"));
	                        	
	                            toast.cancel();
	                            toast = Toast.makeText(getApplicationContext(), "上传失败",
	                                    Toast.LENGTH_SHORT);
	                            toast.setGravity(Gravity.CENTER, 0, 0);
	                            toast.show();
	                        }
	                    }
	                };
	                handler.setUploadListener(new WriteListener(){

						@Override
						public void registerWrite(long amountOfBytesWritten, long totalByteOfFile) {
							// TODO Auto-generated method stub
							Bundle bundle = new Bundle();
							bundle.putInt("result", RESULT_OK);
							bundle.putLong("amountOfBytesWritten", amountOfBytesWritten);
							bundle.putLong("totalByteOfFile", totalByteOfFile);
							Message msg = new Message();
							msg.setData(bundle);
							
							uploadProgressHandler.sendMessage(msg);
						}
	                	
	                });
	                handler.start();
					
					break;
				}
					
				case EVENT_PHOTO:
				{
//					builder = new AlertDialog.Builder(EventActivity.this);
//	                builder.setMessage("上传中");
//	                dialog = builder.show();
//	                showBuilder = true;
//	                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//	                param.add(new BasicNameValuePair("filename", filename));
//	                param.add(new BasicNameValuePair("filetype", "image"));
//	                NetHandler handler = new NetHandler(NetHandler.METHOD_UPLOAD_FILE,
//	                        "/file", param, null) {
//	                    @Override
//	                    public void handleRsp(Message msg) {
//	                        dialog.dismiss();
//	                        showBuilder = false;
//	                        String url = "";
//
//	                        Bundle bundle = msg.getData();
//	                        final int code = bundle.getInt("code");
//	                        if (code == 200) {
//
//	                            String data = bundle.getString("data");
//	                            try {
//	                                JSONArray array = new JSONArray(data);
//	                                url = array.getJSONObject(0).getString("url");
//	                            } catch (Exception e) {
//	                                e.printStackTrace();
//	                            }
//
//	                            Intent intent = new Intent();
//	                            intent.putExtra("EventType", EventActivity.EVENT_PHOTO);
//	                            intent.setData(fileUri);
//	                            intent.putExtra("url", url);
//	                            intent.putExtra("babyId", babyId);
//	                            intent.setClass(MainActivity.this, EventActivity.class);
//	                            startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);
//
//
//	                        } else {
//	                            toast.cancel();
//	                            toast = Toast.makeText(getApplicationContext(), "上传失败",
//	                                    Toast.LENGTH_SHORT);
//	                            toast.show();
//	                        }
//	                    }
//	                };
//	                handler.start();
					
					uploadPhotos(0);
					
					break;
				}
//					
//				case EVENT_NOTE:
//					break;
					
				default:
					try {
	                    builder = new AlertDialog.Builder(EventActivity.this);
	                    builder.setMessage("上传中");
	                    showBuilder = true;
	                    dialog = builder.show();
	
	                    JSONObject feeds = new JSONObject();
	                    feeds.put("baby", babyId);
	                    feeds.put("text", noteEditText.getText().toString());
	                    if (!positionText.getText().toString().equals(POSITION_TEXT)) {
	                        feeds.put("location", positionText.getText());
	                    }
	                    int type = 0;
	                    switch(eventType) {
	                        case EVENT_VOICE:
	                            type = 1 << 3;
	                            break;
	                        case EVENT_NOTE:
	                            type = 1 << 4;
	                            break;
	                        case EVENT_PHOTO:
	                            type = 1 << 1;
	                            break;
	                        case EVENT_VIDEO:
	                            type = 1 << 2;
	                            break;
	                    }
	                    feeds.put("type", type);
	                    boolean eventMode;
	                    int modeTag = (Integer)modeImage.getTag();
	                    eventMode = (modeTag==1)?true:false;
	                    feeds.put("private", eventMode);
	                    int tag = 0;
	                    int radioButtonId = tagGroup.getCheckedRadioButtonId();
	                    switch (radioButtonId) {
	                        case R.id.event_tag_milk:
	                            tag = 1;
	                            break;
	                        case R.id.event_tag_feed:
	                            tag = 1 << 1;
	                            break;
	                        case R.id.event_tag_liquid:
	                            tag = 1 << 2;
	                            break;
	                        case R.id.event_tag_napkin:
	                            tag = 1 << 3;
	                            break;
	                        case R.id.event_tag_sleep:
	                            tag = 1 << 4;
	                            break;
	                    }
	                    feeds.put("tag", tag);
	                    if (urlList.size() > 0) {
	                        JSONArray urlArray = new JSONArray();
	                        for(int i = 0; i < urlList.size(); i++) {
	                            urlArray.put(i, urlList.get(i));
	                        }
	                        feeds.put("urls", urlArray);
	                    }
	
	                    NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_POST,
	                            "/feeds", new LinkedList<BasicNameValuePair>(), feeds) {
	                        @Override
	                        public void handleRsp(Message msg) {
	                            dialog.dismiss();
	                            showBuilder = false;
	                            Bundle bundle = msg.getData();
	                            int code = bundle.getInt("code");
	                            Log.v("Kite", "post feed code is " + code);
	                            if (code == 200) {
	                                Intent intent = new Intent();
	                                setResult(RESULT_OK, intent);
	                                EventActivity.this.finish();
	                                toast.cancel();
	                                toast = Toast.makeText(getApplicationContext(), "保存成功",
	                                        Toast.LENGTH_SHORT);
	                                toast.setGravity(Gravity.CENTER, 0, 0);
	                                toast.show();
	                            } else {
	                                toast.cancel();
	                                Log.v("Kite", "feed error: " + bundle);
	                                toast = Toast.makeText(getApplicationContext(), "保存失败",
	                                        Toast.LENGTH_SHORT);
	                                toast.setGravity(Gravity.CENTER, 0, 0);
	                                toast.show();
	                            }
	                        }
	                    };
	                    handler.start();
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
				}
            
			}
        	
        });
        
        SetBabyPhoto();
        
        showGuide();
    }
    
    private void showGuide()
    {
    	boolean eventGuideShown = StoreHandler.getGuide(StoreHandler.eventGuide);
		if(!eventGuideShown)
		{
			event_guide.setVisibility(View.VISIBLE);
			event_guide.setImageResource(R.drawable.event_guide_1);
			event_guide.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					event_guide.setVisibility(View.GONE);

					StoreHandler.setGuide(StoreHandler.eventGuide, true);
				}
				
			});
		}
    }

    private void updateToNewLocation(Location location) {

        if (location != null) {
        	positionText.setText("定位中...");
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            // positionText.setText("纬度：" + latitude + " 经度：" + longitude);
            // positionText.setVisibility(View.VISIBLE);
            Log.v("DBG", "纬度：" + latitude + " 经度：" + longitude);
            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
            param.add(new BasicNameValuePair("ak", "0E1f6db56b29795a5f5939cd83a35a54"));
            param.add(new BasicNameValuePair("output", "json"));
            param.add(new BasicNameValuePair("location", latitude + "," + longitude));
            NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_GET_LOCATION, "http://api.map.baidu.com/geocoder/v2/",
                    param, null) {
                @Override
                public void handleRsp(Message msg) {
                    Bundle bundle = msg.getData();
                    int code = bundle.getInt("code");
                    if (code == 200) {
                        try {
                            JSONObject object = new JSONObject(bundle.getString("data"));
                            if (object.getInt("status") == 0) {
                                positionText.setText(object.getJSONObject("result").getString("formatted_address"));
                                positionText.setVisibility(View.VISIBLE);
                                positionText.requestFocus();
                                
                                positionButton.setClickable(true);
                            } else {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                positionButton.setClickable(true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            positionButton.setClickable(true);
                        }
                    } else {
                        toast.cancel();
                        toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        positionButton.setClickable(true);
                    }
                }
            };
            handler.start();
        } else {
            toast.cancel();
            toast = Toast.makeText(getApplicationContext(), "无法获取地理信息",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            positionButton.setClickable(true);
        }

    }

    @Override
    protected void onDestroy() {
    	
    	InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(noteEditText.getWindowToken(), 0);
    	
        switch (eventType) {
            case EVENT_VOICE: {
                for (int i = 0; i < miNumber; i++) {
                    File file = getOutputMediaFile(MEDIA_TYPE_VOICE, i);
                    file.delete();
                    
                    if(mRecorder != null)
                    {
                    	mRecorder.release();
                    }
                }
                break;
            }
            case EVENT_PHOTO: {
                for (int i = 0; i < miNumber; i++) {
                    File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
                    file.delete();
                }
                break;
            }
            case EVENT_VIDEO: {
                File file = getOutputMediaFile(MEDIA_TYPE_VIDEO, 0);
                file.delete();
                break;
            }
        }
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
        super.onDestroy();
    }

    private void uploadPhotos(final int photoIndex)
    {
    	if(photoIndex == miNumber)
    	{
    		try {
                JSONObject feeds = new JSONObject();
                feeds.put("baby", babyId);
                feeds.put("text", noteEditText.getText().toString());
                if (!positionText.getText().toString().equals(POSITION_TEXT)) {
                    feeds.put("location", positionText.getText());
                }
                int type = 0;
                type = 1 << 1;
                   
                feeds.put("type", type);
                boolean eventMode;
                int modeTag = (Integer)modeImage.getTag();
                eventMode = (modeTag==1)?true:false;
                feeds.put("private", eventMode);
                int tag = 0;
                int radioButtonId = tagGroup.getCheckedRadioButtonId();
                switch (radioButtonId) {
                    case R.id.event_tag_milk:
                        tag = 1;
                        break;
                    case R.id.event_tag_feed:
                        tag = 1 << 1;
                        break;
                    case R.id.event_tag_liquid:
                        tag = 1 << 2;
                        break;
                    case R.id.event_tag_napkin:
                        tag = 1 << 3;
                        break;
                    case R.id.event_tag_sleep:
                        tag = 1 << 4;
                        break;
                }
                feeds.put("tag", tag);
                if (urlList.size() > 0) {
                    JSONArray urlArray = new JSONArray();
                    for(int i = 0; i < urlList.size(); i++) {
                        urlArray.put(i, urlList.get(i));
                    }
                    feeds.put("urls", urlArray);
                }

                NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_POST,
                        "/feeds", new LinkedList<BasicNameValuePair>(), feeds) {
                    @Override
                    public void handleRsp(Message msg) {
                        dialog.dismiss();
                        showBuilder = false;
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        Log.v("Kite", "post feed code is " + code);
                        if (code == 200) {
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            EventActivity.this.finish();
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "保存成功",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else {
                            toast.cancel();
                            Log.v("Kite", "feed error: " + bundle);
                            toast = Toast.makeText(getApplicationContext(), "保存失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }
                };
                handler.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        
    		return;
    	}
    	if(photoIndex == 0)
    	{
    		builder = new AlertDialog.Builder(EventActivity.this);
            builder.setMessage("上传中");
            dialog = builder.show();
            showBuilder = true;
    	}
    	
//        File fileUri = getOutputMediaFile(MEDIA_TYPE_IMAGE, photoIndex);
        List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
        param.add(new BasicNameValuePair("filename", imageResourceList.get(photoIndex)));
        param.add(new BasicNameValuePair("filetype", "image"));
        NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_UPLOAD_FILE,
                "/file", param, null) {
            @Override
            public void handleRsp(Message msg) {
                dialog.dismiss();
                showBuilder = false;
                String url = "";

                Bundle bundle = msg.getData();
                int code = bundle.getInt("code");
                if (code == 200) {

                    String data = bundle.getString("data");
                    try {
                        JSONArray array = new JSONArray(data);
                        url = array.getJSONObject(0).getString("url");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    urlList.add(url);

                    int nextIndex = photoIndex+1;
                    uploadPhotos(nextIndex);

                } else {
                	Log.v("Kite", "upload file error: " + bundle.getString("data"));
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "上传失败",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        };
        handler.start();
    }
    
    private void SetBabyPhoto() {
        try {
        	StoreHandler.getAndSetBabies();
            JSONArray babyArray = new JSONArray(MemoryHandler.getInstance().getKey("baby"));
            
            otherBabyView = new ArrayList<TextView>();
            if(babyArray.length() == 2)
            {
            	otherBabyView.add(otherBaby1);
            }
            else if(babyArray.length() == 3)
            {
            	otherBabyView.add(otherBaby1);
            	otherBabyView.add(otherBaby2);
            }
            
            otherBabyName = new ArrayList<String>();
            otherBabyIndex = new ArrayList<Integer>();
            for(int i=0; i<babyArray.length(); i++)
            {
            	JSONObject baby = babyArray.getJSONObject(i);
            	if(i!=currentBaby)
            	{
            		Log.v("Kite", "baby " + i + " is " + baby);
            		Log.v("Kite", "baby " + i + " name is " + baby.getString("name"));
            		otherBabyName.add(baby.getString("name"));
            		otherBabyIndex.add(i);
            	}
            }
            
            for(int i=0; i<otherBabyView.size(); i++)
            {
            	otherBabyView.get(i).setText(otherBabyName.get(i));
            	otherBabyView.get(i).setTag(otherBabyIndex.get(i));
            	otherBabyView.get(i).setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						currentBaby = (Integer)view.getTag();
						try {
							String baby = MemoryHandler.getInstance().getKey("baby");
				            JSONArray babyArray = new JSONArray(baby);
				            babyId = babyArray.getJSONObject(currentBaby).getString("id");
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
				        Log.v("DBG", "babyId " + babyId);
						hideOtherBaby();
						SetBabyPhoto();
					}
            		
            	});
            }
            
            babyAvatar.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if(!otherBabyShown)
					{
						showOtherBaby();
					}
					else
					{
						hideOtherBaby();
					}
				}
            	
            });
            
            JSONObject baby = babyArray.getJSONObject(currentBaby);
            Log.v("DBG", baby.toString() + " " + currentBaby);
            if (baby.has("avatar") && !baby.getString("avatar").isEmpty()) {
                NetHandler handler = new NetHandler(EventActivity.this, NetHandler.METHOD_DOWNLOAD_FILE, baby.getString("avatar"), null, null) {
                    @Override
                    public void handleRsp(Message msg) {
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {
                            String data = bundle.getString("data");
                            try {
                                int height = 66;
                                int width = 66;
//                                Log.v("DBG", String.format("%d %d", height, width));
                                BitmapFactory.Options options = new BitmapFactory.Options();
//                                options.inJustDecodeBounds = true;
//
//                                BitmapFactory.decodeByteArray(data.getBytes("ISO-8859-1"), 0, data.getBytes("ISO-8859-1").length, options);
//                                int heightRatio = (int)Math.floor(options.outHeight / (float) height);
//                                int widthRatio = (int)Math.floor(options.outWidth / (float) width);
//
//                                Log.v("HEIGHRATIO", ""+heightRatio);
//                                Log.v("WIDTHRATIO", ""+widthRatio);
//
//                                if (heightRatio > 1 && widthRatio > 1)
//                                {
//                                    options.inSampleSize =  heightRatio > widthRatio ? heightRatio:widthRatio;
//                                }
                                options.inJustDecodeBounds = false;
                                options.inPreferredConfig = Bitmap.Config.RGB_565; 
                                options.inPurgeable = true;
                                options.inInputShareable = true;
                                Bitmap bit = BitmapFactory.decodeByteArray(data.getBytes("ISO-8859-1"), 0, data.getBytes("ISO-8859-1").length, options);
                                Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(output);
//                                if(!output.isRecycled())
//                                {
//                                	output.recycle()
//                                }

                                final int color = 0xff424242;
                                final Paint paint = new Paint();
                                final Rect rect = new Rect(0, 0, width, height);
                                final RectF rectF = new RectF(rect);
                                final float roundPx = (float) width / 2.0f;

                                paint.setAntiAlias(true);
                                canvas.drawARGB(0, 0, 0, 0);

                                paint.setColor(color);
                                canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
                                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                                canvas.drawBitmap(bit, null, rect, paint);

                                // options.inSampleSize = 4;
                                babyAvatar.setImageBitmap(output);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };
                handler.start();
            }
            else
            {
            	babyAvatar.setImageResource(R.drawable.tree_baby_pic);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Kite", "error is " + e);
        }

    }
    
    private void showOtherBaby()
    {
    	otherBabyShown = true;
    	
    	if(otherBabyView.size()!=0)
    	{
    		TextView babyView1 = otherBabyView.get(0);
    		
    		int width = babyView1.getWidth();
            int height = babyView1.getHeight();
            babyView1.setTranslationX((int)(1.5*width));
            babyView1.setTranslationY(0);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.5f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            if(otherBabyView.size() == 2)
            {
            	myAnimation.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationRepeat(Animation arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animation arg0) {
						// TODO Auto-generated method stub
						TextView babyView2 = otherBabyView.get(1);
			    		
			    		int width = babyView2.getWidth();
			            int height = babyView2.getHeight();
			            babyView2.setTranslationX((int)(2.6*width));
			            babyView2.setTranslationY(0);
			            
			            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -2.6f,
			                    Animation.RELATIVE_TO_SELF, 0.0f,
			                    Animation.RELATIVE_TO_SELF, 0.0f,
			                    Animation.RELATIVE_TO_SELF, 0.0f);
			            myAnimation.setDuration(300);
			            myAnimation.setFillAfter(false);
			            
			            babyView2.startAnimation(myAnimation);
					}
				});
            }
            
            babyView1.startAnimation(myAnimation);
    	}
    	
    }
    
    private void hideOtherBaby()
    {
    	otherBabyShown = false;
    	
    	if(otherBabyView.size()!=0)
    	{
    		TextView babyView1 = otherBabyView.get(0);
    		
    		int width = babyView1.getWidth();
            int height = babyView1.getHeight();
            babyView1.setTranslationX(0);
            babyView1.setTranslationY(0);
            
            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.5f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            myAnimation.setDuration(300);
            myAnimation.setFillAfter(false);
            
            if(otherBabyView.size() == 2)
            {
            	myAnimation.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationRepeat(Animation arg0) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animation arg0) {
						// TODO Auto-generated method stub
						TextView babyView2 = otherBabyView.get(1);
			    		
			    		int width = babyView2.getWidth();
			            int height = babyView2.getHeight();
			            babyView2.setTranslationX(0);
			            babyView2.setTranslationY(0);
			            
			            TranslateAnimation myAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 2.6f,
			                    Animation.RELATIVE_TO_SELF, 0.0f,
			                    Animation.RELATIVE_TO_SELF, 0.0f,
			                    Animation.RELATIVE_TO_SELF, 0.0f);
			            myAnimation.setDuration(300);
			            myAnimation.setFillAfter(false);
			            
			            babyView2.startAnimation(myAnimation);
					}
				});
            }
            
            babyView1.startAnimation(myAnimation);
    	}
    }
    
    private void updateImage()
    {
    	for(int i = 0; i < imageResourceList.size() / 3 + 1; i++) {
        	
        	if(i == 3)
        	{
        		break;
        	}
            LayoutInflater inflater = getLayoutInflater();
            RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.photo_layout, null);
            if (i * 3 <= imageResourceList.size()) {
                if (i * 3 < imageResourceList.size()) {
                    ImageView leftView = (ImageView) layout.findViewById(R.id.left_photo);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
//                    File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i * 3);
                    Bitmap bit = BitmapFactory.decodeFile(imageResourceList.get(i*3), options);
                    leftView.setImageBitmap(bit);
                    setImageViewToGalleryActivity(leftView, i * 3, imageResourceList);
                } else {
                    ImageView leftView = (ImageView) layout.findViewById(R.id.left_photo);
                    leftView.setImageResource(R.drawable.event_add_photo_pic);
                    leftView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final CharSequence[] items = { "相册", "拍照", "取消" };
                            builder = new AlertDialog.Builder(EventActivity.this);
                            builder.setTitle("选择图片");
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == 1) {
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, maxNumber++); // create a file to save the image
                                        miNumber++;
                                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); // set the image file name

                                        // start the image capture Intent
                                        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else if (i == 0) {
//                                        Intent getImage = new Intent(Intent.ACTION_GET_CONTENT);
//                                        getImage.addCategory(Intent.CATEGORY_OPENABLE);
//                                        getImage.setType("image/jpeg");
//                                        startActivityForResult(getImage, GET_IMAGE_ACTIVITY_REQUEST_CODE);
//                                    	Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                		startActivityForResult(intent,
//                                				GET_IMAGE_ACTIVITY_REQUEST_CODE);
                                    	Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
                                    	intent.putExtra("maxSelection", 9-miNumber);
                        				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            });
                            dialog = builder.show();
                        }
                    });
                }
            }
            if (i * 3 + 1 <= imageResourceList.size()) {
                if (i * 3 + 1< imageResourceList.size()) {
                    ImageView middleView = (ImageView) layout.findViewById(R.id.middle_photo);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
//                    File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i * 3 + 1);
                    Bitmap bit = BitmapFactory.decodeFile(imageResourceList.get(i*3+1), options);
                    middleView.setImageBitmap(bit);
                    setImageViewToGalleryActivity(middleView, i * 3 + 1, imageResourceList);
                } else {
                    ImageView middleView = (ImageView) layout.findViewById(R.id.middle_photo);
                    middleView.setImageResource(R.drawable.event_add_photo_pic);
                    middleView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final CharSequence[] items = { "相册", "拍照", "取消" };
                            builder = new AlertDialog.Builder(EventActivity.this);
                            builder.setTitle("选择图片");
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == 1) {
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, maxNumber++); // create a file to save the image
                                        miNumber++;
                                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); // set the image file name

                                        // start the image capture Intent
                                        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else if (i == 0) {
//                                        Intent getImage = new Intent(Intent.ACTION_GET_CONTENT);
//                                        getImage.addCategory(Intent.CATEGORY_OPENABLE);
//                                        getImage.setType("image/jpeg");
//                                        startActivityForResult(getImage, GET_IMAGE_ACTIVITY_REQUEST_CODE);
//                                    	Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                		startActivityForResult(intent,
//                                				GET_IMAGE_ACTIVITY_REQUEST_CODE);
                                    	Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
                                    	intent.putExtra("maxSelection", 9-miNumber);
                        				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            });
                            dialog = builder.show();
                        }
                    });
                }
            }
            if (i * 3 + 2 <= imageResourceList.size()) {
                if (i * 3 + 2 < imageResourceList.size()) {
                    ImageView rightView = (ImageView) layout.findViewById(R.id.right_photo);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
//                    File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i * 3 + 2);
                    Bitmap bit = BitmapFactory.decodeFile(imageResourceList.get(i*3+2), options);
                    rightView.setImageBitmap(bit);
                    setImageViewToGalleryActivity(rightView, i * 3 + 2, imageResourceList);
                } else {
                    ImageView rightView = (ImageView) layout.findViewById(R.id.right_photo);
                    rightView.setImageResource(R.drawable.event_add_photo_pic);
                    rightView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final CharSequence[] items = { "相册", "拍照", "取消" };
                            builder = new AlertDialog.Builder(EventActivity.this);
                            builder.setTitle("选择图片");
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == 1) {
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, maxNumber++); // create a file to save the image
                                        miNumber++;
                                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); // set the image file name

                                        // start the image capture Intent
                                        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else if (i == 0) {
//                                        Intent getImage = new Intent(Intent.ACTION_GET_CONTENT);
//                                        getImage.addCategory(Intent.CATEGORY_OPENABLE);
//                                        getImage.setType("image/jpeg");
//                                        startActivityForResult(getImage, GET_IMAGE_ACTIVITY_REQUEST_CODE);
//                                    	Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                		startActivityForResult(intent,
//                                				GET_IMAGE_ACTIVITY_REQUEST_CODE);
                                    	Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
                                    	intent.putExtra("maxSelection", 9-miNumber);
                        				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            });
                            dialog = builder.show();
                        }
                    });
                }
            }
            photoLayout.addView(layout);
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//              
//            	ArrayList<String> imageResourceList = new ArrayList<String>();
//            	for(int i=0; i<miNumber; i++)
//            	{
//            		//addedThings
//            		File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
//            		imageResourceList.add(fileName.getPath());
//            	}
            	
                File fileUri = getOutputMediaFile(MEDIA_TYPE_IMAGE, maxNumber - 1);
                Log.v("DBG", fileUri.getPath().toString());

                String filename = fileUri.getPath();
                imageResourceList.add(filename);
                Bitmap bitmap = null;
                try {
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    BitmapFactory.decodeFile(filename, opts);
//                    int srcWidth = opts.outWidth;
//                    int srcHeight = opts.outHeight;
                	int srcWidth = opts.outHeight;
                	int srcHeight = opts.outWidth;
                	
                    int desWidth = 0;
                    int desHeight = 0;
                    // 缩放比例
                    double ratio = 0.0;
                    if (srcWidth > srcHeight) {
                        ratio = srcWidth / 640;
                        desWidth = 640;
                        desHeight = (int) (srcHeight / ratio);
                    } else {
                        ratio = srcHeight / 640;
                        desHeight = 640;
                        desWidth = (int) (srcWidth / ratio);
                    }
                    // 设置输出宽度、高度
                    BitmapFactory.Options newOpts = new BitmapFactory.Options();
                    newOpts.inSampleSize = (int) (ratio) + 1;
                    newOpts.inJustDecodeBounds = false;
                    newOpts.outWidth = desWidth;
                    newOpts.outHeight = desHeight;
//                    bitmap = BitmapFactory.decodeFile(filename, newOpts);
                    bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
                    
                    FileOutputStream out = new FileOutputStream(filename);
                    if(bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out)){
                        out.flush();
                        out.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Kite
				photoLayout.removeAllViews();
				
				updateImage();
//				float density = getResources().getDisplayMetrics().density;
//				int finalWeight = (int) (weight * density);
//				int finalHeight = (int) (height * density);
//				int finalMarginTop = (int) (marginTop * density);
//				for (int i = 0; i < miNumber / 3 + 1; i++) {
//					
//					if(i == 3)
//                	{
//                		break;
//                	}
//					
//					LayoutInflater inflater = getLayoutInflater();
//					RelativeLayout layout = (RelativeLayout) inflater.inflate(
//							R.layout.photo_layout, null);
//					if (i * 3 <= miNumber) {
//						if (i * 3 < miNumber) {
//							ImageView leftView = (ImageView) layout
//									.findViewById(R.id.left_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							leftView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(leftView, i * 3, imageResourceList);
//						} else if(miNumber != 9){
////							ImageView leftView = (ImageView) layout
////									.findViewById(R.id.left_photo);
////							leftView.setImageResource(R.drawable.event_add_photo_pic);
////							leftView.setOnClickListener(new View.OnClickListener() {
////								@Override
////								public void onClick(View view) {
////									Intent intent = new Intent(
////											MediaStore.ACTION_IMAGE_CAPTURE);
////									File fileUri = getOutputMediaFile(
////											MEDIA_TYPE_IMAGE, miNumber++); // create
////																			// a
////																			// file
////																			// to
////																			// save
////																			// the
////																			// image
////
////									Log.v("DBG", "save " + fileUri.getPath());
////									intent.putExtra(MediaStore.EXTRA_OUTPUT,
////											Uri.fromFile(fileUri)); // set the
////																	// image
////																	// file name
////
////									// start the image capture Intent
////									startActivityForResult(intent,
////											CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
////								}
////							});
//
//							ImageView leftView = (ImageView) layout
//									.findViewById(R.id.left_photo);
//							leftView.setImageResource(R.drawable.event_add_photo_pic);
//							leftView.setOnClickListener(new View.OnClickListener() {
//								@Override
//								public void onClick(View view) {
//									final CharSequence[] items = { "相册", "拍照",
//											"取消" };
//									builder = new AlertDialog.Builder(
//											EventActivity.this);
//									builder.setTitle("选择图片");
//									builder.setItems(
//											items,
//											new DialogInterface.OnClickListener() {
//												@Override
//												public void onClick(
//														DialogInterface dialogInterface,
//														int i) {
//													if (i == 1) {
//														Intent intent = new Intent(
//																MediaStore.ACTION_IMAGE_CAPTURE);
//														File file = getOutputMediaFile(
//																MEDIA_TYPE_IMAGE,
//																maxNumber++); // create
//																				// a
//																				// file
//																				// to
//																				// save
//																				// the
//																				// image
//														miNumber++;
//														intent.putExtra(
//																MediaStore.EXTRA_OUTPUT,
//																Uri.fromFile(file)); // set
//																						// the
//																						// image
//																						// file
//																						// name
//
//														// start the image
//														// capture Intent
//														startActivityForResult(
//																intent,
//																CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//													} else if (i == 0) {
////														Intent getImage = new Intent(
////																Intent.ACTION_GET_CONTENT);
////														getImage.addCategory(Intent.CATEGORY_OPENABLE);
////														getImage.setType("image/jpeg");
////														startActivityForResult(
////																getImage,
////																GET_IMAGE_ACTIVITY_REQUEST_CODE);
////														Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////		                                        		startActivityForResult(intent,
////		                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//														Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//														intent.putExtra("maxSelection", 9-miNumber);
//		                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//													} else {
//														dialog.dismiss();
//													}
//												}
//											});
//									dialog = builder.show();
//								}
//							});
//						
//						}
//					}
//					if (i * 3 + 1 <= miNumber) {
//						if (i * 3 + 1 < miNumber) {
//							ImageView middleView = (ImageView) layout
//									.findViewById(R.id.middle_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3 + 1);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							middleView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(middleView, i * 3 + 1, imageResourceList);
//						} else if(miNumber != 9){
////							ImageView middleView = (ImageView) layout
////									.findViewById(R.id.middle_photo);
////							middleView
////									.setImageResource(R.drawable.event_add_photo_pic);
////							middleView
////									.setOnClickListener(new View.OnClickListener() {
////										@Override
////										public void onClick(View view) {
////											Intent intent = new Intent(
////													MediaStore.ACTION_IMAGE_CAPTURE);
////											File fileUri = getOutputMediaFile(
////													MEDIA_TYPE_IMAGE,
////													miNumber++); // create a
////																	// file to
////																	// save the
////																	// image
////
////											Log.v("DBG",
////													"save " + fileUri.getPath());
////											intent.putExtra(
////													MediaStore.EXTRA_OUTPUT,
////													Uri.fromFile(fileUri)); // set
////																			// the
////																			// image
////																			// file
////																			// name
////
////											// start the image capture Intent
////											startActivityForResult(intent,
////													CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
////										}
////									});
//
//							ImageView middleView = (ImageView) layout
//									.findViewById(R.id.middle_photo);
//							middleView
//									.setImageResource(R.drawable.event_add_photo_pic);
//							middleView
//									.setOnClickListener(new View.OnClickListener() {
//										@Override
//										public void onClick(View view) {
//											final CharSequence[] items = {
//													"相册", "拍照", "取消" };
//											builder = new AlertDialog.Builder(
//													EventActivity.this);
//											builder.setTitle("选择图片");
//											builder.setItems(
//													items,
//													new DialogInterface.OnClickListener() {
//														@Override
//														public void onClick(
//																DialogInterface dialogInterface,
//																int i) {
//															if (i == 1) {
//																Intent intent = new Intent(
//																		MediaStore.ACTION_IMAGE_CAPTURE);
//																File file = getOutputMediaFile(
//																		MEDIA_TYPE_IMAGE,
//																		maxNumber++); // create
//																						// a
//																						// file
//																						// to
//																						// save
//																						// the
//																						// image
//																miNumber++;
//																intent.putExtra(
//																		MediaStore.EXTRA_OUTPUT,
//																		Uri.fromFile(file)); // set
//																								// the
//																								// image
//																								// file
//																								// name
//
//																// start the
//																// image capture
//																// Intent
//																startActivityForResult(
//																		intent,
//																		CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else if (i == 0) {
////																Intent getImage = new Intent(
////																		Intent.ACTION_GET_CONTENT);
////																getImage.addCategory(Intent.CATEGORY_OPENABLE);
////																getImage.setType("image/jpeg");
////																startActivityForResult(
////																		getImage,
////																		GET_IMAGE_ACTIVITY_REQUEST_CODE);
////																Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////				                                        		startActivityForResult(intent,
////				                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//																Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//																intent.putExtra("maxSelection", 9-miNumber);
//				                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else {
//																dialog.dismiss();
//															}
//														}
//													});
//											dialog = builder.show();
//										}
//									});
//						
//						}
//					}
//					if (i * 3 + 2 <= miNumber) {
//						if (i * 3 + 2 < miNumber) {
//							ImageView rightView = (ImageView) layout
//									.findViewById(R.id.right_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3 + 2);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							rightView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(rightView, i * 3 + 2, imageResourceList);
//						} else if(miNumber != 9){
////							ImageView rightView = (ImageView) layout
////									.findViewById(R.id.right_photo);
////							rightView
////									.setImageResource(R.drawable.event_add_photo_pic);
////							rightView
////									.setOnClickListener(new View.OnClickListener() {
////										@Override
////										public void onClick(View view) {
////											Intent intent = new Intent(
////													MediaStore.ACTION_IMAGE_CAPTURE);
////											File fileUri = getOutputMediaFile(
////													MEDIA_TYPE_IMAGE,
////													miNumber++); // create a
////																	// file to
////																	// save the
////																	// image
////
////											Log.v("DBG",
////													"save " + fileUri.getPath());
////											intent.putExtra(
////													MediaStore.EXTRA_OUTPUT,
////													Uri.fromFile(fileUri)); // set
////																			// the
////																			// image
////																			// file
////																			// name
////
////											// start the image capture Intent
////											startActivityForResult(intent,
////													CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
////										}
////									});
//							ImageView rightView = (ImageView) layout
//									.findViewById(R.id.right_photo);
//							rightView
//									.setImageResource(R.drawable.event_add_photo_pic);
//							rightView
//									.setOnClickListener(new View.OnClickListener() {
//										@Override
//										public void onClick(View view) {
//											final CharSequence[] items = {
//													"相册", "拍照", "取消" };
//											builder = new AlertDialog.Builder(
//													EventActivity.this);
//											builder.setTitle("选择图片");
//											builder.setItems(
//													items,
//													new DialogInterface.OnClickListener() {
//														@Override
//														public void onClick(
//																DialogInterface dialogInterface,
//																int i) {
//															if (i == 1) {
//																Intent intent = new Intent(
//																		MediaStore.ACTION_IMAGE_CAPTURE);
//																File file = getOutputMediaFile(
//																		MEDIA_TYPE_IMAGE,
//																		maxNumber++); // create
//																						// a
//																						// file
//																						// to
//																						// save
//																						// the
//																						// image
//																miNumber++;
//																intent.putExtra(
//																		MediaStore.EXTRA_OUTPUT,
//																		Uri.fromFile(file)); // set
//																								// the
//																								// image
//																								// file
//																								// name
//
//																// start the
//																// image capture
//																// Intent
//																startActivityForResult(
//																		intent,
//																		CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else if (i == 0) {
////																Intent getImage = new Intent(
////																		Intent.ACTION_GET_CONTENT);
////																getImage.addCategory(Intent.CATEGORY_OPENABLE);
////																getImage.setType("image/jpeg");
////																startActivityForResult(
////																		getImage,
////																		GET_IMAGE_ACTIVITY_REQUEST_CODE);
////																Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////				                                        		startActivityForResult(intent,
////				                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//																Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//																intent.putExtra("maxSelection", 9-miNumber);
//				                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else {
//																dialog.dismiss();
//															}
//														}
//													});
//											dialog = builder.show();
//										}
//									});
//						}
//					}
//					photoLayout.addView(layout);
//				}

                // Image captured and saved to fileUri specified in the Intent

            } else if (resultCode == RESULT_CANCELED) {
                miNumber--;
                maxNumber--;
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
                miNumber--;
                maxNumber--;
            }
        }
        if (requestCode == GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (data != null) {
            	
            	String[] all_path = data.getStringArrayExtra("all_path");
            	
                miNumber += all_path.length;
                maxNumber += all_path.length;
                
//                ArrayList<String> imageResourceList = new ArrayList<String>();
//            	for(int i=0; i<miNumber; i++)
//            	{
//            		//addedThings
//            		File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
//            		imageResourceList.add(fileName.getPath());
//            	}
                
            	
            	for(int i=0; i<all_path.length; i++)
            	{
            		String img_path = all_path[i];
                    File file = new File(img_path);
                    Log.v("DBG", "Get File Path " + file.getAbsolutePath());
                    String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, maxNumber - (all_path.length-i)).getAbsolutePath();
                    imageResourceList.add(filename);
                    Bitmap bitmap = null;
                    try {
                        BitmapFactory.Options opts = new BitmapFactory.Options();
                        BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                        int srcWidth = opts.outWidth;
                        int srcHeight = opts.outHeight;
                        int desWidth = 0;
                        int desHeight = 0;
                        int more = 0;
                        // 缩放比例
                        double ratio = 0.0;
                        if (srcWidth > srcHeight) {
                            ratio = srcWidth / 640;
                            desWidth = 640;
                            desHeight = (int) (srcHeight / ratio);
                            more = srcWidth % 640 == 0 ? 0 : 1;
                        } else {
                            ratio = srcHeight / 640;
                            desHeight = 640;
                            desWidth = (int) (srcWidth / ratio);
                            more = srcHeight % 640 == 0 ? 0 : 1;
                        }
                        // 设置输出宽度、高度
                        BitmapFactory.Options newOpts = new BitmapFactory.Options();
                        newOpts.inSampleSize = (int) (ratio) + more;
                        newOpts.inJustDecodeBounds = false;
                        newOpts.outWidth = desWidth;
                        newOpts.outHeight = desHeight;
//                        bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), newOpts);
                        bitmap = RotatePhoto.getPhotoRotated(file.getAbsolutePath(), newOpts);
                        FileOutputStream out = new FileOutputStream(filename);
                        if(bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out)){
                            out.flush();
                            out.close();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            	}
                

				photoLayout.removeAllViews();
				updateImage();
//				float density = getResources().getDisplayMetrics().density;
//				int finalWeight = (int) (weight * density);
//				int finalHeight = (int) (height * density);
//				int finalMarginTop = (int) (marginTop * density);
				//Kite
//				for (int i = 0; i < miNumber / 3 + 1; i++) {
//					
//					if(i == 3)
//                	{
//                		break;
//                	}
//					
//					LayoutInflater inflater = getLayoutInflater();
//					RelativeLayout layout = (RelativeLayout) inflater.inflate(
//							R.layout.photo_layout, null);
//					if (i * 3 <= miNumber) {
//						if (i * 3 < miNumber) {
//							ImageView leftView = (ImageView) layout
//									.findViewById(R.id.left_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							leftView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(leftView, i * 3, imageResourceList);
//						} else if(miNumber != 9){
//							ImageView leftView = (ImageView) layout
//									.findViewById(R.id.left_photo);
//							leftView.setImageResource(R.drawable.event_add_photo_pic);
//							leftView.setOnClickListener(new View.OnClickListener() {
//								@Override
//								public void onClick(View view) {
//									final CharSequence[] items = { "相册", "拍照",
//											"取消" };
//									builder = new AlertDialog.Builder(
//											EventActivity.this);
//									builder.setTitle("选择图片");
//									builder.setItems(
//											items,
//											new DialogInterface.OnClickListener() {
//												@Override
//												public void onClick(
//														DialogInterface dialogInterface,
//														int i) {
//													if (i == 1) {
//														Intent intent = new Intent(
//																MediaStore.ACTION_IMAGE_CAPTURE);
//														File file = getOutputMediaFile(
//																MEDIA_TYPE_IMAGE,
//																maxNumber++); // create
//																				// a
//																				// file
//																				// to
//																				// save
//																				// the
//																				// image
//														miNumber++;
//														intent.putExtra(
//																MediaStore.EXTRA_OUTPUT,
//																Uri.fromFile(file)); // set
//																						// the
//																						// image
//																						// file
//																						// name
//
//														// start the image
//														// capture Intent
//														startActivityForResult(
//																intent,
//																CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//													} else if (i == 0) {
////														Intent getImage = new Intent(
////																Intent.ACTION_GET_CONTENT);
////														getImage.addCategory(Intent.CATEGORY_OPENABLE);
////														getImage.setType("image/jpeg");
////														startActivityForResult(
////																getImage,
////																GET_IMAGE_ACTIVITY_REQUEST_CODE);
////														Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////		                                        		startActivityForResult(intent,
////		                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//														Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//														intent.putExtra("maxSelection", 9-miNumber);
//		                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//													} else {
//														dialog.dismiss();
//													}
//												}
//											});
//									dialog = builder.show();
//								}
//							});
//						}
//					}
//					if (i * 3 + 1 <= miNumber) {
//						if (i * 3 + 1 < miNumber) {
//							ImageView middleView = (ImageView) layout
//									.findViewById(R.id.middle_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3 + 1);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							middleView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(middleView, i * 3 + 1, imageResourceList);
//						} else if(miNumber != 9){
//							ImageView middleView = (ImageView) layout
//									.findViewById(R.id.middle_photo);
//							middleView
//									.setImageResource(R.drawable.event_add_photo_pic);
//							middleView
//									.setOnClickListener(new View.OnClickListener() {
//										@Override
//										public void onClick(View view) {
//											final CharSequence[] items = {
//													"相册", "拍照", "取消" };
//											builder = new AlertDialog.Builder(
//													EventActivity.this);
//											builder.setTitle("选择图片");
//											builder.setItems(
//													items,
//													new DialogInterface.OnClickListener() {
//														@Override
//														public void onClick(
//																DialogInterface dialogInterface,
//																int i) {
//															if (i == 1) {
//																Intent intent = new Intent(
//																		MediaStore.ACTION_IMAGE_CAPTURE);
//																File file = getOutputMediaFile(
//																		MEDIA_TYPE_IMAGE,
//																		maxNumber++); // create
//																						// a
//																						// file
//																						// to
//																						// save
//																						// the
//																						// image
//																miNumber++;
//																intent.putExtra(
//																		MediaStore.EXTRA_OUTPUT,
//																		Uri.fromFile(file)); // set
//																								// the
//																								// image
//																								// file
//																								// name
//
//																// start the
//																// image capture
//																// Intent
//																startActivityForResult(
//																		intent,
//																		CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else if (i == 0) {
////																Intent getImage = new Intent(
////																		Intent.ACTION_GET_CONTENT);
////																getImage.addCategory(Intent.CATEGORY_OPENABLE);
////																getImage.setType("image/jpeg");
////																startActivityForResult(
////																		getImage,
////																		GET_IMAGE_ACTIVITY_REQUEST_CODE);
////																Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////				                                        		startActivityForResult(intent,
////				                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//																Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//																intent.putExtra("maxSelection", 9-miNumber);
//				                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else {
//																dialog.dismiss();
//															}
//														}
//													});
//											dialog = builder.show();
//										}
//									});
//						}
//					}
//					if (i * 3 + 2 <= miNumber) {
//						if (i * 3 + 2 < miNumber) {
//							ImageView rightView = (ImageView) layout
//									.findViewById(R.id.right_photo);
//							BitmapFactory.Options options = new BitmapFactory.Options();
//							options.inSampleSize = 2;
//							File fileName = getOutputMediaFile(
//									MEDIA_TYPE_IMAGE, i * 3 + 2);
//							Bitmap bit = BitmapFactory.decodeFile(
//									fileName.getPath(), options);
//							rightView.setImageBitmap(bit);
//							setImageViewToGalleryActivity(rightView, i * 3 + 2, imageResourceList);
//						} else if(miNumber != 9){
//							ImageView rightView = (ImageView) layout
//									.findViewById(R.id.right_photo);
//							rightView
//									.setImageResource(R.drawable.event_add_photo_pic);
//							rightView
//									.setOnClickListener(new View.OnClickListener() {
//										@Override
//										public void onClick(View view) {
//											final CharSequence[] items = {
//													"相册", "拍照", "取消" };
//											builder = new AlertDialog.Builder(
//													EventActivity.this);
//											builder.setTitle("选择图片");
//											builder.setItems(
//													items,
//													new DialogInterface.OnClickListener() {
//														@Override
//														public void onClick(
//																DialogInterface dialogInterface,
//																int i) {
//															if (i == 1) {
//																Intent intent = new Intent(
//																		MediaStore.ACTION_IMAGE_CAPTURE);
//																File file = getOutputMediaFile(
//																		MEDIA_TYPE_IMAGE,
//																		maxNumber++); // create
//																						// a
//																						// file
//																						// to
//																						// save
//																						// the
//																						// image
//																miNumber++;
//																intent.putExtra(
//																		MediaStore.EXTRA_OUTPUT,
//																		Uri.fromFile(file)); // set
//																								// the
//																								// image
//																								// file
//																								// name
//
//																// start the
//																// image capture
//																// Intent
//																startActivityForResult(
//																		intent,
//																		CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else if (i == 0) {
////																Intent getImage = new Intent(
////																		Intent.ACTION_GET_CONTENT);
////																getImage.addCategory(Intent.CATEGORY_OPENABLE);
////																getImage.setType("image/jpeg");
////																startActivityForResult(
////																		getImage,
////																		GET_IMAGE_ACTIVITY_REQUEST_CODE);
////																Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////				                                        		startActivityForResult(intent,
////				                                        				GET_IMAGE_ACTIVITY_REQUEST_CODE);
//																Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
//																intent.putExtra("maxSelection", 9-miNumber);
//				                                				startActivityForResult(intent, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
//															} else {
//																dialog.dismiss();
//															}
//														}
//													});
//											dialog = builder.show();
//										}
//									});
//						}
//					}
//					photoLayout.addView(layout);
//				}
				/*
				 * for (int i = 0; i < miNumber; i++) { ImageView photoView =
				 * new ImageView(EventActivity.this); LinearLayout.LayoutParams
				 * imgvwDimens = new LinearLayout.LayoutParams(finalWeight,
				 * finalHeight); imgvwDimens.topMargin = finalMarginTop;
				 * imgvwDimens.gravity = Gravity.CENTER_HORIZONTAL;
				 * photoView.setLayoutParams(imgvwDimens); BitmapFactory.Options
				 * options = new BitmapFactory.Options(); options.inSampleSize =
				 * 2; File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
				 * Bitmap bit = BitmapFactory.decodeFile(fileName.getPath(),
				 * options); photoView.setImageBitmap(bit);
				 * photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
				 * photoView
				 * .setBackgroundResource(R.drawable.event_edit_background_pic);
				 * photoLayout.addView(photoView); } { ImageView photoView = new
				 * ImageView(EventActivity.this); LinearLayout.LayoutParams
				 * imgvwDimens = new LinearLayout.LayoutParams(finalWeight,
				 * finalHeight); imgvwDimens.topMargin = finalMarginTop;
				 * imgvwDimens.gravity = Gravity.CENTER_HORIZONTAL;
				 * photoView.setLayoutParams(imgvwDimens);
				 * photoView.setImageResource(R.drawable.event_add_photo_pic);
				 * photoView
				 * .setBackgroundColor(getResources().getColor(R.color.transparent
				 * )); photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
				 * photoView.setOnClickListener(new View.OnClickListener() {
				 * 
				 * @Override public void onClick(View view) { Intent intent =
				 * new Intent(MediaStore.ACTION_IMAGE_CAPTURE); File fileUri =
				 * getOutputMediaFile(MEDIA_TYPE_IMAGE, miNumber++); // create a
				 * file to save the image
				 * 
				 * Log.v("DBG", "save " + fileUri.getPath());
				 * intent.putExtra(MediaStore.EXTRA_OUTPUT,
				 * Uri.fromFile(fileUri)); // set the image file name
				 * 
				 * // start the image capture Intent
				 * startActivityForResult(intent,
				 * CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE); } });
				 * photoLayout.addView(photoView); }
				 */

            }
        }

        if(requestCode == GALLERY_ACTIVITY_REQUEST_CODE)
        {
        	if(resultCode == RESULT_OK)
        	{
        		int imageIndex = data.getIntExtra("imageIndex", -1);
        		if(imageIndex != -1)
        		{
        			imageResourceList.remove(imageIndex);
        			miNumber--;
        			
        			photoLayout.removeAllViews();
        			updateImage();
        		}
        	}
        }
    }

    private void setImageViewToGalleryActivity(ImageView imageView, final int index, final ArrayList<String> imageResourceList)
    {
    	imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventActivity.this, GalleryActivity.class);
		    	intent.putStringArrayListExtra("imageResourceList",
						imageResourceList);
				intent.putExtra("imageIndex", index);
				intent.putExtra("deleteOption", true);
				
				EventActivity.this.startActivityForResult(intent, GALLERY_ACTIVITY_REQUEST_CODE);
				EventActivity.this.overridePendingTransition(
						R.anim.zoom_in, 0);
			}
		});
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			
			if(showBuilder)
			{
				return true;
			}
			
			if(otherBabyShown)
			{
				hideOtherBaby();
				
				return true;
			}
			
			Intent intent = new Intent();
			setResult(RESULT_CANCELED, intent);
			
			EventActivity.this.finish();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
    
    
}