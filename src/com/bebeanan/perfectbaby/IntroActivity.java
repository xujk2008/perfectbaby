package com.bebeanan.perfectbaby;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Created by KiteXu.
 */
public class IntroActivity extends Activity{

	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		
		ImageView leftButton = (ImageView) findViewById(R.id.title_left_button);
		leftButton.setImageResource(R.drawable.title_back_pic);
		leftButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				IntroActivity.this.finish();
			}
			
		});
		
		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);
		
		TextView introText = (TextView) findViewById(R.id.intro_text);
	}

}
