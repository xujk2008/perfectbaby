package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.bebeanan.perfectbaby.common.MultipartEntityWithProgressBar;
import com.bebeanan.perfectbaby.common.MultipartEntityWithProgressBar.WriteListener;

import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;


/**
 * Created by guomoyi on 14-4-23.
 */
public abstract class NetHandler {
    public static String METHOD_GET = "GET";
    public static String METHOD_POST = "POST";
    public static String METHOD_PUT = "PUT";
    public static String METHOD_DELETE = "DELETE";
    public static String METHOD_UPLOAD_FILE = "UPLOAD_FILE";
    public static String METHOD_DOWNLOAD_FILE = "DOWNLOAD_FILE";
    public static String METHOD_HTTPS_POST = "HTTPS_POST";
    public static String METHOD_GET_HOST = "GET_HOST";
    public static String METHOD_GET_LOCATION = "GET_LOCATION";
//    public static String BAKIP = "http://115.29.37.67/";
//    public static String BAKIP = "http://baby.shaream.cn:2403/";
    public static String BAKIP = "https://server.bebeanan.com/";
    public static int BakPort = 80;
    public static int exceptionCode = -100;
    private Context context;
    private Handler handler;
    private String method;
    private String uri;
    private JSONObject data;
    private List<BasicNameValuePair> param;
    
    private WriteListener writeListener;

    public NetHandler(Context context, String method, String uri,
               List<BasicNameValuePair> param,
               JSONObject data) {
    	this.context = context;
        this.method = method;
        this.uri = uri;
        this.data = data;
        this.param = param;
        
        StoreHandler.getAndSetCookies();
    }


    private final static int SET_CONNECTION_TIMEOUT = 5 * 1000;
    private final static int SET_SOCKET_TIMEOUT = 20 * 1000;


    public void setUploadListener(WriteListener writeListener)
    {
    	this.writeListener = writeListener;
    }
    
    private HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();

            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            HttpConnectionParams.setConnectionTimeout(params, SET_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SET_SOCKET_TIMEOUT);
            HttpClient client = new DefaultHttpClient(ccm, params);

            return client;
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public abstract void handleRsp(Message msg);

    public void start() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
            	
            	Bundle bundle = msg.getData();
            	int code = bundle.getInt("code");
            	
            	if(code == 403)
            	{
            		Toast toast = Toast.makeText(context, "您的账号已在其他设备登陆", Toast.LENGTH_SHORT);
            		toast.setGravity(Gravity.CENTER, 0, 0);
            		toast.show();
            		
            		Intent intent = new Intent(context, LoginActivity.class);
            		intent.putExtra("loginRequired", true);
            		context.startActivity(intent);
            		
            		return;
            	}
            	
                handleRsp(msg);
                super.handleMessage(msg);
            }
        };
        new Thread() {
            @Override
            public void run() {
                if (method.equals(METHOD_GET)) {
                    HttpGet req = null;
                    HttpClient client = new DefaultHttpClient();
                    String IP = "";
                    HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                    if (IpInfo.get("IP") == null) {
                        IP = BAKIP;
                    } else if (!IpInfo.get("Status").toString().equals("0")) {
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", -200);
                        bundle.putString("data", IpInfo.get("Message").toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } else {
                        IP = IpInfo.get("IP").toString();
                        if (IP.indexOf("https://") == 0) {
                            client = getNewHttpClient();
                        }
                    }
                    if (IP.charAt(IP.length() - 1) == '/') {
                        IP = IP.substring(0, IP.length() - 1);
                    }
                    if (param.size() != 0) {
                        
						String encodeListParam = URLEncodedUtils.format(param,
								"UTF-8");
						req = new HttpGet(IP + uri + "?" + encodeListParam);
                    } else {
						req = new HttpGet(IP + uri);
                    }
                    StringBuilder sb = new StringBuilder();
                    HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                    Iterator iter = cookieContainer.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        String key = entry.getKey().toString();
                        String val = entry.getValue().toString();
                        sb.append(key);
                        sb.append("=");
                        sb.append(val);
                        sb.append(";");
                    }
                    req.addHeader("cookie", sb.toString());
                    Log.v("Kite", "cookie is " + sb.toString());

                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method.equals(METHOD_POST)) {
                    HttpPost req = null;
                    HttpClient client = new DefaultHttpClient();
                    String IP = "";
                    HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                    if (IpInfo.get("IP") == null) {
                        IP = BAKIP;
                    } else if (!IpInfo.get("Status").toString().equals("0")) {
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", -200);
                        bundle.putString("data", IpInfo.get("Message").toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } else {
                        IP = IpInfo.get("IP").toString();
                        if (IP.indexOf("https://") == 0) {
                            client = getNewHttpClient();
                        }
                    }
                    if (IP.charAt(IP.length() - 1) == '/') {
                        IP = IP.substring(0, IP.length() - 1);
                    }
                    if (param.size() != 0) {
                        String encodeParam = URLEncodedUtils.format(param, "UTF-8");
                        req = new HttpPost(IP + uri + "?" + encodeParam);
                    } else {
                        req = new HttpPost(IP + uri);
                    }
                    req.setHeader("Content-Type", "application/json");
                    try {
                    	if(data != null)
                    	{
                    		req.setEntity(new StringEntity(data.toString(), "UTF-8"));
                    	}
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                    if (!uri.equals("/users/login") && !uri.equals("/oauth")) {
                        StringBuilder sb = new StringBuilder();
                        HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                        Iterator iter = cookieContainer.entrySet().iterator();
                        while (iter.hasNext()) {
                            Map.Entry entry = (Map.Entry) iter.next();
                            String key = entry.getKey().toString();
                            String val = entry.getValue().toString();
                            sb.append(key);
                            sb.append("=");
                            sb.append(val);
                            sb.append(";");
                        }
                        req.addHeader("cookie", sb.toString());
                        Log.v("Kite", "cookie is " + sb.toString());
                    }
                    try {
                        HttpResponse rsp = client.execute(req);
                        Header[] headers = rsp.getHeaders("Set-Cookie");
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);

                        if (uri.equals("/users/login") || uri.equals("/oauth")) {
                        	
                            HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                            for (int i = 0; i < headers.length; i++) {
                                String cookie = headers[i].getValue();
                                String[] cookievalues = cookie.split(";");
                                for (int j = 0; j < cookievalues.length; j++) {
                                    String[] keyPair = cookievalues[j].split("=");
                                    String key = keyPair[0].trim();
                                    String value = keyPair.length > 1 ? keyPair[1].trim() : "";
                                    cookieContainer.put(key, value);
                                }
                            }
                            MemoryHandler.getInstance().setCookieContainer(cookieContainer);
                            StoreHandler.storeCookies(headers);
                        }

                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method.equals(METHOD_PUT)) {
                    HttpPut req = null;
                    HttpClient client = new DefaultHttpClient();
                    String IP = "";
                    HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                    if (IpInfo.get("IP") == null) {
                        IP = BAKIP;
                    } else if (!IpInfo.get("Status").toString().equals("0")) {
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", -200);
                        bundle.putString("data", IpInfo.get("Message").toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } else {
                        IP = IpInfo.get("IP").toString();
                        if (IP.indexOf("https://") == 0) {
                            client = getNewHttpClient();
                        }
                    }
                    if (IP.charAt(IP.length() - 1) == '/') {
                        IP = IP.substring(0, IP.length() - 1);
                    }
                    if (param.size() != 0) {
                        String encodeParam = URLEncodedUtils.format(param, "UTF-8");
                        req = new HttpPut(IP + uri + "?" + encodeParam);
                    } else {
                        req = new HttpPut(IP + uri);
                    }
                    StringBuilder sb = new StringBuilder();
                    HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                    Iterator iter = cookieContainer.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        String key = entry.getKey().toString();
                        String val = entry.getValue().toString();
                        sb.append(key);
                        sb.append("=");
                        sb.append(val);
                        sb.append(";");
                    }
                    req.addHeader("cookie", sb.toString());
                    req.setHeader("Content-type", "application/json");
                    try {
                        req.setEntity(new StringEntity(data.toString(), "UTF-8"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method.equals(METHOD_DELETE)) {
                    HttpDelete req = null;
                    HttpClient client = new DefaultHttpClient();
                    String IP = "";
                    HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                    if (IpInfo.get("IP") == null) {
                        IP = BAKIP;
                    } else if (!IpInfo.get("Status").toString().equals("0")) {
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", -200);
                        bundle.putString("data", IpInfo.get("Message").toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } else {
                        IP = IpInfo.get("IP").toString();
                        if (IP.indexOf("https://") == 0) {
                            client = getNewHttpClient();
                        }
                    }
                    if (IP.charAt(IP.length() - 1) == '/') {
                        IP = IP.substring(0, IP.length() - 1);
                    }
                    if (param.size() != 0) {
                        String encodeParam = URLEncodedUtils.format(param, "UTF-8");
                        req = new HttpDelete(IP + uri + "?" + encodeParam);
                    } else {
                        req = new HttpDelete(IP + uri);
                    }
                    StringBuilder sb = new StringBuilder();
                    HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                    Iterator iter = cookieContainer.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        String key = entry.getKey().toString();
                        String val = entry.getValue().toString();
                        sb.append(key);
                        sb.append("=");
                        sb.append(val);
                        sb.append(";");
                    }
                    req.addHeader("cookie", sb.toString());
                    Log.v("Kite", "cookie is " + sb.toString());
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method.equals(METHOD_UPLOAD_FILE)) {
                    HttpClient client = new DefaultHttpClient();
                    String IP = "";
                    HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
                    if (IpInfo.get("IP") == null) {
                        IP = BAKIP;
                    } else if (!IpInfo.get("Status").toString().equals("0")) {
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", -200);
                        bundle.putString("data", IpInfo.get("Message").toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } else {
                        IP = IpInfo.get("IP").toString();
                        if (IP.indexOf("https://") == 0) {
                            client = getNewHttpClient();
                        }
                    }
                    if (IP.charAt(IP.length() - 1) == '/') {
                        IP = IP.substring(0, IP.length() - 1);
                    }
                    HttpPost req = new HttpPost(IP + uri);
                    StringBuilder sb = new StringBuilder();
                    HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
                    Iterator iter = cookieContainer.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        String key = entry.getKey().toString();
                        String val = entry.getValue().toString();
                        sb.append(key);
                        sb.append("=");
                        sb.append(val);
                        sb.append(";");
                    }
                    
                    req.addHeader("cookie", sb.toString());
                    File file = new File(param.get(0).getValue());
                    String filename = param.get(1).getValue();
                    MultipartEntity mpEntity = new MultipartEntity(); //文件传输
                    
                    if(writeListener != null)
                    {
                    	mpEntity = new MultipartEntityWithProgressBar(writeListener);
                    }
                    ContentBody cbFile = new FileBody(file);
                    mpEntity.addPart(filename, cbFile);
                    try {
                        req.setEntity(mpEntity);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        
//                        double endTime = System.currentTimeMillis();
//                        double tranferTime = (endTime - startTime)/1000;
//
//                        double rate = 
//                            (  TrafficStats.getTotalTxBytes() - totalTxBytes
//                             + TrafficStats.getTotalRxBytes() - totalRxBytes) / tranferTime;         
//
//                        /** Log */
//                        Log.d("UploadManager", "Upload Start: " + startTime            );
//                        Log.d("UploadManager", "Upload End:   " + endTime              );
//                        Log.d("UploadManager", "Tranfer Time: " + tranferTime + " secs");
//                        Log.d("UploadManager", "Upload speed: " + rate        + " MB/s"); 
                        
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method == METHOD_DOWNLOAD_FILE) {
                    HttpGet req = new HttpGet(uri);
                    HttpClient client = new DefaultHttpClient();
                    try {
                        /* 这里需要验证拉取回来的数据是不是图片，判断是不是option的问题还是其他问题 */
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String result = EntityUtils.toString(rsp.getEntity(), "ISO-8859-1");

                        Log.v("NetHandler", String.valueOf(rsp.getEntity().getContentLength()));
                        Log.v("NetHandler", String.format("%d %d", code, result.getBytes("ISO-8859-1").length));
                        // Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", result);
                        bundle.putString("uri", uri);
                        if (data != null) {
                            bundle.putString("extra", data.toString());
                        }
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method == METHOD_HTTPS_POST) {
                    HttpClient client = getNewHttpClient();
                    HttpPost req = null;
                    if (param.size() != 0) {
                        String encodeParam = URLEncodedUtils.format(param, "UTF-8");
                        req = new HttpPost(uri + "?" + encodeParam);
                        Log.v("NetHandler", uri + "?" + encodeParam);
                    } else {
                        req = new HttpPost(uri);
                        Log.v("NetHandler", uri);
                    }
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                } else if (method == METHOD_GET_HOST) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost req = new HttpPost(uri);
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        if (code == 200) {
                            JSONObject object = new JSONObject(data);
                            HashMap<String, Object> IpInfo = new HashMap<String, Object>();
                            try {
                                IpInfo.put("IP", object.getString("server"));
                                IpInfo.put("Status", object.getInt("status"));
                                IpInfo.put("Message", object.getString("message"));
                                IpInfo.put("Version", object.getInt("version"));
                                MemoryHandler.handler.setHost(IpInfo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else if (method == METHOD_GET_LOCATION) {
                    HttpClient client = new DefaultHttpClient();
                    HttpGet req = null;
                    if (param.size() != 0) {
                        String encodeParam = URLEncodedUtils.format(param, "UTF-8");
                        req = new HttpGet(uri + "?" + encodeParam);
                    } else {
                        req = new HttpGet(uri);
                    }
                    try {
                        HttpResponse rsp = client.execute(req);
                        int code = rsp.getStatusLine().getStatusCode();
                        String data = EntityUtils.toString(rsp.getEntity(), "utf-8");
                        Log.v("NetHandler", String.format("%d", code));
                        Log.v("NetHandler", data);
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", code);
                        bundle.putString("data", data);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Bundle bundle = new Bundle();
                        Message msg = new Message();
                        bundle.putInt("code", exceptionCode);
                        bundle.putString("data", e.getStackTrace().toString());
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                        return;
                    }
                }
            }
        }.start();
    }
}
