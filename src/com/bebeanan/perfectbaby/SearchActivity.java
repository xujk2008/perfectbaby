package com.bebeanan.perfectbaby;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxingpack.BarScanActivity;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class SearchActivity extends Activity {

	private EditText qrCodeEdit;
	private EditText birthdayEdit;

	private final Calendar calendar = Calendar.getInstance();

	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
		builder = new AlertDialog.Builder(SearchActivity.this);
		builder.setCancelable(false);

		TextView titleView = (TextView) findViewById(R.id.title_text);
		titleView.setTypeface(Utils.typeface);
		titleView.setText(R.string.search_title);

		ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
		backButton.setImageResource(R.drawable.title_back_pic);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}
		});

		qrCodeEdit = (EditText) findViewById(R.id.search_qrcode_editview);
//		qrCodeEdit.setTypeface(Utils.typeface);
		
		birthdayEdit = (EditText) findViewById(R.id.search_birth_editview);
//		birthdayEdit.setTypeface(Utils.typeface);
		birthdayEdit.setInputType(InputType.TYPE_NULL);
		birthdayEdit.setFocusable(false);
		birthdayEdit.setText(calendar.get(Calendar.YEAR) + "-"
				+ (calendar.get(Calendar.MONTH)+1) + "-"
				+ calendar.get(Calendar.DAY_OF_MONTH));
		birthdayEdit.setOnClickListener(new OnClickListener() {

			DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
				@Override
				public void onDateSet(DatePicker datePicker, int year,
						int month, int dayOfMonth) {
					birthdayEdit.setText(year + "-" + (month+1) + "-" + dayOfMonth);
				}
			};

			@Override
			public void onClick(View view) {
				builder = new AlertDialog.Builder(SearchActivity.this);
				DatePickerDialog datePickerDialog = new DatePickerDialog(
						SearchActivity.this, dateListener, calendar
								.get(Calendar.YEAR), calendar
								.get(Calendar.MONTH), calendar
								.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.setCancelable(false);
				datePickerDialog.show();
			}

		});

		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);

		ImageButton searchHistoryButton = (ImageButton) findViewById(R.id.search_history_result_button);
		searchHistoryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				intent.setClass(SearchActivity.this,
						SearchHistoryActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.activity_open_in_anim,
						R.anim.activity_open_out_anim);
			}
		});

		ImageButton searchButton = (ImageButton) findViewById(R.id.search_button);
		searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				String qrCode = qrCodeEdit.getText().toString();
				String birthday = birthdayEdit.getText().toString();

				if (qrCode.isEmpty() || birthday.isEmpty()) {
					toast.cancel();
					toast = Toast.makeText(getApplicationContext(), "参数不完全",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					return;
				}
				String user = MemoryHandler.getInstance().getKey("users");

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
				try {
					JSONObject object = new JSONObject(user);
					param.add(new BasicNameValuePair("userid", object
							.getString("id")));
				} catch (Exception e) {
					e.printStackTrace();
				}

				builder = new AlertDialog.Builder(SearchActivity.this);
				builder.setMessage("上查询中");
				dialog = builder.show();
				showBuilder = true;

				param.add(new BasicNameValuePair("barcodestring", qrCode));
				param.add(new BasicNameValuePair("birthday", birthday));
				NetHandler handler = new NetHandler(SearchActivity.this, 
						NetHandler.METHOD_HTTPS_POST,
						"https://api.isgenetic.com/Favor/getscreenresult",
						param, null) {
					@Override
					public void handleRsp(Message msg) {

						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						if (code == 200) {
							Log.v("DBG", "search done");
							Intent intent = new Intent();
							try {
								JSONArray resultArray = new JSONArray(bundle
										.getString("data"));
								JSONObject result = resultArray
										.getJSONObject(0);
								if (!result.getString("birthday").isEmpty()) {
									intent.putExtra("result",
											bundle.getString("data"));
									intent.setClass(SearchActivity.this,
											SearchResultActivity.class);
									startActivity(intent);
									overridePendingTransition(
											R.anim.activity_open_in_anim,
											R.anim.activity_open_out_anim);
								} else {
									toast.cancel();
									toast = Toast.makeText(
											getApplicationContext(),
											"条形码或者生日不符", Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
								}
							} catch (Exception e) {
								e.printStackTrace();
								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"输入信息有误", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						} else {
							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"查询失败", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}

					}
				};
				handler.start();
			}
		});

		ImageButton scanButton = (ImageButton) findViewById(R.id.search_scan_button);
		scanButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				intent.setClass(SearchActivity.this, BarScanActivity.class);
				startActivityForResult(intent, 100);
				overridePendingTransition(R.anim.activity_open_in_anim,
						R.anim.activity_open_out_anim);
			}
		});

	}

	// private DatePicker findDatePicker(ViewGroup group) {
	// if (group != null) {
	// for (int i = 0, j = group.getChildCount(); i < j; i++) {
	// View child = group.getChildAt(i);
	// if (child instanceof DatePicker) {
	// return (DatePicker) child;
	// } else if (child instanceof ViewGroup) {
	// DatePicker result = findDatePicker((ViewGroup) child);
	// if (result != null)
	// return result;
	// }
	// }
	// }
	// return null;
	// }

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			overridePendingTransition(R.anim.activity_close_in_anim,
					R.anim.activity_close_out_anim);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 100 && resultCode == 101) {
			qrCodeEdit.setText(data.getStringExtra("result"));
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
