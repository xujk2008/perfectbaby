package com.bebeanan.perfectbaby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
import cn.jpush.android.api.InstrumentedActivity;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;

import com.bebeanan.perfectbaby.common.Utils;
import com.perfectbaby.adapters.StartWelcomePagerAdapter;
//import java.util.Objects;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
/**
 * Created by KiteXu.
 */
public class StartActivity extends InstrumentedActivity {

	private boolean loggedIn;

	private ArrayList<ImageView> mData = new ArrayList<ImageView>();
	private int[] welcomImages = { R.drawable.welcome_1, R.drawable.welcome_2,
			R.drawable.welcome_3, R.drawable.welcome_4 };
	
	private ImageView start_pic;
	private Handler handler;

	private SharedPreferences userPreferences;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		UmengUpdateAgent.update(this);

		setContentView(R.layout.activity_start);

		if (Utils.application == null) {
			Utils.init(StartActivity.this);
		}

		if (userPreferences == null) {
			userPreferences = Utils.application.getSharedPreferences(
					"userInfo", Context.MODE_PRIVATE);
		}

		MemoryHandler.getInstance().clearAll();
		MemoryHandler.getInstance().clearHost();

		JPushInterface.setDebugMode(true);
		JPushInterface.init(getApplicationContext());
		JPushInterface.resumePush(getApplicationContext());

		initShareSDK(StartActivity.this);
		
		start_pic = (ImageView) findViewById(R.id.start_pic);
//		start_pic.setImageResource(R.drawable.start_pic);
		start_pic.setImageResource(R.drawable.start_pic_360);
		
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub

				start_pic.setVisibility(View.GONE);
				
				ViewPager start_viewpager = (ViewPager) findViewById(R.id.start_viewpage);
				start_viewpager.setVisibility(View.VISIBLE);
				
				for (int i = 0; i < welcomImages.length; i++) {
					ImageView imageView = new ImageView(StartActivity.this);
					imageView.setScaleType(ScaleType.FIT_XY);
					imageView.setImageResource(welcomImages[i]);
					if (i == welcomImages.length - 1) {
						imageView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent intent = new Intent();
								intent.setClass(StartActivity.this, LoginActivity.class);
								startActivity(intent);
								finish();
							}

						});
					}

					mData.add(imageView);
				}

				StartWelcomePagerAdapter mAdapter = new StartWelcomePagerAdapter(
						StartActivity.this, mData);
				start_viewpager.setAdapter(mAdapter);

				StoreHandler.setGuide(StoreHandler.welcome, true);
			
			}
			
		};
		
		boolean welcomeShown = StoreHandler.getGuide(StoreHandler.welcome);
		if (!welcomeShown) {
			new Thread()
			{

				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					handler.sendEmptyMessage(0);
				}
				
			}.start();
		} else {
			start_pic.setVisibility(View.VISIBLE);
			
			ViewPager start_viewpager = (ViewPager) findViewById(R.id.start_viewpage);
			start_viewpager.setVisibility(View.GONE);
			
			loggedIn = StoreHandler.getAndSetCookies();
			StoreHandler.getAndSetBabies();
			Start();
		}
	}

	private void Start() {

		if (loggedIn) {
			String userId = userPreferences.getString("userId", "");

			JPushInterface.setAlias(StartActivity.this, userId,
					new TagAliasCallback() {

						@Override
						public void gotResult(int arg0, String arg1,
								Set<String> arg2) {
							// TODO Auto-generated method stub
							Log.v("Kite", "JPUSH " + arg0 + " " + arg1 + " "
									+ arg2);
						}

					});

			List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
			NetHandler handler1 = new NetHandler(StartActivity.this,
					NetHandler.METHOD_GET, "/users/me", param, null) {
				@Override
				public void handleRsp(Message msg) {
					Bundle bundle1 = msg.getData();
					int code = bundle1.getInt("code");
					if (code == 200) {

						MemoryHandler.getInstance().setKey("users",
								bundle1.getString("data"));
						try {
							JSONObject users = new JSONObject(
									bundle1.getString("data"));

							Log.v("Kite", "users data is " + users.toString());

							if (!users.has("nickname")) {
								Intent intent = new Intent();
								intent.setClass(StartActivity.this,
										RegisterFullActivity.class);
								startActivity(intent);
								finish();
								return;
							}

							String nickName = users.getString("nickname");
							Editor editor = userPreferences.edit();
							editor.putString("nickName", nickName);
							editor.putInt("usertype", users.getInt("usertype"));
							// Utils.nickName = nickname;
							if (users.has("avatar")) {
								// Utils.avatarUrl = users.getString("avatar");
								editor.putString("avatarUrl",
										users.getString("avatar"));
							}

							// Utils.gender = -1;
							editor.putInt("gender", -1);
							if (users.has("gender")) {
								editor.putInt("gender", users.getInt("gender"));
							}

							if (users.has("city")) {
								editor.putString("city",
										users.getString("city"));
							}

							editor.commit();

							if (nickName.isEmpty()) {
								// dialog.dismiss();
								Intent intent = new Intent();
								intent.setClass(StartActivity.this,
										RegisterFullActivity.class);
								startActivity(intent);
								finish();
								return;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
						NetHandler handler2 = new NetHandler(
								StartActivity.this, NetHandler.METHOD_GET,
								"/baby?owner="
										+ userPreferences.getString("userId",
												""), param, null) {
							@Override
							public void handleRsp(Message msg) {
								Bundle bundle2 = msg.getData();
								int code = bundle2.getInt("code");
								if (code == 200) {
									// dialog.dismiss();

									try {
										JSONArray baby = new JSONArray(
												bundle2.getString("data"));
										// if (baby.length() == 0) {
										// Log.v("DBG", "no baby");
										// MemoryHandler.getInstance().setKey("baby",
										// bundle2.getString("data"));
										// StoreHandler.storeBabies(bundle2.getString("data"));
										// Intent intent = new Intent();
										// intent.putExtra("next", true);
										// intent.putExtra("addBaby", true);
										// intent.setClass(StartActivity.this,
										// BabyActivity.class);
										// startActivity(intent);
										// finish();
										// /* 这里需要跳到增加baby的Activity */
										// } else {
										MemoryHandler.getInstance().setKey(
												"baby",
												bundle2.getString("data"));
										StoreHandler.storeBabies(bundle2
												.getString("data"));
										Intent intent = new Intent();
										intent.setClass(StartActivity.this,
												MainActivity.class);
										startActivity(intent);
										finish();
										// }
									} catch (Exception e) {
										e.printStackTrace();
									}
								} else {
									// dialog.dismiss();
									Log.v("Kite",
											"start baby code is " + code
													+ " error is "
													+ bundle2.getString("data"));

									if (code == -100) {
										Toast toast = Toast.makeText(
												StartActivity.this, "网络不给力哟~",
												Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										System.exit(0);
									}

									return;
								}
							}
						};
						handler2.start();

					} else {
						// dialog.dismiss();
						Intent intent = new Intent(StartActivity.this,
								LoginActivity.class);
						StartActivity.this.startActivity(intent);

						StartActivity.this.finish();

						return;
					}
				}
			};
			handler1.start();

		} else {
			Intent intent = new Intent();
			intent.setClass(StartActivity.this, LoginActivity.class);
			startActivity(intent);
			finish();
		}
	}

	public static void initShareSDK(Context context) {
		ShareSDK.initSDK(context.getApplicationContext(), "1bf31aa5fde1");
		{
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("Id", "1");
			hashMap.put("SortId", "1");
			hashMap.put("AppKey", "4060819693");
			hashMap.put("AppSecret", "5EDACA8B93E91127C53CACCF480E3DA6");
			hashMap.put("RedirectUrl",
					"https://api.weibo.com/oauth2/default.html");
			hashMap.put("ShareByAppClient", "true");
			hashMap.put("Enable", "true");
			ShareSDK.setPlatformDevInfo(SinaWeibo.NAME, hashMap);
		}

		{
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("Id", "3");
			hashMap.put("SortId", "3");
			hashMap.put("AppId", "101084830");
			hashMap.put("AppKey", "71a9cf47a645ce4e306839e0e976c1e5");
			hashMap.put("ShareByAppClient", "true");
			hashMap.put("Enable", "true");
			ShareSDK.setPlatformDevInfo(QZone.NAME, hashMap);
		}
		{
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("Id", "4");
			hashMap.put("SortId", "4");
			hashMap.put("AppId", "wx68db34add2f5b9ad");
			hashMap.put("AppSecret", "03e09c4b16134e0a86453e3553795a67");
			hashMap.put("Enable", "true");
			hashMap.put("BypassApproval", false);
			ShareSDK.setPlatformDevInfo(Wechat.NAME, hashMap);
		}

		{
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("Id", "4");
			hashMap.put("SortId", "4");
			hashMap.put("AppId", "wx68db34add2f5b9ad");
			hashMap.put("AppSecret", "03e09c4b16134e0a86453e3553795a67");
			hashMap.put("Enable", "true");
			hashMap.put("BypassApproval", false);
			ShareSDK.setPlatformDevInfo("WechatMoments", hashMap);
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
