package com.bebeanan.perfectbaby;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class SearchResultActivity extends Activity {
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
		private WebView describeWeb;
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        if(Utils.application == null)
		{
			Utils.init(SearchResultActivity.this);
		}
        
        describeWeb = (WebView) findViewById(R.id.search_result_webview);
        describeWeb.setVisibility(View.GONE);
        
        String data = this.getIntent().getStringExtra("result");
        Log.v("Kite", "search result: " + this.getIntent().getStringExtra("result"));
        storeHistory(data);
        try {
            JSONArray resultArray = new JSONArray(data);
            JSONObject object = resultArray.getJSONObject(0);
            
            TextView birthdayWord = (TextView) findViewById(R.id.search_result_birth);
            birthdayWord.setTypeface(Utils.typeface);
            TextView birthday = (TextView) findViewById(R.id.search_result_birth_text);
            birthday.setTypeface(Utils.typeface);
            birthday.setText(object.getString("birthday"));
            
            TextView nameWord = (TextView) findViewById(R.id.search_result_mother_name);
            nameWord.setTypeface(Utils.typeface);
            TextView name = (TextView) findViewById(R.id.search_result_mother_name_text);
            name.setTypeface(Utils.typeface);
            name.setText(object.getString("patientname"));
            
            TextView bloodDateWord = (TextView) findViewById(R.id.search_result_blood_date);
            bloodDateWord.setTypeface(Utils.typeface);
            TextView bloodDate = (TextView) findViewById(R.id.search_result_blood_date_text);
            bloodDate.setTypeface(Utils.typeface);
            bloodDate.setText(object.getString("collectdate"));
            
            TextView babyGenderWord = (TextView) findViewById(R.id.search_result_baby_gender);
            babyGenderWord.setTypeface(Utils.typeface);
            TextView babyGender = (TextView) findViewById(R.id.search_result_baby_gender_text);
            babyGender.setTypeface(Utils.typeface);
            babyGender.setText(object.getString("sex"));
            
            TextView bloodHospitalWord = (TextView) findViewById(R.id.search_result_blood_hospital);
            bloodHospitalWord.setTypeface(Utils.typeface);
            TextView bloodHospital = (TextView) findViewById(R.id.search_result_blood_hospital_text);
            bloodHospital.setTypeface(Utils.typeface);
            bloodHospital.setText(object.getString("departmentname"));
            
            LinearLayout layout = (LinearLayout) findViewById(R.id.search_result_layout);
            JSONArray items = object.getJSONArray("item");
            for(int i = 0; i < items.length(); i++) {
                
            	LayoutInflater inflater = getLayoutInflater();
                RelativeLayout resultItem = (RelativeLayout) inflater.inflate(R.layout.search_result_item, null);
                
                LinearLayout search_result_item_layout = (LinearLayout) resultItem.findViewById(R.id.search_result_item);
                TextView search_result_item_text1 = (TextView) resultItem.findViewById(R.id.search_result_item_text1);
                search_result_item_text1.setTypeface(Utils.typeface);
                search_result_item_text1.setText(items.getJSONObject(i).getString("itemname"));
                TextView search_result_item_text2 = (TextView) resultItem.findViewById(R.id.search_result_item_text2);
                search_result_item_text2.setTypeface(Utils.typeface);
                search_result_item_text2.setText(items.getJSONObject(i).getString("conclusion"));
                
                layout.addView(resultItem);
                final String link = "https://api.isgenetic.com/Favor/page/webs/screeninfo/" + items.getJSONObject(i).getString("link") +".html";
                
                search_result_item_layout.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						describeWeb.getSettings().setJavaScriptEnabled(true);
						describeWeb.setWebViewClient(new WebViewClient(){

							@Override
							public boolean shouldOverrideUrlLoading(WebView view, String url) {
								// TODO Auto-generated method stub
								view.loadUrl(url);
								
								return true;
							}
							
						});
						describeWeb.loadUrl(link);
						
						describeWeb.setVisibility(View.VISIBLE);
					}
                	
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setTypeface(Utils.typeface);
        titleView.setText(R.string.search_result_title);

        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });

        ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
        rightButton.setVisibility(View.GONE);
        
        Button announceButton = (Button) findViewById(R.id.search_result_announce_button);
        announceButton.setTypeface(Utils.typeface);
        announceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(SearchResultActivity.this, AnnounceActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_in_anim, R.anim.activity_open_out_anim);
            }
        });
    }

    private void storeHistory(String data)
    {
		try {
			JSONArray resultArray = new JSONArray(data);
			JSONObject object = resultArray.getJSONObject(0);
			String collectdate = object.getString("collectdate");
			
			SharedPreferences searchHistoryPreferences = Utils.application.getSharedPreferences("searchHistory",Context.MODE_PRIVATE);
			int count = searchHistoryPreferences.getInt("count", 0);
			boolean hasStored = false;
			for(int i=0; i<count; i++)
			{
				String currentData = searchHistoryPreferences.getString("history_" + i, null);
				if(currentData != null)
				{
					JSONArray currentResultArray = new JSONArray(currentData);
					JSONObject currentObject = currentResultArray.getJSONObject(0);
					String currentCollectdate = currentObject.getString("collectdate");
					
					if(collectdate.equals(currentCollectdate))
					{
						hasStored = true;
						break;
					}
				}
			}
			
			if(!hasStored)
			{
				Editor editor=searchHistoryPreferences.edit(); 
		    	editor.putString("history_" + count, data);
		    	editor.putInt("count", count+1);
		    	editor.commit();
			}
	    	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
