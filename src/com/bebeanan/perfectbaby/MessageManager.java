package com.bebeanan.perfectbaby;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by guomoyi on 14-6-2.
 */
public class MessageManager {
    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public MessageManager(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void AddMessage(PushMessage message) {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("insert into BabyMessage(id, isread, text, isid, url) values(" + message.id
                    + "," + message.isread + ",\"" + message.text + "\"," + message.isid + ",\"" + message.url + "\");");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setRead(PushMessage message) {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("update BabyMessage set isread = " + message.isread + " where id = "
                    + message.id + ";");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<PushMessage> getMessage() {
        List<PushMessage> pushMessageList = new LinkedList<PushMessage>();
        try {
            db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from BabyMessage order by date;", null);
            while (cursor.moveToNext()) {
                PushMessage message = new PushMessage();
                message.id = cursor.getInt(cursor.getColumnIndex("id"));
                message.isread = cursor.getInt(cursor.getColumnIndex("isread"));
                message.isid = cursor.getInt(cursor.getColumnIndex("isid"));
                message.text = cursor.getString(cursor.getColumnIndex("text"));
                message.url = cursor.getString(cursor.getColumnIndex("url"));
                message.dateStr = cursor.getString(cursor.getColumnIndex("date"));
                pushMessageList.add(message);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pushMessageList;
    }

    public void deleteMessage(int id) {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("delete from BabyMessage where id = "
                    + id + ";");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
