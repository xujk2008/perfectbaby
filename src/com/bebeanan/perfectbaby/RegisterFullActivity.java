package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.RotatePhoto;
import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class RegisterFullActivity extends Activity {
    private TextView cityText;
    private ImageView cityBackground;
    private RelativeLayout cityLayout;

    private EditText nicknameEditText;
    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;
    private ImageButton registerButton;
    private ImageView avatarButton;
    private LinearLayout slideSwitch;

    private String avatarUrl = null;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE = 2000;
    private static final int CITY_REQUEST_CODE = 90;

    private boolean hasPhoto = false;

    private static int height = 106;
    private static int width = 106;

    private SharedPreferences userPreferences;
    
    private String allowString = "";
    
    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type, int number) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PerfectBaby");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.


        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PerfectBaby", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + String.valueOf(number) + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + String.valueOf(number) + ".mp4");
        } else {
            return null;
        }

        Log.v("DBG", mediaFile.getPath());

        return mediaFile;

    }

    private void CheckPass() {
        String nickname = nicknameEditText.getText().toString();
        if (!nickname.isEmpty()) {
            registerButton.setImageResource(R.drawable.register_full_complete_enable);
            registerButton.setClickable(true);
        } else {
            registerButton.setImageResource(R.drawable.register_full_complete_disable);
            registerButton.setClickable(false);
        }

        /* 这里需要添加每一个edittext的判断，这里还需要判断软键盘消失的时候 */
    }

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_full);

        if(Utils.application == null)
		{
			Utils.init(RegisterFullActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
        
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        dm = this.getApplicationContext().getResources().getDisplayMetrics();
        height = (int) (106 * dm.density);
        width = (int) (106 * dm.density);

        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(RegisterFullActivity.this);
        builder.setCancelable(false);

        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setTypeface(Utils.typeface);
        titleView.setText(R.string.register_full_title);

        slideSwitch = (LinearLayout) findViewById(R.id.register_full_gender);
        initGender();

        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
        rightButton.setVisibility(View.GONE);
        
        TextView register_full_nickname_text = (TextView) findViewById(R.id.register_full_nickname_text);
        register_full_nickname_text.setTypeface(Utils.typeface);

        cityText = (TextView) findViewById(R.id.register_full_city_text);
        cityText.setTypeface(Utils.typeface);
        cityText.setText("所在城市");
        cityBackground = (ImageView) findViewById(R.id.register_full_city_button);
        cityBackground.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
                intent.setClass(RegisterFullActivity.this, CityActivity.class);
                intent.putExtra("category", "don't upload.");
                startActivityForResult(intent, CITY_REQUEST_CODE);
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
			}
		});
        cityLayout = (RelativeLayout) findViewById(R.id.register_full_city_layout);
        cityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(RegisterFullActivity.this, CityActivity.class);
                intent.putExtra("category", "don't upload.");
                startActivityForResult(intent, CITY_REQUEST_CODE);
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });

        nicknameEditText = (EditText) findViewById(R.id.register_full_nickname_edit);
        nicknameEditText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				CheckPass();

				int bytesNumber = 0;
				try {
					bytesNumber = s.toString().getBytes("GBK").length;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Log.v("Kite", "bytesNumber is " + bytesNumber);

				if (bytesNumber == 32) {
					allowString = s.toString();
				}

				if (bytesNumber > 32) {
					nicknameEditText.setText(allowString);

					nicknameEditText.setSelection(allowString.length());

					Toast toast = Toast.makeText(RegisterFullActivity.this, "亲，昵称太长了～",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
        	
        });
//        nicknameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                    CheckPass();
//                }
//            }
//        });
//        nicknameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId,
//                                          KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEND
//                        || actionId == EditorInfo.IME_ACTION_DONE
//                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//                    CheckPass();
//                    return true;
//                }
//                return false;
//            }
//        });

        registerButton = (ImageButton) findViewById(R.id.register_full_done_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nickname = nicknameEditText.getText().toString();
                if (nickname.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "昵称不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                
				int bytesNumber = 0;
				try {
					bytesNumber = nickname.getBytes("GBK").length;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(bytesNumber < 4)
				{
					toast = Toast.makeText(getApplicationContext(), "亲，昵称长度不够噢～",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    
                    return;
				}

                builder = new AlertDialog.Builder(RegisterFullActivity.this);
                builder.setMessage("添加中");
                dialog = builder.show();
                showBuilder = true;

                try {
                    JSONObject data = new JSONObject();
                    
                    final Editor editor = userPreferences.edit();
                    data.put("nickname", nickname);
                    editor.putString("nickName", nickname);
                    
                    if (!cityText.getText().toString().equals("所在城市")) {
                        data.put("city", cityText.getText().toString());
                        editor.putString("city", cityText.getText().toString());
                    }
                    if (avatarUrl != null) {
                        data.put("avatar", avatarUrl);
                        editor.putString("avatarUrl", avatarUrl);
                    }
                    data.put("gender", (Integer)slideSwitch.getTag());
                    editor.putInt("gender", (Integer)slideSwitch.getTag());
                    Log.v("Kite", "new user data is " + data);
                    List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                    String userId = "";
                    try {
                        JSONObject users = new JSONObject(MemoryHandler.getInstance().getKey("users"));
                        if(users.has("uid"))
                        {
                        	userId = users.getString("uid");
                        }
                        else
                        {
                        	userId = users.getString("id");
                        }
                        
                        editor.putString("userId", userId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    NetHandler handler = new NetHandler(RegisterFullActivity.this, NetHandler.METHOD_PUT,
                            "/users/" + userId, param, data) {
                        @Override
                        public void handleRsp(Message msg) {
                            dialog.dismiss();
                            showBuilder = false;

                            Bundle bundle = msg.getData();
                            int code = bundle.getInt("code");
                            Log.v("Kite", "users data is " + bundle.getString("data"));
                            if (code == 200) {

                                String data = bundle.getString("data");
                                editor.commit();
                                MemoryHandler.getInstance().setKey("users", data);

                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "添加完成",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                
                                StoreHandler.clearBabies();

                                new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        Intent intent = new Intent();
                                        intent.putExtra("next", true);
                                        intent.putExtra("addBaby", true);
                                        intent.setClass(RegisterFullActivity.this, BabyActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.activity_close_in_anim,
                                                R.anim.activity_close_out_anim);
                                    }
                                }.start();

                                return;

                            }
                            else if(code==400){
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "此昵称已存在",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            }
                            else {
                                toast.cancel();
                                toast = Toast.makeText(getApplicationContext(), "添加失败",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return;
                            }

                        }
                    };
                    handler.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        registerButton.setClickable(false);

        avatarButton = (ImageView) findViewById(R.id.register_add_photo);
        avatarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = { "相册", "拍照", "取消" };
                builder = new AlertDialog.Builder(RegisterFullActivity.this);
                builder.setTitle("选择图片");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 1) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0); // create a file to save the image
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); // set the image file name

                            // start the image capture Intent
                            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                        } else if (i == 0) {
//                            Intent getImage = new Intent(Intent.ACTION_GET_CONTENT);
//                            getImage.addCategory(Intent.CATEGORY_OPENABLE);
//                            getImage.setType("image/jpeg");
//                            startActivityForResult(getImage, GET_IMAGE_ACTIVITY_REQUEST_CODE);
                        	Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    		startActivityForResult(intent,
                    				GET_IMAGE_ACTIVITY_REQUEST_CODE);
                        } else {
                            dialog.dismiss();
                        }
                    }
                });
                dialog = builder.show();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (showBuilder) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        } else {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    this.finish();
                    overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initGender() {
		RelativeLayout femaleLayout = (RelativeLayout) slideSwitch
				.findViewById(R.id.register_full_female_layout);
		RelativeLayout maleLayout = (RelativeLayout) slideSwitch
				.findViewById(R.id.register_full_male_layout);

		final ImageView femaleChooser = (ImageView) slideSwitch
				.findViewById(R.id.register_full_female_chooser);
		final ImageView maleChooser = (ImageView) slideSwitch
				.findViewById(R.id.register_full_male_chooser);

		femaleChooser.setVisibility(View.INVISIBLE);
		maleChooser.setVisibility(View.VISIBLE);
		slideSwitch.setTag(1);
		
		femaleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.VISIBLE);
				maleChooser.setVisibility(View.INVISIBLE);

				slideSwitch.setTag(2);
			}

		});

		maleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.INVISIBLE);
				maleChooser.setVisibility(View.VISIBLE);

				slideSwitch.setTag(1);
			}

		});
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CITY_REQUEST_CODE) {
        	if(resultCode == RESULT_OK)
        	{
        		cityText.setText(data.getStringExtra("city"));
        	}
            
        } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                builder = new AlertDialog.Builder(RegisterFullActivity.this);
                builder.setMessage("上传中");
                dialog = builder.show();
                showBuilder = true;

                String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0).getAbsolutePath();
                Bitmap bitmap = null;
                try {
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    BitmapFactory.decodeFile(filename, opts);
                    int srcWidth = opts.outWidth;
                    int srcHeight = opts.outHeight;
                    int desWidth = 0;
                    int desHeight = 0;
                    // 缩放比例
                    double ratio = 0.0;
                    if (srcWidth > srcHeight) {
                        ratio = srcWidth / 640;
                        desWidth = 640;
                        desHeight = (int) (srcHeight / ratio);
                    } else {
                        ratio = srcHeight / 640;
                        desHeight = 640;
                        desWidth = (int) (srcWidth / ratio);
                    }
                    // 设置输出宽度、高度
                    BitmapFactory.Options newOpts = new BitmapFactory.Options();
                    newOpts.inSampleSize = (int) (ratio) + 1;
                    newOpts.inJustDecodeBounds = false;
                    newOpts.outWidth = desWidth;
                    newOpts.outHeight = desHeight;
//                    bitmap = BitmapFactory.decodeFile(filename, newOpts);
                    bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
                    FileOutputStream out = new FileOutputStream(filename);
                    if(bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out)){
                        out.flush();
                        out.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                param.add(new BasicNameValuePair("filename", getOutputMediaFile(MEDIA_TYPE_IMAGE, 0).getAbsolutePath()));
                param.add(new BasicNameValuePair("filetype", "image"));
                NetHandler handler = new NetHandler(RegisterFullActivity.this, NetHandler.METHOD_UPLOAD_FILE,
                        "/file", param, null) {
                    @Override
                    public void handleRsp(Message msg) {
                        dialog.dismiss();
                        showBuilder = false;

                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {

                            String data = bundle.getString("data");
                            try {
                                JSONArray array = new JSONArray(data);
                                avatarUrl = array.getJSONObject(0).getString("url");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            hasPhoto = true;

                            File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0);
                            try {
                                Log.v("DBG", String.format("%d %d", height, width));
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;

                                BitmapFactory.decodeFile(fileName.getPath(), options);
                                int heightRatio = (int)Math.floor(options.outHeight / (float) height);
                                int widthRatio = (int)Math.floor(options.outWidth / (float) width);

                                Log.v("HEIGHRATIO", ""+heightRatio);
                                Log.v("WIDTHRATIO", ""+widthRatio);

                                Bitmap bit;
                                if(heightRatio == 0 && widthRatio == 0)
								{
									  BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
										options.inJustDecodeBounds = false;
									  Bitmap originalBmp = BitmapFactory
												.decodeFile(
														fileName.getPath(),
														enlargeOptions);
									  
									  Matrix matrix = new Matrix();
									  
									  int enlargeWidthRatio = (int)(width/originalBmp.getWidth())+2;
									  int enlargeHeightRatio = (int)(height/originalBmp.getHeight())+2;
									  int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio)?enlargeWidthRatio:enlargeHeightRatio;
									  
									  matrix.postScale(enlargeRatio, enlargeRatio); // 长和宽放大缩小的比例
									  bit = Bitmap.createBitmap(originalBmp, 0, 0, originalBmp.getWidth(),
											  originalBmp.getHeight(), matrix, true);
								}
                                else
                                {
                                	if (heightRatio > 1 && widthRatio > 1)
                                    {
                                        options.inSampleSize =  heightRatio > widthRatio ? heightRatio:widthRatio;
                                    }
                                    options.inJustDecodeBounds = false;
                                    bit = BitmapFactory.decodeFile(fileName.getPath(), options);
                                }
                                
                                Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(output);

                                final int color = 0xff424242;
                                final Paint paint = new Paint();
                                final Rect rect = new Rect(0, 0, width, height);
                                final RectF rectF = new RectF(rect);
                                final float roundPx = (float) width / 2.0f;

                                paint.setAntiAlias(true);
                                canvas.drawARGB(0, 0, 0, 0);

                                paint.setColor(color);
                                canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
                                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                                canvas.drawBitmap(bit, null, rect, paint);

                                // options.inSampleSize = 4;
                                avatarButton.setImageBitmap(output);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "上传完成",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;

                        } else {
                        	Log.v("Kite", "upload avatar error: " + bundle.getString("data"));
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "上传失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }
                };
                handler.start();


            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
        if (requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (data != null) {
                Log.v("DBG", "Data: " + data.toString());
                Uri originalUri = data.getData();
                String[] proj = {MediaStore.Images.Media.DATA};
                Cursor actualimagecursor = managedQuery(originalUri, proj, null, null, null);
                int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                actualimagecursor.moveToFirst();
                String img_path = actualimagecursor.getString(actual_image_column_index);
                File file = new File(img_path);
                Log.v("DBG", "Get File Path " + file.getAbsolutePath());
                String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0).getAbsolutePath();
                Bitmap bitmap = null;
                try {
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                    int srcWidth = opts.outWidth;
                    int srcHeight = opts.outHeight;
                    int desWidth = 0;
                    int desHeight = 0;
                    int more = 0;
                    // 缩放比例
                    double ratio = 0.0;
                    if (srcWidth > srcHeight) {
                        ratio = srcWidth / 640;
                        desWidth = 640;
                        desHeight = (int) (srcHeight / ratio);
                        more = srcWidth % 640 == 0 ? 0 : 1;
                    } else {
                        ratio = srcHeight / 640;
                        desHeight = 640;
                        desWidth = (int) (srcWidth / ratio);
                        more = srcHeight % 640 == 0 ? 0 : 1;
                    }
                    // 设置输出宽度、高度
                    BitmapFactory.Options newOpts = new BitmapFactory.Options();
                    newOpts.inSampleSize = (int) (ratio) + more;
                    newOpts.inJustDecodeBounds = false;
                    newOpts.outWidth = desWidth;
                    newOpts.outHeight = desHeight;
//                    bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), newOpts);
                    bitmap = RotatePhoto.getPhotoRotated(file.getAbsolutePath(), newOpts);
                    FileOutputStream out = new FileOutputStream(filename);
                    if(bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out)){
                        out.flush();
                        out.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                builder = new AlertDialog.Builder(RegisterFullActivity.this);
                builder.setMessage("上传中");
                dialog = builder.show();
                showBuilder = true;
                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
                param.add(new BasicNameValuePair("filename", filename));
                param.add(new BasicNameValuePair("filetype", "image"));
                NetHandler handler = new NetHandler(RegisterFullActivity.this, NetHandler.METHOD_UPLOAD_FILE,
                        "/file", param, null) {
                    @Override
                    public void handleRsp(Message msg) {
                        dialog.dismiss();
                        showBuilder = false;

                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {

                            String data = bundle.getString("data");
                            try {
                                JSONArray array = new JSONArray(data);
                                avatarUrl = array.getJSONObject(0).getString("url");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            hasPhoto = true;

                            File fileName = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0);
                            try {
                                Log.v("DBG", String.format("%d %d", height, width));
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;

                                BitmapFactory.decodeFile(fileName.getPath(), options);
                                int heightRatio = (int)Math.floor(options.outHeight / (float) height);
                                int widthRatio = (int)Math.floor(options.outWidth / (float) width);

                                Log.v("HEIGHRATIO", ""+heightRatio);
                                Log.v("WIDTHRATIO", ""+widthRatio);
                                
                                Bitmap bit;
                                if(heightRatio == 0 && widthRatio == 0)
								{
									  BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
										options.inJustDecodeBounds = false;
									  Bitmap originalBmp = BitmapFactory
												.decodeFile(
														fileName.getPath(),
														enlargeOptions);
									  
									  Matrix matrix = new Matrix();
									  
									  int enlargeWidthRatio = (int)(width/originalBmp.getWidth())+2;
									  int enlargeHeightRatio = (int)(height/originalBmp.getHeight())+2;
									  int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio)?enlargeWidthRatio:enlargeHeightRatio;
									  
									  matrix.postScale(enlargeRatio, enlargeRatio); // 长和宽放大缩小的比例
									  bit = Bitmap.createBitmap(originalBmp, 0, 0, originalBmp.getWidth(),
											  originalBmp.getHeight(), matrix, true);
								}
                                else
                                {
                                	if (heightRatio > 1 && widthRatio > 1)
                                    {
                                        options.inSampleSize =  heightRatio > widthRatio ? heightRatio:widthRatio;
                                    }
                                    options.inJustDecodeBounds = false;
                                    bit = BitmapFactory.decodeFile(fileName.getPath(), options);
                                }

                                Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(output);

                                final int color = 0xff424242;
                                final Paint paint = new Paint();
                                final Rect rect = new Rect(0, 0, width, height);
                                final RectF rectF = new RectF(rect);
                                final float roundPx = (float) width / 2.0f;

                                paint.setAntiAlias(true);
                                canvas.drawARGB(0, 0, 0, 0);

                                paint.setColor(color);
                                canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
                                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                                canvas.drawBitmap(bit, null, rect, paint);

                                // options.inSampleSize = 4;
                                avatarButton.setImageBitmap(output);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "上传完成",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;

                        } else {
                        	Log.v("Kite", "upload avatar error: " + bundle.getString("data"));
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "上传失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }
                };
                handler.start();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        nicknameEditText.clearFocus();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        if (hasPhoto) {
            File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0);
            file.delete();
        }
        super.onDestroy();
    }
}
