package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class QaActivity extends Activity{

	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qa);
		
		ImageView leftButton = (ImageView) findViewById(R.id.title_left_button);
		leftButton.setImageResource(R.drawable.title_back_pic);
		leftButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				QaActivity.this.finish();
			}
		});
		
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("常见问题");
		
		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);
		
		WebView qaWeb = (WebView) findViewById(R.id.qa_webview);
		qaWeb.getSettings().setJavaScriptEnabled(true);
		qaWeb.setWebViewClient(new WebViewClient(){

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				view.loadUrl(url);
				
				return true;
			}
			
		});
		qaWeb.loadUrl("file:///android_asset/index.html");
	}

}
