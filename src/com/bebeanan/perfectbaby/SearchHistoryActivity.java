package com.bebeanan.perfectbaby;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by KiteXu.
 */
public class SearchHistoryActivity extends Activity {
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_history);
        
        if(Utils.application == null)
		{
			Utils.init(SearchHistoryActivity.this);
		}

        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setTypeface(Utils.typeface);
        titleView.setText(R.string.search_history_title);

        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
        rightButton.setVisibility(View.GONE);
        
        LinearLayout rootLayout = (LinearLayout) findViewById(R.id.search_history_root);
        
        SharedPreferences searchHistoryPreferences = Utils.application.getSharedPreferences("searchHistory",Context.MODE_PRIVATE);
		int count = searchHistoryPreferences.getInt("count", 0);
		for(int i=0; i<count; i++)
		{
			final String currentData = searchHistoryPreferences.getString("history_" + i, null);
			if(currentData != null)
			{
				try {
					JSONArray currentResultArray = new JSONArray(currentData);
					JSONObject currentObject = currentResultArray.getJSONObject(0);
					String currentCollectdate = currentObject.getString("collectdate");
					
					LayoutInflater inflater = SearchHistoryActivity.this.getLayoutInflater();
					View historyItem = (View)inflater.inflate(R.layout.item_search_history, rootLayout, false);
					TextView historyItemText = (TextView)historyItem.findViewById(R.id.item_search_history_text);
					historyItemText.setTypeface(Utils.typeface);
					historyItemText.setText(currentCollectdate);
					historyItemText.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(SearchHistoryActivity.this, SearchResultActivity.class);
							intent.putExtra("result", currentData);
							
							SearchHistoryActivity.this.startActivity(intent);
						}
					});
					
					rootLayout.addView(historyItem);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
        
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
