package com.bebeanan.perfectbaby;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bebeanan.perfectbaby.common.Utils;

/**
 * Created by KiteXu.
 */
public class StoreHandler {

	public static String welcome = "welcome";
    public static String storeFile = "StoreFile";
    public static String guideFile = "GuideFile";
    
    public static String mainGuide = "mainGuide";
    public static String eventGuide = "eventGuide";
    public static String treeGuide = "treeGuide";
    public static String babyGuide = "babyGuide";
    public static String followBabiesGuide = "followBabiesGuide";
    public static String followerGuide = "followerGuide";
    
    static public void setGuide(String key, boolean value)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("guide",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putBoolean(key, value);
    	editor.commit();
    }
    
    static public boolean getGuide(String key)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("guide",Context.MODE_PRIVATE);
    	
    	return preferences.getBoolean(key, false);
    }
    
    static public void clearCookies()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cookies",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.clear();
    	editor.commit();
    }

    static public void storeCookies(Header[] headers)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cookies",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putInt("cookiesSize", headers.length); 
    	for (int i = 0; i < headers.length; i++) {
            String cookie = headers[i].getValue();
            editor.putString("cookie" + i, cookie);
        }
    	editor.commit();
    }
    
    static public boolean getAndSetCookies()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cookies",Context.MODE_PRIVATE);
    	int cookiesSize = preferences.getInt("cookiesSize", 0);
    	HashMap<String, String> cookieContainer = MemoryHandler.getInstance().getCookieContainer();
    	for(int i=0; i<cookiesSize; i++)
    	{
    		String cookie = preferences.getString("cookie" + i, null);
            String[] cookievalues = cookie.split(";");
            for (int j = 0; j < cookievalues.length; j++) {
                String[] keyPair = cookievalues[j].split("=");
                String key = keyPair[0].trim();
                String value = keyPair.length > 1 ? keyPair[1].trim() : "";
                cookieContainer.put(key, value);
            }
    	}
    	if(cookieContainer.size() != 0)
    	{
    		MemoryHandler.getInstance().setCookieContainer(cookieContainer);
    		
    		return true;
    	}
    	
    	return false;
    }
    
    static public void clearBabies()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("babies",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.clear();
    	editor.commit();
    }
    
    static public void storeBabies(String babyData)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("babies",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putString("babyData", babyData); 
    	editor.commit();
    }
    
    static public void getAndSetBabies()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("babies",Context.MODE_PRIVATE);
    	String babyData = preferences.getString("babyData", null);
    	MemoryHandler.getInstance().setKey("baby", babyData);
    }
    
    static public void clearCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.clear();
    	editor.commit();
    }
    
    static public void storeCahce(String cacheData)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putString("cacheData", cacheData); 
    	editor.commit();
    }
    
    static public String getCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("cache",Context.MODE_PRIVATE);
    	String cacheData = preferences.getString("cacheData", null);

    	return cacheData;
    }
    
    static public void clearFolloweeCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("followeeCache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.clear();
    	editor.commit();
    }
    
    static public void storeFolloweeCahce(String cacheData)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("followeeCache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putString("cacheData", cacheData); 
    	editor.commit();
    }
    
    static public String getFolloweeCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("followeeCache",Context.MODE_PRIVATE);
    	String cacheData = preferences.getString("cacheData", null);

    	return cacheData;
    }
    
    static public void clearFansCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("fansCache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.clear();
    	editor.commit();
    }
    
    static public void storeFansCahce(String cacheData)
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("fansCache",Context.MODE_PRIVATE);
    	Editor editor=preferences.edit(); 
    	editor.putString("cacheData", cacheData); 
    	editor.commit();
    }
    
    static public String getFansCache()
    {
    	SharedPreferences preferences = Utils.application.getSharedPreferences("fansCache",Context.MODE_PRIVATE);
    	String cacheData = preferences.getString("cacheData", null);

    	return cacheData;
    }
    
    static public String GetStoreFile(Context context) {
        try {
            FileInputStream fileInput = context.openFileInput(storeFile);
            InputStreamReader isr = new InputStreamReader(fileInput);
            char[] inputBuffer = new char[256];
            String s = "";
            int charRead;
            while ((charRead = isr.read(inputBuffer)) > 0) {
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                s += readString;
                inputBuffer = new char[256];
            }
            return s;
        } catch (Exception e) {
            return "";
        }
    }

    static public void SetStoreFile(Context context, String content) {
        try {
            FileOutputStream fileOut = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fileOut);
            osw.write(content);
            osw.flush();
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static public String GetGuideFile(Context context) {
        try {
            FileInputStream fileInput = context.openFileInput(guideFile);
            InputStreamReader isr = new InputStreamReader(fileInput);
            char[] inputBuffer = new char[256];
            String s = "";
            int charRead;
            while ((charRead = isr.read(inputBuffer)) > 0) {
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                s += readString;
                inputBuffer = new char[256];
            }
            return s;
        } catch (Exception e) {
            return "";
        }
    }

    static public void SetGuideFile(Context context, String content) {
        try {
            FileOutputStream fileOut = context.openFileOutput(guideFile, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fileOut);
            osw.write(content);
            osw.flush();
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
