package com.bebeanan.perfectbaby;

import java.util.HashMap;

import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

/**
 * Created by KiteXu.
 */
public class DetailSystemActivity extends Activity {
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_system);
        boolean bHasUrl = this.getIntent().hasExtra("url");
        if (bHasUrl) {
            String url = this.getIntent().getStringExtra("url");
            WebView webView = (WebView) findViewById(R.id.detail_webview);
            webView.loadUrl(url);
        } else {
            String id = this.getIntent().getStringExtra("id");
            WebView webView = (WebView) findViewById(R.id.detail_webview);
            HashMap<String, Object> IpInfo = MemoryHandler.getInstance().getHost();
            if (IpInfo.get("IP") == null) {
                webView.loadUrl(NetHandler.BAKIP + "/system-event/" + id);
            } else {
                webView.loadUrl(IpInfo.get("IP").toString() + "/system-event/" + id);
            }
        }


        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });

        ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
        rightButton.setVisibility(View.GONE);
    }
}
