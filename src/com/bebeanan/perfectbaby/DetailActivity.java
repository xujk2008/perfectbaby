package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;

import com.bebeanan.perfectbaby.common.CustomMediaPlayer;
import com.bebeanan.perfectbaby.common.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class DetailActivity extends Activity {

	int weight = 100;
	int height = 80;
	int marginTop = 20;
	int finalWeight;
	int finalHeight;
	int finalMarginTop;
	private List<String> urlList;
	private int totalUrl = 0;
	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	private PullToRefreshScrollView detailScrollView;

	private Handler thirdLoginHandler;

	private String babyId, baby, dateStr = "";
	private String oldestDate, latestDate;
	
	private int[] imageIds = { R.id.detail_photo_image_single,
			R.id.detail_photo_image1, R.id.detail_photo_image2,
			R.id.detail_photo_image3, R.id.detail_photo_image4,
			R.id.detail_photo_image5, R.id.detail_photo_image6,
			R.id.detail_photo_image7, R.id.detail_photo_image8,
			R.id.detail_photo_image9 };
	
	private ArrayList<String> imageResourceList = null;
	
	private Handler voiceHandler = null;
	private Runnable voiceRunnable = null;

	/**
	 * Create a File for saving an image or video
	 */
	private static File getOutputMediaFile(String filename) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ filename);

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	private void getEvents(String uri, final String currentDateStr, final boolean scrollToTop) {
		NetHandler handler = new NetHandler(DetailActivity.this, NetHandler.METHOD_GET, uri,
				new LinkedList<BasicNameValuePair>(), null) {
			@Override
			public void handleRsp(Message msg) {
				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				if (code == 200) {
					detailScrollView.onRefreshComplete();
					showView(bundle.getString("data"), currentDateStr, scrollToTop);
				} else {
					dialog.dismiss();
					showBuilder = false;
					toast.cancel();
					toast = Toast.makeText(getApplicationContext(), "下载失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		};
		handler.start();
	}

	private void showView(String data, String currentDateStr, boolean scrollToTop) {

		LinearLayout eventLayout = (LinearLayout) findViewById(R.id.detail_layout);
		TextView dayText = new TextView(DetailActivity.this);
		dayText.setTypeface(Utils.typeface);
		dayText.setTextSize(15 * Utils.density);
		dayText.setTextColor(getResources().getColor(R.color.brown));
		dayText.setText(getDayNumber(currentDateStr));
		Log.v("Kite", "detail events is " + data);
		try {
			JSONArray eventArray = new JSONArray(data);
			
			if(!scrollToTop && eventArray.length() != 0)
			{
				eventLayout.addView(dayText);
			}
			
			for (int i = 0; i < eventArray.length(); i++) {
				final JSONObject event = eventArray.getJSONObject(i);
				int type = event.getInt("type");
				int tag = 0;
				if(event.has("tag"))
				{
					tag = event.getInt("tag");
				}
				
				switch (type) {
				case 1: {
					LayoutInflater inflater = getLayoutInflater();
					LinearLayout layout = (LinearLayout) inflater.inflate(
							R.layout.detail_system, null);

					layout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							Intent intent = new Intent();
							try {
								intent.putExtra("id", event.getString("id"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							intent.setClass(DetailActivity.this,
									DetailSystemActivity.class);
							startActivity(intent);
							overridePendingTransition(
									R.anim.activity_open_in_anim,
									R.anim.activity_open_out_anim);
						}
					});
					layout.setOnLongClickListener(new View.OnLongClickListener() {
						@Override
						public boolean onLongClick(View view) {
							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(
									DetailActivity.this);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											if (i == 0) {
												String title = "";
												try {
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												Platform weibo = ShareSDK
														.getPlatform(
																DetailActivity.this,
																SinaWeibo.NAME);
												weibo.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WEIBO SHARE CANCEL");
													}
												});
												weibo.share(sp);
											} else if (i == 1) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												QZone.ShareParams sp = new QZone.ShareParams();
												sp.setTitle("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												HashMap<String, Object> IpInfo = MemoryHandler
														.getInstance()
														.getHost();
												if (IpInfo.get("IP") == null) {
													sp.setTitleUrl(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
													sp.setSiteUrl(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												} else {
													sp.setTitleUrl(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
													sp.setSiteUrl(IpInfo.get(
															"IP").toString()
															+ "/system-event/"
															+ id);
												}
												Platform qzone = ShareSDK
														.getPlatform(
																DetailActivity.this,
																QZone.NAME);
												qzone.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"QZONE SHARE CANCEL");
													}
												});
												qzone.share(sp);
											} else if (i == 2) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												Wechat.ShareParams sp = new Wechat.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												/*
												 * HashMap<String, Object>
												 * IpInfo =
												 * MemoryHandler.getInstance
												 * ().getHost(); if
												 * (IpInfo.get("IP") == null) {
												 * sp
												 * .setTitleUrl(NetHandler.BAKIP
												 * + "/system-event/" + id); }
												 * else {
												 * sp.setTitleUrl(IpInfo.get
												 * ("IP").toString() +
												 * "/system-event/" + id); }
												 */
												sp.shareType = Platform.SHARE_IMAGE;
												Platform wechat = ShareSDK
														.getPlatform(
																DetailActivity.this,
																Wechat.NAME);
												wechat.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WECHAT SHARE CANCEL");
													}
												});
												wechat.share(sp);
											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();
							return true;
						}
					});
					TextView textView = (TextView) layout
							.findViewById(R.id.detail_content_textview);
					textView.setTypeface(Utils.typeface);
					textView.setText(event.getString("text"));
					ImageView photoView = (ImageView) layout
							.findViewById(R.id.detail_content_photo);

					if (scrollToTop) {
						eventLayout.addView(layout, 0);
					} else {
						eventLayout.addView(layout);
					}

					if (event.has("urls")
							&& event.getJSONArray("urls").length() > 0) {

						String originalUrl = event.getJSONArray("urls")
								.getString(0);

						photoView.setVisibility(View.VISIBLE);
						new UrlImageViewHelper().setUrlDrawable(photoView, originalUrl);

					}
					else
					{
						photoView.setVisibility(View.GONE);
						LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)textView.getLayoutParams();
						params.gravity = Gravity.CENTER_VERTICAL;
						textView.setLayoutParams(params);
					}
					
					break;
				}
				case 2: {
					LayoutInflater inflater = getLayoutInflater();
					RelativeLayout layout = (RelativeLayout) inflater.inflate(
							R.layout.detail_photo, null);
					ImageView tagView = (ImageView) layout
							.findViewById(R.id.detail_content_tag);
					if (tag == 0) {
						tagView.setVisibility(View.GONE);
					} else if ((tag & 1) != 0) {
						tagView.setImageResource(R.drawable.event_milk_pic);
					} else if ((tag & 2) != 0) {
						tagView.setImageResource(R.drawable.event_feed_pic);
					} else if ((tag & 4) != 0) {
						tagView.setImageResource(R.drawable.event_liquid_pic);
					} else if ((tag & 8) != 0) {
						tagView.setImageResource(R.drawable.event_napkin_pic);
					} else if ((tag & 16) != 0) {
						tagView.setImageResource(R.drawable.event_sleep_pic);
					}
					layout.setOnLongClickListener(new View.OnLongClickListener() {
						@Override
						public boolean onLongClick(View view) {
							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(
									DetailActivity.this);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											if (i == 0) {
												String title = "";
												try {
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												Platform weibo = ShareSDK
														.getPlatform(
																DetailActivity.this,
																SinaWeibo.NAME);
												weibo.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WEIBO SHARE CANCEL");
													}
												});
												weibo.share(sp);
											} else if (i == 1) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												QZone.ShareParams sp = new QZone.ShareParams();
												sp.setTitle("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												HashMap<String, Object> IpInfo = MemoryHandler
														.getInstance()
														.getHost();
												if (IpInfo.get("IP") == null) {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												} else {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												}
												Platform qzone = ShareSDK
														.getPlatform(
																DetailActivity.this,
																QZone.NAME);
												qzone.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"QZONE SHARE CANCEL");
													}
												});
												qzone.share(sp);
											} else if (i == 2) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												Wechat.ShareParams sp = new Wechat.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												sp.shareType = Platform.SHARE_IMAGE;
												Platform wechat = ShareSDK
														.getPlatform(
																DetailActivity.this,
																Wechat.NAME);
												wechat.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WECHAT SHARE CANCEL");
													}
												});
												wechat.share(sp);
											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();
							return true;
						}
					});
					TextView textView = (TextView) layout
							.findViewById(R.id.detail_content_textview);
					textView.setTypeface(Utils.typeface);
					textView.setText(event.getString("text"));
					
					JSONArray urlArray = event.getJSONArray("urls");
					imageResourceList = new ArrayList<String>();
					for(int j=0; j<urlArray.length(); j++)
					{
						imageResourceList.add(urlArray.getString(j));
					}
					final ArrayList<String> imageUrlList = imageResourceList;
					
					List<ImageView> imageViewList = new ArrayList<ImageView>();
					for (int j = 0; j < 10; j++) {
						ImageView curImageView = (ImageView) layout.findViewById(imageIds[j]);
						curImageView.setVisibility(View.GONE);
						imageViewList.add(curImageView);
					}
					
					if (imageUrlList.size() == 1) {
						imageViewList.get(0).setVisibility(View.VISIBLE);
						imageViewList.get(0).setTag(0);
						
						new UrlImageViewHelper(true, Utils.density, 100)
								.setUrlDrawable(imageViewList.get(0),
										imageUrlList.get(0));
						
						imageViewList.get(0).setOnClickListener(new OnClickListener(){

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								
								Intent intent = new Intent(DetailActivity.this,
										GalleryActivity.class);
								intent.putStringArrayListExtra("imageResourceList",
										imageUrlList);
								intent.putExtra("imageIndex", 0);

								DetailActivity.this.startActivity(intent);
								DetailActivity.this.overridePendingTransition(
										R.anim.zoom_in, 0);
							}
							
						});
					}
					else {
						for (int j = 0, k = 1; j < imageUrlList.size()
								&& k < imageViewList.size(); j++, k++) {
							imageViewList.get(k).setVisibility(View.VISIBLE);
							imageViewList.get(k).setTag(j);

							new UrlImageViewHelper().setUrlDrawable(
									imageViewList.get(k), imageUrlList.get(j));
							
							final int currentUrlIndex = j;
							imageViewList.get(k).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									
									Intent intent = new Intent(DetailActivity.this,
											GalleryActivity.class);
									intent.putStringArrayListExtra("imageResourceList",
											imageUrlList);
									intent.putExtra("imageIndex", currentUrlIndex);

									DetailActivity.this.startActivity(intent);
									DetailActivity.this.overridePendingTransition(
											R.anim.zoom_in, 0);
									
								}
							});
						}
					}
					
					if (scrollToTop) {
						eventLayout.addView(layout, 0);
					} else {
						eventLayout.addView(layout);
					}

					break;
				}
				case 4: {
					LayoutInflater inflater = getLayoutInflater();
					RelativeLayout layout = (RelativeLayout) inflater.inflate(
							R.layout.detail_video, null);
					ImageView tagView = (ImageView) layout
							.findViewById(R.id.detail_content_tag);
					if (tag == 0) {
						tagView.setVisibility(View.GONE);
					} else if ((tag & 1) != 0) {
						tagView.setImageResource(R.drawable.event_milk_pic);
					} else if ((tag & 2) != 0) {
						tagView.setImageResource(R.drawable.event_feed_pic);
					} else if ((tag & 4) != 0) {
						tagView.setImageResource(R.drawable.event_liquid_pic);
					} else if ((tag & 8) != 0) {
						tagView.setImageResource(R.drawable.event_napkin_pic);
					} else if ((tag & 16) != 0) {
						tagView.setImageResource(R.drawable.event_sleep_pic);
					}
					layout.setOnLongClickListener(new View.OnLongClickListener() {
						@Override
						public boolean onLongClick(View view) {
							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(
									DetailActivity.this);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											if (i == 0) {
												String title = "";
												try {
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												Platform weibo = ShareSDK
														.getPlatform(
																DetailActivity.this,
																SinaWeibo.NAME);
												weibo.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WEIBO SHARE CANCEL");
													}
												});
												weibo.share(sp);
											} else if (i == 1) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												QZone.ShareParams sp = new QZone.ShareParams();
												sp.setTitle("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												HashMap<String, Object> IpInfo = MemoryHandler
														.getInstance()
														.getHost();
												if (IpInfo.get("IP") == null) {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												} else {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												}
												Platform qzone = ShareSDK
														.getPlatform(
																DetailActivity.this,
																QZone.NAME);
												qzone.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"QZONE SHARE CANCEL");
													}
												});
												qzone.share(sp);
											} else if (i == 2) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												Wechat.ShareParams sp = new Wechat.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												sp.shareType = Platform.SHARE_IMAGE;
												Platform wechat = ShareSDK
														.getPlatform(
																DetailActivity.this,
																Wechat.NAME);
												wechat.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WECHAT SHARE CANCEL");
													}
												});
												wechat.share(sp);
											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();
							return true;
						}
					});
					TextView textView = (TextView) layout
							.findViewById(R.id.detail_content_textview);
					textView.setTypeface(Utils.typeface);
					textView.setText(event.getString("text"));

					if (scrollToTop) {
						eventLayout.addView(layout, 0);
					} else {
						eventLayout.addView(layout);
					}

					final String originalUrl = event.getJSONArray("urls")
							.getString(0);
					
					RelativeLayout videoLayout = (RelativeLayout) layout.findViewById(R.id.detail_video_layout);
					videoLayout.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(DetailActivity.this, VideoActivity.class);
							intent.putExtra("videoUrl", originalUrl);
							DetailActivity.this.startActivity(intent);
						}
						
					});
					
					break;
				}
				case 8: {
					LayoutInflater inflater = getLayoutInflater();
					final RelativeLayout layout = (RelativeLayout) inflater.inflate(
							R.layout.detail_voice, null);
					ImageView tagView = (ImageView) layout
							.findViewById(R.id.detail_content_tag);
					if (tag == 0) {
						tagView.setVisibility(View.GONE);
					} else if ((tag & 1) != 0) {
						tagView.setImageResource(R.drawable.event_milk_pic);
					} else if ((tag & 2) != 0) {
						tagView.setImageResource(R.drawable.event_feed_pic);
					} else if ((tag & 4) != 0) {
						tagView.setImageResource(R.drawable.event_liquid_pic);
					} else if ((tag & 8) != 0) {
						tagView.setImageResource(R.drawable.event_napkin_pic);
					} else if ((tag & 16) != 0) {
						tagView.setImageResource(R.drawable.event_sleep_pic);
					}
					layout.setOnLongClickListener(new View.OnLongClickListener() {
						@Override
						public boolean onLongClick(View view) {
							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(
									DetailActivity.this);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											if (i == 0) {
												String title = "";
												try {
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												Platform weibo = ShareSDK
														.getPlatform(
																DetailActivity.this,
																SinaWeibo.NAME);
												weibo.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WEIBO SHARE CANCEL");
													}
												});
												weibo.share(sp);
											} else if (i == 1) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												QZone.ShareParams sp = new QZone.ShareParams();
												sp.setTitle("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												HashMap<String, Object> IpInfo = MemoryHandler
														.getInstance()
														.getHost();
												if (IpInfo.get("IP") == null) {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												} else {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												}
												Platform qzone = ShareSDK
														.getPlatform(
																DetailActivity.this,
																QZone.NAME);
												qzone.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"QZONE SHARE CANCEL");
													}
												});
												qzone.share(sp);
											} else if (i == 2) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												Wechat.ShareParams sp = new Wechat.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												sp.shareType = Platform.SHARE_IMAGE;
												Platform wechat = ShareSDK
														.getPlatform(
																DetailActivity.this,
																Wechat.NAME);
												wechat.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WECHAT SHARE CANCEL");
													}
												});
												wechat.share(sp);
											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();
							return true;
						}
					});
					
					final String originalUrl = event.getJSONArray("urls")
							.getString(0);
					
					if (scrollToTop) {
						eventLayout.addView(layout, 0);
					} else {
						eventLayout.addView(layout);
					}
					
					TextView textView = (TextView) layout
							.findViewById(R.id.detail_content_textview);
					textView.setTypeface(Utils.typeface);
					textView.setText(event.getString("text"));
					
					TextView timeView = (TextView) layout.findViewById(R.id.detail_voice_time);
					timeView.setText("");
					
					final DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) layout.findViewById(R.id.detail_voice_button);
					voiceButton
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {

									NetHandler handler = new NetHandler(DetailActivity.this, NetHandler.METHOD_DOWNLOAD_FILE,
											originalUrl, null, null) {
										@Override
										public void handleRsp(Message msg) {
											Bundle bundle = msg.getData();
											int code = bundle.getInt("code");
											if (code == 200) {
												String data = bundle.getString("data");
												String fileName = bundle.getString("uri");
												fileName = fileName
														.substring(fileName.lastIndexOf('/') + 1);

												try {
													File file = getOutputMediaFile(fileName);
													FileOutputStream fop = new FileOutputStream(file);
													fop.write(data.getBytes("ISO-8859-1"), 0,
															data.getBytes("ISO-8859-1").length);
													fop.flush();
													fop.close();
												} catch (Exception e) {
													e.printStackTrace();
												}

												Log.v("Kite", "file path is "
														+ getOutputMediaFile(fileName).getAbsolutePath());

												final SeekBar voiceSeekBar = (SeekBar)layout.findViewById(R.id.detail_voice_seekbar);
												final TextView voiceTime = (TextView)layout.findViewById(R.id.detail_voice_time);
												updateTime(voiceTime, 0);
												
												voiceButton.sourceFile = fileName;
												{
													voiceButton.mediaPlayer = new CustomMediaPlayer();
													try {
														voiceButton.mediaPlayer
																.setDataSource(getOutputMediaFile(
																		voiceButton.sourceFile).getPath());
														voiceButton.mediaPlayer.prepare();
														voiceSeekBar.setMax(voiceButton.mediaPlayer
																.getDuration());
														final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
														voiceHandler = new Handler();
														voiceRunnable = new Runnable() {

															int count = 10;
															int currentTime = 0;
															
															@Override
															public void run() {
																// TODO Auto-generated method stub
																count--;
																if(count == 0)
																{
																	currentTime++;
																	updateTime(voiceTime, currentTime);
																	
																	count = 10;
																}
																
																voiceSeekBar.setProgress(voiceMediaPlayer
																		.getCurrentPosition());
																voiceHandler.postDelayed(this, 100);
															}
														};

														voiceHandler.removeCallbacks(voiceRunnable);
														voiceHandler.postDelayed(voiceRunnable, 100);

														voiceButton.mediaPlayer.start();
														voiceButton.mediaPlayer
																.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
																	@Override
																	public void onCompletion(
																			MediaPlayer mediaPlayer) {

																		voiceSeekBar.setProgress(0);
																		voiceHandler
																				.removeCallbacks(voiceRunnable);
																		updateTime(voiceTime, 0);
																		voiceButton.setChecked(true);

																		mediaPlayer.release();
																		mediaPlayer = null;
																	}
																});
													} catch (IOException e) {
														e.printStackTrace();
														Log.e("DBG", "播放失败");
													}
													voiceButton.setChecked(false);

												}

												voiceButton.setOnClickListener(new View.OnClickListener() {
													@Override
													public void onClick(View view) {
														if (!voiceButton.isChecked()) {
															voiceButton.mediaPlayer = new CustomMediaPlayer();
															try {
																voiceButton.mediaPlayer
																		.setDataSource(getOutputMediaFile(
																				voiceButton.sourceFile).getPath());
																voiceButton.mediaPlayer.prepare();
																voiceSeekBar.setMax(voiceButton.mediaPlayer
																		.getDuration());
																final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
																voiceHandler = new Handler();
																voiceRunnable = new Runnable() {

																	int count = 10;
																	int currentTime = 0;
																	
																	@Override
																	public void run() {
																		// TODO Auto-generated method stub
																		count--;
																		if(count == 0)
																		{
																			currentTime++;
																			updateTime(voiceTime, currentTime);
																			
																			count = 10;
																		}
																		
																		voiceSeekBar.setProgress(voiceMediaPlayer
																				.getCurrentPosition());
																		voiceHandler.postDelayed(this, 100);
																	}
																};

																voiceHandler.removeCallbacks(voiceRunnable);
																voiceHandler.postDelayed(voiceRunnable, 100);

																voiceButton.mediaPlayer.start();
																voiceButton.mediaPlayer
																		.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
																			@Override
																			public void onCompletion(
																					MediaPlayer mediaPlayer) {

																				voiceSeekBar.setProgress(0);
																				voiceHandler
																						.removeCallbacks(voiceRunnable);
																				updateTime(voiceTime, 0);
																				voiceButton.setChecked(true);

																				mediaPlayer.release();
																				mediaPlayer = null;
																			}
																		});
															} catch (IOException e) {
																e.printStackTrace();
																Log.e("DBG", "播放失败");
															}
															voiceButton.setChecked(false);

														} else {
															
															voiceHandler
															.removeCallbacks(voiceRunnable);
															voiceSeekBar.setProgress(0);
															updateTime(voiceTime, 0);
															
															if (voiceButton.mediaPlayer != null) {
																voiceButton.mediaPlayer.release();
															}
															voiceButton.mediaPlayer = null;
															voiceButton.setChecked(true);
														}
													}
												});

											} else {
												Log.v("DBG", "Download " + bundle.getString("uri")
														+ " failed");
											}

										}
									};
									handler.start();
								
								}
							});
	            
					break;
				}
				case 16: {
					LayoutInflater inflater = getLayoutInflater();
					RelativeLayout layout = (RelativeLayout) inflater.inflate(
							R.layout.detail_note, null);
					ImageView tagView = (ImageView) layout
							.findViewById(R.id.detail_content_tag);
					if (tag == 0) {
						tagView.setVisibility(View.GONE);
					} else if ((tag & 1) != 0) {
						tagView.setImageResource(R.drawable.event_milk_pic);
					} else if ((tag & 2) != 0) {
						tagView.setImageResource(R.drawable.event_feed_pic);
					} else if ((tag & 4) != 0) {
						tagView.setImageResource(R.drawable.event_liquid_pic);
					} else if ((tag & 8) != 0) {
						tagView.setImageResource(R.drawable.event_napkin_pic);
					} else if ((tag & 16) != 0) {
						tagView.setImageResource(R.drawable.event_sleep_pic);
					}
					layout.setOnLongClickListener(new View.OnLongClickListener() {
						@Override
						public boolean onLongClick(View view) {
							final CharSequence[] items = { "新浪微博", "QQ空间",
									"微信", "取消" };
							builder = new AlertDialog.Builder(
									DetailActivity.this);
							builder.setTitle("分享");
							builder.setItems(items,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialogInterface,
												int i) {
											if (i == 0) {
												String title = "";
												try {
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												Platform weibo = ShareSDK
														.getPlatform(
																DetailActivity.this,
																SinaWeibo.NAME);
												weibo.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WEIBO SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WEIBO SHARE CANCEL");
													}
												});
												weibo.share(sp);
											} else if (i == 1) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												QZone.ShareParams sp = new QZone.ShareParams();
												sp.setTitle("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												HashMap<String, Object> IpInfo = MemoryHandler
														.getInstance()
														.getHost();
												if (IpInfo.get("IP") == null) {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												} else {
													sp.setSite(NetHandler.BAKIP
															+ "/system-event/"
															+ id);
												}
												Platform qzone = ShareSDK
														.getPlatform(
																DetailActivity.this,
																QZone.NAME);
												qzone.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"QZONE SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"QZONE SHARE CANCEL");
													}
												});
												qzone.share(sp);
											} else if (i == 2) {
												String title = "";
												String id = "";
												try {
													id = event.getString("id");
													title = event
															.getString("text");
												} catch (Exception e) {
													e.printStackTrace();
												}
												Wechat.ShareParams sp = new Wechat.ShareParams();
												sp.setText("我在”完美宝贝“发现好东西了！【"
														+ title + "】");
												Bitmap bitmap = BitmapFactory
														.decodeResource(
																getResources(),
																R.drawable.login_logo);
												sp.setImageData(bitmap);
												sp.shareType = Platform.SHARE_IMAGE;
												Platform wechat = ShareSDK
														.getPlatform(
																DetailActivity.this,
																Wechat.NAME);
												wechat.setPlatformActionListener(new PlatformActionListener() {
													@Override
													public void onComplete(
															Platform platform,
															int i,
															HashMap<String, Object> stringObjectHashMap) {
														Message msg = new Message();
														msg.arg1 = 1;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE DONE");
													}

													@Override
													public void onError(
															Platform platform,
															int i,
															Throwable throwable) {
														throwable
																.printStackTrace();
														Message msg = new Message();
														msg.arg1 = 2;
														msg.arg2 = i;
														msg.obj = platform;
														thirdLoginHandler
																.sendMessage(msg);
														Log.v("DBG",
																"WECHAT SHARE ERROR");
													}

													@Override
													public void onCancel(
															Platform platform,
															int i) {
														Log.v("DBG",
																"WECHAT SHARE CANCEL");
													}
												});
												wechat.share(sp);
											} else {
												dialog.dismiss();
											}
										}
									});
							dialog = builder.show();
							return true;
						}
					});
					TextView textView = (TextView) layout
							.findViewById(R.id.detail_content_textview);
					textView.setText(event.getString("text"));

					break;
				}
				}
			}
			
			if (scrollToTop && eventArray.length() != 0) {
				detailScrollView.scrollTo(0, detailScrollView.getTop());
				eventLayout.addView(dayText, 0);
			} 
			
		} catch (Exception e) {
			eventLayout.removeView(dayText);
			e.printStackTrace();
		}

	}

	private void updateTime(TextView recordTimeView, int recordTime) {
		int minute = recordTime / 60;
		int second = recordTime % 60;
		recordTimeView.setText(String.format("%02d:%02d", minute, second));
	}
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		float density = getResources().getDisplayMetrics().density;
		finalWeight = (int) (weight * density);
		finalHeight = (int) (height * density);
		finalMarginTop = (int) (marginTop * density);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
		builder = new AlertDialog.Builder(DetailActivity.this);
		builder.setCancelable(false);

		thirdLoginHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				int id = msg.arg1;
				if (id == 1) {
					toast.cancel();
					toast = Toast.makeText(getApplicationContext(), "分享成功",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else if (id == 2) {
					toast.cancel();
					toast = Toast.makeText(getApplicationContext(), "分享失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		};

		oldestDate = latestDate = this.getIntent().getStringExtra("date");

		urlList = new LinkedList<String>();
		MemoryHandler.getInstance().clearCount();

		detailScrollView = (PullToRefreshScrollView) findViewById(R.id.detail_scroll_view);
		detailScrollView.setMode(Mode.BOTH);
		detailScrollView
				.setOnRefreshListener(new OnRefreshListener2<ScrollView>() {

					@Override
					public void onPullDownToRefresh(
							PullToRefreshBase<ScrollView> refreshView) {
						// TODO Auto-generated method stub

						String nextDayUri = "/feeds/nextday?";
						try {
							nextDayUri += URLEncoder.encode("{\"baby\":\""
									+ babyId + "\",\"day\":\"" + latestDate
									+ "\"}", "UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						NetHandler dateHandler = new NetHandler(DetailActivity.this, 
								NetHandler.METHOD_GET, nextDayUri,
								new LinkedList<BasicNameValuePair>(), null) {

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub
								Bundle bundle = msg.getData();
								int code = bundle.getInt("code");

								if (code == 200) {
									String data = bundle.getString("data");
									try {
										JSONObject dateObject = new JSONObject(
												data);
										String nextDay = dateObject
												.getString("day");

										String uri = "/feeds?";
										
										try {
											uri += URLEncoder.encode(
													"{\"baby\":\""
															+ babyId
															+ "\",\"day\":\""
															+ nextDay
															+ "\"}", "UTF-8");
										} catch (UnsupportedEncodingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										latestDate = nextDay;

										getEvents(uri, nextDay, true);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									detailScrollView.onRefreshComplete();
								}
							}
						};
						dateHandler.start();
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<ScrollView> refreshView) {
						// TODO Auto-generated method stub

						String prevDayUri = "/feeds/prevday?";
						try {
							prevDayUri += URLEncoder.encode("{\"baby\":\""
									+ babyId + "\",\"day\":\"" + oldestDate
									+ "\"}", "UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						NetHandler dateHandler = new NetHandler(DetailActivity.this, 
								NetHandler.METHOD_GET, prevDayUri,
								new LinkedList<BasicNameValuePair>(), null) {

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub
								Bundle bundle = msg.getData();
								int code = bundle.getInt("code");

								if (code == 200) {
									String data = bundle.getString("data");
									try {
										JSONObject dateObject = new JSONObject(
												data);
										String prevDay = dateObject
												.getString("day");

										String uri = "/feeds?";
										
										try {
											uri += URLEncoder.encode(
													"{\"baby\":\""
															+ babyId
															+ "\",\"day\":\""
															+ prevDay
															+ "\"}", "UTF-8");
										} catch (UnsupportedEncodingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										oldestDate = prevDay;

										getEvents(uri, prevDay, false);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									detailScrollView.onRefreshComplete();
								}
							}
						};
						dateHandler.start();
					}
				});

		ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
		backButton.setImageResource(R.drawable.title_back_pic);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}
		});

		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);

		babyId = this.getIntent().getStringExtra("babyid");
		StoreHandler.getAndSetBabies();
		baby = MemoryHandler.getInstance().getKey("baby");
//		dateStr = "";
//
//		try {
//			JSONArray babyArray = new JSONArray(baby);
//			for (int i = 0; i < babyArray.length(); i++) {
//				JSONObject object = babyArray.getJSONObject(i);
//				if (object.getString("id").equals(babyId)) {
//					dateStr = object.getString("birthday");
//					Log.v("DBG", "dateStr " + dateStr);
//					Log.v("DBG",
//							"dateStr1 "
//									+ this.getIntent().getStringExtra("date"));
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//					Date date = sdf.parse(dateStr);
//					Date date1 = sdf.parse(this.getIntent().getStringExtra(
//							"date"));
//					Calendar calendar = Calendar.getInstance();
//					Calendar calendar1 = Calendar.getInstance();
//					calendar1.setTime(calendar.getTime());
//					calendar1.setTime(date1);
//					calendar.setTime(date);
//					Long time = (calendar1.getTimeInMillis() - calendar
//							.getTimeInMillis()) / 1000 / 60 / 60 / 24;
//					dateStr = "第" + String.valueOf(time) + "天";
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
//		dateStr = getDayNumber(this.getIntent().getStringExtra(
//				"date"));
//
//		TextView dateText = (TextView) findViewById(R.id.detail_date);
//		dateText.setText(dateStr);

		// showView();
		dateStr = this.getIntent().getStringExtra(
				"date");
		getEvents(getIntent().getStringExtra("uri"), dateStr, true);

	}

	private String getDayNumber(String currentDateStr)
	{

		JSONArray babyArray;
		try {
			babyArray = new JSONArray(baby);
			
			for (int i = 0; i < babyArray.length(); i++) {
				JSONObject object = babyArray.getJSONObject(i);
				if (object.getString("id").equals(babyId)) {
					dateStr = object.getString("birthday");
					Log.v("DBG", "dateStr " + dateStr);
					Log.v("DBG",
							"dateStr1 "
									+ this.getIntent().getStringExtra("date"));
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date date = sdf.parse(dateStr);
					Date date1 = sdf.parse(currentDateStr);
					
					Calendar calendar = Calendar.getInstance();
					Calendar calendar1 = Calendar.getInstance();
					calendar1.setTime(calendar.getTime());
					calendar1.setTime(date1);
					calendar.setTime(date);
					Long time = (calendar1.getTimeInMillis() - calendar
							.getTimeInMillis()) / 1000 / 60 / 60 / 24;
					dateStr = "第" + String.valueOf(time) + "天";
					
					return dateStr;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			overridePendingTransition(R.anim.activity_close_in_anim,
					R.anim.activity_close_out_anim);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		for (int i = 0; i != urlList.size(); i++) {
			File file = getOutputMediaFile(urlList.get(i));
			file.delete();
		}
		super.onDestroy();
	}
}