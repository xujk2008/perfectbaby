package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.RotatePhoto;
import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class AddBabyActivity extends Activity {

	private RelativeLayout add_baby_layout;
	
	private RelativeLayout flipper;

	private TextView birthday1;
	private ImageView shield1;

	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	private String baby;
	private JSONArray babyArray;

	private String avatarUrl = "";

	private static int height = 215;
	private static int width = 215;

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE = 2000;

	private static File getOutputMediaFile(int type, int number) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + String.valueOf(number) + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + String.valueOf(number) + ".mp4");
		} else {
			return null;
		}

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_baby);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		dm = this.getApplicationContext().getResources().getDisplayMetrics();
		height = (int) (180 * dm.density);
		width = (int) (180 * dm.density);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
		builder = new AlertDialog.Builder(AddBabyActivity.this);
		builder.setCancelable(false);

		StoreHandler.getAndSetBabies();
		baby = MemoryHandler.getInstance().getKey("baby");
		Log.v("Kite", "babyArray is " + baby);
		try {
			babyArray = new JSONArray(baby);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		add_baby_layout = (RelativeLayout) findViewById(R.id.add_baby_layout);
		
		LayoutInflater inflater = getLayoutInflater();

		flipper = (RelativeLayout) inflater.inflate(R.layout.baby_detail, null);

		{
			LinearLayout slideSwitch1 = (LinearLayout) flipper
					.findViewById(R.id.baby_gender);
			initGender(slideSwitch1);
			TextView babyText1 = (TextView) flipper
					.findViewById(R.id.baby_text);
			babyText1.setTypeface(Utils.typeface);
			shield1 = (ImageView) flipper.findViewById(R.id.baby_shield);
			birthday1 = (TextView) flipper
					.findViewById(R.id.baby_birthday_edittext);
			birthday1.setTypeface(Utils.typeface);
			birthday1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					String dateStr = birthday1.getText().toString();
					Calendar calendar = Calendar.getInstance();
					try {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Date date = sdf.parse(dateStr);
						calendar.setTime(calendar.getTime());
					} catch (Exception e) {
						e.printStackTrace();
					}
					DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker datePicker, int year,
								int month, int dayOfMonth) {
							birthday1.setText(String.format("%04d-%02d-%02d",
									year, month + 1, dayOfMonth));

							initShield(birthday1.getText().toString(), shield1);
						}
					};
					DatePickerDialog datePickerDialog = new DatePickerDialog(
							AddBabyActivity.this, dateListener, calendar
									.get(Calendar.YEAR), calendar
									.get(Calendar.MONDAY), calendar
									.get(Calendar.DAY_OF_MONTH));
					datePickerDialog.setCancelable(false);
					datePickerDialog.show();
				}
			});

		}

		{
			ImageView process = (ImageView) flipper
					.findViewById(R.id.baby_process);
			process.setImageResource(R.drawable.baby_detail_1_1);
			ImageView avatarButton = (ImageView) flipper
					.findViewById(R.id.baby_add_photo);
			avatarButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					final CharSequence[] items = { "相册", "拍照", "取消" };
					builder = new AlertDialog.Builder(AddBabyActivity.this);
					builder.setTitle("选择图片");
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialogInterface, int i) {
									if (i == 1) {
										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										File file = getOutputMediaFile(
												MEDIA_TYPE_IMAGE, 0); // create
																		// a
																		// file
																		// to
																		// save
																		// the
																		// image
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(file)); // set the
																		// image
																		// file
																		// name

										// start the image capture Intent
										startActivityForResult(intent,
												CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
									} else if (i == 0) {
										// Intent getImage = new Intent(
										// Intent.ACTION_GET_CONTENT);
										// getImage.addCategory(Intent.CATEGORY_OPENABLE);
										// getImage.setType("image/jpeg");
										// startActivityForResult(getImage,
										// GET_IMAGE_ACTIVITY_REQUEST_CODE_1);
										Intent intent = new Intent(
												Intent.ACTION_PICK,
												android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
										startActivityForResult(intent,
												GET_IMAGE_ACTIVITY_REQUEST_CODE);
									} else {
										dialog.dismiss();
									}
								}
							});
					dialog = builder.show();
				}
			});
		}

		add_baby_layout.addView(flipper);
		
		ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
		backButton.setImageResource(R.drawable.title_back_pic);
		backButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddBabyActivity.this.setResult(RESULT_CANCELED);
				AddBabyActivity.this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
			}
			
		});
		
		ImageView addButton = (ImageView) findViewById(R.id.title_right_button);
		addButton.setVisibility(View.VISIBLE);
		addButton.setImageResource(R.drawable.baby_done_selector);
		addButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				EditText name = (EditText) flipper
						.findViewById(R.id.baby_name_edittext);
//				name.setTypeface(Utils.typeface);
				
				TextView birthday = (TextView) flipper
						.findViewById(R.id.baby_birthday_edittext);
				
				if (birthday.getText().toString().isEmpty()) {
					toast.cancel();
					toast = Toast.makeText(getApplicationContext(), "信息不完整",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					return;
				}
				
				if(shield1.getVisibility() == View.GONE)
				{
					if (name.getText().toString().isEmpty()) {
						toast.cancel();
						toast = Toast.makeText(getApplicationContext(), "信息不完整",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						return;
					}
				}

				try {
					builder.setMessage("上传中");
					dialog = builder.show();
					showBuilder = true;

					JSONObject babyObject1 = new JSONObject();
					LinearLayout slideSwitch = (LinearLayout) flipper
							.findViewById(R.id.baby_gender);
					// initGender(slideSwitch);
					babyObject1.put("gender", (Integer) (slideSwitch.getTag()));
					birthday = (TextView) flipper
							.findViewById(R.id.baby_birthday_edittext);
					birthday.setTypeface(Utils.typeface);
					babyObject1.put("birthday", birthday.getText().toString());
					name = (EditText) flipper
							.findViewById(R.id.baby_name_edittext);
//					name.setTypeface(Utils.typeface);
					String nameStr;
					if (name.getText().toString().equals("")) {
						nameStr = " ";
					} else {
						nameStr = name.getText().toString();
					}
					babyObject1.put("name", nameStr);
					babyObject1.put("avatar", avatarUrl);

					String method = NetHandler.METHOD_POST;
					String uri = "/baby";

					NetHandler handlerBaby1 = new NetHandler(
							AddBabyActivity.this, method, uri,
							new LinkedList<BasicNameValuePair>(), babyObject1) {
						@Override
						public void handleRsp(Message msg) {
							Bundle bundleBaby1 = msg.getData();
							int codeBaby1 = bundleBaby1.getInt("code");
							if (codeBaby1 == 200) {
								try {
									JSONObject baby1 = new JSONObject(
											bundleBaby1.getString("data"));
									babyArray.put(baby1);
								} catch (Exception e) {
									e.printStackTrace();
								}

								dialog.dismiss();
								showBuilder = false;
								toast.cancel();
								Log.v("Kite", "babyArray is " + babyArray.toString());
								StoreHandler.storeBabies(babyArray.toString());
								MemoryHandler.getInstance().setKey("baby",
										babyArray.toString());
								toast = Toast.makeText(getApplicationContext(),
										"保存成功", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();

								Intent intent = AddBabyActivity.this
										.getIntent();

								setResult(RESULT_OK);
								finish();

							} else {
								dialog.dismiss();
								showBuilder = false;
								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"保存失败", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();

								Log.v("Kite", bundleBaby1.getString("data"));
							}
						}
					};
					handlerBaby1.start();

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	private void initGender(final LinearLayout view) {
		RelativeLayout femaleLayout = (RelativeLayout) view
				.findViewById(R.id.baby_detail_female_layout);
		RelativeLayout maleLayout = (RelativeLayout) view
				.findViewById(R.id.baby_detail_male_layout);

		final ImageView femaleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_female_chooser);
		final ImageView maleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_male_chooser);

		femaleChooser.setVisibility(View.INVISIBLE);
		maleChooser.setVisibility(View.VISIBLE);
		view.setTag(1);

		femaleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.VISIBLE);
				maleChooser.setVisibility(View.INVISIBLE);

				view.setTag(2);
			}

		});

		maleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.INVISIBLE);
				maleChooser.setVisibility(View.VISIBLE);

				view.setTag(1);
			}

		});
	}

	private void initShield(String dateStr, View shieldView) {
		if (dateStr == null) {
			shieldView.setVisibility(View.GONE);

			return;
		}

		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		String curDateStr = dFormat.format(date);

		if (curDateStr.compareTo(dateStr) < 0) {
			shieldView.setVisibility(View.VISIBLE);
		} else {
			shieldView.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, int resultCode,
			Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {

				builder = new AlertDialog.Builder(AddBabyActivity.this);
				builder.setMessage("上传中");
				dialog = builder.show();
				showBuilder = true;

				String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE,
						requestCode - CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
						.getAbsolutePath();
				Bitmap bitmap = null;
				try {
					BitmapFactory.Options opts = new BitmapFactory.Options();
					opts.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(filename, opts);
					int srcWidth = opts.outWidth;
					int srcHeight = opts.outHeight;
					int desWidth = 0;
					int desHeight = 0;
					// 缩放比例
					double ratio = 0.0;
					int more = 0;
					if (srcWidth > srcHeight) {
						ratio = srcWidth / 640;
						desWidth = 640;
						desHeight = (int) (srcHeight / ratio);
						more = srcWidth % 640 == 0 ? 0 : 1;
					} else {
						ratio = srcHeight / 640;
						desHeight = 640;
						desWidth = (int) (srcWidth / ratio);
						more = srcHeight % 640 == 0 ? 0 : 1;
					}
					// 设置输出宽度、高度
					BitmapFactory.Options newOpts = new BitmapFactory.Options();
					newOpts.inSampleSize = (int) (ratio) + more;
					newOpts.inJustDecodeBounds = false;
					// newOpts.outWidth = desWidth;
					// newOpts.outHeight = desHeight;
					// bitmap = BitmapFactory.decodeFile(filename, newOpts);
					bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
					FileOutputStream out = new FileOutputStream(filename);
					if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
						out.flush();
						out.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
				param.add(new BasicNameValuePair("filename",
						getOutputMediaFile(
								MEDIA_TYPE_IMAGE,
								requestCode
										- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
								.getAbsolutePath()));
				param.add(new BasicNameValuePair("filetype", "image"));
				NetHandler handler = new NetHandler(AddBabyActivity.this,
						NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
					@Override
					public void handleRsp(Message msg) {
						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						if (code == 200) {

							String data = bundle.getString("data");
							try {
								JSONArray array = new JSONArray(data);
								avatarUrl = array.getJSONObject(0).getString(
										"url");
							} catch (Exception e) {
								e.printStackTrace();
							}

							ImageView avatarButton;
							avatarButton = (ImageView) flipper
									.findViewById(R.id.baby_add_photo);
							File fileName = getOutputMediaFile(
									MEDIA_TYPE_IMAGE,
									requestCode
											- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
							try {
								Log.v("DBG",
										String.format("%d %d", height, width));
								BitmapFactory.Options options = new BitmapFactory.Options();
								options.inJustDecodeBounds = true;

								BitmapFactory.decodeFile(fileName.getPath(),
										options);
								int heightRatio = (int) Math
										.floor(options.outHeight
												/ (float) height);
								int widthRatio = (int) Math
										.floor(options.outWidth / (float) width);

								Log.v("HEIGHRATIO", "" + heightRatio);
								Log.v("WIDTHRATIO", "" + widthRatio);

								if (heightRatio > 1 && widthRatio > 1) {
									options.inSampleSize = heightRatio > widthRatio ? heightRatio
											: widthRatio;
								}
								options.inJustDecodeBounds = false;
								Bitmap bit = BitmapFactory.decodeFile(
										fileName.getPath(), options);

								Bitmap output = Bitmap.createBitmap(width,
										height, Bitmap.Config.ARGB_8888);
								Canvas canvas = new Canvas(output);

								final int color = 0xff424242;
								final Paint paint = new Paint();
								final Rect rect = new Rect(0, 0, width, height);
								final RectF rectF = new RectF(rect);
								final float roundPx = (float) width / 2.0f;

								paint.setAntiAlias(true);
								canvas.drawARGB(0, 0, 0, 0);

								paint.setColor(color);
								canvas.drawRoundRect(rectF, roundPx, roundPx,
										paint);
								paint.setXfermode(new PorterDuffXfermode(
										PorterDuff.Mode.SRC_IN));
								canvas.drawBitmap(bit, rect, rect, paint);

								// options.inSampleSize = 4;
								avatarButton.setImageBitmap(output);
							} catch (Exception e) {
								e.printStackTrace();
							}

							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"上传完成", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;

						} else {
							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"上传失败", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;
						}
					}
				};
				handler.start();

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the image capture
			} else {
				// Image capture failed, advise user
			}
		}
		if (requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				if (data != null) {
					Log.v("DBG", "Data: " + data.toString());
					Uri originalUri = data.getData();
					String[] proj = { MediaStore.Images.Media.DATA };
					Cursor actualimagecursor = managedQuery(originalUri, proj,
							null, null, null);
					int actual_image_column_index = actualimagecursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					actualimagecursor.moveToFirst();
					String img_path = actualimagecursor
							.getString(actual_image_column_index);
					File file = new File(img_path);
					Log.v("DBG", "Get File Path " + file.getAbsolutePath());

					String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE,
							requestCode - GET_IMAGE_ACTIVITY_REQUEST_CODE)
							.getAbsolutePath();
					Bitmap bitmap = null;
					try {
						BitmapFactory.Options opts = new BitmapFactory.Options();
						BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
						int srcWidth = opts.outWidth;
						int srcHeight = opts.outHeight;
						int desWidth = 0;
						int desHeight = 0;
						int more = 0;
						// 缩放比例
						double ratio = 0.0;
						if (srcWidth > srcHeight) {
							ratio = srcWidth / 640;
							desWidth = 640;
							desHeight = (int) (srcHeight / ratio);
							more = srcWidth % 640 == 0 ? 0 : 1;
						} else {
							ratio = srcHeight / 640;
							desHeight = 640;
							desWidth = (int) (srcWidth / ratio);
							more = srcHeight % 640 == 0 ? 0 : 1;
						}
						// 设置输出宽度、高度
						BitmapFactory.Options newOpts = new BitmapFactory.Options();
						newOpts.inSampleSize = (int) (ratio) + more;
						newOpts.inJustDecodeBounds = false;
						newOpts.outWidth = desWidth;
						newOpts.outHeight = desHeight;
						// bitmap =
						// BitmapFactory.decodeFile(file.getAbsolutePath(),
						// newOpts);
						bitmap = RotatePhoto.getPhotoRotated(
								file.getAbsolutePath(), newOpts);
						FileOutputStream out = new FileOutputStream(filename);
						if (bitmap
								.compress(Bitmap.CompressFormat.JPEG, 50, out)) {
							out.flush();
							out.close();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					builder = new AlertDialog.Builder(AddBabyActivity.this);
					builder.setMessage("上传中");
					dialog = builder.show();
					showBuilder = true;
					List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
					param.add(new BasicNameValuePair("filename",
							getOutputMediaFile(
									MEDIA_TYPE_IMAGE,
									requestCode
											- GET_IMAGE_ACTIVITY_REQUEST_CODE)
									.getAbsolutePath()));
					param.add(new BasicNameValuePair("filetype", "image"));
					NetHandler handler = new NetHandler(AddBabyActivity.this,
							NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
						@Override
						public void handleRsp(Message msg) {
							dialog.dismiss();
							showBuilder = false;

							Bundle bundle = msg.getData();
							int code = bundle.getInt("code");
							if (code == 200) {

								String data = bundle.getString("data");
								try {
									JSONArray array = new JSONArray(data);
									avatarUrl = array.getJSONObject(0)
											.getString("url");
								} catch (Exception e) {
									e.printStackTrace();
								}

								ImageView avatarButton;
								avatarButton = (ImageView) flipper
										.findViewById(R.id.baby_add_photo);
								File fileName = getOutputMediaFile(
										MEDIA_TYPE_IMAGE,
										requestCode
												- GET_IMAGE_ACTIVITY_REQUEST_CODE);
								try {
									Log.v("DBG", String.format("%d %d", height,
											width));
									BitmapFactory.Options options = new BitmapFactory.Options();
									options.inJustDecodeBounds = true;

									BitmapFactory.decodeFile(
											fileName.getPath(), options);
									int heightRatio = (int) Math
											.floor(options.outHeight
													/ (float) height);
									int widthRatio = (int) Math
											.floor(options.outWidth
													/ (float) width);

									Log.v("HEIGHRATIO", "" + heightRatio);
									Log.v("WIDTHRATIO", "" + widthRatio);

									Bitmap bit;
									if (heightRatio == 0 && widthRatio == 0) {
										BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
										options.inJustDecodeBounds = false;
										Bitmap originalBmp = BitmapFactory
												.decodeFile(fileName.getPath(),
														enlargeOptions);

										Matrix matrix = new Matrix();

										int enlargeWidthRatio = (int) (width / originalBmp
												.getWidth()) + 2;
										int enlargeHeightRatio = (int) (height / originalBmp
												.getHeight()) + 2;
										int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio) ? enlargeWidthRatio
												: enlargeHeightRatio;

										matrix.postScale(enlargeRatio,
												enlargeRatio); // 长和宽放大缩小的比例
										bit = Bitmap.createBitmap(originalBmp,
												0, 0, originalBmp.getWidth(),
												originalBmp.getHeight(),
												matrix, true);
									} else {
										if (heightRatio > 1 && widthRatio > 1) {
											options.inSampleSize = heightRatio > widthRatio ? heightRatio
													: widthRatio;
											bit = BitmapFactory
													.decodeFile(
															fileName.getPath(),
															options);
										} else {
											bit = BitmapFactory
													.decodeFile(fileName
															.getPath());
										}

									}

									Bitmap output = Bitmap.createBitmap(width,
											height, Bitmap.Config.ARGB_8888);
									Canvas canvas = new Canvas(output);

									final int color = 0xff424242;
									final Paint paint = new Paint();
									final Rect rect = new Rect(0, 0, width,
											height);
									final RectF rectF = new RectF(rect);
									final float roundPx = (float) width / 2.0f;

									paint.setAntiAlias(true);
									canvas.drawARGB(0, 0, 0, 0);

									paint.setColor(color);
									canvas.drawRoundRect(rectF, roundPx,
											roundPx, paint);
									paint.setXfermode(new PorterDuffXfermode(
											PorterDuff.Mode.SRC_IN));
									canvas.drawBitmap(bit, rect, rect, paint);

									// options.inSampleSize = 4;
									avatarButton.setImageBitmap(output);
								} catch (Exception e) {
									Log.v("Kite", "exception: " + e);
									e.printStackTrace();
								}

								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"上传完成", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								return;

							} else {
								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"上传失败", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();

								return;
							}
						}
					};
					handler.start();
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
