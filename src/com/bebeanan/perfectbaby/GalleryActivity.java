package com.bebeanan.perfectbaby;

import java.io.File;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.zxing.view.HackyViewPager;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class GalleryActivity extends FragmentActivity{

	private HackyViewPager gallery_pager;
	private ArrayList<String> imageResourceList;
	private int imageIndex;

	private ImageView gallery_option;
	private String imageUrl;
	
	private static final int[] INDICATOR_ID = { R.id.dot1, R.id.dot2,
			R.id.dot3, R.id.dot4, R.id.dot5, R.id.dot6, R.id.dot7, R.id.dot8,
			R.id.dot9 };
	private TextView[] PagerIndicator = new TextView[9];
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		
		gallery_pager = (HackyViewPager)findViewById(R.id.gallery_pager);
		gallery_option = (ImageView) findViewById(R.id.gallery_option);
		
		boolean deleteOption = GalleryActivity.this.getIntent().getBooleanExtra("deleteOption", false);
		
		if(!deleteOption)
		{
			gallery_option.setImageResource(R.drawable.ic_menu_save);
			gallery_option.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.v("Kite", "imageUrl is " + imageUrl);
					
					NetHandler handler = new NetHandler(GalleryActivity.this,
							NetHandler.METHOD_DOWNLOAD_FILE, imageUrl,
							null, null) {
						@Override
						public void handleRsp(Message msg) {
							Bundle bundle = msg.getData();
							int code = bundle.getInt("code");
							if (code == 200) {
								String data = bundle.getString("data");
								try {
									BitmapFactory.Options options = new BitmapFactory.Options();
									options.inJustDecodeBounds = false;
									Bitmap bitmap = BitmapFactory
											.decodeByteArray(
													data.getBytes("ISO-8859-1"),
													0,
													data.getBytes("ISO-8859-1").length,
													options);
									
									ContentResolver cr = GalleryActivity.this.getContentResolver();
									String savedImageUriString = MediaStore.Images.Media.insertImage(cr, bitmap, imageUrl.substring(imageUrl.lastIndexOf("/")), "");
									Log.v("Kite", "savedImageUri is " + savedImageUriString);
//									GalleryActivity.this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));
									MediaScannerConnection.scanFile(GalleryActivity.this, new String[] { Environment.getExternalStorageDirectory().toString() }, null, new MediaScannerConnection.OnScanCompletedListener() {
							            /*
							             *   (non-Javadoc)
							             * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
							             */
							            public void onScanCompleted(String path, Uri uri) 
							              {
							                  Log.i("ExternalStorage", "Scanned " + path + ":");
							                  Log.i("ExternalStorage", "-> uri=" + uri);
							              }
							            });
									
									String savedImagePath = "";
									if (savedImageUriString != null) 
									{
										Uri savedImageUri = Uri
												.parse(savedImageUriString);
										savedImagePath = getFilePathFromContentUri(
												savedImageUri, cr);

										Intent intent = new Intent(
												Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
										Uri uri = Uri.fromFile(new File(
												savedImagePath));
										intent.setData(uri);
										GalleryActivity.this
												.sendBroadcast(intent);
									}
									
									Toast toast = Toast.makeText(getApplicationContext(),
											"已成功保存至" + savedImagePath, Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									
								} catch (Exception e) {
									e.printStackTrace();
									
									Toast toast = Toast.makeText(getApplicationContext(),
											"保存失败" + e.toString(), Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
								}
							}
							else
							{
								Toast toast = Toast.makeText(getApplicationContext(),
										"保存失败" + bundle.getString("data"), Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						}
					};
					handler.start();
				}
				
			});
		}
		else
		{
			gallery_option.setImageResource(R.drawable.ic_menu_delete);
			gallery_option.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					Intent intent = new Intent();
					intent.putExtra("imageIndex", imageIndex);
					GalleryActivity.this.setResult(RESULT_OK, intent);
					GalleryActivity.this.finish();
				}
				
			});
		}
		
		for(int i=0; i<INDICATOR_ID.length; i++)
		{
			PagerIndicator[i] = (TextView)findViewById(INDICATOR_ID[i]);
		}
		
		initView();
	}
	
	private void initView()
	{
		imageResourceList = getIntent().getStringArrayListExtra("imageResourceList");
		imageIndex = getIntent().getIntExtra("imageIndex", -1);
		
		for (TextView indicator : PagerIndicator) {
			indicator.setVisibility(View.GONE);
		}

		for (int i = 0; i < imageResourceList.size(); i++) {
			PagerIndicator[i].setVisibility(View.VISIBLE);
			PagerIndicator[i].setTextColor(0xff666666);

			if (i == imageIndex) {
				PagerIndicator[i].setTextColor(0xffffffff);
				imageUrl = imageResourceList.get(i);
			}
		}

		gallery_pager.setAdapter(new MyPagerAdapter(GalleryActivity.this.getSupportFragmentManager(),
				imageResourceList, imageIndex));
		gallery_pager.setCurrentItem(imageIndex);
		gallery_pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageSelected(int index) {
				// TODO Auto-generated method stub
				for (int i = 0; i < imageResourceList.size(); i++) {
					PagerIndicator[i].setVisibility(View.VISIBLE);
					PagerIndicator[i].setTextColor(0xff666666);

					if (i == index) {
						PagerIndicator[i].setTextColor(0xffffffff);
						imageUrl = imageResourceList.get(i);
						imageIndex = i;
					}
				}
			}

		});

	}
	
	private class MyPagerAdapter extends FragmentPagerAdapter {

		private ArrayList<String> imageList;
		private int imagePosition;
		
		public MyPagerAdapter(FragmentManager fragmentManager, ArrayList<String> imageList, int imagePosition) {
			super(fragmentManager);
			// TODO Auto-generated constructor stub
			
			this.imageList = imageList;
			this.imagePosition = imagePosition;
		}

		@Override
		public Fragment getItem(int index) {
			// TODO Auto-generated method stub
			return new GalleryFragment(imageList.get(index));
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imageList.size();
		}

	}

	public static String getFilePathFromContentUri(Uri fileUri,  
            ContentResolver contentResolver) {  
		
		if(fileUri == null)
		{
			return null;
		}
		
        String filePath;  
        String[] filePathColumn = {MediaColumns.DATA};  
  
        Cursor cursor = contentResolver.query(fileUri, filePathColumn, null, null, null);  
//      也可用下面的方法拿到cursor  
//      Cursor cursor = this.context.managedQuery(selectedVideoUri, filePathColumn, null, null, null);  
          
        cursor.moveToFirst();  
  
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);  
        filePath = cursor.getString(columnIndex);  
        cursor.close();  
        return filePath;  
    }  
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			setResult(RESULT_CANCELED);
			GalleryActivity.this.finish();
			overridePendingTransition(0, R.anim.zoom_out);
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	
}
