package com.bebeanan.perfectbaby;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by KiteXu.
 */
public class RegisterItemActivity extends Activity {

    private ImageView nextButton;
    private ImageView agreeButton;
    private TextView register_item_content;
    
    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_item);

        nextButton = (ImageView) findViewById(R.id.register_item_next_button);

        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setTypeface(Utils.typeface);
        titleView.setText(R.string.register_item_title);

        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.id.title_back_button);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        TextView title_text = (TextView) findViewById(R.id.title_text);
        title_left.setVisibility(View.GONE);
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);

        register_item_content = (TextView) findViewById(R.id.register_item_content);
//        register_item_content.setTypeface(Utils.typeface);
        
        agreeButton = (ImageView) findViewById(R.id.register_item_agree_button);
        agreeButton.setTag(1);
        agreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	
            	int tag = (Integer) agreeButton.getTag();
            	
            	if(tag == 0)
            	{
            		agreeButton.setBackgroundResource(R.drawable.register_item_agree_checked);
            		agreeButton.setTag(1);
            		
            		nextButton.setBackgroundResource(R.drawable.register_item_next_checked);
            		nextButton.setClickable(true);
            	}
            	else if(tag == 1)
            	{
            		agreeButton.setBackgroundResource(R.drawable.register_item_agree_unchecked);
            		agreeButton.setTag(0);
            		
            		nextButton.setBackgroundResource(R.drawable.register_item_next_unchecked);
            		nextButton.setClickable(false);
            	}
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(RegisterItemActivity.this, RegisterAddActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_in_anim, R.anim.activity_open_out_anim);
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
