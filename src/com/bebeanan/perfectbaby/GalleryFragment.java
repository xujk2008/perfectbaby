package com.bebeanan.perfectbaby;

import java.io.File;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class GalleryFragment extends Fragment{

	private Context context;
	private String imageUri;
	private PhotoViewAttacher mAttacher;
	
	@Override
	public void onResume() {
	    super.onResume();
	    MobclickAgent.onPageStart("gallery"); //统计页面
	}
	@Override
	public void onPause() {
	    super.onPause();
	    MobclickAgent.onPageEnd("gallery"); 
	}
	
	public GalleryFragment(String imageUrl)
	{
		this.imageUri = imageUrl;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		context = GalleryFragment.this.getActivity();
		
		PhotoView image = new PhotoView(context);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		image.setLayoutParams(params);
		image.setScaleType(ScaleType.FIT_CENTER);
		
		mAttacher = new PhotoViewAttacher(image);
		
		if(imageUri.contains("http"))
		{
//			new UrlImageViewHelper().setUrlDrawable(image, imageUri);
			
			final ProgressBar progress = new ProgressBar(context);
			progress.setIndeterminate(true);
			
	        Ion.with(this)
	        .load(imageUri)
	        .setLogging("DeepZoom", Log.VERBOSE)
	        .withBitmap()
	        .deepZoom()
	        .intoImageView(image)
	        .setCallback(new FutureCallback<ImageView>() {
	            @Override
	            public void onCompleted(Exception e, ImageView result) {
	                progress.setVisibility(View.GONE);
	                mAttacher.update();
	            }
	        });
		}
		else
		{
//			File fileName = getOutputMediaFile(Integer.parseInt(imageUri));
            Bitmap bit = BitmapFactory.decodeFile(imageUri);
            image.setImageBitmap(bit);
		}
		
		LinearLayout layout = new LinearLayout(context);
		layout.setGravity(Gravity.CENTER);
		layout.addView(image);
		
		return layout;
	}
	
	private static File getOutputMediaFile(int number) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + String.valueOf(number) + ".jpg");

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}
}
