package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengDialogButtonListener;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UpdateStatus;

/**
 * Created by KiteXu.
 */
public class AboutActivity extends Activity {
	
	private TextView about_version;
	private ImageView about_update, about_qa, about_intro;
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UmengUpdateAgent.update(AboutActivity.this);
        setContentView(R.layout.activity_about);

        if(Utils.application == null)
		{
			Utils.init(AboutActivity.this);
		}
        
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.about_title);
        titleView.setTypeface(Utils.typeface);

        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
        backButton.setImageResource(R.drawable.title_back_pic);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);
        
        about_version = (TextView) findViewById(R.id.about_version);
//        about_version.setTypeface(Utils.typeface);
        
        about_update = (ImageView) findViewById(R.id.about_update);
        
        about_qa = (ImageView) findViewById(R.id.about_qa);
        
        about_intro = (ImageView) findViewById(R.id.about_intro);
     
        initView();
    }
    
    private void initView()
    {
    	about_update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				UmengUpdateAgent.setDefault();
				UmengUpdateAgent.forceUpdate(AboutActivity.this);
				UmengUpdateAgent.setDialogListener(new UmengDialogButtonListener(){

					@Override
					public void onClick(int status) {
						// TODO Auto-generated method stub
						switch (status) {
				        case UpdateStatus.Update:

				        	StoreHandler.clearBabies();
				        	StoreHandler.clearCookies();
				        	MemoryHandler.getInstance().clearAll();
				        	SharedPreferences userPreferences = Utils.application.getSharedPreferences("userInfo",Context.MODE_PRIVATE);
				        	Editor editor = userPreferences.edit();
				        	editor.clear();
				        	editor.commit();
				        	
				            break;
				        case UpdateStatus.Ignore:
				            
				            break;
				        case UpdateStatus.NotNow:

				        	break;
				        }
					}
					
				});
			}
		});
    	
    	about_intro.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(AboutActivity.this, IntroActivity.class);
				AboutActivity.this.startActivity(intent);
			}
        	
        });
    	
    	about_qa.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(AboutActivity.this, QaActivity.class);
				AboutActivity.this.startActivity(intent);
			}
        	
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
