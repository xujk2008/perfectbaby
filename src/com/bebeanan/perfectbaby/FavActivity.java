package com.bebeanan.perfectbaby;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.perfectbaby.adapters.MainListAdapter;
import com.perfectbaby.adapters.MainListAdapter.OnDeleteImageClickListener;
import com.perfectbaby.adapters.MainListAdapter.OnTouchingZoomInListener;
import com.perfectbaby.adapters.MainListAdapter.OnVideoClickListener;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class FavActivity extends Activity{
	private ImageView title_left_button, title_right_button;
	private TextView title_text;

	private PullToRefreshListView fav_list;
	private List<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
	private MainListAdapter mAdapter;

	private boolean hasMorePages;
//	private String since = null;
	private int skip;

	private ProgressBar fav_progress;

	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	private SharedPreferences userPreferences;

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public static final int EVENT_SYSTEM = 1;
	public static final int EVENT_VOICE = 8;
	public static final int EVENT_NOTE = 16;
	public static final int EVENT_PHOTO = 2;
	public static final int EVENT_VIDEO = 4;

	private boolean isLoading = false;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fav);

		if(Utils.application == null)
		{
			Utils.init(FavActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		title_left_button = (ImageView) findViewById(R.id.title_left_button);
		title_right_button = (ImageView) findViewById(R.id.title_right_button);
		title_text = (TextView) findViewById(R.id.title_text);

		fav_list = (PullToRefreshListView) findViewById(R.id.fav_list);

		fav_progress = (ProgressBar) findViewById(R.id.fav_progress);
		fav_progress.setVisibility(View.VISIBLE);

		initTitle();

		initView();

		// builder = new AlertDialog.Builder(FavActivity.this);
		// builder.setMessage("加载中");
		// dialog = builder.show();
		// showBuilder = true;

		hasMorePages = true;
		skip = 0;

		refresh(false, 0);
	}

	private void initView() {

		fav_list.setMode(Mode.BOTH);
		ILoadingLayout RefreshLabels = fav_list.getLoadingLayoutProxy();
		RefreshLabels.setTextTypeface(Utils.typeface);
		fav_list.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				hasMorePages = true;
				skip = 0;

				refresh(false, 0);
			}

			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub

				if (hasMorePages) {
					refresh(true, mData.size() + 1);
				} else {
					Toast toast = Toast.makeText(FavActivity.this, "没有更多了",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					Runnable runnable = new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							fav_list.onRefreshComplete();
						}

					};

					Handler handler = new Handler();
					handler.postDelayed(runnable, 1000);
				}
			}

		});


		fav_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Utils.feedDetail = mData.get(position-1);
				
				Intent intent = new Intent(FavActivity.this,
						FeedDetailActivity.class);
				intent.putExtra("feedId", (String)mData.get(position-1).get("feedId"));

				FavActivity.this.startActivity(intent);
			}

		});

		fav_list.setSelected(true);

	}

	private void refresh(boolean getMore, final int selectionAfterLoad) {

		if (!isLoading) {
			isLoading = true;

			if (!getMore) {
//				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//				NetHandler bannerHandler = new NetHandler(FavActivity.this, 
//						NetHandler.METHOD_GET, "/banner", param, null) {
//
//					@Override
//					public void handleRsp(Message msg) {
//						// TODO Auto-generated method stub
//						Bundle bundle = msg.getData();
//						int code = bundle.getInt("code");
//						if (code == 200) {
//							String data = bundle.getString("data");
//							Log.v("Kite", "banner is " + data);
//							try {
//								JSONArray bannerArray = new JSONArray(data);
//								JSONObject banner = bannerArray
//										.getJSONObject(0);
//								Map<String, Object> temp = new HashMap<String, Object>();
//								temp.put(MainListAdapter.HEAD_BACKGROUND,
//										banner.getString("image"));
//								temp.put(MainListAdapter.HEAD_BACKGROUND_URL,
//										banner.getString("url"));
//								temp.put(MainListAdapter.HEAD_NICKNAME,
//										userPreferences.getString("nickName",
//												""));
//								temp.put(MainListAdapter.HEAD_AVATAR,
//										userPreferences.getString("avatarUrl",
//												null));
//								mData.add(temp);
//
//								Editor editor = userPreferences.edit();
//								editor.putString("headBackground",
//										banner.getString("image"));
//								editor.putString("headBackgroundUrl",
//										banner.getString("url"));
//								editor.commit();
//
//								getFeeds(false, selectionAfterLoad, temp);
//
//							} catch (JSONException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						} else {
//							fav_list.onRefreshComplete();
//							// dialog.dismiss();
//							// showBuilder = false;
//							fav_progress.setVisibility(View.GONE);
//
//							Toast.makeText(FavActivity.this, "加载失败",
//									Toast.LENGTH_SHORT).show();
//
//							return;
//						}
//					}
//				};
//
//				bannerHandler.start();
				
				getFeeds(false, selectionAfterLoad);
			} else {
				getFeeds(true, selectionAfterLoad);
			}
		}
	}

	private void getFeeds(final boolean getMore, final int selectionAfterLoad) {

		List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
		param.add(new BasicNameValuePair("userId", userPreferences.getString(
				"userId", "")));
		param.add(new BasicNameValuePair("include", "user,comment,like"));
		param.add(new BasicNameValuePair("$limit", "10"));
		param.add(new BasicNameValuePair("$skip", skip+""));
		
		JSONObject paramObject = new JSONObject();

		JSONObject sortObject = new JSONObject();
		try {
			sortObject.put("createdAt", -1);
			paramObject.put("$sort", sortObject);
			paramObject.put("userId", userPreferences.getString(
					"userId", ""));
			paramObject.put("include", "user,comment,like");
			paramObject.put("$limit", "10");
			paramObject.put("$skip", skip+"");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String encodeParam = new String();
		try {
			encodeParam = URLEncoder.encode(paramObject.toString(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		NetHandler hanlder = new NetHandler(FavActivity.this, NetHandler.METHOD_GET,
				"/favorfeeds?"+encodeParam, new LinkedList<BasicNameValuePair>(), null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				// dialog.dismiss();
				// showBuilder = false;
				fav_progress.setVisibility(View.GONE);

				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				Log.v("Kite", "fav code is " + code);

				fav_list.onRefreshComplete();

				if (code == 200) {
					String data = bundle.getString("data");
					Log.v("Kite", "fav data is " + data);

					try {
						JSONArray feedsArray = new JSONArray(data);

						if (feedsArray.length() < 10) {
							hasMorePages = false;
						}
						ArrayList<Map<String, Object>> refreshData = null;
						for (int i = 0; i < feedsArray.length(); i++) {
							JSONObject feed = feedsArray.getJSONObject(i).getJSONObject("feed");
							Log.v("Kite", "fav feed object is " + feed);

							skip++;
							
							if(feed.length() == 0)
							{
								continue;
							}
							
							Map<String, Object> temp = new HashMap<String, Object>();

							temp.put("favId", feedsArray.getJSONObject(i).getString("id"));
							temp.put("feedId", feed.getString("id"));

							JSONObject owner = null;
							try
							{
								owner = feed.getJSONObject("owner");
							}catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								
								continue;
							}
							
							if (owner.has("avatar")) {
								temp.put(MainListAdapter.AVATAR,
										owner.getString("avatar"));
							} else {
								temp.put(MainListAdapter.AVATAR, "");
							}
							temp.put(MainListAdapter.NICKNAME,
									owner.getString("nickname"));
							temp.put(MainListAdapter.FEEDS_OWNER_ID,
									owner.getString("id"));
							temp.put(MainListAdapter.USER_TYPE,
									owner.getInt("usertype"));

							double createdAt = feedsArray.getJSONObject(i).getDouble("createdAt");
							DecimalFormat df = new DecimalFormat("0.000");

							String time = getTime(createdAt);
							temp.put(MainListAdapter.TIME, time);

							if (feed.has("location")) {
								temp.put(MainListAdapter.LOCATION,
										feed.getString("location"));
							} else {
								temp.put(MainListAdapter.LOCATION, "");
							}

							if (feed.has("tag")) {
								temp.put(MainListAdapter.EVENT_TAG,
										feed.getInt("tag"));
								Log.v("Kite", "has tag: " + feed.getInt("tag")
										+ " pos is " + i);
							} else {
								temp.put(MainListAdapter.EVENT_TAG, 0);
								Log.v("Kite", "no tag pos is " + i);
							}

							temp.put(MainListAdapter.CONTENT,
									feed.getString("text"));
							if (feed.has("shareCount")) {
								temp.put(MainListAdapter.SHARE_NUMBER,
										feed.getInt("shareCount"));
							} else {
								temp.put(MainListAdapter.SHARE_NUMBER, 0);
							}

							temp.put(MainListAdapter.UP_NUMBER,
									feed.getInt("upCount"));
							temp.put(MainListAdapter.COMMENT_NUMBER,
									feed.getInt("commentCount"));

							List<Map<String, String>> upList = new ArrayList<Map<String, String>>();
							temp.put(MainListAdapter.UP_LIST, upList);

							List<Map<String, String>> comments = new ArrayList<Map<String, String>>();
							temp.put(MainListAdapter.COMMENTS, comments);

							int type = feed.getInt("type");
							temp.put(MainListAdapter.TYPE, type);
							Log.v("Kite", "i is " + i + " type is " + type);

							if (feed.has("urls")) {
								JSONArray urls = feed.getJSONArray("urls");

								switch (type) {
								case EVENT_PHOTO:

									List<String> imageList = new ArrayList<String>();
									for (int j = 0; j < urls.length(); j++) {
										String url = urls.getString(j);
										imageList.add(url);
										// imageList.add("http://ww1.sinaimg.cn/thumbnail/61e36371jw1elizp53a5rg206o04wb29.gif");
									}
									temp.put(MainListAdapter.IMAGE_LIST,
											imageList);

									break;

								case EVENT_VIDEO:

									String video_url = urls.getString(0);
									temp.put(MainListAdapter.VIDEO_URL,
											video_url);

									break;

								case EVENT_VOICE:

									String voice_url = urls.getString(0);
									temp.put(MainListAdapter.VOICE_URL,
											voice_url);

									break;
								}

							}

							Log.v("Kite", "array" + i);
							if (!getMore) {
								if (refreshData == null) {
									refreshData = new ArrayList<Map<String, Object>>();
								}
								refreshData.add(temp);
							} else {
								mData.add(temp);
							}
						}

						if (!getMore) {
							mData.clear();

							if(refreshData != null)
							{
								for (int i = 0; i < refreshData.size(); i++) {
									mData.add(refreshData.get(i));
								}
							}
						}

						Log.v("Kite", "mData size is " + mData.size());
						mAdapter = new MainListAdapter(FavActivity.this, mData);
						mAdapter.hideBanner();
						mAdapter.setSimpleStyle(true);
						mAdapter.setDeleteStyle(true);
						mAdapter.setOnDeleteImageClickListener(new OnDeleteImageClickListener(){

							@Override
							public void onDeleteImageClick(final int position) {
								// TODO Auto-generated method stub

								AlertDialog.Builder builder = new Builder(FavActivity.this);

								builder.setMessage("确定删除本条收藏吗?");
								builder.setTitle("提示");

								builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {

										deleteFeed(position);
									}
								});

								builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {

									}
								});

								builder.create().show();
							
							}
							
						});
						
						mAdapter.setOnTouchingZoomInListener(new OnTouchingZoomInListener() {

							@Override
							public void onTouchingZoomIn() {
								// TODO Auto-generated method stub
								int itemIndex = mAdapter.getItemIndex();
								ArrayList<String> imageResourceList = (ArrayList) mData
										.get(itemIndex).get(
												MainListAdapter.IMAGE_LIST);

								int imageIndex = mAdapter.getImageIndex();

								Intent intent = new Intent(FavActivity.this,
										GalleryActivity.class);
								intent.putStringArrayListExtra(
										"imageResourceList", imageResourceList);
								intent.putExtra("imageIndex", imageIndex);

								FavActivity.this.startActivity(intent);
								FavActivity.this.overridePendingTransition(
										R.anim.zoom_in, 0);
							}
						});

						mAdapter.setOnVideoClickListener(new OnVideoClickListener() {

							@Override
							public void onVideoClickListener(String videoUrl) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(FavActivity.this,
										VideoActivity.class);
								intent.putExtra("videoUrl", videoUrl);
								FavActivity.this.startActivity(intent);
							}
						});


						fav_list.setAdapter(mAdapter);

						fav_list.getRefreshableView().setSelection(
								selectionAfterLoad);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					Toast toast = Toast.makeText(FavActivity.this, "加载失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					return;
				}
				isLoading = false;
			}

		};

		hanlder.start();
	}

	private void initTitle() {
		title_text.setVisibility(View.VISIBLE);
		title_text.setTypeface(Utils.typeface);
		title_text.setText(userPreferences.getString("nickName", "") + "的收藏");

		title_left_button.setVisibility(View.VISIBLE);
		title_left_button.setImageResource(R.drawable.title_back_pic);
		title_left_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				FavActivity.this.finish();
			}

		});

		title_right_button.setVisibility(View.GONE);

	}

	private Date getDateFromSeconds(double seconds) {
		long millis = (long) (seconds * 1000);
		Date date = new Date(millis);

		return date;
	}

	private String getTime(double seconds) {
		Date curDate = getDateFromSeconds(System.currentTimeMillis());
		int curYear = curDate.getYear();
		int curMonth = curDate.getMonth();
		int curDay = curDate.getDay();
		int curHour = curDate.getHours();
		int curMinute = curDate.getMinutes();
		int curSecond = curDate.getSeconds();

		Date date = getDateFromSeconds(seconds);
		int year = date.getYear();
		int month = date.getMonth();
		int day = date.getDay();
		int hour = date.getHours();
		int minute = date.getMinutes();
		int second = date.getSeconds();

		if (curYear == year) {
			if (curMonth == month) {
				if (curDay == day) {
					if (curHour == hour) {
						if (curMinute == minute) {
							return "刚刚";
						}

						return (curMinute - minute) + "分钟前";
					}

					return (curHour - hour) + "小时前";
				}
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
		String formattedDate = sdf.format(date);

		return formattedDate;
	}

	private void deleteFeed(final int position)
	{
		NetHandler handler = new NetHandler(FavActivity.this, NetHandler.METHOD_DELETE, "/favorfeeds/"+mData.get(position).get("favId"), new LinkedList<BasicNameValuePair>(), null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				Bundle bundle = msg.getData();
				
				String data = bundle.getString("data");
				int code = bundle.getInt("code");
				if(code ==200)
				{
					Log.v("Kite", "删除成功");
					mData.remove(position);
					mAdapter.notifyDataSetChanged();
				}
				else
				{
					Log.v("Kite", "删除失败");
					Log.v("Kite", "delete fail because " + data);
				}
			}
			
		};
		
		handler.start();
	}
}
