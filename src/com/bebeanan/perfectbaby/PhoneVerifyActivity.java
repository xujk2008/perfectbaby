package com.bebeanan.perfectbaby;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class PhoneVerifyActivity extends Activity{

	private TextView phone_verify_title_text;
	private EditText phone_verify_number_edittext;
	private ImageButton phone_verify_send_button;
	
	private String phoneNumber;
	private String password;
	
	private OnClickListener passwordCorrectListener, passwordWrongListener;
	
	private int PHONE_VERIFY_REQUEST_CODE = 1;
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phone_verify);
		
		ImageView leftButton = (ImageView) findViewById(R.id.title_left_button);
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setImageResource(R.drawable.title_back_pic);
		leftButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PhoneVerifyActivity.this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
			}
			
		});
		
		TextView titleText = (TextView) findViewById(R.id.title_text);
		titleText.setTypeface(Utils.typeface);
		titleText.setText("身份验证");
		
		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);
		
		phoneNumber = PhoneVerifyActivity.this.getIntent().getStringExtra("phone");
		{
            String data = StoreHandler.GetStoreFile(PhoneVerifyActivity.this);
            if (!data.isEmpty()) {
                try {
                    JSONObject object = new JSONObject(data);
                    if (object.has("password")) {
                        password = object.getString("password");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
		
		phone_verify_title_text = (TextView) findViewById(R.id.phone_verify_title_text);
		phone_verify_title_text.setTypeface(Utils.typeface);
		
		phone_verify_send_button = (ImageButton) findViewById(R.id.phone_verify_send_button);
		
		class PasswordCorrectOnClickListener implements OnClickListener
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PhoneVerifyActivity.this, PhoneBindActivity.class);
				intent.putExtra("phone", phoneNumber);
				
				PhoneVerifyActivity.this.startActivityForResult(intent, PHONE_VERIFY_REQUEST_CODE);
			}
			
		}
		
		passwordCorrectListener = new PasswordCorrectOnClickListener();
		
		class PasswordWrongOnClickListener implements OnClickListener
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast toast = Toast.makeText(PhoneVerifyActivity.this, "密码错误", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			
		}
		
		passwordWrongListener = new PasswordWrongOnClickListener();
		
		phone_verify_number_edittext = (EditText) findViewById(R.id.phone_verify_number_edittext);
//		phone_verify_number_edittext.setTypeface(Utils.typeface);
		phone_verify_number_edittext.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().equals(password))
				{
					phone_verify_send_button.setClickable(true);
					phone_verify_send_button.setImageResource(R.drawable.bind_send_selector);
					phone_verify_send_button.setOnClickListener(passwordCorrectListener);
				}
				else
				{
					phone_verify_send_button.setClickable(true);
					phone_verify_send_button.setImageResource(R.drawable.bind_send_disable);
					phone_verify_send_button.setOnClickListener(passwordWrongListener);
				}
			}
			
		});
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == PHONE_VERIFY_REQUEST_CODE)
		{
			if(resultCode == RESULT_OK)
			{
				PhoneVerifyActivity.this.setResult(RESULT_OK);
				
				PhoneVerifyActivity.this.finish();
			}
		}
	}
}
