package com.bebeanan.perfectbaby;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by KiteXu.
 */
public class MemoryHandler {

    static MemoryHandler handler = null;
    static Map<String, String> data;
    static HashMap<String, String> CookieContainer;
    static HashMap<String, Object> IpInfo;
    int count;

    private MemoryHandler() {

    }

    public static synchronized MemoryHandler getInstance() {
        if (handler == null) {
            handler = new MemoryHandler();
            handler.data = new HashMap<String, String>();
            handler.CookieContainer = new HashMap<String, String>();
            handler.IpInfo = new HashMap<String, Object>();
            handler.IpInfo.put("IP", null);
            handler.IpInfo.put("Status", 0);
            handler.IpInfo.put("Message", null);
            handler.IpInfo.put("Version", 0);
            handler.count = 0;
        }
        return handler;
    }

    public synchronized String getKey(String key) {
        Log.v("MemoryHandler", String.format("Get %s", key));
        return handler.data.get(key);
    }

    synchronized void setKey(String key, String value) {
        Log.v("MemoryHandler", String.format("Set %s", key));

        handler.data.put(key, value);
    }

    synchronized void deleteKey(String key) {
        Log.v("MemoryHandler", String.format("Del %s", key));
        handler.data.remove(key);
    }

    synchronized HashMap<String, String> getCookieContainer() {
        return handler.CookieContainer;
    }

    synchronized void addCount() {
        handler.count++;
    }

    synchronized int getCount() {
        return handler.count;
    }

    synchronized void clearCount() {
        handler.count = 0;
    }

    synchronized void setCookieContainer(HashMap<String, String> cookieContainer) {
        handler.CookieContainer = cookieContainer;
    }

    synchronized void clearAll() {
        handler.data.clear();
        handler.CookieContainer.clear();
        handler.count = 0;
    }

    synchronized void clearHost() {
        handler.IpInfo.put("IP", null);
        handler.IpInfo.put("Status", 0);
        handler.IpInfo.put("Message", null);
        handler.IpInfo.put("Version", 0);
    }

    synchronized HashMap<String, Object> getHost() {
        return handler.IpInfo;
    }
    synchronized void setHost(HashMap<String, Object> ipInfo) {
        handler.IpInfo = ipInfo;
    }

}
