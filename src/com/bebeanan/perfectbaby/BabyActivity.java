package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bebeanan.perfectbaby.common.RotatePhoto;
import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class BabyActivity extends Activity implements View.OnTouchListener,
		GestureDetector.OnGestureListener {
	private int originalBabyNum = 0;
	private int babyNum = 0;
	private int currentBaby = 0;
	private ViewFlipper viewFlipper;
	private RelativeLayout flipper1;
	private RelativeLayout flipper2;
	private RelativeLayout flipper3;
	private ArrayList<String> allowNameList = new ArrayList<String>();
	private boolean isNew = true;
	private List<RelativeLayout> flipperList;
	private LinearLayout baby_list;
	private ImageView baby_list_add, baby_list_delete;

	private ImageView addBabyButton;

	private List<String> avatarList;

	private GestureDetector detector = new GestureDetector(
			this.getBaseContext(), this);

	private Animation closeInAnimation;
	private Animation closeOutAnimation;
	private Animation openInAnimation;
	private Animation openOutAnimation;

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1 = 100;
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_2 = 101;
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_3 = 102;
	private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE_1 = 2000;
	private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE_2 = 2001;
	private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE_3 = 2002;

	boolean bChanged = false;

	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	private String baby;

	private JSONArray finalBabyArray;
	private JSONArray babyArray;

	private static int height = 215;
	private static int width = 215;

	private TextView birthday1;
	private TextView birthday2;
	private TextView birthday3;

	private boolean isAddingBaby;

	private ImageView shield1;
	private ImageView shield2;
	private ImageView shield3;

	private ImageView baby_guide;
	private int[] baby_guide_src = { R.drawable.baby_guide_1,
			R.drawable.baby_guide_2 };
	private int guideIndex;

	private SharedPreferences userPreferences;

	/**
	 * Create a File for saving an image or video
	 */
	private static File getOutputMediaFile(int type, int number) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + String.valueOf(number) + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + String.valueOf(number) + ".mp4");
		} else {
			return null;
		}

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_baby);

		if (Utils.application == null) {
			Utils.init(BabyActivity.this);
		}
		if (userPreferences == null) {
			userPreferences = Utils.application.getSharedPreferences(
					"userInfo", Context.MODE_PRIVATE);
		}

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		dm = this.getApplicationContext().getResources().getDisplayMetrics();
		height = (int) (180 * dm.density);
		width = (int) (180 * dm.density);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
		builder = new AlertDialog.Builder(BabyActivity.this);
		builder.setCancelable(false);

		isAddingBaby = BabyActivity.this.getIntent().getBooleanExtra("addBaby",
				false);
		// if(isAddingBaby)
		// {
		// Toast toast = Toast.makeText(BabyActivity.this, "请添加宝宝",
		// Toast.LENGTH_SHORT);
		// toast.setGravity(Gravity.CENTER, 0, 0);
		// toast.show();
		// }

		baby_list = (LinearLayout) findViewById(R.id.baby_list);
		baby_list_add = (ImageView) findViewById(R.id.baby_list_add);
		baby_list_delete = (ImageView) findViewById(R.id.baby_list_delete);

		addBabyButton = (ImageView) findViewById(R.id.title_right_button);
		addBabyButton.setVisibility(View.VISIBLE);
		addBabyButton.setImageResource(R.drawable.baby_add_selector);

		viewFlipper = (ViewFlipper) findViewById(R.id.baby_flipper);

		LayoutInflater inflater = getLayoutInflater();

		flipper1 = (RelativeLayout) inflater
				.inflate(R.layout.baby_detail, null);
		flipper2 = (RelativeLayout) inflater
				.inflate(R.layout.baby_detail, null);
		flipper3 = (RelativeLayout) inflater
				.inflate(R.layout.baby_detail, null);

		closeInAnimation = AnimationUtils.loadAnimation(this,
				R.anim.activity_close_in_anim);
		closeOutAnimation = AnimationUtils.loadAnimation(this,
				R.anim.activity_close_out_anim);
		openInAnimation = AnimationUtils.loadAnimation(this,
				R.anim.activity_open_in_anim);
		openOutAnimation = AnimationUtils.loadAnimation(this,
				R.anim.activity_open_out_anim);

		{
			LinearLayout slideSwitch1 = (LinearLayout) flipper1
					.findViewById(R.id.baby_gender);
			initGender(slideSwitch1);
			TextView babyText1 = (TextView) flipper1
					.findViewById(R.id.baby_text);
			babyText1.setTypeface(Utils.typeface);
			shield1 = (ImageView) flipper1.findViewById(R.id.baby_shield);
			birthday1 = (TextView) flipper1
					.findViewById(R.id.baby_birthday_edittext);
			birthday1.setTypeface(Utils.typeface);
			birthday1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					String dateStr = birthday1.getText().toString();
					Calendar calendar = Calendar.getInstance();
					try {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Date date = sdf.parse(dateStr);
						calendar.setTime(date);
					} catch (Exception e) {
						e.printStackTrace();
					}
					builder = new AlertDialog.Builder(BabyActivity.this);
					DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker datePicker, int year,
								int month, int dayOfMonth) {
							birthday1.setText(String.format("%04d-%02d-%02d",
									year, month + 1, dayOfMonth));

							initShield(birthday1.getText().toString(), shield1);
						}
					};
					DatePickerDialog datePickerDialog = new DatePickerDialog(
							BabyActivity.this, dateListener, calendar
									.get(Calendar.YEAR), calendar
									.get(Calendar.MONDAY), calendar
									.get(Calendar.DAY_OF_MONTH));
					datePickerDialog.setCancelable(false);
					datePickerDialog.show();
				}
			});

		}

		{
			LinearLayout slideSwitch2 = (LinearLayout) flipper2
					.findViewById(R.id.baby_gender);
			initGender(slideSwitch2);
			TextView babyText2 = (TextView) flipper2
					.findViewById(R.id.baby_text);
			babyText2.setTypeface(Utils.typeface);
			shield2 = (ImageView) flipper2.findViewById(R.id.baby_shield);
			birthday2 = (TextView) flipper2
					.findViewById(R.id.baby_birthday_edittext);
			birthday2.setTypeface(Utils.typeface);
			birthday2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					String dateStr = birthday2.getText().toString();
					Calendar calendar = Calendar.getInstance();
					try {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Date date = sdf.parse(dateStr);
						calendar.setTime(date);
					} catch (Exception e) {
						e.printStackTrace();
					}
					builder = new AlertDialog.Builder(BabyActivity.this);
					DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker datePicker, int year,
								int month, int dayOfMonth) {
							birthday2.setText(String.format("%04d-%02d-%02d",
									year, month + 1, dayOfMonth));

							initShield(birthday2.getText().toString(), shield2);
						}
					};
					DatePickerDialog datePickerDialog = new DatePickerDialog(
							BabyActivity.this, dateListener, calendar
									.get(Calendar.YEAR), calendar
									.get(Calendar.MONDAY), calendar
									.get(Calendar.DAY_OF_MONTH));
					datePickerDialog.setCancelable(false);
					datePickerDialog.show();
				}
			});

		}

		{
			LinearLayout slideSwitch3 = (LinearLayout) flipper3
					.findViewById(R.id.baby_gender);
			initGender(slideSwitch3);
			TextView babyText3 = (TextView) flipper3
					.findViewById(R.id.baby_text);
			babyText3.setTypeface(Utils.typeface);
			shield3 = (ImageView) flipper3.findViewById(R.id.baby_shield);
			birthday3 = (TextView) flipper3
					.findViewById(R.id.baby_birthday_edittext);
			birthday3.setTypeface(Utils.typeface);
			birthday3.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					String dateStr = birthday3.getText().toString();
					Calendar calendar = Calendar.getInstance();
					try {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						Date date = sdf.parse(dateStr);
						calendar.setTime(date);
					} catch (Exception e) {
						e.printStackTrace();
					}
					builder = new AlertDialog.Builder(BabyActivity.this);
					DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker datePicker, int year,
								int month, int dayOfMonth) {
							birthday3.setText(String.format("%04d-%02d-%02d",
									year, month + 1, dayOfMonth));

							initShield(birthday3.getText().toString(), shield3);
						}
					};
					DatePickerDialog datePickerDialog = new DatePickerDialog(
							BabyActivity.this, dateListener, calendar
									.get(Calendar.YEAR), calendar
									.get(Calendar.MONDAY), calendar
									.get(Calendar.DAY_OF_MONTH));
					datePickerDialog.setCancelable(false);
					datePickerDialog.show();
				}
			});

		}

		{
			ImageView process = (ImageView) flipper1
					.findViewById(R.id.baby_process);
			process.setImageResource(R.drawable.baby_detail_1_1);
			ImageView avatarButton = (ImageView) flipper1
					.findViewById(R.id.baby_add_photo);
			avatarButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					final CharSequence[] items = { "相册", "拍照", "取消" };
					builder = new AlertDialog.Builder(BabyActivity.this);
					builder.setTitle("选择图片");
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialogInterface, int i) {
									if (i == 1) {
										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										File file = getOutputMediaFile(
												MEDIA_TYPE_IMAGE, 0); // create
																		// a
																		// file
																		// to
																		// save
																		// the
																		// image
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(file)); // set the
																		// image
																		// file
																		// name

										// start the image capture Intent
										startActivityForResult(intent,
												CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1);
									} else if (i == 0) {
										// Intent getImage = new Intent(
										// Intent.ACTION_GET_CONTENT);
										// getImage.addCategory(Intent.CATEGORY_OPENABLE);
										// getImage.setType("image/jpeg");
										// startActivityForResult(getImage,
										// GET_IMAGE_ACTIVITY_REQUEST_CODE_1);
										Intent intent = new Intent(
												Intent.ACTION_PICK,
												android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
										startActivityForResult(intent,
												GET_IMAGE_ACTIVITY_REQUEST_CODE_1);
									} else {
										dialog.dismiss();
									}
								}
							});
					dialog = builder.show();
				}
			});

		}

		{
			ImageView process = (ImageView) flipper2
					.findViewById(R.id.baby_process);
			process.setImageResource(R.drawable.register_add_process_pic);
			ImageView avatarButton = (ImageView) flipper2
					.findViewById(R.id.baby_add_photo);
			avatarButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					final CharSequence[] items = { "相册", "拍照", "取消" };
					builder = new AlertDialog.Builder(BabyActivity.this);
					builder.setTitle("选择图片");
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialogInterface, int i) {
									if (i == 1) {
										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										File file = getOutputMediaFile(
												MEDIA_TYPE_IMAGE, 1); // create
																		// a
																		// file
																		// to
																		// save
																		// the
																		// image
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(file)); // set the
																		// image
																		// file
																		// name

										// start the image capture Intent
										startActivityForResult(intent,
												CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_2);
									} else if (i == 0) {
										// Intent getImage = new Intent(
										// Intent.ACTION_GET_CONTENT);
										// getImage.addCategory(Intent.CATEGORY_OPENABLE);
										// getImage.setType("image/jpeg");
										// startActivityForResult(getImage,
										// GET_IMAGE_ACTIVITY_REQUEST_CODE_2);
										Intent intent = new Intent(
												Intent.ACTION_PICK,
												android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
										startActivityForResult(intent,
												GET_IMAGE_ACTIVITY_REQUEST_CODE_2);
									} else {
										dialog.dismiss();
									}
								}
							});
					dialog = builder.show();
				}
			});

		}
		{
			ImageView process = (ImageView) flipper3
					.findViewById(R.id.baby_process);
			process.setImageResource(R.drawable.register_full_process_pic);
			ImageView avatarButton = (ImageView) flipper3
					.findViewById(R.id.baby_add_photo);
			avatarButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					final CharSequence[] items = { "相册", "拍照", "取消" };
					builder = new AlertDialog.Builder(BabyActivity.this);
					builder.setTitle("选择图片");
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialogInterface, int i) {
									if (i == 1) {
										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										File file = getOutputMediaFile(
												MEDIA_TYPE_IMAGE, 2); // create
																		// a
																		// file
																		// to
																		// save
																		// the
																		// image
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(file)); // set the
																		// image
																		// file
																		// name

										// start the image capture Intent
										startActivityForResult(intent,
												CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_3);
									} else if (i == 0) {
										// Intent getImage = new Intent(
										// Intent.ACTION_GET_CONTENT);
										// getImage.addCategory(Intent.CATEGORY_OPENABLE);
										// getImage.setType("image/jpeg");
										// startActivityForResult(getImage,
										// GET_IMAGE_ACTIVITY_REQUEST_CODE_3);
										Intent intent = new Intent(
												Intent.ACTION_PICK,
												android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
										startActivityForResult(intent,
												GET_IMAGE_ACTIVITY_REQUEST_CODE_3);
									} else {
										dialog.dismiss();
									}
								}
							});
					dialog = builder.show();
				}
			});

		}

		flipperList = new LinkedList<RelativeLayout>();
		flipperList.add(flipper1);
		flipperList.add(flipper2);
		flipperList.add(flipper3);

		avatarList = new LinkedList<String>();

		/*
		 * JSONObject data = new JSONObject(); try { data.put("username",
		 * "zrr@qq.com"); data.put("password", "123456"); } catch (Exception e)
		 * { e.printStackTrace(); }
		 * 
		 * List<BasicNameValuePair> param = new
		 * LinkedList<BasicNameValuePair>();
		 * 
		 * NetHandler handlerLogin = new NetHandler(NetHandler.METHOD_POST,
		 * "/users/login", param, data) {
		 * 
		 * @Override public void handleRsp(Message msg) {
		 * 
		 * } }; handlerLogin.start();
		 */

		// "[{\"name\":\"eye\",\"avatar\":\"http://prefectbaby.oss-cn-hangzhou.aliyuncs.com/image/f5dd2aeecc0be2d66ac22b7e57a4b5d3.jpg\",\"birthday\":\"2012-03-20\",\"gender\":1,\"owner\":\"cfd2c90fc3e17bb0\",\"createdAt\":1397969507.104,\"updatedAt\":1397977645.599,\"id\":\"7d1ef1920fb8784f\"},{\"name\":\"dd\",\"avatar\":\"http://prefectbaby.oss-cn-hangzhou.aliyuncs.com/image/a1448f37662009f2ceb7b17fa59a43d6.jpg\",\"birthday\":\"2013-06-18\",\"gender\":2,\"owner\":\"cfd2c90fc3e17bb0\",\"createdAt\":1397977628.515,\"updatedAt\":1397977647.67,\"id\":\"21d2408ada21c82e\"}]";
		StoreHandler.getAndSetBabies();
		baby = MemoryHandler.getInstance().getKey("baby");
		Log.v("Kite", "babyArray is " + baby);
		try {
			babyArray = new JSONArray(baby);
			if (babyArray.length() == 0) {
				avatarList.add("");
				bChanged = true;
				babyNum = 0;
				currentBaby = 0;
				viewFlipper.addView(flipper1);
			} else {
				bChanged = false;
				originalBabyNum = babyNum = babyArray.length();
				if (babyNum > 3)
					babyNum = 3;
				Log.v("DBG", "babyNum:" + babyNum);
				for (int i = 0; i < babyNum; i++) {
					final int index = i;
					ImageView process = (ImageView) flipperList.get(i)
							.findViewById(R.id.baby_process);
					initProcess(process, i, babyNum);
					LinearLayout slideSwitch = (LinearLayout) flipperList
							.get(i).findViewById(R.id.baby_gender);
					initGender(slideSwitch);
					setGender(slideSwitch,
							babyArray.getJSONObject(i).getInt("gender"));
					TextView birthday = (TextView) flipperList.get(i)
							.findViewById(R.id.baby_birthday_edittext);
					birthday.setTypeface(Utils.typeface);
					birthday.setText(babyArray.getJSONObject(i).getString(
							"birthday"));

					ImageView shield = (ImageView) flipperList.get(i)
							.findViewById(R.id.baby_shield);
					initShield(
							babyArray.getJSONObject(i).getString("birthday"),
							shield);

					final EditText name = (EditText) flipperList.get(i).findViewById(
							R.id.baby_name_edittext);
					allowNameList.add("");
					// name.setTypeface(Utils.typeface);
					String babyNameString = babyArray.getJSONObject(i)
							.getString("name");
					if (!babyNameString.equals(" ")) {
						name.setText(babyNameString);
					} else {
						name.setText(userPreferences.getString("nickName", "")
								+ "的萌宝");
					}
					
					name.addTextChangedListener(new TextWatcher(){

						@Override
						public void beforeTextChanged(CharSequence s,
								int start, int count, int after) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onTextChanged(CharSequence s, int start,
								int before, int count) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub

								int bytesNumber = 0;
								try {
									bytesNumber = s.toString().getBytes("GBK").length;
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								Log.v("Kite", "bytesNumber is " + bytesNumber);

								if (bytesNumber == 32) {
									String allowString = s.toString();
									allowNameList.remove(index);
									allowNameList.add(index, allowString);
								}

								if (bytesNumber > 32) {
									name.setText(allowNameList.get(index));

									name.setSelection(allowNameList.get(index)
											.length());

									Toast toast = Toast.makeText(
											BabyActivity.this, "亲，昵称太长了～",
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
								}

						}
						
					});

					String avatar = babyArray.getJSONObject(i).has("avatar") ? babyArray
							.getJSONObject(i).getString("avatar") : "";
					if (!avatar.isEmpty()) {
						switch (i) {
						case 0: {
							NetHandler handler = new NetHandler(
									BabyActivity.this,
									NetHandler.METHOD_DOWNLOAD_FILE, avatar,
									null, null) {
								@Override
								public void handleRsp(Message msg) {
									Bundle bundle = msg.getData();
									int code = bundle.getInt("code");
									if (code == 200) {
										String data = bundle.getString("data");
										try {
											ImageView avatarButton = (ImageView) flipper1
													.findViewById(R.id.baby_add_photo);
											Log.v("DBG", String.format("%d %d",
													height, width));
//											BitmapFactory.Options options = new BitmapFactory.Options();
//											options.inJustDecodeBounds = true;
//
//											BitmapFactory
//													.decodeByteArray(
//															data.getBytes("ISO-8859-1"),
//															0,
//															data.getBytes("ISO-8859-1").length,
//															options);
//											int heightRatio = (int) Math
//													.floor(options.outHeight
//															/ (float) height);
//											int widthRatio = (int) Math
//													.floor(options.outWidth
//															/ (float) width);
//
//											Log.v("HEIGHRATIO", ""
//													+ heightRatio);
//											Log.v("WIDTHRATIO", "" + widthRatio);
//											Bitmap bit;
//											if (heightRatio == 0
//													&& widthRatio == 0) {
//												BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
//												options.inJustDecodeBounds = false;
//												Bitmap originalBmp = BitmapFactory
//														.decodeByteArray(
//																data.getBytes("ISO-8859-1"),
//																0,
//																data.getBytes("ISO-8859-1").length,
//																enlargeOptions);
//
//												Matrix matrix = new Matrix();
//
//												int enlargeWidthRatio = (int) (width / originalBmp
//														.getWidth()) + 2;
//												int enlargeHeightRatio = (int) (height / originalBmp
//														.getHeight()) + 2;
//												int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio) ? enlargeWidthRatio
//														: enlargeHeightRatio;
//
//												matrix.postScale(enlargeRatio,
//														enlargeRatio); // 长和宽放大缩小的比例
//												bit = Bitmap
//														.createBitmap(
//																originalBmp,
//																0,
//																0,
//																originalBmp
//																		.getWidth(),
//																originalBmp
//																		.getHeight(),
//																matrix, true);
//											} else {
//												if (heightRatio > 1
//														&& widthRatio > 1) {
//													options.inSampleSize = heightRatio > widthRatio ? heightRatio
//															: widthRatio;
//												}

											BitmapFactory.Options options = new BitmapFactory.Options();
												options.inJustDecodeBounds = false;
												options.inPreferredConfig = Bitmap.Config.RGB_565;
												options.inPurgeable = true;
												options.inInputShareable = true;
												Bitmap bit = BitmapFactory
														.decodeByteArray(
																data.getBytes("ISO-8859-1"),
																0,
																data.getBytes("ISO-8859-1").length,
																options);
												
												setAvatar(bit, avatarButton);
//											}

//											Bitmap output = Bitmap
//													.createBitmap(
//															width,
//															height,
//															Bitmap.Config.ARGB_8888);
//											Canvas canvas = new Canvas(output);
//
//											final int color = 0xff424242;
//											final Paint paint = new Paint();
//											final Rect rect = new Rect(0, 0,
//													width, height);
//											final RectF rectF = new RectF(rect);
//											final float roundPx = (float) width / 2.0f;
//
//											paint.setAntiAlias(true);
//											canvas.drawARGB(0, 0, 0, 0);
//
//											paint.setColor(color);
//											canvas.drawRoundRect(rectF,
//													roundPx, roundPx, paint);
//											paint.setXfermode(new PorterDuffXfermode(
//													PorterDuff.Mode.SRC_IN));
//											canvas.drawBitmap(bit, rect, rect,
//													paint);
//
//											// options.inSampleSize = 4;
//											avatarButton.setImageBitmap(output);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							};
							handler.start();
							break;
						}
						case 1: {
							NetHandler handler = new NetHandler(
									BabyActivity.this,
									NetHandler.METHOD_DOWNLOAD_FILE, avatar,
									null, null) {
								@Override
								public void handleRsp(Message msg) {
									Bundle bundle = msg.getData();
									int code = bundle.getInt("code");
									if (code == 200) {
										String data = bundle.getString("data");
										try {
											ImageView avatarButton = (ImageView) flipper2
													.findViewById(R.id.baby_add_photo);
											Log.v("DBG", String.format("%d %d",
													height, width));
//											BitmapFactory.Options options = new BitmapFactory.Options();
//											options.inJustDecodeBounds = true;
//
//											BitmapFactory
//													.decodeByteArray(
//															data.getBytes("ISO-8859-1"),
//															0,
//															data.getBytes("ISO-8859-1").length,
//															options);
//											int heightRatio = (int) Math
//													.floor(options.outHeight
//															/ (float) height);
//											int widthRatio = (int) Math
//													.floor(options.outWidth
//															/ (float) width);
//
//											Log.v("HEIGHRATIO", ""
//													+ heightRatio);
//											Log.v("WIDTHRATIO", "" + widthRatio);
//											Bitmap bit;
//											if (heightRatio == 0
//													&& widthRatio == 0) {
//												BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
//												options.inJustDecodeBounds = false;
//												Bitmap originalBmp = BitmapFactory
//														.decodeByteArray(
//																data.getBytes("ISO-8859-1"),
//																0,
//																data.getBytes("ISO-8859-1").length,
//																enlargeOptions);
//
//												Matrix matrix = new Matrix();
//
//												int enlargeWidthRatio = (int) (width / originalBmp
//														.getWidth()) + 2;
//												int enlargeHeightRatio = (int) (height / originalBmp
//														.getHeight()) + 2;
//												int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio) ? enlargeWidthRatio
//														: enlargeHeightRatio;
//
//												matrix.postScale(enlargeRatio,
//														enlargeRatio); // 长和宽放大缩小的比例
//												bit = Bitmap
//														.createBitmap(
//																originalBmp,
//																0,
//																0,
//																originalBmp
//																		.getWidth(),
//																originalBmp
//																		.getHeight(),
//																matrix, true);
//											} else {
//												if (heightRatio > 1
//														&& widthRatio > 1) {
//													options.inSampleSize = heightRatio > widthRatio ? heightRatio
//															: widthRatio;
//												}

											BitmapFactory.Options options = new BitmapFactory.Options();
												options.inJustDecodeBounds = false;
												options.inPreferredConfig = Bitmap.Config.RGB_565;
												options.inPurgeable = true;
												options.inInputShareable = true;
												Bitmap bit = BitmapFactory
														.decodeByteArray(
																data.getBytes("ISO-8859-1"),
																0,
																data.getBytes("ISO-8859-1").length,
																options);
												
												setAvatar(bit, avatarButton);
//											}

//											Bitmap output = Bitmap
//													.createBitmap(
//															width,
//															height,
//															Bitmap.Config.ARGB_8888);
//											Canvas canvas = new Canvas(output);
//
//											final int color = 0xff424242;
//											final Paint paint = new Paint();
//											final Rect rect = new Rect(0, 0,
//													width, height);
//											final RectF rectF = new RectF(rect);
//											final float roundPx = (float) width / 2.0f;
//
//											paint.setAntiAlias(true);
//											canvas.drawARGB(0, 0, 0, 0);
//
//											paint.setColor(color);
//											canvas.drawRoundRect(rectF,
//													roundPx, roundPx, paint);
//											paint.setXfermode(new PorterDuffXfermode(
//													PorterDuff.Mode.SRC_IN));
//											canvas.drawBitmap(bit, rect, rect,
//													paint);
//
//											// options.inSampleSize = 4;
//											avatarButton.setImageBitmap(output);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							};
							handler.start();
							break;
						}
						case 2: {
							NetHandler handler = new NetHandler(
									BabyActivity.this,
									NetHandler.METHOD_DOWNLOAD_FILE, avatar,
									null, null) {
								@Override
								public void handleRsp(Message msg) {
									Bundle bundle = msg.getData();
									int code = bundle.getInt("code");
									if (code == 200) {
										String data = bundle.getString("data");
										try {
											ImageView avatarButton = (ImageView) flipper3
													.findViewById(R.id.baby_add_photo);
											Log.v("DBG", String.format("%d %d",
													height, width));
//											BitmapFactory.Options options = new BitmapFactory.Options();
//											options.inJustDecodeBounds = true;
//
//											BitmapFactory
//													.decodeByteArray(
//															data.getBytes("ISO-8859-1"),
//															0,
//															data.getBytes("ISO-8859-1").length,
//															options);
//											int heightRatio = (int) Math
//													.floor(options.outHeight
//															/ (float) height);
//											int widthRatio = (int) Math
//													.floor(options.outWidth
//															/ (float) width);
//
//											Log.v("HEIGHRATIO", ""
//													+ heightRatio);
//											Log.v("WIDTHRATIO", "" + widthRatio);
//											Bitmap bit;
//											if (heightRatio == 0
//													&& widthRatio == 0) {
//												BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
//												options.inJustDecodeBounds = false;
//												Bitmap originalBmp = BitmapFactory
//														.decodeByteArray(
//																data.getBytes("ISO-8859-1"),
//																0,
//																data.getBytes("ISO-8859-1").length,
//																enlargeOptions);
//
//												Matrix matrix = new Matrix();
//
//												int enlargeWidthRatio = (int) (width / originalBmp
//														.getWidth()) + 2;
//												int enlargeHeightRatio = (int) (height / originalBmp
//														.getHeight()) + 2;
//												int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio) ? enlargeWidthRatio
//														: enlargeHeightRatio;
//
//												matrix.postScale(enlargeRatio,
//														enlargeRatio); // 长和宽放大缩小的比例
//												bit = Bitmap
//														.createBitmap(
//																originalBmp,
//																0,
//																0,
//																originalBmp
//																		.getWidth(),
//																originalBmp
//																		.getHeight(),
//																matrix, true);
//											} else {
//												if (heightRatio > 1
//														&& widthRatio > 1) {
//													options.inSampleSize = heightRatio > widthRatio ? heightRatio
//															: widthRatio;
//												}

											BitmapFactory.Options options = new BitmapFactory.Options();
												options.inJustDecodeBounds = false;
												options.inPreferredConfig = Bitmap.Config.RGB_565;
												options.inPurgeable = true;
												options.inInputShareable = true;
												Bitmap bit = BitmapFactory
														.decodeByteArray(
																data.getBytes("ISO-8859-1"),
																0,
																data.getBytes("ISO-8859-1").length,
																options);
												
												setAvatar(bit, avatarButton);
//											}

//											Bitmap output = Bitmap
//													.createBitmap(
//															width,
//															height,
//															Bitmap.Config.ARGB_8888);
//											Canvas canvas = new Canvas(output);
//
//											final int color = 0xff424242;
//											final Paint paint = new Paint();
//											final Rect rect = new Rect(0, 0,
//													width, height);
//											final RectF rectF = new RectF(rect);
//											final float roundPx = (float) width / 2.0f;
//
//											paint.setAntiAlias(true);
//											canvas.drawARGB(0, 0, 0, 0);
//
//											paint.setColor(color);
//											canvas.drawRoundRect(rectF,
//													roundPx, roundPx, paint);
//											paint.setXfermode(new PorterDuffXfermode(
//													PorterDuff.Mode.SRC_IN));
//											canvas.drawBitmap(bit, rect, rect,
//													paint);
//
//											// options.inSampleSize = 4;
//											avatarButton.setImageBitmap(output);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
							};
							handler.start();
							break;
						}
						}

					}
					avatarList.add(avatar);
					viewFlipper.addView(flipperList.get(i));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			bChanged = true;
			babyNum = 0;
			currentBaby = 0;
			avatarList.add("");
			viewFlipper.addView(flipper1);
		}

		{
			Intent intent = BabyActivity.this.getIntent();
			currentBaby = intent.getIntExtra("currentBaby", 0);
			if (currentBaby != 0) {
				int j = currentBaby;
				while (j != 0) {
					viewFlipper.showNext();
					j--;
				}
			}
		}

		setBabyOptionButton();

		ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
		backButton.setImageResource(R.drawable.baby_done_selector);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				for (int i = 0; i < babyNum; i++) {

					if (i == currentBaby) {
						ImageView shield = (ImageView) flipperList.get(i)
								.findViewById(R.id.baby_shield);
						TextView birthday = (TextView) flipperList.get(i)
								.findViewById(R.id.baby_birthday_edittext);

						if (shield.getVisibility() == View.VISIBLE
								&& birthday.getText().toString() != null) {
							continue;
						}
					}

					EditText name = (EditText) flipperList.get(i).findViewById(
							R.id.baby_name_edittext);
					// name.setTypeface(Utils.typeface);
					if (name.getText().toString().isEmpty()) {
						toast.cancel();
						toast = Toast.makeText(getApplicationContext(),
								"信息不完整", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						return;
					}
					
					int bytesNumber = 0;
					try {
						bytesNumber = name.getText().toString().getBytes("GBK").length;
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(bytesNumber < 4)
					{
						toast = Toast.makeText(getApplicationContext(), "亲，昵称长度不够噢～",
	                            Toast.LENGTH_SHORT);
	                    toast.setGravity(Gravity.CENTER, 0, 0);
	                    toast.show();
	                    
	                    return;
					}
				}
				if (!bChanged) {
					try {
						for (int i = 0; i < babyArray.length(); i++) {
							LinearLayout slideSwitch = (LinearLayout) flipperList
									.get(i).findViewById(R.id.baby_gender);
							// initGender(slideSwitch);
							int gender = babyArray.getJSONObject(i).getInt(
									"gender");
							if ((Integer) (slideSwitch.getTag()) != gender) {
								bChanged = true;
								break;
							}
							TextView birthday = (TextView) flipperList.get(i)
									.findViewById(R.id.baby_birthday_edittext);
							birthday.setTypeface(Utils.typeface);
							if (!birthday
									.getText()
									.toString()
									.equals(babyArray.getJSONObject(i)
											.getString("birthday"))) {
								bChanged = true;
								break;
							}
							EditText name = (EditText) flipperList.get(i)
									.findViewById(R.id.baby_name_edittext);
							// name.setTypeface(Utils.typeface);
							if (!name
									.getText()
									.toString()
									.equals(babyArray.getJSONObject(i)
											.getString("name"))) {
								bChanged = true;
								break;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (bChanged) {
					finalBabyArray = new JSONArray();
					try {
						if (babyNum > 0) {
							builder.setMessage("上传中");
							dialog = builder.show();
							showBuilder = true;

							JSONObject babyObject1 = new JSONObject();
							LinearLayout slideSwitch = (LinearLayout) flipperList
									.get(0).findViewById(R.id.baby_gender);
							// initGender(slideSwitch);
							babyObject1.put("gender",
									(Integer) (slideSwitch.getTag()));
							TextView birthday = (TextView) flipperList.get(0)
									.findViewById(R.id.baby_birthday_edittext);
							birthday.setTypeface(Utils.typeface);
							babyObject1.put("birthday", birthday.getText()
									.toString());
							EditText name = (EditText) flipperList.get(0)
									.findViewById(R.id.baby_name_edittext);
							// name.setTypeface(Utils.typeface);
							String nameStr;
							if (name.getText().toString().equals("")) {
								nameStr = " ";
							} else {
								nameStr = name.getText().toString();
							}
							babyObject1.put("name", nameStr);
							babyObject1.put("avatar", avatarList.get(0));

							String method = NetHandler.METHOD_POST;
							String uri = "/baby";
							if (originalBabyNum > 0) {
								method = NetHandler.METHOD_PUT;
								uri += "/"
										+ babyArray.getJSONObject(0).getString(
												"id");
							}
							NetHandler handlerBaby1 = new NetHandler(
									BabyActivity.this, method, uri,
									new LinkedList<BasicNameValuePair>(),
									babyObject1) {
								@Override
								public void handleRsp(Message msg) {
									Bundle bundleBaby1 = msg.getData();
									int codeBaby1 = bundleBaby1.getInt("code");
									if (codeBaby1 == 200) {
										try {
											JSONObject baby1 = new JSONObject(
													bundleBaby1
															.getString("data"));
											finalBabyArray.put(0, baby1);
										} catch (Exception e) {
											e.printStackTrace();
										}

										if (babyNum > 1) {

											JSONObject babyObject2 = new JSONObject();
											String method = NetHandler.METHOD_POST;
											String uri = "/baby";
											try {
												LinearLayout slideSwitch = (LinearLayout) flipperList
														.get(1)
														.findViewById(
																R.id.baby_gender);
												// initGender(slideSwitch);
												babyObject2.put("gender",
														(Integer) (slideSwitch
																.getTag()));
												TextView birthday = (TextView) flipperList
														.get(1)
														.findViewById(
																R.id.baby_birthday_edittext);
												birthday.setTypeface(Utils.typeface);
												babyObject2.put("birthday",
														birthday.getText()
																.toString());
												EditText name = (EditText) flipperList
														.get(1)
														.findViewById(
																R.id.baby_name_edittext);
												// name.setTypeface(Utils.typeface);
												String nameStr;
												if (name.getText().toString()
														.equals("")) {
													nameStr = " ";
												} else {
													nameStr = name.getText()
															.toString();
												}
												babyObject2
														.put("name", nameStr);
												babyObject2.put("avatar",
														avatarList.get(1));
												if (originalBabyNum > 1) {
													method = NetHandler.METHOD_PUT;
													uri += "/"
															+ babyArray
																	.getJSONObject(
																			1)
																	.getString(
																			"id");
												}
											} catch (Exception e) {
												e.printStackTrace();
											}
											NetHandler handlerBaby2 = new NetHandler(
													BabyActivity.this,
													method,
													uri,
													new LinkedList<BasicNameValuePair>(),
													babyObject2) {
												@Override
												public void handleRsp(
														Message msg) {
													Bundle bundleBaby2 = msg
															.getData();
													int codeBaby2 = bundleBaby2
															.getInt("code");
													if (codeBaby2 == 200) {
														try {
															JSONObject baby2 = new JSONObject(
																	bundleBaby2
																			.getString("data"));
															finalBabyArray.put(
																	1, baby2);
														} catch (Exception e) {
															e.printStackTrace();
														}

														if (babyNum > 2) {
															JSONObject babyObject3 = new JSONObject();
															String method = NetHandler.METHOD_POST;
															String uri = "/baby";
															try {
																LinearLayout slideSwitch = (LinearLayout) flipperList
																		.get(2)
																		.findViewById(
																				R.id.baby_gender);
																// initGender(slideSwitch);
																babyObject3
																		.put("gender",
																				(Integer) (slideSwitch
																						.getTag()));
																TextView birthday = (TextView) flipperList
																		.get(2)
																		.findViewById(
																				R.id.baby_birthday_edittext);
																birthday.setTypeface(Utils.typeface);
																babyObject3
																		.put("birthday",
																				birthday.getText()
																						.toString());
																EditText name = (EditText) flipperList
																		.get(2)
																		.findViewById(
																				R.id.baby_name_edittext);
																// name.setTypeface(Utils.typeface);
																String nameStr;
																if (name.getText()
																		.toString()
																		.equals("")) {
																	nameStr = " ";
																} else {
																	nameStr = name
																			.getText()
																			.toString();
																}
																babyObject3
																		.put("name",
																				nameStr);
																babyObject3
																		.put("avatar",
																				avatarList
																						.get(2));
																if (originalBabyNum > 2) {
																	method = NetHandler.METHOD_PUT;
																	uri += "/"
																			+ babyArray
																					.getJSONObject(
																							2)
																					.getString(
																							"id");
																}
															} catch (Exception e) {
																e.printStackTrace();
															}
															NetHandler handlerBaby3 = new NetHandler(
																	BabyActivity.this,
																	method,
																	uri,
																	new LinkedList<BasicNameValuePair>(),
																	babyObject3) {
																@Override
																public void handleRsp(
																		Message msg) {
																	Bundle bundleBaby2 = msg
																			.getData();
																	int codeBaby2 = bundleBaby2
																			.getInt("code");
																	if (codeBaby2 == 200) {
																		try {
																			JSONObject baby3 = new JSONObject(
																					bundleBaby2
																							.getString("data"));
																			finalBabyArray
																					.put(2,
																							baby3);
																		} catch (Exception e) {
																			e.printStackTrace();
																		}

																		dialog.dismiss();
																		showBuilder = false;
																		StoreHandler
																				.storeBabies(finalBabyArray
																						.toString());
																		MemoryHandler
																				.getInstance()
																				.setKey("baby",
																						finalBabyArray
																								.toString());
																		toast.cancel();
																		toast = Toast
																				.makeText(
																						getApplicationContext(),
																						"保存成功",
																						Toast.LENGTH_SHORT);
																		toast.setGravity(
																				Gravity.CENTER,
																				0,
																				0);
																		toast.show();

																		Intent intent = BabyActivity.this
																				.getIntent();
																		boolean bNext = intent
																				.getBooleanExtra(
																						"next",
																						false);
																		if (bNext) {
																			Intent intent1 = new Intent();
																			intent1.setClass(
																					BabyActivity.this,
																					MainActivity.class);
																			startActivity(intent1);
																			finish();
																		} else {
																			Log.v("DBG",
																					String.format(
																							"baby activity finish %d",
																							currentBaby));
																			Intent intent1 = new Intent();
																			intent1.putExtra(
																					"currentBaby",
																					currentBaby);
																			setResult(
																					RESULT_OK,
																					intent1);
																			finish();
																		}

																	} else {
																		dialog.dismiss();
																		showBuilder = false;
																		toast.cancel();
																		toast = Toast
																				.makeText(
																						getApplicationContext(),
																						"保存失败",
																						Toast.LENGTH_SHORT);
																		toast.setGravity(
																				Gravity.CENTER,
																				0,
																				0);
																		toast.show();

																		Log.v("Kite",
																				bundleBaby2
																						.getString("data"));
																	}

																}
															};
															handlerBaby3
																	.start();
														} else {
															dialog.dismiss();
															showBuilder = false;
															StoreHandler
																	.storeBabies(finalBabyArray
																			.toString());
															MemoryHandler
																	.getInstance()
																	.setKey("baby",
																			finalBabyArray
																					.toString());
															toast.cancel();
															toast = Toast
																	.makeText(
																			getApplicationContext(),
																			"保存成功",
																			Toast.LENGTH_SHORT);
															toast.setGravity(
																	Gravity.CENTER,
																	0, 0);
															toast.show();

															Intent intent = BabyActivity.this
																	.getIntent();
															boolean bNext = intent
																	.getBooleanExtra(
																			"next",
																			false);
															if (bNext) {
																Intent intent1 = new Intent();
																intent1.setClass(
																		BabyActivity.this,
																		MainActivity.class);
																startActivity(intent1);
																finish();
															} else {
																Log.v("DBG",
																		String.format(
																				"baby activity finish %d",
																				currentBaby));
																Intent intent1 = new Intent();
																intent1.putExtra(
																		"currentBaby",
																		currentBaby);
																setResult(
																		RESULT_OK,
																		intent1);
																finish();
															}
														}
													} else {
														dialog.dismiss();
														showBuilder = false;
														toast.cancel();
														toast = Toast
																.makeText(
																		getApplicationContext(),
																		"保存失败",
																		Toast.LENGTH_SHORT);
														toast.setGravity(
																Gravity.CENTER,
																0, 0);
														toast.show();

														Log.v("Kite",
																bundleBaby2
																		.getString("data"));
													}

												}
											};
											handlerBaby2.start();

										} else {
											dialog.dismiss();
											showBuilder = false;
											toast.cancel();
											StoreHandler
													.storeBabies(finalBabyArray
															.toString());
											MemoryHandler.getInstance().setKey(
													"baby",
													finalBabyArray.toString());
											toast = Toast.makeText(
													getApplicationContext(),
													"保存成功", Toast.LENGTH_SHORT);
											toast.setGravity(Gravity.CENTER, 0,
													0);
											toast.show();

											Intent intent = BabyActivity.this
													.getIntent();
											boolean bNext = intent
													.getBooleanExtra("next",
															false);
											if (bNext) {
												Intent intent1 = new Intent();
												intent1.setClass(
														BabyActivity.this,
														MainActivity.class);
												startActivity(intent1);
												finish();
											} else {
												Log.v("DBG",
														String.format(
																"baby activity finish %d",
																currentBaby));
												Intent intent1 = new Intent();
												intent1.putExtra("currentBaby",
														currentBaby);
												setResult(RESULT_OK, intent1);
												finish();
											}
										}

									} else {
										dialog.dismiss();
										showBuilder = false;
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"保存失败", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										Log.v("Kite",
												bundleBaby1.getString("data"));
									}
								}
							};
							handlerBaby1.start();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					Intent intent = BabyActivity.this.getIntent();
					boolean bNext = intent.getBooleanExtra("next", false);
					if (bNext) {
						Intent intent1 = new Intent();
						intent1.setClass(BabyActivity.this, MainActivity.class);
						startActivity(intent1);
						finish();
					} else {
						Log.v("DBG", String.format("baby activity finish %d",
								currentBaby));
						Intent intent1 = new Intent();
						intent1.putExtra("currentBaby", currentBaby);
						setResult(RESULT_OK, intent1);
						finish();
					}
				}

			}
		});

		baby_guide = (ImageView) findViewById(R.id.baby_guide);
		showGuide();

		if (isAddingBaby) {
			if (babyNum >= 3) {
				Toast toast = Toast.makeText(BabyActivity.this,
						"亲，最多只能添加3个宝宝哦~", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			} else {
				addBaby();
			}
		}
	}

	private void showGuide() {
		boolean babyGuideShown = StoreHandler.getGuide(StoreHandler.babyGuide);
		if (!babyGuideShown) {
			baby_guide.setVisibility(View.VISIBLE);
			guideIndex = 0;
			baby_guide.setImageResource(baby_guide_src[guideIndex++]);
			baby_guide.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (guideIndex == baby_guide_src.length) {
						baby_guide.setVisibility(View.GONE);

						StoreHandler.setGuide(StoreHandler.babyGuide, true);

						return;
					}

					baby_guide.setImageResource(baby_guide_src[guideIndex++]);
				}

			});
		}
	}

	void setBabyOptionButton() {

		addBabyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				baby_list_add.setImageResource(R.drawable.baby_list_add);
				baby_list_add.setClickable(true);

				if (babyNum >= 3) {
					// baby_list_add.setImageResource(R.drawable.baby_list_add_disable);
					// baby_list_add.setClickable(false);
					baby_list_add
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {
									baby_list.setVisibility(View.GONE);

									Toast toast = Toast.makeText(
											BabyActivity.this,
											"亲，最多只能添加3个宝宝哦~",
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
								}
							});
				} else {
					baby_list_add
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {
									addBaby();
								}
							});

				}

				// if(currentBaby == 0)
				// {
				// baby_list_delete.setVisibility(View.GONE);
				// }
				// else
				// {
				baby_list_delete.setVisibility(View.VISIBLE);
				baby_list_delete.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						baby_list.setVisibility(View.GONE);

						try {
							List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
							String uri;
							uri = "/baby/"
									+ babyArray.getJSONObject(currentBaby)
											.getString("id");
							NetHandler handler = new NetHandler(
									BabyActivity.this,
									NetHandler.METHOD_DELETE, uri, param, null) {

								@Override
								public void handleRsp(Message msg) {
									// TODO Auto-generated method
									// stub
									Bundle bundle = msg.getData();
									final int code = bundle.getInt("code");
									if (code == 200) {
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"删除成功", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										JSONArray newBabyArray = new JSONArray();
										for (int i = 0; i < babyArray.length(); i++) {
											if (i != currentBaby) {
												try {
													newBabyArray.put(babyArray
															.get(i));
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										}
										StoreHandler.storeBabies(newBabyArray
												.toString());
										MemoryHandler
												.getInstance()
												.setKey("baby",
														newBabyArray.toString());

										Intent intent = new Intent();
										if (currentBaby != 0) {
											currentBaby = 0;
											intent.putExtra("currentBaby",
													currentBaby);
										}
										setResult(RESULT_OK, intent);
										Log.v("DBG", String.format(
												"baby activity finish %d",
												currentBaby));
										BabyActivity.this.finish();
									} else {
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"删除失败", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
									}
								}
							};

							handler.start();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				});
				// }

				int visibility = baby_list.getVisibility();

				switch (visibility) {
				case View.VISIBLE:

					baby_list.setVisibility(View.GONE);

					break;

				case View.GONE:

					baby_list.setVisibility(View.VISIBLE);

					break;
				}
			}

		});

	}

	private void addBaby() {

		baby_list.setVisibility(View.GONE);
		isAddingBaby = true;

		bChanged = true;
		switch (babyNum) {
		case 0:
			ViewGroup parent1 = (ViewGroup) flipper1.getParent();
			if (parent1 != null) {
				parent1.removeView(flipper1);
			}
			viewFlipper.addView(flipper1);
			ImageView process1 = (ImageView) flipper1
					.findViewById(R.id.baby_process);
			initProcess(process1, 0, 1);
			ImageView shield1 = (ImageView) flipper1
					.findViewById(R.id.baby_shield);
			shield1.setVisibility(View.GONE);
			break;
		case 1:
			ViewGroup parent2 = (ViewGroup) flipper2.getParent();
			if (parent2 != null) {
				parent2.removeView(flipper2);
			}
			viewFlipper.addView(flipper2);
			ImageView process2 = (ImageView) flipper2
					.findViewById(R.id.baby_process);
			initProcess(process2, 1, 2);
			ImageView shield2 = (ImageView) flipper2
					.findViewById(R.id.baby_shield);
			shield2.setVisibility(View.GONE);
			break;
		case 2:
			ViewGroup parent3 = (ViewGroup) flipper3.getParent();
			if (parent3 != null) {
				parent3.removeView(flipper3);
			}
			viewFlipper.addView(flipper3);
			ImageView process3 = (ImageView) flipper3
					.findViewById(R.id.baby_process);
			initProcess(process3, 2, 3);
			ImageView shield3 = (ImageView) flipper3
					.findViewById(R.id.baby_shield);
			shield3.setVisibility(View.GONE);
			break;
		}

		for (; currentBaby < babyNum; currentBaby++) {
			viewFlipper.setInAnimation(openInAnimation);
			viewFlipper.setOutAnimation(openOutAnimation);
			viewFlipper.showNext();
		}
		babyNum++;
		avatarList.add("");

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:

			if (baby_list.getVisibility() == View.VISIBLE) {
				baby_list.setVisibility(View.GONE);
			} else {
				BabyActivity.this.setResult(RESULT_CANCELED);
				BabyActivity.this.finish();
			}

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onDown(MotionEvent motionEvent) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent motionEvent) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent motionEvent) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2,
			float v, float v2) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent motionEvent) {

	}

	@Override
	public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2,
			float v, float v2) {
		if (motionEvent.getX() - motionEvent2.getX() > 120) {
			if (currentBaby < babyNum - 1) {

				viewFlipper.setInAnimation(openInAnimation);
				viewFlipper.setOutAnimation(openOutAnimation);
				viewFlipper.showNext();
				currentBaby++;

				return true;
			}
		} else if (motionEvent.getX() - motionEvent2.getX() < -120) {
			if (currentBaby > 0) {

				if (isAddingBaby && babyNum < 3) {
					switch (currentBaby) {
					case 0:

						viewFlipper.removeView(flipper1);

						break;

					case 1:

						viewFlipper.removeView(flipper2);

						break;

					case 2:

						viewFlipper.removeView(flipper3);

						break;

					}
					isAddingBaby = false;
					babyNum--;

					currentBaby--;
				}

				viewFlipper.setInAnimation(closeInAnimation);
				viewFlipper.setOutAnimation(closeOutAnimation);
				viewFlipper.showPrevious();
				currentBaby--;

				return true;
			}
		}

		return false;
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		return false;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean bDispatch = detector.onTouchEvent(ev);
		if (!bDispatch)
			return super.dispatchTouchEvent(ev);
		return true;
	}

	@Override
	protected void onDestroy() {
		for (int i = 0; i != 3; i++) {
			File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, i);
			file.delete();
		}

		viewFlipper.removeAllViews();

		super.onDestroy();
	}

	@Override
	protected void onActivityResult(final int requestCode, int resultCode,
			Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1
				|| requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_2
				|| requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_3) {
			if (resultCode == RESULT_OK) {

				builder = new AlertDialog.Builder(BabyActivity.this);
				builder.setMessage("上传中");
				dialog = builder.show();
				showBuilder = true;

				String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE,
						requestCode - CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1)
						.getAbsolutePath();
				Bitmap bitmap = null;
				try {
					BitmapFactory.Options opts = new BitmapFactory.Options();
					opts.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(filename, opts);
					int srcWidth = opts.outWidth;
					int srcHeight = opts.outHeight;
					int desWidth = 0;
					int desHeight = 0;
					// 缩放比例
					double ratio = 0.0;
					if (srcWidth > srcHeight) {
						ratio = srcWidth / 640;
						desWidth = (int) (srcWidth / ratio);
						desHeight = (int) (srcHeight / ratio);
					} else {
						ratio = srcHeight / 640;
						desWidth = (int) (srcWidth / ratio);
						desHeight = (int) (srcHeight / ratio);
					}
					// 设置输出宽度、高度
					BitmapFactory.Options newOpts = new BitmapFactory.Options();
					newOpts.inSampleSize = (int) ratio;
					newOpts.inJustDecodeBounds = false;
					
					// newOpts.outWidth = desWidth;
					// newOpts.outHeight = desHeight;
					// bitmap = BitmapFactory.decodeFile(filename, newOpts);
					bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
					FileOutputStream out = new FileOutputStream(filename);
					if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
						out.flush();
						out.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
				param.add(new BasicNameValuePair(
						"filename",
						getOutputMediaFile(
								MEDIA_TYPE_IMAGE,
								requestCode
										- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1)
								.getAbsolutePath()));
				param.add(new BasicNameValuePair("filetype", "image"));
				NetHandler handler = new NetHandler(BabyActivity.this,
						NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
					@Override
					public void handleRsp(Message msg) {
						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						if (code == 200) {

							String data = bundle.getString("data");
							try {
								JSONArray array = new JSONArray(data);
								avatarList
										.set(requestCode
												- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1,
												array.getJSONObject(0)
														.getString("url"));
							} catch (Exception e) {
								e.printStackTrace();
							}

							ImageView avatarButton;
							avatarButton = (ImageView) flipperList
									.get(requestCode
											- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1)
									.findViewById(R.id.baby_add_photo);
							File fileName = getOutputMediaFile(
									MEDIA_TYPE_IMAGE,
									requestCode
											- CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_1);
							BitmapFactory.Options options = new BitmapFactory.Options();
							options.inJustDecodeBounds = false;

							Bitmap bit = BitmapFactory.decodeFile(fileName.getPath(), options);
							
							setAvatar(bit, avatarButton);
//							try {
//								Log.v("DBG",
//										String.format("%d %d", height, width));
//								BitmapFactory.Options options = new BitmapFactory.Options();
//								options.inJustDecodeBounds = true;
//
//								BitmapFactory.decodeFile(fileName.getPath(),
//										options);
//								int heightRatio = (int) Math
//										.floor(options.outHeight
//												/ (float) height);
//								int widthRatio = (int) Math
//										.floor(options.outWidth / (float) width);
//
//								Log.v("HEIGHRATIO", "" + heightRatio);
//								Log.v("WIDTHRATIO", "" + widthRatio);
//
//								if (heightRatio > 1 && widthRatio > 1) {
//									options.inSampleSize = heightRatio > widthRatio ? heightRatio
//											: widthRatio;
//								}
//								options.inJustDecodeBounds = false;
//								Bitmap bit = BitmapFactory.decodeFile(
//										fileName.getPath(), options);
//
//								Bitmap output = Bitmap.createBitmap(width,
//										height, Bitmap.Config.ARGB_8888);
//								Canvas canvas = new Canvas(output);
//
//								final int color = 0xff424242;
//								final Paint paint = new Paint();
//								final Rect rect = new Rect(0, 0, width, height);
//								final RectF rectF = new RectF(rect);
//								final float roundPx = (float) width / 2.0f;
//
//								paint.setAntiAlias(true);
//								canvas.drawARGB(0, 0, 0, 0);
//
//								paint.setColor(color);
//								canvas.drawRoundRect(rectF, roundPx, roundPx,
//										paint);
//								paint.setXfermode(new PorterDuffXfermode(
//										PorterDuff.Mode.SRC_IN));
//								canvas.drawBitmap(bit, rect, rect, paint);
//
//								// options.inSampleSize = 4;
//								avatarButton.setImageBitmap(output);
//							} catch (Exception e) {
//								e.printStackTrace();
//							}

							bChanged = true;

							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"上传完成", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;

						} else {
							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"上传失败", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;
						}
					}
				};
				handler.start();

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the image capture
			} else {
				// Image capture failed, advise user
			}
		}
		if (requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE_1
				|| requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE_2
				|| requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE_3) {
			if (resultCode == RESULT_OK) {
				if (data != null) {
					Log.v("DBG", "Data: " + data.toString());
					Uri originalUri = data.getData();
					String[] proj = { MediaStore.Images.Media.DATA };
					Cursor actualimagecursor = managedQuery(originalUri, proj,
							null, null, null);
					int actual_image_column_index = actualimagecursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					actualimagecursor.moveToFirst();
					String img_path = actualimagecursor
							.getString(actual_image_column_index);
					File file = new File(img_path);
					Log.v("DBG", "Get File Path " + file.getAbsolutePath());

					String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE,
							requestCode - GET_IMAGE_ACTIVITY_REQUEST_CODE_1)
							.getAbsolutePath();
					Bitmap bitmap = null;
					try {
						BitmapFactory.Options opts = new BitmapFactory.Options();
						BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
						int srcWidth = opts.outWidth;
						int srcHeight = opts.outHeight;
						int desWidth = 0;
						int desHeight = 0;
						// 缩放比例
						double ratio = 0.0;
						if (srcWidth > srcHeight) {
							// ratio = srcWidth / 640;
							// desWidth = 640;
							// desHeight = (int) (srcHeight / ratio);
							// more = srcWidth % 640 == 0 ? 0 : 1;
							ratio = srcWidth / 640;
							desWidth = (int) (srcWidth / ratio);
							desHeight = (int) (srcHeight / ratio);
						} else {
							// ratio = srcHeight / 640;
							// desHeight = 640;
							// desWidth = (int) (srcWidth / ratio);
							// more = srcHeight % 640 == 0 ? 0 : 1;
							ratio = srcHeight / 640;
							desHeight = (int) (srcHeight / ratio);
							desWidth = (int) (srcWidth / ratio);
						}
						// 设置输出宽度、高度
						BitmapFactory.Options newOpts = new BitmapFactory.Options();
						// newOpts.inSampleSize = (int) (ratio) + more;
						newOpts.inSampleSize = (int) ratio;
						newOpts.inJustDecodeBounds = false;
						newOpts.outWidth = desWidth;
						newOpts.outHeight = desHeight;
						// bitmap =
						// BitmapFactory.decodeFile(file.getAbsolutePath(),
						// newOpts);
						bitmap = RotatePhoto.getPhotoRotated(
								file.getAbsolutePath(), newOpts);
						FileOutputStream out = new FileOutputStream(filename);
						if (bitmap
								.compress(Bitmap.CompressFormat.JPEG, 50, out)) {
							out.flush();
							out.close();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					builder = new AlertDialog.Builder(BabyActivity.this);
					builder.setMessage("上传中");
					dialog = builder.show();
					showBuilder = true;
					List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
					param.add(new BasicNameValuePair(
							"filename",
							getOutputMediaFile(
									MEDIA_TYPE_IMAGE,
									requestCode
											- GET_IMAGE_ACTIVITY_REQUEST_CODE_1)
									.getAbsolutePath()));
					param.add(new BasicNameValuePair("filetype", "image"));
					NetHandler handler = new NetHandler(BabyActivity.this,
							NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
						@Override
						public void handleRsp(Message msg) {
							dialog.dismiss();
							showBuilder = false;

							Bundle bundle = msg.getData();
							int code = bundle.getInt("code");
							if (code == 200) {

								String data = bundle.getString("data");
								try {
									JSONArray array = new JSONArray(data);
									avatarList
											.set(requestCode
													- GET_IMAGE_ACTIVITY_REQUEST_CODE_1,
													array.getJSONObject(0)
															.getString("url"));
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								ImageView avatarButton;
								avatarButton = (ImageView) flipperList
										.get(requestCode
												- GET_IMAGE_ACTIVITY_REQUEST_CODE_1)
										.findViewById(R.id.baby_add_photo);
								File fileName = getOutputMediaFile(
										MEDIA_TYPE_IMAGE,
										requestCode
												- GET_IMAGE_ACTIVITY_REQUEST_CODE_1);
								BitmapFactory.Options options = new BitmapFactory.Options();
								options.inJustDecodeBounds = false;

								Bitmap bit = BitmapFactory.decodeFile(fileName.getPath(), options);
								
								setAvatar(bit, avatarButton);
//								try {
//									Log.v("DBG", String.format("%d %d", height,
//											width));
//									BitmapFactory.Options options = new BitmapFactory.Options();
//									options.inJustDecodeBounds = true;
//
//									BitmapFactory.decodeFile(
//											fileName.getPath(), options);
//									int heightRatio = (int) Math
//											.floor(options.outHeight
//													/ (float) height);
//									int widthRatio = (int) Math
//											.floor(options.outWidth
//													/ (float) width);
//
//									Log.v("HEIGHRATIO", "" + heightRatio);
//									Log.v("WIDTHRATIO", "" + widthRatio);
//
//									Bitmap bit;
//									if (heightRatio == 0 || widthRatio == 0) {
//										BitmapFactory.Options enlargeOptions = new BitmapFactory.Options();
//										options.inJustDecodeBounds = false;
//										Bitmap originalBmp = BitmapFactory
//												.decodeFile(fileName.getPath(),
//														enlargeOptions);
//
//										Matrix matrix = new Matrix();
//
//										int enlargeWidthRatio = (int) (width / originalBmp
//												.getWidth());
//										enlargeWidthRatio = (enlargeWidthRatio == 1) ? enlargeWidthRatio + 1
//												: enlargeWidthRatio;
//										
//										int enlargeHeightRatio = (int) (height / originalBmp
//												.getHeight());
//										enlargeHeightRatio = (enlargeHeightRatio == 1) ? enlargeHeightRatio + 1
//												: enlargeHeightRatio;
//										
//										int enlargeRatio = (enlargeWidthRatio > enlargeHeightRatio) ? enlargeWidthRatio
//												: enlargeHeightRatio;
//
//										matrix.postScale(enlargeRatio,
//												enlargeRatio); // 长和宽放大缩小的比例
//										bit = Bitmap.createBitmap(originalBmp,
//												0, 0, originalBmp.getWidth(),
//												originalBmp.getHeight(),
//												matrix, true);
//									} else {
//										options.inJustDecodeBounds = false;
//										// if (heightRatio > 1 && widthRatio >
//										// 1) {
//										options.inSampleSize = heightRatio > widthRatio ? heightRatio
//												: widthRatio;
//										bit = BitmapFactory.decodeFile(
//												fileName.getPath(), options);
//										// }
//										// if (widthRatio == 0) {
//										// options.outWidth = width;
//										// bit = BitmapFactory
//										// .decodeFile(
//										// fileName.getPath(),
//										// options);
//										// } else {
//										// options.outHeight = height;
//										// bit = BitmapFactory
//										// .decodeFile(
//										// fileName.getPath(),
//										// options);
//										// }
//
//									}
//
//									Bitmap output = Bitmap.createBitmap(width,
//											height, Bitmap.Config.ARGB_8888);
//									Canvas canvas = new Canvas(output);
//
//									final int color = 0xff424242;
//									final Paint paint = new Paint();
//									final Rect rect = new Rect(0, 0, width,
//											height);
//									final RectF rectF = new RectF(rect);
//									final float roundPx = (float) width / 2.0f;
//
//									paint.setAntiAlias(true);
//									canvas.drawARGB(0, 0, 0, 0);
//
//									paint.setColor(color);
//									canvas.drawRoundRect(rectF, roundPx,
//											roundPx, paint);
//									paint.setXfermode(new PorterDuffXfermode(
//											PorterDuff.Mode.SRC_IN));
//									canvas.drawBitmap(
//											bit,
//											null, rect, paint);
//									// options.inSampleSize = 4;
//									avatarButton.setImageBitmap(output);
//								} catch (Exception e) {
//									Log.v("Kite", "exception: " + e);
//									e.printStackTrace();
//								}

								bChanged = true;

								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"上传完成", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								return;

							} else {
								toast.cancel();
								toast = Toast.makeText(getApplicationContext(),
										"上传失败", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();

								return;
							}
						}
					};
					handler.start();
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setAvatar(Bitmap bit, ImageView avatarButton) 
	{
		try {
			Log.v("DBG", String.format("%d %d", height, width));

			Bitmap output = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, width, height);
			final RectF rectF = new RectF(rect);
			final float roundPx = (float) width / 2.0f;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);

			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
			canvas.drawBitmap(bit, null, rect, paint);
			// options.inSampleSize = 4;
			avatarButton.setImageBitmap(output);
		} catch (Exception e) {
			Log.v("Kite", "exception: " + e);
			e.printStackTrace();
		}

		return;
	}
	
	private void initGender(final LinearLayout view) {
		RelativeLayout femaleLayout = (RelativeLayout) view
				.findViewById(R.id.baby_detail_female_layout);
		RelativeLayout maleLayout = (RelativeLayout) view
				.findViewById(R.id.baby_detail_male_layout);

		final ImageView femaleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_female_chooser);
		final ImageView maleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_male_chooser);

		femaleChooser.setVisibility(View.INVISIBLE);
		maleChooser.setVisibility(View.VISIBLE);
		view.setTag(1);

		femaleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.VISIBLE);
				maleChooser.setVisibility(View.INVISIBLE);

				view.setTag(2);
			}

		});

		maleLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				femaleChooser.setVisibility(View.INVISIBLE);
				maleChooser.setVisibility(View.VISIBLE);

				view.setTag(1);
			}

		});
	}

	private void setGender(LinearLayout view, int gender) {
		ImageView femaleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_female_chooser);
		ImageView maleChooser = (ImageView) view
				.findViewById(R.id.baby_detail_male_chooser);

		if (gender == 1) {
			femaleChooser.setVisibility(View.INVISIBLE);
			maleChooser.setVisibility(View.VISIBLE);
		} else {
			femaleChooser.setVisibility(View.VISIBLE);
			maleChooser.setVisibility(View.INVISIBLE);
		}

		view.setTag(gender);
	}

	private void initShield(String dateStr, View shieldView) {
		if (isAddingBaby && dateStr == null) {
			shieldView.setVisibility(View.GONE);

			return;
		}

		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		String curDateStr = dFormat.format(date);

		if (curDateStr.compareTo(dateStr) < 0) {
			shieldView.setVisibility(View.VISIBLE);
		} else {
			shieldView.setVisibility(View.GONE);
		}
	}

	private void initProcess(ImageView process, int index, int babyNum) {
		switch (babyNum) {
		case 1:

			process.setImageResource(R.drawable.baby_detail_1_1);

			break;

		case 2:

			if (index == 0) {
				process.setImageResource(R.drawable.baby_detail_1_2);
			} else {
				process.setImageResource(R.drawable.baby_detail_2_2);
			}

			break;

		case 3:

			if (index == 0) {
				process.setImageResource(R.drawable.baby_detail_1_3);
			} else if (index == 1) {
				process.setImageResource(R.drawable.baby_detail_2_3);
			} else {
				process.setImageResource(R.drawable.baby_detail_3_3);
			}

			break;

		}
	}
}
