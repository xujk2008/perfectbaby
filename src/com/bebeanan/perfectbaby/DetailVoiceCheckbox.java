package com.bebeanan.perfectbaby;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.bebeanan.perfectbaby.common.CustomMediaPlayer;

/**
 * Created by KiteXu.
 */
public class DetailVoiceCheckbox extends CheckBox {

    public CustomMediaPlayer mediaPlayer;
    public String sourceFile;

    public DetailVoiceCheckbox(Context context) {
        super(context);
    }

    public DetailVoiceCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DetailVoiceCheckbox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

}
