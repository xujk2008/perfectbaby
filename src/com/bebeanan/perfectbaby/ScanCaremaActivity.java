package com.bebeanan.perfectbaby;

/**
 * Created by guomoyi on 14-5-11.
 */
import java.util.Timer;
import java.util.TimerTask;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;

public class ScanCaremaActivity extends Activity {
    /** Called when the activity is first created. */
    private SurfaceView sfvCamera;
    private SFHCamera sfhCamera;
    private View centerView;
    private Timer mTimer;
    private MyTimerTask mTimerTask;
    // 按照标准HVGA
    final static int width = 480;
    final static int height = 320;
    int dstLeft, dstTop, dstWidth, dstHeight;
    
    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_camera);
        centerView = (View) findViewById(R.id.centerView);

        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();

        ImageButton backButton = (ImageButton) findViewById(R.id.title_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });

        sfvCamera = (SurfaceView) this.findViewById(R.id.sfvCamera);
        sfhCamera = new SFHCamera(sfvCamera.getHolder(), width, height,
                previewCallback);
        // 初始化定时器
        mTimer = new Timer();
        mTimerTask = new MyTimerTask();
        mTimer.schedule(mTimerTask, 0, 80);
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            if (dstLeft == 0) {//只赋值一次
                dstLeft = centerView.getLeft() * width
                        / getWindowManager().getDefaultDisplay().getWidth();
                dstTop = centerView.getTop() * height
                        / getWindowManager().getDefaultDisplay().getHeight();
                dstWidth = (centerView.getRight() - centerView.getLeft())* width
                        / getWindowManager().getDefaultDisplay().getWidth();
                dstHeight = (centerView.getBottom() - centerView.getTop())* height
                        / getWindowManager().getDefaultDisplay().getHeight();
            }
            sfhCamera.AutoFocusAndPreviewCallback();
        }
    }
    /**
     *  自动对焦后输出图片
     */
    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera arg1) {
            //取得指定范围的帧的数据
            PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(
                    data, width, height, dstLeft, dstTop, dstWidth, dstHeight, false);
            //取得灰度图
            // Bitmap mBitmap = source.renderCroppedGreyscaleBitmap();
            //显示灰度图
            // imgView.setImageBitmap(mBitmap);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            MultiFormatReader reader = new MultiFormatReader();
            try {
                Result result = reader.decode(bitmap);
                String strResult = "BarcodeFormat:"
                        + result.getBarcodeFormat().toString() + "  text:"
                        + result.getText();
                Log.v("DBG", strResult);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
