package com.bebeanan.perfectbaby;

import java.net.URLEncoder;
import java.text.Collator;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.PinyinUtil;
import com.bebeanan.perfectbaby.common.Utils;
import com.perfectbaby.adapters.FollowBabiesExpandableListAdapter;
import com.perfectbaby.adapters.MainListAdapter;
import com.perfectbaby.adapters.FollowBabiesExpandableListAdapter.OnAddBabyListener;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class FollowBabiesActivity extends Activity {

	private ExpandableListView follow_babies_list;
	private ProgressBar follow_babies_progress;

	private ImageView follow_babies_guide;

	private ArrayList<String> mainData;
	private ArrayList<ArrayList<Map<String, Object>>> childData;
	private ArrayList<Map<String, Object>> myBabies = new ArrayList<Map<String, Object>>();
	private ArrayList<Map<String, Object>> myFriends = new ArrayList<Map<String, Object>>();
	private FollowBabiesExpandableListAdapter mAdapter;
	
	private String followeeCacheDataString;

	private static SharedPreferences userPreferences;

	private boolean shouldRefresh = false;
	
	public final int ADD_BABY = 100;

	@Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  		
  		shouldRefresh = true;
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  		
  		if(shouldRefresh)
  		{
  			getBabies();
  			
  			mAdapter.notifyDataSetChanged();
  		}
  	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(followeeCacheDataString != null)
		{
			StoreHandler.storeFolloweeCahce(followeeCacheDataString);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follow_babies);

		if (Utils.application == null) {
			Utils.init(FollowBabiesActivity.this);
		}
		if (userPreferences == null) {
			userPreferences = Utils.application.getSharedPreferences(
					"userInfo", Context.MODE_PRIVATE);
		}

		follow_babies_list = (ExpandableListView) findViewById(R.id.follow_babies_list);
		follow_babies_list.setDivider(null);
		follow_babies_progress = (ProgressBar) findViewById(R.id.follow_babies_progress);

		follow_babies_guide = (ImageView) findViewById(R.id.follow_babies_guide);

		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FollowBabiesActivity.this.finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}

		});

		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("宝宝们");

		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);

		getData();
	}

	private void getData() {
		follow_babies_progress.setVisibility(View.VISIBLE);

		mainData = new ArrayList<String>();
		childData = new ArrayList<ArrayList<Map<String, Object>>>();

		mainData.add("我的宝宝");
		mainData.add("朋友的宝宝");

		getBabies();

		followeeCacheDataString = StoreHandler.getFolloweeCache();
		if(followeeCacheDataString != null)
		{
			follow_babies_progress.setVisibility(View.GONE);
			
			setData(followeeCacheDataString);
			
			getFollowee();
		}
		else
		{
			getFollowee();
		}
	}

	private void getBabies() {
		StoreHandler.getAndSetBabies();
		String baby = MemoryHandler.getInstance().getKey("baby");
		Log.v("Kite", "babyArray is " + baby);

		myBabies = new ArrayList<Map<String, Object>>();
		try {
			JSONArray babyArray = new JSONArray(baby);
			if (babyArray.length() == 0) {

			} else {
				int babyNum = babyArray.length();
				Log.v("DBG", "babyNum:" + babyNum);
				for (int i = 0; i < babyNum; i++) {

					Map<String, Object> temp = new HashMap<String, Object>();

					String avatar = babyArray.getJSONObject(i).has("avatar") ? babyArray
							.getJSONObject(i).getString("avatar") : "";
					temp.put("avatarUrl", avatar);

					String babyId = babyArray.getJSONObject(i).getString("id");
					temp.put("babyId", babyId);

					String babyNameString = babyArray.getJSONObject(i)
							.getString("name");
					if (!babyNameString.equals(" ")) {
						temp.put("nickName", babyNameString);
					} else {
						temp.put("nickName",
								userPreferences.getString("nickName", "")
										+ "的萌宝");
					}

					temp.put("gender",
							babyArray.getJSONObject(i).getInt("gender"));
					String birthday = babyArray.getJSONObject(i).getString(
							"birthday");
					temp.put("birthday", birthday);
					temp.put("age", getAge(birthday));
					myBabies.add(temp);

				}

				Collections.sort(myBabies, comparator);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (childData.size() != 0) {
			childData.remove(0);
			childData.add(0, myBabies);
		} else {
			childData.add(myBabies);
		}
	}

	private void getFollowee()
	{
		myFriends = new ArrayList<Map<String, Object>>();
		NetHandler followeeHandler = new NetHandler(FollowBabiesActivity.this,
				NetHandler.METHOD_GET,
				"/followee?userId=" + userPreferences.getString("userId", "")
						+ "&$limit=0", new LinkedList<BasicNameValuePair>(),
				null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				follow_babies_progress.setVisibility(View.GONE);

				Bundle followeeBundle = msg.getData();
				int code = followeeBundle.getInt("code");
				if (code == 200) {
					Log.v("Kite",
							"followee data is "
									+ followeeBundle.getString("data"));
					
					followeeCacheDataString = followeeBundle.getString("data");
					
					setData(followeeBundle.getString("data"));
				} else {
					Toast toast = Toast.makeText(FollowBabiesActivity.this,
							"加载数据失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		};

		followeeHandler.start();
	}
	
	private void setData(String myFriendsString)
	{
		showGuide();

		try {
			JSONArray followeeArray = new JSONArray(myFriendsString);
			for (int i = 0; i < followeeArray.length(); i++) {
				
				JSONObject curObject = followeeArray
						.getJSONObject(i);
				JSONObject followeeObject = curObject
						.getJSONObject("followee");
				
				Map<String, Object> temp = new HashMap<String, Object>();
				temp.put("id", curObject.getString("id"));
				if (followeeObject.has("avatar")) {
					temp.put("avatarUrl",
							followeeObject.getString("avatar"));
				} else {
					temp.put("avatarUrl", "");
				}

				if(followeeObject.has("nickname"))
				{
					temp.put("nickName",
							followeeObject.getString("nickname"));
				}
				else
				{
					continue;
				}
				
				temp.put("followee", followeeObject.getString("id"));
				
				if(followeeObject.has("gender"))
				{
					temp.put("gender", followeeObject.getInt("gender"));
				}
				else
				{
					temp.put("gender", 1);
				}

				myFriends.add(temp);
			}

			Collections.sort(myFriends, comparator);
			
			childData.add(myFriends);

			mAdapter = new FollowBabiesExpandableListAdapter(
					FollowBabiesActivity.this, mainData, childData);
			mAdapter.setOnAddBabyListener(new OnAddBabyListener() {

				@Override
				public void onAddBaby() {
					// TODO Auto-generated method stub
					// Intent intent = new
					// Intent(FollowBabiesActivity.this,
					// AddBabyActivity.class);
					//
					// FollowBabiesActivity.this.startActivityForResult(intent,
					// ADD_BABY);
					Intent intent = new Intent(
							FollowBabiesActivity.this,
							BabyActivity.class);

					intent.putExtra("addBaby", true);
					FollowBabiesActivity.this
							.startActivityForResult(intent,
									ADD_BABY);
				}

			});
			follow_babies_list.setAdapter(mAdapter);
			follow_babies_list.expandGroup(0);
			follow_babies_list.expandGroup(1);
			follow_babies_list
					.setOnChildClickListener(new OnChildClickListener() {

						@Override
						public boolean onChildClick(
								ExpandableListView parent, View v,
								int groupPosition,
								int childPosition, long id) {
							// TODO Auto-generated method stub

							if (childData.get(groupPosition).size() == 0) {
								return false;
							}

							if (groupPosition == 1) {
								// Intent intent = new
								// Intent(FollowBabiesActivity.this,
								// PersonalZoneActivity.class);
								// intent.putExtra("followeeId",
								// (String)childData.get(groupPosition).get(childPosition).get("followee"));
								// intent.putExtra("followeeNickName",
								// (String)childData.get(groupPosition).get(childPosition).get("nickName"));
								// intent.putExtra("followeeAvatarUrl",
								// (String)childData.get(groupPosition).get(childPosition).get("avatarUrl"));

								Intent intent = new Intent(
										FollowBabiesActivity.this,
										PersonalDetailActivity.class);
								intent.putExtra(
										"followeeId",
										(String) childData
												.get(groupPosition)
												.get(childPosition)
												.get("followee"));
								intent.putExtra(
										"followeeNickName",
										(String) childData
												.get(groupPosition)
												.get(childPosition)
												.get("nickName"));
								intent.putExtra(
										"followeeAvatarUrl",
										(String) childData
												.get(groupPosition)
												.get(childPosition)
												.get("avatarUrl"));

								FollowBabiesActivity.this
										.startActivity(intent);
							} else {
								Map<String, Object> currentBaby = childData
										.get(groupPosition).get(
												childPosition);
								String babyId = (String) currentBaby
										.get("babyId");

								SimpleDateFormat sdf = new SimpleDateFormat(
										"yyyy-MM-dd");
								Calendar calendar = Calendar
										.getInstance();
								calendar.set(Calendar.HOUR_OF_DAY,
										0);
								calendar.set(Calendar.MINUTE, 0);
								calendar.set(Calendar.SECOND, 0);
								calendar.set(Calendar.MILLISECOND,
										0);
								calendar.setFirstDayOfWeek(Calendar.SUNDAY);

								String uri = "/feeds?";
								try {
									uri += URLEncoder
											.encode("{\"baby\":\""
													+ babyId
													+ "\",\"day\":\""
													+ sdf.format(calendar
															.getTime())
													+ "\"}",
													"UTF-8");
								} catch (Exception e) {
									e.printStackTrace();
								}

								Intent intent = new Intent();
								intent.putExtra("babyid", babyId);
								intent.putExtra("uri", uri);
								intent.putExtra("date", sdf
										.format(calendar.getTime()));
								intent.putExtra("nickName",
										(String) currentBaby
												.get("nickName"));
								intent.putExtra("avatarUrl",
										(String) currentBaby
												.get("avatarUrl"));
								intent.putExtra("birthday",
										(String) currentBaby
												.get("birthday"));
								// intent.setClass(TreeActivity.this,
								// DetailActivity.class);
								intent.setClass(
										FollowBabiesActivity.this,
										PersonalZoneActivity.class);
								startActivity(intent);
							}

							return false;
						}

					});
			follow_babies_list
					.setOnItemLongClickListener(new OnItemLongClickListener() {

						@Override
						public boolean onItemLongClick(
								AdapterView<?> parent, View view,
								int position, long id) {
							// TODO Auto-generated method stub
							int groupPosition = (Integer) view
									.getTag(R.id.group_position);
							final int childPosition = (Integer) view
									.getTag(R.id.child_position);

							if (groupPosition != -1
									&& childPosition != -1) {
								AlertDialog.Builder builder = new Builder(
										FollowBabiesActivity.this);

								switch (groupPosition) {
								case 0:

									builder.setMessage("确定删除自己的宝宝和他的所有记录吗？");
									builder.setTitle("提示");

									builder.setPositiveButton(
											"确定",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {

													deleteBaby(childPosition);
												}
											});

									builder.setNegativeButton(
											"取消",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {

												}
											});

									builder.create().show();

									break;

								case 1:

									builder.setMessage("确认取消关注朋友的宝宝吗？");
									builder.setTitle("提示");

									builder.setPositiveButton(
											"确定",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {

													deleteFollowee(childPosition);
												}
											});

									builder.setNegativeButton(
											"取消",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {

												}
											});

									builder.create().show();

									break;
								}
							}

							return true;
						}

					});

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	private void showGuide() {
		boolean followBabiesGuideShown = StoreHandler
				.getGuide(StoreHandler.followBabiesGuide);
		if (!followBabiesGuideShown) {
			follow_babies_guide.setVisibility(View.VISIBLE);
			follow_babies_guide
					.setImageResource(R.drawable.follow_babies_guide);
			follow_babies_guide.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					follow_babies_guide.setVisibility(View.GONE);

					StoreHandler.setGuide(StoreHandler.followBabiesGuide, true);

					return;
				}

			});
		}
	}

	private void deleteBaby(final int childPosition) {
		try {
			StoreHandler.getAndSetBabies();
			String baby = MemoryHandler.getInstance().getKey("baby");
			Log.v("Kite", "babyArray is " + baby);

			myBabies = new ArrayList<Map<String, Object>>();
			final JSONArray babyArray = new JSONArray(baby);

			List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
			String uri;
			uri = "/baby/" + childData.get(0).get(childPosition).get("babyId");
			NetHandler handler = new NetHandler(FollowBabiesActivity.this,
					NetHandler.METHOD_DELETE, uri, param, null) {

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method
					// stub
					Bundle bundle = msg.getData();
					final int code = bundle.getInt("code");
					if (code == 200) {
						Toast toast = Toast.makeText(getApplicationContext(),
								"删除成功", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();

						JSONArray newBabyArray = new JSONArray();
						for (int i = 0; i < babyArray.length(); i++) {
							if (i != childPosition) {
								try {
									newBabyArray.put(babyArray.get(i));
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						Log.v("Kite",
								"newBabyArray is " + newBabyArray.toString());
						StoreHandler.storeBabies(newBabyArray.toString());
						MemoryHandler.getInstance().setKey("baby",
								newBabyArray.toString());

						myBabies = childData.get(0);
						myBabies.remove(childPosition);
						childData.remove(0);
						childData.add(0, myBabies);
						mAdapter.notifyDataSetChanged();
					} else {
						Toast toast = Toast.makeText(getApplicationContext(),
								"删除失败", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				}
			};

			handler.start();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void deleteFollowee(final int childPosition) {
		NetHandler followeeHandler = new NetHandler(FollowBabiesActivity.this,
				NetHandler.METHOD_DELETE, "/followee?id="
						+ childData.get(1).get(childPosition).get("id"),
				new LinkedList<BasicNameValuePair>(), null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				follow_babies_progress.setVisibility(View.GONE);

				Bundle followeeBundle = msg.getData();
				int code = followeeBundle.getInt("code");
				if (code == 200) {

					Toast toast = Toast.makeText(getApplicationContext(),
							"删除成功", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					myFriends = childData.get(1);
					myFriends.remove(childPosition);
					childData.remove(1);
					childData.add(1, myFriends);

					mAdapter.notifyDataSetChanged();
					
					if(followeeCacheDataString != null)
					{
						try {
							
							JSONArray followeeCacheDataArray = new JSONArray(followeeCacheDataString);
							
							JSONArray tempFolloweeCacheDataArray = new JSONArray();
							
							for (int i = 0; i < followeeCacheDataArray.length(); i++) 
							{
								if(i != childPosition)
								{
									tempFolloweeCacheDataArray.put(followeeCacheDataArray
											.getJSONObject(i));
								}
							}
							
							followeeCacheDataString = tempFolloweeCacheDataArray.toString();
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				} else {
					Toast toast = Toast.makeText(FollowBabiesActivity.this,
							"删除失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		};

		followeeHandler.start();
	}

	private String getAge(String birthday) {
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date curData = new Date(System.currentTimeMillis());
		String curDateStr = dFormat.format(curData);

		if (curDateStr.compareTo(birthday) < 0) {
			return "尚未出生";
		}

		try {
			Date birthdayDate = dFormat.parse(birthday);
			long diffDay = (curData.getTime() - birthdayDate.getTime())
					/ (1000 * 3600 * 24);
			long diffYear = diffDay / 365;
			long diffMonth = (diffDay % 365) / 30;

			StringBuffer buffer = new StringBuffer();
			if (diffYear != 0) {
				buffer.append(diffYear + "岁");

				if (diffYear<3 && diffMonth != 0) {
					buffer.append(diffMonth + "个月");
				}
			} else if (diffMonth != 0) {
				buffer.append(diffMonth + "个月");

				long leftDay = (diffDay%365)%30;
				if (leftDay != 0) {
					buffer.append(leftDay + "天");
				}
			} else {
				buffer.append(diffDay + "天");
			}

			return buffer.toString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

	Comparator<Map<String, Object>> comparator = new Comparator<Map<String, Object>>(){

//		RuleBasedCollator collator = (RuleBasedCollator) Collator.getInstance(Locale.CHINA);
		
		@Override
		public int compare(Map<String, Object> lhs, Map<String, Object> rhs) {
			// TODO Auto-generated method stub
			String lName = PinyinUtil.getPingYin((String)lhs.get("nickName"));
			String rName = PinyinUtil.getPingYin((String)rhs.get("nickName"));
			
			return lName.compareToIgnoreCase(rName);
		}
		
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == ADD_BABY) {
			if (resultCode == RESULT_OK) {
				getBabies();

				mAdapter.notifyDataSetChanged();
			}
		}
	}

}
