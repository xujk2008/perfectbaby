package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * Created by KiteXu.
 */
public class MyMessageActivity extends Activity {

    private List<PushMessage> pushMessageList;
    
    MessageManager messageManager;

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            for(int i = 0; i < pushMessageList.size(); i++) {
                if (pushMessageList.get(i).id == view.getId()) {
                    if (pushMessageList.get(i).isid == 0) {
                        Intent intent = new Intent(MyMessageActivity.this, DetailSystemActivity.class);  //自定义打开的界面
                        intent.putExtra("url", pushMessageList.get(i).url);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(MyMessageActivity.this, DetailSystemActivity.class);  //自定义打开的界面
                        intent.putExtra("id", pushMessageList.get(i).url);
                        startActivity(intent);
                    }
                    
                    PushMessage message = new PushMessage();
                    message.id = view.getId();
                    message.isread = 1;
                    messageManager.setRead(message);
                    
                    view.setBackgroundResource(R.drawable.post_item_selector);
                }
            }
        }
    }
    
    private class LongClickListener implements View.OnLongClickListener {

		@Override
		public boolean onLongClick(final View view) {
			// TODO Auto-generated method stub
			
			AlertDialog.Builder builder = new Builder(
					MyMessageActivity.this);

                	builder.setMessage("确定删除这条通知吗？");
					builder.setTitle("提示");

					builder.setPositiveButton(
							"确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {

									messageManager.deleteMessage(view.getId());
								}
							});

					builder.setNegativeButton(
							"取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {

								}
							});

					builder.create().show();
        
			
			return true;
		}
    }

    private ClickListener clickListener;
    
    private LongClickListener longClickListener;

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_message);

        clickListener = new ClickListener();

        final LinearLayout layout = (LinearLayout) findViewById(R.id.message_list_layout);
        messageManager = new MessageManager(this);
        pushMessageList = messageManager.getMessage();
        for(int i = 0; i < pushMessageList.size(); i++) {
//            LayoutInflater inflater = getLayoutInflater();
//            TextView textView = (TextView) inflater.inflate(R.layout.message_layout, null);
//            textView.setText(pushMessageList.get(i).text);
//            textView.setId(pushMessageList.get(i).id);
//            textView.setOnClickListener(clickListener);
//            layout.addView(textView);
            
            final TextView textView = new TextView(MyMessageActivity.this);
			textView.setTypeface(Utils.typeface);
			textView.setSingleLine(true);
			textView.setText(pushMessageList.get(i).text);
            textView.setId(pushMessageList.get(i).id);
			textView.setClickable(true);
			textView.setOnClickListener(clickListener);
			textView.setOnLongClickListener(new OnLongClickListener(){

				@Override
				public boolean onLongClick(final View view) {
					// TODO Auto-generated method stub
					AlertDialog.Builder builder = new Builder(
							MyMessageActivity.this);

		                	builder.setMessage("确定删除这条通知吗？");
							builder.setTitle("提示");

							builder.setPositiveButton(
									"确定",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											
											layout.removeView(view);
											messageManager.deleteMessage(view.getId());
										}
									});

							builder.setNegativeButton(
									"取消",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {

										}
									});

							builder.create().show();
		        
					
					return true;
				}
				
			});
			
			if(pushMessageList.get(i).isread == 0)
			{
				textView.setBackgroundResource(R.drawable.post_item_option_selector);
			}
			else
			{
				textView.setBackgroundResource(R.drawable.post_item_selector);
			}
			
			layout.addView(textView);
			
			textView.setGravity(Gravity.CENTER_VERTICAL);
			textView.setPadding((int)(10*Utils.density), 0, (int)(10*Utils.density), 0);
        }

        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.drawable.title_back_pic);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText("通知");
        titleView.setTypeface(Utils.typeface);
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
