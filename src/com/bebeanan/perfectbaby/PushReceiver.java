package com.bebeanan.perfectbaby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

import java.util.Set;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by guomoyi on 14-6-2.
 */
public class PushReceiver extends cn.jpush.android.service.PushReceiver {
    public PushReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.v("DBG", "onReceive - " + intent.getAction());

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            System.out.println("收到了自定义消息。消息内容是：" + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            // 自定义消息不会展示在通知栏，完全要开发者写代码去处理
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            System.out.println("收到了通知");
            // 在这里可以做些统计，或者做些其他工作

            MessageManager messageManager = new MessageManager(context);
            PushMessage message = new PushMessage();
            message.id = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            message.isread = 0;
            message.text = bundle.getString(JPushInterface.EXTRA_ALERT);
            String jsonStr = bundle.getString(JPushInterface.EXTRA_EXTRA);
            try {
                JSONObject object = new JSONObject(jsonStr);
                if (object.has("sid")) {
                    message.isid = 1;
                    message.url = object.getString("sid");
                }
                if (object.has("url")) {
                    message.isid = 0;
                    message.url = object.getString("url");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            messageManager.AddMessage(message);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            System.out.println("用户点击打开了通知");
            String jsonStr = bundle.getString(JPushInterface.EXTRA_EXTRA);

            MessageManager messageManager = new MessageManager(context);
            PushMessage message = new PushMessage();
            message.id = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            message.isread = 1;
            messageManager.setRead(message);

            // 在这里可以自己写代码去定义用户点击后的行为
            Intent i = new Intent(context, DetailSystemActivity.class);  //自定义打开的界面
            try {
                JSONObject object = new JSONObject(jsonStr);
                if (object.has("sid")) {
                    i.putExtra("id", object.getString("sid"));
                }
                if (object.has("url")) {
                    i.putExtra("url", object.getString("url"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);


        } else {
            Log.v("DBG", "Unhandled intent - " + intent.getAction());
        }
        super.onReceive(context, intent);
    }
}
