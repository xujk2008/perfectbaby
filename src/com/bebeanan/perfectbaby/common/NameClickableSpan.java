package com.bebeanan.perfectbaby.common;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.bebeanan.perfectbaby.FeedDetailActivity;
import com.bebeanan.perfectbaby.PersonalDetailActivity;
/**
 * Created by KiteXu.
 */
public class NameClickableSpan extends ClickableSpan{

	private Context context;
	private String userId;
	private String nickName;
//	private String avatarUrl;
	
	private static SharedPreferences userPreferences = Utils.application
			.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
	
	public NameClickableSpan(Context context, String userId, String nickName)
	{
		this.context = context;
		this.userId = userId;
		this.nickName = nickName;
//		this.avatarUrl = avatarUrl;
	}
	
	@Override
	public void onClick(View widget) {
		// TODO Auto-generated method stub
		if (userId.equals(userPreferences.getString("userId", ""))) {
			Intent intent = new Intent(
					context,
					PersonalDetailActivity.class);
			intent.putExtra(
					"followeeId",
					userId);
			intent.putExtra(
					"followeeNickName",
					userPreferences.getString("nickName", ""));
			intent.putExtra(
					"followeeAvatarUrl",
					userPreferences.getString("avatarUrl", ""));

			context.startActivity(intent);
		} else {
			
			Intent intent = new Intent(context, PersonalDetailActivity.class);
			intent.putExtra("followeeId", userId);
			
			context.startActivity(intent);
		}
	}

	@Override
	public void updateDrawState(TextPaint ds) {
		// TODO Auto-generated method stub
		super.updateDrawState(ds);
		
		ds.setUnderlineText(false);
		ds.setARGB(255, 35, 177, 131);
	}
}
