package com.bebeanan.perfectbaby.common;

import java.io.IOException;
import java.io.OutputStream;

import com.bebeanan.perfectbaby.common.MultipartEntityWithProgressBar.WriteListener;
/**
 * Created by KiteXu.
 */
public class OutputStreamProgress extends OutputStream {

	private long totalByteOfFile;
    private final OutputStream outstream;
    private long bytesWritten=0;
    private final WriteListener writeListener;
    public OutputStreamProgress(OutputStream outstream, WriteListener writeListener, long totalByteOfFile) {
        this.outstream = outstream;
        this.writeListener = writeListener;
        this.totalByteOfFile = totalByteOfFile;
    }

    @Override
    public void write(int b) throws IOException {
        outstream.write(b);
        bytesWritten++;
        writeListener.registerWrite(bytesWritten, totalByteOfFile);
    }

    @Override
    public void write(byte[] b) throws IOException {
        outstream.write(b);
        bytesWritten += b.length;
        writeListener.registerWrite(bytesWritten, totalByteOfFile);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        outstream.write(b, off, len);
        bytesWritten += len;
        writeListener.registerWrite(bytesWritten, totalByteOfFile);
    }

    @Override
    public void flush() throws IOException {
        outstream.flush();
    }

    @Override
    public void close() throws IOException {
        outstream.close();
    }
}
