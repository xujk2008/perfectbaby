package com.bebeanan.perfectbaby.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
/**
 * Created by KiteXu.
 */
public class Utils {
	public static Application application;
	public static float density;
	public static int screenHeight;
//	public static String nickName;
//	public static String city;
//	public static String avatarUrl;
//	public static int gender;
//	public static String userId;
	public static Typeface typeface;
	public static Map<String, Object> feedDetail;
	
	public static void init(Context context)
	{
		DisplayMetrics dm = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        dm = context.getApplicationContext().getResources().getDisplayMetrics();
        Utils.density = dm.density;
        Utils.screenHeight = dm.heightPixels;
        
        Utils.application = ((Activity)context).getApplication();
        
        Utils.typeface = Typeface.createFromAsset(context.getAssets(),"fonts/handwriting.fon");
	}
	
	public static List<Map<String, Integer>> getUrl(String text)
    {
    	Pattern urlPattern = Pattern.compile(
    	        "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
    	                + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
    	                + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
    	        Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    	Matcher matcher = urlPattern.matcher(text);
    	
    	List<Map<String, Integer>> result = new ArrayList<Map<String, Integer>>();
    	while (matcher.find()) {
    	    int matchStart = matcher.start(1);
    	    int matchEnd = matcher.end();
    	    
    	    Map<String, Integer> temp = new HashMap<String, Integer>();
    	    temp.put("urlStart", matchStart);
    	    temp.put("urlEnd", matchEnd);
    	    Log.v("Kite", "In Utils, urlStart: " + matchStart + " urlEnd: " + matchEnd);
    	    
    	    result.add(temp);
    	}
    	
        return result;
    }
}
