package com.bebeanan.perfectbaby.common;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.bebeanan.perfectbaby.NetHandler;
import com.bebeanan.perfectbaby.ReportActivity;
import com.perfectbaby.adapters.MainListAdapter;

/**
 * Created by KiteXu.
 */
public class FeedsOperations {
	
	private Context context;
	
	public FeedsOperations(Context context)
	{
		this.context = context;
	}

	public void addFavFeeds(String feedId, String userId, String nickName, String feedType, String feedOwnerId)
	{

		// TODO Auto-generated method stub

		JSONObject favObject = new JSONObject();
		try {
			favObject.put("feedId", feedId);
//			JSONObject user = new JSONObject();
//			user.put("id", userId);
//			user.put("nickname", nickName);
//			favObject.put("user", user);
			favObject.put("feedType", feedType);
//			favObject.put("feedOwner", feedOwnerId);

			NetHandler handlerCommet = new NetHandler(context, 
					NetHandler.METHOD_POST, "/favorfeeds",
					new LinkedList<BasicNameValuePair>(),
					favObject) {

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					Bundle bundleFav = msg.getData();
					int codeFav = bundleFav.getInt("code");

					if (codeFav == 200) {
						Log.v("Kite", "收藏成功");
						
						Toast toast = Toast.makeText(context, "收藏成功", Toast.LENGTH_SHORT);
						toast.show();
					} else {
						Log.v("Kite",
								"fav fail because: "
										+ bundleFav
												.getString("data"));
						
						Toast toast = Toast.makeText(context, "收藏失败", Toast.LENGTH_SHORT);
						toast.show();
					}
				}

			};

			handlerCommet.start();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public void addShield(String userId, String feedsOwnerId)
	{

		// TODO Auto-generated method stub

		List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
		param.add(new BasicNameValuePair("userId", userId));
        param.add(new BasicNameValuePair("followee", feedsOwnerId));
		NetHandler followeeHandler = new NetHandler(context, NetHandler.METHOD_GET, "/followee/", param, null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				Bundle followeeBundle = msg.getData();
				int followeeCode = followeeBundle
						.getInt("code");
				
				if(followeeCode == 200)
				{
					String followeeData = followeeBundle.getString("data");
					Log.v("Kite", "followee data is " + followeeData);
					try {
						JSONArray followeeArray = new JSONArray(followeeData);
						String followeeId = followeeArray.getJSONObject(0).getString("id");
						
						JSONObject blockObject = new JSONObject();
						try {
							blockObject.put("blocked", true);

							NetHandler handlerCommet = new NetHandler(context, 
									NetHandler.METHOD_PUT, "/followee/" + followeeId,
									new LinkedList<BasicNameValuePair>(),
									blockObject) {

								@Override
								public void handleRsp(Message msg) {
									// TODO Auto-generated method stub
									Bundle bundleBlock = msg.getData();
									int codeBlock = bundleBlock
											.getInt("code");

									if (codeBlock == 200) {
										Log.v("Kite", "屏蔽成功");
									} else {
										Log.v("Kite",
												"block fail because: "
														+ bundleBlock
																.getString("data"));
									}
								}

							};

							handlerCommet.start();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else
				{
					Log.v("Kite",
							"get followeeId fail because: "
									+ followeeBundle
											.getString("data"));
				}
			}
			
		};
		followeeHandler.start();
	
	}
	
	public void reportFeeds(String feedId)
	{
		Intent intent = new Intent(context, ReportActivity.class);
		intent.putExtra("feedId", feedId);
		context.startActivity(intent);
	}
	
}
