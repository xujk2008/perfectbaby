package com.bebeanan.perfectbaby.common;

import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
/**
 * Created by KiteXu.
 */
public class RotatePhoto {

	public static Bitmap getPhotoRotated(String photoPath, BitmapFactory.Options opts)
	{
		Bitmap result = null;

		try {
			ExifInterface ei = new ExifInterface(photoPath);
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

			Bitmap bitmap = BitmapFactory.decodeFile(photoPath, opts);
			
			switch(orientation) {
			    case ExifInterface.ORIENTATION_ROTATE_90:
			    	result = rotateImage(bitmap, 90);
			        break;
			    case ExifInterface.ORIENTATION_ROTATE_180:
			        result = rotateImage(bitmap, 180);
			        break;
			    case ExifInterface.ORIENTATION_ROTATE_270:
			        result = rotateImage(bitmap, 270);
			        break;
			    default:
			        result = bitmap;
			        break;
			    // etc.
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static Bitmap rotateImage(Bitmap bitmap, int degree)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);
		bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		
		return bitmap;
	}
}
