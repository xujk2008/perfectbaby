package com.bebeanan.perfectbaby.common;

import android.media.MediaPlayer;
/**
 * Created by KiteXu.
 */
public class CustomMediaPlayer extends MediaPlayer{
	private boolean isReleased = false;
	private boolean isPaused = false;
	
	public void setReleased(boolean isReleased)
	{
		this.isReleased = isReleased;
	}
	
	public boolean getReleased()
	{
		return isReleased;
	}
	
	public void setPaused(boolean isPaused)
	{
		this.isPaused = isPaused;
	}
	
	public boolean getPaused()
	{
		return isPaused;
	}
}
