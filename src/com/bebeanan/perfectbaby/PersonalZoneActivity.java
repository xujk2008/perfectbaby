package com.bebeanan.perfectbaby;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.perfectbaby.adapters.MainListAdapter;
import com.perfectbaby.adapters.PersonalZoneAdapter;
import com.perfectbaby.adapters.PersonalZoneAdapter.OnTouchingZoomInListener;
import com.perfectbaby.adapters.PersonalZoneAdapter.OnVideoClickListener;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class PersonalZoneActivity extends Activity {

	private PullToRefreshListView personal_zone_list;
	private ArrayList<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
	private PersonalZoneAdapter mAdapter;

	private SharedPreferences userPreferences;

	private String babyId, baby, dateStr = "";
	private String originalDate;
	private String oldestDate, latestDate;
	private String birthday;

	private String followeeId = null;
	private String since = null;

	private Map<String, Object> banner;

	private ProgressBar personal_zone_progress;

	private float lastY = 0;

	private boolean shouldRefresh = false;
	private boolean isOwn = false;

	private static final int FEED_DETAIL_REQUEST_CODE = 1003;
	public static final int FEED_DETAIL_DELETE_CODE = 10031;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);

		shouldRefresh = true;
		
		if(mAdapter != null)
		{
			mAdapter.stopVoice();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);

		if(shouldRefresh)
		{
			if(isOwn && mAdapter != null)
			{
				refreshOwnbabies();
			}
			else
			{
				getOlderEvents(oldestDate);
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_zone);

		if (Utils.application == null) {
			Utils.init(PersonalZoneActivity.this);
		}
		if (userPreferences == null) {
			userPreferences = Utils.application.getSharedPreferences(
					"userInfo", Context.MODE_PRIVATE);
		}

		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PersonalZoneActivity.this.finish();
			}

		});

		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);

		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.VISIBLE);
		title_right.setImageResource(R.drawable.personal_zone_title_right);
		title_right.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PersonalZoneActivity.this,
						BrowserActivity.class);
				intent.putExtra("url", "http://share.bebeanan.com/babysearch/table.html");
				intent.putExtra("title", "育儿指南");

				PersonalZoneActivity.this.startActivity(intent);
			}
			
		});

		personal_zone_list = (PullToRefreshListView) findViewById(R.id.personal_zone_list);

		personal_zone_progress = (ProgressBar) findViewById(R.id.personal_zone_progress);

		initView();

		followeeId = this.getIntent().getStringExtra("followeeId");
		babyId = this.getIntent().getStringExtra("babyid");
		birthday = this.getIntent().getStringExtra("birthday");
		StoreHandler.getAndSetBabies();
		baby = MemoryHandler.getInstance().getKey("baby");

		dateStr = this.getIntent().getStringExtra("date");
		
		latestDate = this.getIntent().getStringExtra("date");

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date curDate = sdf.parse(dateStr);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(curDate);
			calendar.add(Calendar.DATE, 1);
			oldestDate = sdf.format(calendar.getTime());
			originalDate = oldestDate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (followeeId == null
				|| followeeId.equals(userPreferences.getString("userId", ""))) {
			isOwn = true;

			banner = new HashMap<String, Object>();
			banner.put(MainListAdapter.HEAD_BACKGROUND,
					userPreferences.getString("headBackground", ""));
			banner.put(MainListAdapter.HEAD_BACKGROUND_URL,
					userPreferences.getString("headBackgroundUrl", ""));
			String babyName = this.getIntent().getStringExtra("nickName");
			if (babyName.equals(" ")) {
				babyName = userPreferences.getString("nickName", "") + "家宝宝";
			}
			banner.put(MainListAdapter.HEAD_NICKNAME, babyName);
			banner.put(MainListAdapter.HEAD_AVATAR, this.getIntent()
					.getStringExtra("avatarUrl"));
			mData.add(banner);
		} else {
			banner = new HashMap<String, Object>();
			banner.put(MainListAdapter.HEAD_BACKGROUND,
					userPreferences.getString("headBackground", ""));
			banner.put(MainListAdapter.HEAD_BACKGROUND_URL,
					userPreferences.getString("headBackgroundUrl", ""));
			banner.put(MainListAdapter.HEAD_NICKNAME, this.getIntent()
					.getStringExtra("nickName"));
			banner.put(MainListAdapter.HEAD_AVATAR, this.getIntent()
					.getStringExtra("avatarUrl"));
			mData.add(banner);
		}

		title_text.setText(banner.get(MainListAdapter.HEAD_NICKNAME) + "的小窝");

		personal_zone_progress.setVisibility(View.VISIBLE);
//		getOwnEvents(getIntent().getStringExtra("uri"), dateStr, 0);
		
		getOlderEvents(oldestDate);
	}

	private void refreshOwnbabies() {
		StoreHandler.getAndSetBabies();
		String baby = MemoryHandler.getInstance().getKey("baby");
		Log.v("Kite", "babyArray is " + baby);

		JSONArray babiesArray;
		try {
			babiesArray = new JSONArray(baby);

			for (int i = 0; i < babiesArray.length(); i++) {
				JSONObject babyObject = babiesArray.getJSONObject(i);
				if (babyObject.getString("id").equals(babyId)) {

					String babyName = babyObject.getString("name");
					if (babyName.equals("") || babyName.equals(" ")) {
						babyName = userPreferences.getString("nickName", "")
								.toString() + "家宝宝";
					} else {
						babyName = babyObject.getString("name");
					}
					banner.put(MainListAdapter.HEAD_NICKNAME, babyName);

					String babyAvatar = babyObject.has("avatar") ? babyObject
							.getString("avatar") : "";
					banner.put(MainListAdapter.HEAD_AVATAR, babyAvatar);

					birthday = babyObject.getString("birthday");

					if (mData != null) {
						for (int j = 1; j < mData.size(); j++) {
							Map<String, Object> cur = mData.get(j);

							if (cur.containsKey("currentDateStr")) {
								String currentDateStr = (String) cur
										.get("currentDateStr");
								cur.put(MainListAdapter.ZONE_LIST_DATE,
										getDayNumber(currentDateStr));
							}
						}
					}

					mAdapter.notifyDataSetChanged();
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initView() {

		personal_zone_list.setMode(Mode.BOTH);
		ILoadingLayout RefreshLabels = personal_zone_list
				.getLoadingLayoutProxy();
		RefreshLabels.setTextTypeface(Utils.typeface);
		
//		personal_zone_list.setOnRefreshListener(new OnRefreshListener<ListView>(){
//
//			@Override
//			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//				// TODO Auto-generated method stub
//				getOlderEvents(oldestDate);
//			}
//			
//		});

		personal_zone_list
				.setOnRefreshListener(new OnRefreshListener2<ListView>() {

					@Override
					public void onPullDownToRefresh(
							PullToRefreshBase<ListView> refreshView) {
						getOlderEvents(originalDate);
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<ListView> refreshView) {

						// TODO Auto-generated method stub

						getOlderEvents(oldestDate);
					}

				});
	}

	private void getOlderEvents(final String old) {
		String prevDayUri = "/feeds/prevday?";
		try {
			prevDayUri += URLEncoder.encode("{\"baby\":\"" + babyId
					+ "\",\"day\":\"" + old + "\"}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NetHandler dateHandler = new NetHandler(PersonalZoneActivity.this,
				NetHandler.METHOD_GET, prevDayUri,
				new LinkedList<BasicNameValuePair>(), null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");

				if (code == 200) {
					
					if(old.equals(originalDate))
					{
						mAdapter = null;
						mData.clear();
						mData.add(banner);
					}
					
					String data = bundle.getString("data");
					try {
						JSONObject dateObject = new JSONObject(data);
						String prevDay = dateObject.getString("day");

						if (followeeId == null
								|| followeeId.equals(userPreferences.getString("userId", "")))
						{
							String intentDate = PersonalZoneActivity.this.getIntent().getStringExtra("date");
							if(!prevDay.equals(intentDate) && mAdapter == null)
							{
								Map<String, Object> temp = new HashMap<String, Object>();
								temp.put("feedId", "empty");
								temp.put(MainListAdapter.ZONE_LIST_DATE,
										getDayNumber(intentDate));
								
								mData.add(temp);
							}
						}
						
						String uri = "/feeds?";

						try {
							uri += URLEncoder.encode("{\"baby\":\"" + babyId
									+ "\",\"day\":\"" + prevDay + "\"}",
									"UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (followeeId == null
								|| followeeId.equals(userPreferences.getString("userId", "")))
						{
							if(!prevDay.equals(dateStr) && mAdapter == null)
							{
								getOwnEvents(uri, prevDay, 0);
								
								return;
							}
						}
						
						if(mAdapter == null)
						{
							getOwnEvents(uri, prevDay, 0);
						}
						else
						{
							getOwnEvents(uri, prevDay, mData.size() + 1);
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					//第一次刷新时没有数据，需要绑定适配器
					if(mAdapter == null)
					{
						Toast toast = Toast.makeText(PersonalZoneActivity.this,
								"还没有内容哟~", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						
						personal_zone_progress.setVisibility(View.GONE);
						
						if (followeeId == null
								|| followeeId.equals(userPreferences.getString("userId", "")))
						{
								Map<String, Object> temp = new HashMap<String, Object>();
								temp.put("feedId", "empty");
								temp.put(MainListAdapter.ZONE_LIST_DATE,
										getDayNumber(dateStr));
								
								mData.add(temp);
						}
						
						setAdapterToList(0);
					}
					else
					{
						Toast toast = Toast.makeText(PersonalZoneActivity.this,
								"没有更多了", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();

						personal_zone_list.onRefreshComplete();
					}
				}
			}
		};
		dateHandler.start();

	}

	private void getOwnEvents(String uri, final String currentDateStr,
			final int selectionAfterLoad) {

		NetHandler handler = new NetHandler(PersonalZoneActivity.this,
				NetHandler.METHOD_GET, uri,
				new LinkedList<BasicNameValuePair>(), null) {
			@Override
			public void handleRsp(Message msg) {

				personal_zone_progress.setVisibility(View.GONE);

				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				if (code == 200) {
					personal_zone_list.onRefreshComplete();

					Log.v("Kite",
							"personalZone date is " + bundle.getString("data"));

					String data = bundle.getString("data");
					Log.v("Kite", "feeds data is " + data);

					setData(data, selectionAfterLoad);

				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							"加载失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					Log.v("Kite",
							"personalZone date fail because of "
									+ bundle.getString("data"));
				}
			}
		};
		handler.start();
	}

	private void setAdapterToList(int selectionAfterLoad)
	{
		if (followeeId == null) {
			mAdapter = new PersonalZoneAdapter(PersonalZoneActivity.this,
					mData, null, babyId);
		} else {
			mAdapter = new PersonalZoneAdapter(PersonalZoneActivity.this,
					mData, followeeId);
		}

		mAdapter.setOnTouchingZoomInListener(new OnTouchingZoomInListener() {

			@Override
			public void onTouchingZoomIn() {
				// TODO Auto-generated method stub
				int itemIndex = mAdapter.getItemIndex();
				ArrayList<String> imageResourceList = (ArrayList) mData
						.get(itemIndex).get(MainListAdapter.IMAGE_LIST);

				int imageIndex = mAdapter.getImageIndex();

				Intent intent = new Intent(PersonalZoneActivity.this,
						GalleryActivity.class);
				intent.putStringArrayListExtra("imageResourceList",
						imageResourceList);
				intent.putExtra("imageIndex", imageIndex);

				PersonalZoneActivity.this.startActivity(intent);
				PersonalZoneActivity.this.overridePendingTransition(
						R.anim.zoom_in, 0);
			}
		});

		mAdapter.setOnVideoClickListener(new OnVideoClickListener() {

			@Override
			public void onVideoClickListener(String videoUrl) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PersonalZoneActivity.this,
						VideoActivity.class);
				intent.putExtra("videoUrl", videoUrl);
				PersonalZoneActivity.this.startActivity(intent);
			}
		});

		// mAdapter.setOnShareImageClickListener(new
		// OnShareImageClickListener() {
		//
		// @Override
		// public void onShareImageClick() {
		// // TODO Auto-generated method stub
		// }
		//
		// });

		// mAdapter.setOnCommentImageClickListener(new
		// OnCommentImageClickListener() {
		//
		// @Override
		// public void onCommentImageClick() {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// });

		// mAdapter.setOnUpImageClickListener(new OnUpImageClickListener() {
		//
		// @Override
		// public void onUpImageClick(final int upListIndex) {}
		//
		// });

		personal_zone_list.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();

		personal_zone_list.getRefreshableView().setSelection(
				selectionAfterLoad);

		setOnTouchListener();

		personal_zone_list
				.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						if (mAdapter != null
								&& mAdapter.isTriangleListShown()) {

							mAdapter.hideTriangleLayout();

							return;
						}
						
						if(mData.get(position - 1).get(
								MainListAdapter.TYPE) == null)
						{
							return;
						}

						int type = (Integer) mData.get(position - 1).get(
								MainListAdapter.TYPE);
						if (type == MainActivity.EVENT_SYSTEM) {
							final String system_id = (String) mData.get(
									position - 1).get("feedId");
							Intent intent = new Intent();
							intent.putExtra("id", system_id);
							intent.setClass(PersonalZoneActivity.this,
									DetailSystemActivity.class);
							PersonalZoneActivity.this.startActivity(intent);
							(PersonalZoneActivity.this)
									.overridePendingTransition(
											R.anim.activity_open_in_anim,
											R.anim.activity_open_out_anim);
						}

						else {
							Utils.feedDetail = mData.get(position - 1);
							Intent intent = new Intent(
									PersonalZoneActivity.this,
									FeedDetailActivity.class);
							intent.putExtra(
									"feedId",
									(String) mData.get(position - 1).get(
											"feedId"));
							intent.putExtra("position", position);

							PersonalZoneActivity.this
									.startActivityForResult(intent,
											FEED_DETAIL_REQUEST_CODE);
						}

					}

				});

		// personal_zone_list.getRefreshableView().setSelection(
		// selectionAfterLoad);
	}
	
	private void setData(String data,
			final int selectionAfterLoad) {
		try {
			JSONArray feedsArray = new JSONArray(data);

			ArrayList<Map<String, Object>> refreshData = null;
			for (int i = 0; i < feedsArray.length(); i++) {
				JSONObject feed = feedsArray.getJSONObject(i);

				Map<String, Object> temp = new HashMap<String, Object>();

				temp.put("feedId", feed.getString("id"));

				// JSONObject owner = feed.getJSONObject("owner");
				// if (owner.has("avatar")) {
				// temp.put(MainListAdapter.AVATAR,
				// owner.getString("avatar"));
				// } else {
				// temp.put(MainListAdapter.AVATAR, "");
				// }
				// temp.put(MainListAdapter.NICKNAME,
				// owner.getString("nickname"));
				// temp.put(MainListAdapter.FEEDS_OWNER_ID,
				// owner.getString("id"));

				double createdAt = feed.getDouble("createdAt");

				String time = getTime(createdAt);
				temp.put(MainListAdapter.TIME, time);
				
				Date createdDate = new Date((long) (createdAt*1000));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String createdString = sdf.format(createdDate);
				if (!createdString.equals(oldestDate)) {
					temp.put(MainListAdapter.ZONE_LIST_DATE,
							getDayNumber(createdString));
					temp.put("currentDateStr", createdString);
					
					oldestDate = createdString;
				} else {
					temp.put(MainListAdapter.ZONE_LIST_DATE, "");
				}

				temp.put(MainListAdapter.FEEDS_OWNER_ID,
						userPreferences.getString("userId", ""));

				if (feed.has("location")) {
					temp.put(MainListAdapter.LOCATION,
							feed.getString("location"));
				} else {
					temp.put(MainListAdapter.LOCATION, "");
				}

				if (feed.has("tag")) {
					temp.put(MainListAdapter.EVENT_TAG, feed.getInt("tag"));
					Log.v("Kite", "has tag: " + feed.getInt("tag") + " pos is "
							+ i);
				} else {
					temp.put(MainListAdapter.EVENT_TAG, 0);
					Log.v("Kite", "no tag pos is " + i);
				}

				temp.put(MainListAdapter.CONTENT, feed.getString("text"));
				if (feed.has("shareCount")) {
					temp.put(MainListAdapter.SHARE_NUMBER,
							feed.getInt("shareCount"));
				} else {
					temp.put(MainListAdapter.SHARE_NUMBER, 0);
				}

				if (feed.has("upCount")) {
					temp.put(MainListAdapter.UP_NUMBER, feed.getInt("upCount"));
				} else {
					temp.put(MainListAdapter.UP_NUMBER, 0);
				}

				if (feed.has("commentCount")) {
					temp.put(MainListAdapter.COMMENT_NUMBER,
							feed.getInt("commentCount"));
				} else {
					temp.put(MainListAdapter.COMMENT_NUMBER, 0);
				}

				List<Map<String, String>> upList = new ArrayList<Map<String, String>>();
				if (feed.has("likes")) {
					Map<String, String> upTemp;
					{
						JSONArray upNamesArray = feed.getJSONArray("likes");
						for (int j = 0; j < upNamesArray.length(); j++) {
							JSONObject upObject = upNamesArray.getJSONObject(j);
							upTemp = new HashMap<String, String>();
							upTemp.put(MainListAdapter.UP_ID,
									upObject.getString("id"));

							if (upObject.has("user")
									&& !upObject.getString("user").equals(
											"null")) {
								JSONObject userObject = upObject
										.getJSONObject("user");
								upTemp.put(MainListAdapter.UP_USER_NAME,
										userObject.getString("nickname"));
								upTemp.put(MainListAdapter.UP_USER_ID,
										userObject.getString("id"));

								upList.add(upTemp);
							}
						}
					}
				}
				temp.put(MainListAdapter.UP_LIST, upList);

				List<Map<String, String>> comments = new ArrayList<Map<String, String>>();
				if (feed.has("comments")) {
					Map<String, String> tempComment;
					{
						JSONArray commentsArray = feed.getJSONArray("comments");
						for (int j = 0; j < commentsArray.length(); j++) {
							JSONObject commentObject = commentsArray
									.getJSONObject(j);

							if (commentObject.has("user")
									&& !commentObject.getString("user").equals(
											"null")) {
								JSONObject userObject = commentObject
										.getJSONObject("user");

								tempComment = new HashMap<String, String>();
								tempComment.put(
										MainListAdapter.COMMENT_FROM_USER_NAME,
										userObject.getString("nickname"));
								tempComment.put(
										MainListAdapter.COMMENT_CONTENT,
										commentObject.getString("content"));
								comments.add(tempComment);
							}
						}
					}
				}
				temp.put(MainListAdapter.COMMENTS, comments);

				int type = feed.getInt("type");
				temp.put(MainListAdapter.TYPE, type);
				Log.v("Kite", "i is " + i + " type is " + type);

				if (feed.has("urls")) {
					JSONArray urls = feed.getJSONArray("urls");

					switch (type) {
					case MainActivity.EVENT_SYSTEM:

						String system_url = urls.getString(0);
						temp.put(MainListAdapter.SYSTEM_URL, system_url);
						temp.put(MainListAdapter.SYSTEM_ID,
								feed.getString("id"));

						break;

					case MainActivity.EVENT_PHOTO:

						List<String> imageList = new ArrayList<String>();
						for (int j = 0; j < urls.length(); j++) {
							String url = urls.getString(j);
							imageList.add(url);
							// imageList.add("http://ww1.sinaimg.cn/thumbnail/61e36371jw1elizp53a5rg206o04wb29.gif");
						}
						temp.put(MainListAdapter.IMAGE_LIST, imageList);

						break;

					case MainActivity.EVENT_VIDEO:

						String video_url = urls.getString(0);
						temp.put(MainListAdapter.VIDEO_URL, video_url);

						break;

					case MainActivity.EVENT_VOICE:

						String voice_url = urls.getString(0);
						temp.put(MainListAdapter.VOICE_URL, voice_url);

						break;
					}

				}

//				if (selectionAfterLoad == 0) {
//					mData.add(1 + i, temp);
//				} else {
					mData.add(temp);
//				}

			}

			setAdapterToList(selectionAfterLoad);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setOnTouchListener() {
		personal_zone_list.getRefreshableView().setOnTouchListener(
				new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub

						if (event.getAction() == MotionEvent.ACTION_MOVE) {

							float y = event.getRawY();

							if (Math.abs(y - lastY) > 50) {

								if (mAdapter != null
										&& mAdapter.isTriangleListShown()) {

									mAdapter.hideTriangleLayout();

								}

								lastY = y;
							}

						}

						else if (event.getAction() == MotionEvent.ACTION_UP) {
							lastY = 0;
						}

						return false;
					}
				});
	}

	private String getDayNumber(String currentDateStr) {

		try {
			// for (int i = 0; i < babyArray.length(); i++) {
			// JSONObject object = babyArray.getJSONObject(i);
			// if (object.getString("id").equals(babyId)) {
			// dateStr = object.getString("birthday");
			dateStr = birthday;
			Log.v("DBG", "dateStr " + dateStr);
			Log.v("DBG", "dateStr1 " + this.getIntent().getStringExtra("date"));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdf.parse(dateStr);
			Date date1 = sdf.parse(currentDateStr);

			Calendar calendar = Calendar.getInstance();
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(calendar.getTime());
			calendar1.setTime(date1);
			calendar.setTime(date);
			Long time = (calendar1.getTimeInMillis() - calendar
					.getTimeInMillis()) / 1000 / 60 / 60 / 24;
			dateStr = "第" + String.valueOf(time) + "天";

			Log.v("Kite", "currentDateStr is " + currentDateStr);
			Log.v("Kite", "dateStr is " + dateStr);

			return dateStr;
			// }
			// }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private Date getDateFromSeconds(double seconds) {
		long millis = (long) (seconds * 1000);
		Date date = new Date(millis);

		return date;
	}

	private String getTime(double seconds) {
		Date curDate = getDateFromSeconds(System.currentTimeMillis());
		int curYear = curDate.getYear();
		int curMonth = curDate.getMonth();
		int curDay = curDate.getDay();
		int curHour = curDate.getHours();
		int curMinute = curDate.getMinutes();
		int curSecond = curDate.getSeconds();

		Date date = getDateFromSeconds(seconds);
		int year = date.getYear();
		int month = date.getMonth();
		int day = date.getDay();
		int hour = date.getHours();
		int minute = date.getMinutes();
		int second = date.getSeconds();

		if (curYear == year) {
			if (curMonth == month) {
				if (curDay == day) {
					if (curHour == hour) {
						if (curMinute == minute) {
							return "刚刚";
						}

						return (curMinute - minute) + "分钟前";
					}

					return (curHour - hour) + "小时前";
				}
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
		String formattedDate = sdf.format(date);

		return formattedDate;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == FEED_DETAIL_REQUEST_CODE) {
			if (resultCode == FEED_DETAIL_DELETE_CODE) {
				int position = data.getIntExtra("position", -1);
				if (position != -1) {
					mData.remove(position - 1);
					mAdapter.notifyDataSetChanged();
				}
			}
		}
	}

}
