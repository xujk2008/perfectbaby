package com.bebeanan.perfectbaby;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

import com.bebeanan.perfectbaby.common.CustomMediaPlayer;
import com.bebeanan.perfectbaby.common.FeedsOperations;
import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.DetectKeyDownActionEditText;
import com.bebeanan.perfectbaby.zxing.view.DetectKeyDownActionEditText.OnBackPressedListener;
import com.bebeanan.perfectbaby.zxing.view.IMERelativeLayout;
import com.bebeanan.perfectbaby.zxing.view.IMERelativeLayout.IMEStateListener;
import com.bebeanan.perfectbaby.zxing.view.SpannableFixFocusTextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.perfectbaby.adapters.MainListAdapter;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class FeedDetailActivity extends Activity{

	private Context context;
	private FeedsOperations feedsOperations;
	
	private IMERelativeLayout feed_detail_root_layout;
	private IMEStateListener imeStateListener;
	private Runnable imeRunnable;
	
	private RelativeLayout feed_detail_content_layout;
	
	private ImageView feed_detail_avatar;
	private TextView feed_detail_nickname;
	private TextView feed_detail_time;
	private ImageView feed_detail_locator_image;
	private TextView feed_detail_location;
	private LinearLayout feed_detail_triangle_layout;
	private ImageView feed_detail_event;
	private TextView feed_detail_content;

	private ImageView feed_detail_share_image, feed_detail_up_image,
			feed_detail_comment_image;
	private TextView feed_detail_share_number, feed_detail_up_number,
			feed_detail_comment_number;

	private ImageView feed_detail_up_filled;
	private SpannableFixFocusTextView feed_detail_up_names;

	private LinearLayout feed_detail_image_layout;

	private RelativeLayout feed_detail_video_layout;
	private ImageView feed_detail_video_thumb;
	private MediaController feed_detail_video_controller;

	private RelativeLayout feed_detail_voice_layout;
	private DetailVoiceCheckbox feed_detail_voice_button;
	private ImageView feed_detail_voice_background;
	private SeekBar feed_detail_voice_seekbar;
	private TextView feed_detail_voice_time;

	private LinearLayout feed_detail_comment_layout;
	
	private LinearLayout feed_detail_triangle_option_layout;
	
	private RelativeLayout feed_detail_comment_edit_layout;
	private Button feed_detail_comment_send;
	private DetectKeyDownActionEditText feed_detail_comment_edit;
	
	private RelativeLayout feed_detail_mask_layout;
	
	private RelativeLayout share_method_layout;
	private LinearLayout share_wechat_layout, share_friend_circle_layout, share_qzone_layout, share_qq_layout, share_sina_layout;
	private TextView share_method_cancel;
	
	private final static int INVITE_METHOD_HEIGHT_DP = 220;
	private final static int INVITE_LAYOUT_OFFSET_DP = 20;
	
	private ProgressBar feed_detail_progress;
	
	private boolean upStatusChanging = false;
	
	private int[] imageIds = { R.id.feed_detail_image_single,
			R.id.feed_detail_image1, R.id.feed_detail_image2,
			R.id.feed_detail_image3, R.id.feed_detail_image4,
			R.id.feed_detail_image5, R.id.feed_detail_image6,
			R.id.feed_detail_image7, R.id.feed_detail_image8, R.id.feed_detail_image9 };
	private Typeface typeFace;
	
	private String feedId;

	private int imageIndex;
	private ArrayList<String> imageList;
	
	private boolean commentIsToOthers;
	private int commentListIndex;

	private boolean triangleListShown = false;
	private boolean share_method_shown = false;

	List<Integer> loadingItems = new ArrayList<Integer>();
	
	InputMethodManager imm;

	private Handler voiceHandler = new Handler();
	private Runnable voiceTimmerRunnable = null;
	private Runnable voiceRunnable = null;
	private boolean voiceRunnableStop = false;
	
	private Handler thirdLoginHandler = null;
	private Handler thirdShareHandler;
	
	private AlertDialog dialog;
	private AlertDialog.Builder builder;
	
	private SharedPreferences userPreferences;
	
	private String m_strFileURL = "http://www.android-study.com/upload/media/android.mp3";
	private String m_strTempFilePath = "";
	
	private boolean m_bIsReleased = false;
	private boolean m_bIsPaused = false;
	private boolean m_bIsStreaming = true;
	
	private int MAX_COMMENT_LENGTH = 140;

	private class MyClickableSpan extends ClickableSpan
	{
		private String comment_from_name;
		private int index;
		
		public MyClickableSpan(String comment_from_name, int index)
		{
			this.comment_from_name = comment_from_name;
			this.index = index;
		}
		
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub
			
			if(isTriangleListShown())
			{
				hideTriangleLayout();
				
				return;
			}
			
			if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
			{
				hideCommentEditLayout();
				
				return;
			}
			
			showCommentEditLayout();
			feed_detail_comment_edit.setHint("回复" + comment_from_name + ":");
			commentIsToOthers = true;
			commentListIndex = index;
		}
		
		@Override
		public void updateDrawState(TextPaint ds) {
			// TODO Auto-generated method stub
			super.updateDrawState(ds);
			
			ds.setUnderlineText(false);
			ds.setARGB(255, 0, 0, 0);
		}
	}
	
	private void initView(final Map<String, Object> feedDetail) {
		// TODO Auto-generated method stub

		feed_detail_root_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
				}
			}
			
		});
		
		feed_detail_content_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
				}
			}
			
		});
		
		final String avatarUrl = (String)feedDetail.get(
				"avatar");
		if (!avatarUrl.equals("")) {
			new UrlImageViewHelper().setUrlDrawable(feed_detail_avatar,
					avatarUrl);
		} else {
			feed_detail_avatar
					.setImageResource(R.drawable.user_info_default_avatar);
		}
		feed_detail_avatar.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String userId = (String) feedDetail.get(
						"feedsOwnerId");
				
				if (userId.equals(userPreferences.getString("userId", ""))) {
//					Intent intent = new Intent(context, TreeActivity.class);
//					context.startActivity(intent);
					Intent intent = new Intent(
							FeedDetailActivity.this,
							PersonalDetailActivity.class);
					intent.putExtra(
							"followeeId",
							userId);
					intent.putExtra(
							"followeeNickName",
							userPreferences.getString("nickName", ""));
					intent.putExtra(
							"followeeAvatarUrl",
							userPreferences.getString("avatarUrl", ""));

					FeedDetailActivity.this
							.startActivity(intent);
				} else {
					
					Intent intent = new Intent(context, PersonalDetailActivity.class);
					intent.putExtra("followeeId", userId);
					
					context.startActivity(intent);
				}
			}
			
		});

		feed_detail_nickname.setText((String) feedDetail.get("nickname"));
		feed_detail_time.setText((String)feedDetail.get("time"));

		String location = (String) feedDetail.get("location");
		if (location.equals("")) {
			feed_detail_locator_image.setVisibility(View.GONE);
		} else {
			feed_detail_locator_image.setVisibility(View.VISIBLE);
		}
		feed_detail_location.setText((String) feedDetail.get("location"));
		feed_detail_location.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
					
					return;
				}
				
				((TextView)v).requestFocus();
				((TextView)v).setSelected(!v.isSelected());
			}
			
		});

		feed_detail_triangle_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!triangleListShown) {
					showTriangleLayout();
				} else {
					hideTriangleLayout();
				}

			}

		});
		
		final String userId = (String) feedDetail.get(MainListAdapter.FEEDS_OWNER_ID);
		ImageView triangleImage = (ImageView)feed_detail_triangle_layout.findViewById(R.id.feed_detail_triangle);
		
		if (!userId.equals(userPreferences.getString("userId", ""))) {
			
			triangleImage.setImageResource(R.drawable.main_list_triangle_left);
			
			feed_detail_triangle_layout
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							// ImageView main_list_triangle = (ImageView) v
							// .findViewById(R.id.main_list_triangle);

							if (!triangleListShown) {
								
								showTriangleLayout();
								
							} else {
								
								hideTriangleLayout();
								
							}

						}

					});
		} else {
			
			triangleImage.setImageResource(R.drawable.main_list_delete);
			
			feed_detail_triangle_layout
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							AlertDialog.Builder builder = new Builder(FeedDetailActivity.this);

							builder.setMessage("确定删除?");
							builder.setTitle("提示");

							builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

									deleteFeed((String)feedDetail.get("feedId"));
								}
							});

							builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							});

							builder.create().show();
						}

					});
		}
		
		ImageView feed_detail_triangle_option_favorite = (ImageView) feed_detail_triangle_option_layout
				.findViewById(R.id.feed_detail_triangle_option_favorite);
		feed_detail_triangle_option_favorite
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hideTriangleLayout();
						
						if((Boolean)feedDetail.get(MainListAdapter.IS_FAVOR))
						{
							Toast toast = Toast.makeText(context, "已收藏", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							
							return;
						}
						
						feedsOperations.addFavFeeds(
								feedId,
								userPreferences.getString("userId", ""),
								userPreferences.getString("nickName", ""),
								"user", (String)feedDetail.get(MainListAdapter.FEEDS_OWNER_ID));
					}

				});
		ImageView feed_detail_triangle_option_shield = (ImageView) feed_detail_triangle_option_layout
				.findViewById(R.id.feed_detail_triangle_option_shield);
		int userType = (Integer)feedDetail.get(MainListAdapter.USER_TYPE);
		if(userType == -1)
		{
			feed_detail_triangle_option_shield.setVisibility(View.GONE);
		}
		else
		{
			feed_detail_triangle_option_shield.setVisibility(View.VISIBLE);
			
			feed_detail_triangle_option_shield
			.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					hideTriangleLayout();

					feedsOperations.addShield(userPreferences.getString("userId", ""), (String) feedDetail.get(
							MainListAdapter.FEEDS_OWNER_ID));
				}

			});
		}

		ImageView feed_detail_triangle_option_report = (ImageView) feed_detail_triangle_option_layout
				.findViewById(R.id.feed_detail_triangle_option_report);
		feed_detail_triangle_option_report
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hideTriangleLayout();
						
						feedsOperations.reportFeeds((String) feedDetail.get("feedId"));
					}

				});

		ImageView feed_detail_triangle_option_cancel = (ImageView) feed_detail_triangle_option_layout
				.findViewById(R.id.feed_detail_triangle_option_cancel);
		feed_detail_triangle_option_cancel
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hideTriangleLayout();
					}

				});

		int eventTag = (Integer) feedDetail.get("tag");
		switch (eventTag) {
		case 0:

			feed_detail_event.setVisibility(View.GONE);

			break;

		case 1:

			feed_detail_event.setVisibility(View.VISIBLE);
			feed_detail_event.setImageResource(R.drawable.event_milk_pic);

			break;

		case 2:

			feed_detail_event.setVisibility(View.VISIBLE);
			feed_detail_event.setImageResource(R.drawable.event_feed_pic);

			break;

		case 4:

			feed_detail_event.setVisibility(View.VISIBLE);
			feed_detail_event.setImageResource(R.drawable.event_liquid_pic);

			break;

		case 8:

			feed_detail_event.setVisibility(View.VISIBLE);
			feed_detail_event.setImageResource(R.drawable.event_napkin_pic);

			break;

		case 16:

			feed_detail_event.setVisibility(View.VISIBLE);
			feed_detail_event.setImageResource(R.drawable.event_sleep_pic);

			break;
		}

		String content = (String) feedDetail.get(MainListAdapter.CONTENT);
		List<Map<String, Integer>> urlList = Utils.getUrl(content);
		if(urlList.size() == 0)
		{
			feed_detail_content.setText(content);
		}
		else
		{
			SpannableString sp = new SpannableString(content);
			for(int i=0; i<urlList.size(); i++)
			{
				Map<String, Integer> urlMap = urlList.get(i);
				int urlStart = urlMap.get("urlStart");
				int urlEnd = urlMap.get("urlEnd");
				Log.v("Kite", "url String is " + content.substring(urlStart, urlEnd));
				Log.v("Kite", "urlStart is " + urlStart + " urlEnd is " + urlEnd);
				sp.setSpan(new URLSpan(content.substring(urlStart, urlEnd)), urlStart, urlEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			feed_detail_content.setText(sp);
			feed_detail_content.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod.getInstance());
		}

		feed_detail_share_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
					
					return;
				}
				
				List<String> imageResourceList = (List) feedDetail.get(MainListAdapter.IMAGE_LIST);
				if(imageResourceList != null)
				{
					showShareMethodLayout((String) feedDetail
							.get("feedId"), "完美宝贝", (String)feedDetail.get(MainListAdapter.CONTENT), imageResourceList.get(0));
				}
				else
				{
					showShareMethodLayout((String) feedDetail
							.get("feedId"), "完美宝贝", (String)feedDetail.get(MainListAdapter.CONTENT), null);
				}
			}

		
		});

		feed_detail_up_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
					
					return;
				}
				
				JSONObject upObject = new JSONObject();
				try {
					upObject.put("feedId", feedId);
					JSONObject user = new JSONObject();
					user.put("id", userPreferences.getString("userId", ""));
					user.put("nickname",
							userPreferences.getString("nickName", ""));
					upObject.put("user", user);
					
					String method = new String();
					String uri = new String();
					final int upListIndex = (Integer)feed_detail_up_image.getTag();
					if(upListIndex != -1)
					{
						((ImageView)v).setImageResource(R.drawable.main_list_up);
						method = NetHandler.METHOD_DELETE;
						List<Map<String, String>> upList = (List)feedDetail.get(MainListAdapter.UP_LIST);
						uri = "/feedup/" + upList.get(upListIndex).get(MainListAdapter.UP_ID);
						
						Log.v("Kite", "upId is " + upList.get(upListIndex).get(
								MainListAdapter.UP_ID));
					}
					else
					{
						((ImageView)v).setImageResource(R.drawable.main_list_up_filled);
						method = NetHandler.METHOD_POST;
						uri = "/feedup";
					}

					NetHandler handlerCommet = new NetHandler(FeedDetailActivity.this, 
							method, uri,
							new LinkedList<BasicNameValuePair>(), upObject) {

						@Override
						public void handleRsp(Message msg) {
							// TODO Auto-generated method stub
							
							upStatusChanging = false;
							
							Bundle bundleUp = msg.getData();
							
							String upData = bundleUp.getString("data");
							
							int codeUp = bundleUp.getInt("code");
							if (codeUp == 200) {
								
								if(upListIndex == -1)
								{
									Log.v("Kite", "点赞成功");

//									String feed_detail_up_number_string = (String) feed_detail_up_number
//											.getText();
//									int curUpNum = Integer
//											.parseInt((String) feed_detail_up_number_string
//													.subSequence(
//															1,
//															feed_detail_up_number_string
//																	.length() - 1)) + 1;
//									feed_detail_up_number.setText("("
//											+ curUpNum + ")");
//									incNum(feed_detail_up_number);
									int upNumber = (Integer)Utils.feedDetail.get(MainListAdapter.UP_NUMBER);
									Utils.feedDetail.put(MainListAdapter.UP_NUMBER, upNumber+1);

									List<Map<String, String>> upList = (List) feedDetail
											.get("up_list");
									Map<String, String> tempUp = new HashMap<String, String>();
									tempUp.put(MainListAdapter.UP_USER_NAME,
											userPreferences.getString(
													"nickName", ""));
									tempUp.put(MainListAdapter.UP_USER_ID,
											userPreferences.getString("userId",
													""));
									try {
										JSONObject upObject = new JSONObject(upData);
										
										tempUp.put(
												MainListAdapter.UP_ID,
												upObject.getString("id"));
										
										Log.v("Kite", "up_id in json is " + upObject.getString("id"));
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
									upList.add(tempUp);

									refreshUpList(upList);
									
									Utils.feedDetail.put("up_list", upList);
									
									Intent intent = new Intent();
									int position = FeedDetailActivity.this.getIntent().getIntExtra("position", -1);
									intent.putExtra("position", position);
									FeedDetailActivity.this.setResult(MainActivity.FEED_DETAIL_UP_COMMENT_CODE, intent);
								}
								else
								{
									Log.v("Kite", "取消赞成功");

//									String feed_detail_up_number_string = (String) feed_detail_up_number
//											.getText();
//									int curUpNum = Integer
//											.parseInt((String) feed_detail_up_number_string
//													.subSequence(
//															1,
//															feed_detail_up_number_string
//																	.length() - 1)) - 1;
//									feed_detail_up_number.setText("("
//											+ curUpNum + ")");
//									decNum(feed_detail_up_number);
									int upNumber = (Integer)Utils.feedDetail.get(MainListAdapter.UP_NUMBER);
									Utils.feedDetail.put(MainListAdapter.UP_NUMBER, upNumber-1);

									List<Map<String, String>> upList = (List) feedDetail
											.get("up_list");
									upList.remove(upListIndex);

									refreshUpList(upList);
									
									Utils.feedDetail.put("up_list", upList);

									Intent intent = new Intent();
									int position = FeedDetailActivity.this
											.getIntent().getIntExtra(
													"position", -1);
									intent.putExtra("position", position);
									FeedDetailActivity.this
											.setResult(
													MainActivity.FEED_DETAIL_UP_COMMENT_CODE,
													intent);
								}
								
							} else {
								Log.v("Kite",
										"up fail because: "
												+ bundleUp.getString("data"));
								
								List<Map<String, String>> upList = (List) feedDetail
										.get("up_list");

								refreshUpList(upList);
							}
						}

					};

					if(!upStatusChanging)
					{
						upStatusChanging = true;
						handlerCommet.start();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});

		feed_detail_comment_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
					
					return;
				}
				
				switch(feed_detail_comment_edit_layout.getVisibility())
				{
				case View.GONE:
					
					showCommentEditLayout();
					
					break;
					
				case View.VISIBLE:
					
					hideCommentEditLayout();
					
					break;
				}
			}

		});
		
		feed_detail_comment_send.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				final String commentContent = feed_detail_comment_edit.getText().toString();
				
				if(!commentIsToOthers)
				{
					JSONObject commentObject = new JSONObject();
					try {
						commentObject.put("feedId", feedId);
						JSONObject user = new JSONObject();
						user.put("id", userPreferences.getString("userId", ""));
						user.put("nickname", userPreferences.getString("nickName", ""));
						commentObject.put("user", user);
						commentObject.put("content", commentContent);
						commentObject.put("type", "text");
						
						NetHandler handlerCommet = new NetHandler(FeedDetailActivity.this, NetHandler.METHOD_POST,
								"/comments", new LinkedList<BasicNameValuePair>(),
								commentObject)
						{

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub
								Bundle bundleComment = msg.getData();
								int codeComment = bundleComment.getInt("code");
								
								String dataComment = bundleComment.getString("data");
								
								if(codeComment == 200)
								{
									Log.v("Kite", "评论发布成功");

									List<Map<String, String>> comments = (List) feedDetail
											.get("comments");
									Map<String, String> tempComment = new HashMap<String, String>();
									try {
										JSONObject objectComment = new JSONObject(
												dataComment);
										tempComment.put(
												MainListAdapter.COMMENT_ID,
												objectComment.getString("id"));
										tempComment
												.put(MainListAdapter.COMMENT_FROM_USER_NAME,
														userPreferences
																.getString(
																		"nickName",
																		""));
										tempComment
												.put(MainListAdapter.COMMENT_FROM_USER_ID,
														userPreferences
																.getString(
																		"userId",
																		""));
										tempComment
												.put(MainListAdapter.COMMENT_CONTENT,
														commentContent);

										tempComment
												.put(MainListAdapter.COMMENT_TO_USER_NAME,
														"");
										tempComment
												.put(MainListAdapter.COMMENT_TO_USER_ID,
														"");

										comments.add(0, tempComment);
										// String
										// feed_detail_comment_number_string =
										// (String)
										// feed_detail_comment_number.getText();
										// int curCommentNum =
										// Integer.parseInt((String)feed_detail_comment_number_string.subSequence(1,
										// feed_detail_comment_number_string.length()-1))
										// + 1;
										// feed_detail_comment_number.setText("("
										// + curCommentNum + ")");

//										incNum(feed_detail_comment_number);
										int comment_num = (Integer) Utils.feedDetail.get(MainListAdapter.COMMENT_NUMBER);
										Utils.feedDetail.put(MainListAdapter.COMMENT_NUMBER, comment_num+1);
										
										refreshComments(comments);
										
										Utils.feedDetail.put("comments", comments);

										Intent intent = new Intent();
										int position = FeedDetailActivity.this.getIntent().getIntExtra("position", -1);
										intent.putExtra("position", position);
										FeedDetailActivity.this.setResult(MainActivity.FEED_DETAIL_UP_COMMENT_CODE, intent);
										
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								else
								{
									Log.v("Kite", "comment fail because: " + bundleComment.getString("data"));
								}
							}
							
						};
						
						handlerCommet.start();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					JSONObject commentObject = new JSONObject();
					try {
						final List<Map<String, String>> comments = (List)feedDetail.get("comments");
						
						commentObject.put("feedId", feedId);
						JSONObject user = new JSONObject();
						user.put("id", userPreferences.getString("userId", ""));
						user.put("nickname", userPreferences.getString("nickName", ""));
						commentObject.put("user", user);
						commentObject.put("content", commentContent);
						commentObject.put("toComment", comments.get(commentListIndex).get(MainListAdapter.COMMENT_ID));
						commentObject.put("type", "text");
						
						NetHandler handlerCommet = new NetHandler(FeedDetailActivity.this, NetHandler.METHOD_POST,
								"/comments", new LinkedList<BasicNameValuePair>(),
								commentObject)
						{

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub
								Bundle bundleComment = msg.getData();
								int codeComment = bundleComment.getInt("code");
								
								if(codeComment == 200)
								{
									Log.v("Kite", "评论发布成功");
									
									Map<String, String> tempComment = new HashMap<String, String>();
									tempComment.put(MainListAdapter.COMMENT_ID, comments.get(commentListIndex).get(MainListAdapter.COMMENT_ID));
									tempComment
											.put(MainListAdapter.COMMENT_FROM_USER_NAME,
													userPreferences.getString(
															"nickName", ""));
									tempComment
											.put(MainListAdapter.COMMENT_FROM_USER_ID,
													userPreferences.getString(
															"userId", ""));
									tempComment.put(
											MainListAdapter.COMMENT_CONTENT,
											commentContent);
									tempComment.put(
											MainListAdapter.COMMENT_TO_USER_NAME,
											comments.get(commentListIndex).get(MainListAdapter.COMMENT_FROM_USER_NAME));
									tempComment.put(
											MainListAdapter.COMMENT_TO_USER_ID,
											comments.get(commentListIndex).get(MainListAdapter.COMMENT_FROM_USER_ID));
									
									comments.add(0, tempComment);
									
//									String feed_detail_comment_number_string = (String) feed_detail_comment_number.getText();
//									int curCommentNum = Integer.parseInt((String)feed_detail_comment_number_string.subSequence(1, feed_detail_comment_number_string.length()-1)) + 1;
//									feed_detail_comment_number.setText("(" + curCommentNum + ")");
									
//									incNum(feed_detail_comment_number);
									int comment_num = (Integer) Utils.feedDetail.get(MainListAdapter.COMMENT_NUMBER);
									Utils.feedDetail.put(MainListAdapter.COMMENT_NUMBER, comment_num+1);
									
									refreshComments(comments);
									
									Utils.feedDetail.put("comments", comments);

									Intent intent = new Intent();
									int position = FeedDetailActivity.this.getIntent().getIntExtra("position", -1);
									intent.putExtra("position", position);
									FeedDetailActivity.this.setResult(MainActivity.FEED_DETAIL_UP_COMMENT_CODE, intent);
								}
								else
								{
									Log.v("Kite", "comment fail because: " + bundleComment.getString("data"));
								}
							}
							
						};
						
						handlerCommet.start();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				hideCommentEditLayout();
			}
			
		});
		
		feed_detail_comment_edit.setOnBackPressedListener(new OnBackPressedListener(){

			@Override
			public void onBackPressed() {
				// TODO Auto-generated method stub
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
				}
			}
			
		});
		
		feed_detail_comment_edit.addTextChangedListener(new TextWatcher(){

			private int editStart ;
	        private int editEnd ;
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				editStart = feed_detail_comment_edit.getSelectionStart();
	            editEnd = feed_detail_comment_edit.getSelectionEnd();
	            if (s.length() > MAX_COMMENT_LENGTH) {
	                Toast.makeText(FeedDetailActivity.this,
	                        "评论字数不能超过140字哟~", Toast.LENGTH_SHORT)
	                        .show();
	                s.delete(editStart-1, editEnd);
	                int tempSelection = editStart;
	                feed_detail_comment_edit.setText(s);
	                feed_detail_comment_edit.setSelection(tempSelection);
	            }
			}
			
		});
		
		feed_detail_share_number.setText("("
				+ (Integer) feedDetail
				.get("share_number") + ")");
		feed_detail_up_number.setText("(" + (Integer) feedDetail.get("up_number") + ")");
		feed_detail_comment_number.setText("("
				+ (Integer) feedDetail.get("comment_number") + ")");
		
		int share_num = (Integer) feedDetail
				.get("share_number");
		if(share_num != 0)
		{
			feed_detail_share_number.setText("("
					+ share_num + ")");
		}
		else
		{
			feed_detail_share_number.setText("");
		}
		
		int up_num = (Integer) feedDetail.get("up_number");
		if(up_num != 0)
		{
			feed_detail_up_number.setText("("
					+ up_num + ")");
		}
		else
		{
			feed_detail_up_number.setText("");
		}
		
		int comment_num = (Integer) feedDetail.get("comment_number");
		if(comment_num != 0)
		{
			feed_detail_comment_number.setText("("
					+ comment_num + ")");
		}
		else
		{
			feed_detail_comment_number.setText("");
		}

		// holder.feed_detail_up_names.setText((String) cur.get(UP_NAMES));
		List<Map<String, String>> upList = (List) feedDetail.get("up_list");
		refreshUpList(upList);

		int event_type = (Integer) feedDetail.get("type");
		switch (event_type) {
		case MainActivity.EVENT_NOTE:

			feed_detail_video_layout.setVisibility(View.GONE);
			feed_detail_voice_layout.setVisibility(View.GONE);
			feed_detail_image_layout.setVisibility(View.GONE);

			break;

		case MainActivity.EVENT_PHOTO:

			feed_detail_video_layout.setVisibility(View.GONE);
			feed_detail_voice_layout.setVisibility(View.GONE);
			feed_detail_image_layout.setVisibility(View.VISIBLE);


				imageList = (ArrayList) feedDetail.get("image_list");

				ArrayList<ImageView> imageViewList = new ArrayList<ImageView>();
				for (int i = 0; i < 10; i++) {
					ImageView curImageView = (ImageView) feed_detail_image_layout
							.findViewById(imageIds[i]);
					curImageView.setVisibility(View.GONE);
					imageViewList.add(curImageView);
				}

				if (imageList.size() == 1) {
					imageViewList.get(0).setVisibility(View.VISIBLE);
					imageViewList.get(0).setTag(0);
					imageViewList.get(0).setOnClickListener(
							new OnClickListener() {

								@Override
								public void onClick(View view) {
									// TODO Auto-generated method stub
									
									if(isTriangleListShown())
									{
										hideTriangleLayout();
										
										return;
									}
									
									if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
									{
										hideCommentEditLayout();
										
										return;
									}
									
									imageIndex = (Integer) view.getTag();

									Intent intent = new Intent(context,
											GalleryActivity.class);
									intent.putStringArrayListExtra(
											"imageResourceList", imageList);
									intent.putExtra("imageIndex", imageIndex);

									context.startActivity(intent);
									((Activity) context)
											.overridePendingTransition(
													R.anim.zoom_in, 0);
								}
							});

					new UrlImageViewHelper(true, Utils.density, 100)
							.setUrlDrawable(imageViewList.get(0),
									imageList.get(0));
				} else {
					for (int i = 0, j = 1; i < imageList.size()
							&& j < imageViewList.size(); i++, j++) {
						imageViewList.get(j).setVisibility(View.VISIBLE);
						imageViewList.get(j).setTag(i);
						imageViewList.get(j).setOnClickListener(
								new OnClickListener() {

									@Override
									public void onClick(View view) {
										// TODO Auto-generated method stub
										
										if(isTriangleListShown())
										{
											hideTriangleLayout();
											
											return;
										}
										
										if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
										{
											hideCommentEditLayout();
											
											return;
										}
										
										imageIndex = (Integer) view.getTag();

										Intent intent = new Intent(context,
												GalleryActivity.class);
										intent.putStringArrayListExtra(
												"imageResourceList", imageList);
										intent.putExtra("imageIndex",
												imageIndex);

										context.startActivity(intent);
										((Activity) context)
												.overridePendingTransition(
														R.anim.zoom_in, 0);
									}
								});

						new UrlImageViewHelper().setUrlDrawable(
								imageViewList.get(j), imageList.get(i));
					}
				}
			

			break;

		case MainActivity.EVENT_VIDEO:

			feed_detail_video_layout.setVisibility(View.VISIBLE);
			feed_detail_voice_layout.setVisibility(View.GONE);
			feed_detail_image_layout.setVisibility(View.GONE);

			final String videoUrl = (String) feedDetail.get("video_url");

			feed_detail_video_layout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					if(isTriangleListShown())
					{
						hideTriangleLayout();
						
						return;
					}
					
					if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
					{
						hideCommentEditLayout();
						
						return;
					}

					Intent intent = new Intent(context, VideoActivity.class);
					intent.putExtra("videoUrl", videoUrl);
					context.startActivity(intent);
				}
			});

			break;

		case MainActivity.EVENT_VOICE:

			feed_detail_video_layout.setVisibility(View.GONE);
			feed_detail_voice_layout.setVisibility(View.VISIBLE);
			feed_detail_image_layout.setVisibility(View.GONE);

			final String voiceUrl = (String) feedDetail.get("voice_url");

			feed_detail_voice_time.setText("");

			feed_detail_voice_button
					.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {

							if(isTriangleListShown())
							{
								hideTriangleLayout();
								
								return;
							}
							
							if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
							{
								hideCommentEditLayout();
								
								return;
							}
							
							DetailVoiceCheckbox voiceButton = (DetailVoiceCheckbox) view;

//							downloadVoice("http://nj.baidupcs.com/file/f014d07a7e4c7ba45b63eb13287ca681?bkt=p2-nj-904&fid=1246119876-250528-1101180813307342&time=1419505548&sign=FDTAXERLBH-DCb740ccc5511e5e8fedcff06b081203-DS%2B7I5LZqwznX7q7l4E%2B5stAVF4%3D&to=nb&fm=Nan,B,T,t&newver=1&newfm=1&flow_ver=3&sl=81723466&expires=8h&rt=sh&r=606370829&mlogid=918613813&vuk=687926515&vbdid=504383559&fin=mid%20air.mp3&fn=mid%20air.mp3");
							downloadVoice(voiceUrl);
						}
					});

			break;

		default:

			feed_detail_video_layout.setVisibility(View.GONE);
			feed_detail_voice_layout.setVisibility(View.GONE);
			feed_detail_image_layout.setVisibility(View.GONE);

			break;
		}

		refreshComments((List) feedDetail.get("comments"));

	}
	
	private void getFeedDetail()
	{
		feed_detail_progress.setVisibility(View.VISIBLE);
		feed_detail_content_layout.setVisibility(View.GONE);
		
		NetHandler handler = new NetHandler(FeedDetailActivity.this, NetHandler.METHOD_GET, "/feeds/"+feedId + "?include=user,comment,like", new LinkedList<BasicNameValuePair>(), null){

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				
				feed_detail_progress.setVisibility(View.GONE);
				
				Bundle bundle = msg.getData();
				
				String data = bundle.getString("data");
				int code = bundle.getInt("code");
				if(code == 200)
				{
					Log.v("Kite", "feed detail is " + data);
					feed_detail_content_layout.setVisibility(View.VISIBLE);

					try {
						JSONObject feed = new JSONObject(data);


							Map<String, Object> temp = new HashMap<String, Object>();

							temp.put("feedId", feed.getString("id"));

							JSONObject owner = feed.getJSONObject("owner");
							if (owner.has("avatar")) {
								temp.put(MainListAdapter.AVATAR,
										owner.getString("avatar"));
							} else {
								temp.put(MainListAdapter.AVATAR, "");
							}
							temp.put(MainListAdapter.NICKNAME,
									owner.getString("nickname"));
							temp.put(MainListAdapter.FEEDS_OWNER_ID,
									owner.getString("id"));
							temp.put(MainListAdapter.USER_TYPE,
									owner.getInt("usertype"));
							
//							String ownerId = feed.getString("owner");
//							NetHandler ownerHandler = new NetHandler(NetHandler.METHOD_GET, "/users/" + ownerId, new LinkedList<BasicNameValuePair>(), null){
//
//								@Override
//								public void handleRsp(Message msg) {
//									// TODO Auto-generated method stub
//									Bundle ownerBundle = msg.getData();
//									int code = ownerBundle.getInt("code");
//									String ownerData = ownerBundle.getString("data");
//									JSONObject ownerObject = new JSONObject(ownerData);
//									if(code == 200)
//									{
//										if (ownerObject.has("avatar")) {
//											temp.put(MainListAdapter.AVATAR,
//													ownerObject.getString("avatar"));
//										} else {
//											temp.put(MainListAdapter.AVATAR, "");
//										}
//										temp.put(MainListAdapter.NICKNAME,
//												ownerObject.getString("nickname"));
//										temp.put(MainListAdapter.FEEDS_OWNER_ID,
//												ownerObject.getString("id"));
//										temp.put(MainListAdapter.USER_TYPE,
//												ownerObject.getInt("usertype"));
//									}
//									else
//									{
//										
//									}
//								}
//								
//							};
//							ownerHandler.start();

							double createdAt = feed.getDouble("createdAt");
							DecimalFormat df = new DecimalFormat("0.000");

							String time = getTime(createdAt);
							temp.put(MainListAdapter.TIME, time);

							if (feed.has("location")) {
								temp.put(MainListAdapter.LOCATION,
										feed.getString("location"));
							} else {
								temp.put(MainListAdapter.LOCATION, "");
							}

							if (feed.has("tag")) {
								temp.put(MainListAdapter.EVENT_TAG,
										feed.getInt("tag"));
							} else {
								temp.put(MainListAdapter.EVENT_TAG, 0);
							}

							temp.put(MainListAdapter.CONTENT,
									feed.getString("text"));
							if (feed.has("shareCount")) {
								temp.put(MainListAdapter.SHARE_NUMBER,
										feed.getInt("shareCount"));
							} else {
								temp.put(MainListAdapter.SHARE_NUMBER, 0);
							}

							temp.put(MainListAdapter.UP_NUMBER,
									feed.getInt("upCount"));
							temp.put(MainListAdapter.COMMENT_NUMBER,
									feed.getInt("commentCount"));

							List<Map<String, String>> upList = new ArrayList<Map<String, String>>();
							Map<String, String> upTemp;
							if(feed.has("likes")){
								JSONArray upNamesArray = feed
										.getJSONArray("likes");
								for (int j = 0; j < upNamesArray.length(); j++) {
									JSONObject upObject = upNamesArray
											.getJSONObject(j);
									upTemp = new HashMap<String, String>();
									upTemp.put(MainListAdapter.UP_ID,
											upObject.getString("id"));
									
									if(upObject.has("user")&&!upObject.getString("user").equals("null"))
									{
										JSONObject userObject = upObject
												.getJSONObject("user");
										upTemp.put(MainListAdapter.UP_USER_NAME,
												userObject.getString("nickname"));
										upTemp.put(MainListAdapter.UP_USER_ID,
												userObject.getString("id"));

										upList.add(upTemp);
									}
								}
							}
							temp.put(MainListAdapter.UP_LIST, upList);

							List<Map<String, String>> comments = new ArrayList<Map<String, String>>();
							Map<String, String> tempComment;
							if(feed.has("comments")){
								JSONArray commentsArray = feed
										.getJSONArray("comments");
								
								HashMap<String, JSONObject> commentDict = new HashMap<String, JSONObject>();
								for (int j = 0; j < commentsArray.length(); j++) {
									JSONObject commentObject = commentsArray
											.getJSONObject(j);
									
									if(commentObject.has("user")&&!commentObject.getString("user").equals("null"))
									{
										JSONObject userObject = commentObject
												.getJSONObject("user");
										
										commentDict.put(commentObject.getString("id"), userObject);
									}
								}
								
								for (int j = 0; j < commentsArray.length(); j++) {
									JSONObject commentObject = commentsArray
											.getJSONObject(j);
									
									if(commentObject.has("user") && !commentObject.getString("user").equals("null"))
									{
										JSONObject userObject = commentObject
												.getJSONObject("user");

										tempComment = new HashMap<String, String>();
										tempComment.put(MainListAdapter.COMMENT_ID, commentObject.getString("id"));
										tempComment.put(
												MainListAdapter.COMMENT_FROM_USER_NAME,
												userObject.getString("nickname"));
										tempComment.put(
												MainListAdapter.COMMENT_FROM_USER_ID,
												userObject.getString("id"));
										
										if(commentObject.has("toComment"))
										{
											String toCommentId = commentObject.getString("toComment");
											tempComment.put(
													MainListAdapter.COMMENT_TO_USER_NAME,
													commentDict.get(toCommentId).getString("nickname"));
											tempComment.put(
													MainListAdapter.COMMENT_TO_USER_ID,
													commentDict.get(toCommentId).getString("id"));
										}
										else
										{
											tempComment.put(
													MainListAdapter.COMMENT_TO_USER_NAME,
													"");
											tempComment.put(
													MainListAdapter.COMMENT_TO_USER_ID,
													"");
										}
										
										tempComment.put(
												MainListAdapter.COMMENT_CONTENT,
												commentObject.getString("content"));
										comments.add(tempComment);
									}
								}
							}
							temp.put(MainListAdapter.COMMENTS, comments);
							
							if(feed.has("isFavor"))
							{
								temp.put(MainListAdapter.IS_FAVOR, feed.getBoolean("isFavor"));
							}
							else
							{
								temp.put(MainListAdapter.IS_FAVOR, false);
							}

							int type = feed.getInt("type");
							temp.put(MainListAdapter.TYPE, type);

							if (feed.has("urls")) {
								JSONArray urls = feed.getJSONArray("urls");

								switch (type) {
								case MainActivity.EVENT_PHOTO:

									List<String> imageList = new ArrayList<String>();
									for (int j = 0; j < urls.length(); j++) {
										String url = urls.getString(j);
										imageList.add(url);
										// imageList.add("http://ww1.sinaimg.cn/thumbnail/61e36371jw1elizp53a5rg206o04wb29.gif");
									}
									temp.put(MainListAdapter.IMAGE_LIST,
											imageList);

									break;

								case MainActivity.EVENT_VIDEO:

									String video_url = urls.getString(0);
									temp.put(MainListAdapter.VIDEO_URL,
											video_url);

									break;

								case MainActivity.EVENT_VOICE:

									String voice_url = urls.getString(0);
									temp.put(MainListAdapter.VOICE_URL,
											voice_url);

									break;
								}

							}
							
							Utils.feedDetail = temp;
							initView(temp);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				
				}
				else
				{
					Toast toast = Toast.makeText(FeedDetailActivity.this, "加载失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
			
		};
		
		handler.start();
	}
	
	private Date getDateFromSeconds(double seconds) {
		long millis = (long) (seconds * 1000);
		Date date = new Date(millis);

		return date;
	}
	
	private String getTime(double seconds) {
		Date curDate = getDateFromSeconds(System.currentTimeMillis());
		int curYear = curDate.getYear();
		int curMonth = curDate.getMonth();
		int curDay = curDate.getDay();
		int curHour = curDate.getHours();
		int curMinute = curDate.getMinutes();
		int curSecond = curDate.getSeconds();

		Date date = getDateFromSeconds(seconds);
		int year = date.getYear();
		int month = date.getMonth();
		int day = date.getDay();
		int hour = date.getHours();
		int minute = date.getMinutes();
		int second = date.getSeconds();

		if (curYear == year) {
			if (curMonth == month) {
				if (curDay == day) {
					if (curHour == hour) {
						if (curMinute == minute) {
							return "刚刚";
						}

						return (curMinute - minute) + "分钟前";
					}

					return (curHour - hour) + "小时前";
				}
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
		String formattedDate = sdf.format(date);

		return formattedDate;
	}
	
	private void refreshUpList(List<Map<String, String>> upList)
	{
		feed_detail_up_image.setImageResource(R.drawable.main_list_up);
		feed_detail_up_image.setTag(-1);
		
		if (upList.isEmpty()) 
		{
			feed_detail_up_filled.setVisibility(View.GONE);
			feed_detail_up_names.setVisibility(View.GONE);
		}
		else 
		{
			feed_detail_up_filled.setVisibility(View.VISIBLE);
			feed_detail_up_names.setVisibility(View.VISIBLE);
			
			StringBuffer namesStringBuffer = new StringBuffer();
			int maxLength = (upList.size()>5) ? 5:upList.size();
			for (int i = 0; i < upList.size(); i++) 
			{
				String upName = upList.get(i).get("up_user_name");
				String upId = upList.get(i).get("up_user_id");
				if(upId.equals(userPreferences.getString("userId", "")))
				{
					feed_detail_up_image.setImageResource(R.drawable.main_list_up_filled);
					feed_detail_up_image.setTag(i);
				}
				
				namesStringBuffer.append(upName);
				if(i != upList.size()-1)
				{
					namesStringBuffer.append(",");
				}
			}
			namesStringBuffer.append(upList.size() + "人");
			
			SpannableString sp = new SpannableString(namesStringBuffer);
			int spanStart = 0;
			for (int i = 0; i < upList.size(); i++) 
			{
				String upName = upList.get(i).get("up_user_name");
				String upId = upList.get(i).get("up_user_id");
				sp.setSpan(new com.bebeanan.perfectbaby.common.NameClickableSpan(context, upId, upName), spanStart, spanStart+upName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				
				if(i != upList.size()-1)
				{
					spanStart = spanStart+upName.length()+1;
				}
			}
			feed_detail_up_names.setText(sp);
			feed_detail_up_names.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod.getInstance());
		}
		
		int up_num = (Integer) Utils.feedDetail.get(MainListAdapter.UP_NUMBER);
		if (up_num != 0) {
			feed_detail_up_number.setText("(" + up_num + ")");
		} else {
			feed_detail_up_number.setText("");
		}
	}
	
	private void refreshComments(List<Map<String, String>> comments)
	{
		feed_detail_comment_layout.removeAllViews();
		for (int i=0; i<comments.size(); i++) {

			Map<String, String> curComment = comments.get(i);
			
			final int index = i;
			
			final String commentId = curComment.get(MainListAdapter.COMMENT_ID);
			final String comment_from_name = curComment.get(MainListAdapter.COMMENT_FROM_USER_NAME);
			final String comment_from_id = curComment.get(MainListAdapter.COMMENT_FROM_USER_ID);
			String comment_to_name = curComment.get(MainListAdapter.COMMENT_TO_USER_NAME);
			final String comment_to_id = curComment.get(MainListAdapter.COMMENT_TO_USER_ID);
			String comment_content = curComment.get(MainListAdapter.COMMENT_CONTENT);

//			LinearLayout commentView = new LinearLayout(context);
//			commentView.setOrientation(LinearLayout.HORIZONTAL);
//			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			params.topMargin = 20;
//			commentView.setLayoutParams(params);
//
//			TextView commentFromNameView = new TextView(context);
////			commentFromNameView.setTypeface(typeFace);
////			commentFromNameView.getPaint().setFakeBoldText(true);
//			commentFromNameView.setTextColor(0xFF23B183);
//			commentFromNameView.setText(comment_from_name);
//			commentFromNameView.setOnClickListener(new OnClickListener(){
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					if (comment_from_id.equals(userPreferences.getString("userId", ""))) {
////						Intent intent = new Intent(context, TreeActivity.class);
////						context.startActivity(intent);
//						Intent intent = new Intent(
//								FeedDetailActivity.this,
//								PersonalDetailActivity.class);
//						intent.putExtra(
//								"followeeId",
//								comment_from_id);
//						intent.putExtra(
//								"followeeNickName",
//								userPreferences.getString("nickName", ""));
//						intent.putExtra(
//								"followeeAvatarUrl",
//								userPreferences.getString("avatarUrl", ""));
//
//						FeedDetailActivity.this
//								.startActivity(intent);
//					} else {
//						
//						Intent intent = new Intent(context, PersonalDetailActivity.class);
//						intent.putExtra("followeeId", comment_from_id);
//						
//						context.startActivity(intent);
//					}
//				}
//				
//			});
//			commentView.addView(commentFromNameView);
//
//			if(!comment_to_name.equals(""))
//			{
//				TextView commentReplyView = new TextView(context);
////				commentReplyView.setTypeface(typeFace);
////				commentReplyView.getPaint().setFakeBoldText(true);
//				commentReplyView.setText("回复");
//				commentView.addView(commentReplyView);
//				
//				TextView commentToNameView = new TextView(context);
////				commentToNameView.setTypeface(typeFace);
////				commentToNameView.getPaint().setFakeBoldText(true);
//				commentToNameView.setTextColor(0xFF23B183);
//				commentToNameView.setText(comment_to_name);
//				commentToNameView.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						if (comment_to_id.equals(userPreferences.getString("userId", ""))) {
////							Intent intent = new Intent(context, TreeActivity.class);
////							context.startActivity(intent);
//							Intent intent = new Intent(
//									FeedDetailActivity.this,
//									PersonalDetailActivity.class);
//							intent.putExtra(
//									"followeeId",
//									comment_from_id);
//							intent.putExtra(
//									"followeeNickName",
//									userPreferences.getString("nickName", ""));
//							intent.putExtra(
//									"followeeAvatarUrl",
//									userPreferences.getString("avatarUrl", ""));
//
//							FeedDetailActivity.this
//									.startActivity(intent);
//						} else {
//							
//							Intent intent = new Intent(context, PersonalDetailActivity.class);
//							intent.putExtra("followeeId", comment_to_id);
//							
//							context.startActivity(intent);
//						}
//					}
//					
//				});
//				commentView.addView(commentToNameView);
//			}
//			
//			TextView commentContentView = new TextView(context);
////			commentContentView.setTypeface(typeFace);
////			commentContentView.getPaint().setFakeBoldText(true);
//			commentContentView.setText(":" + comment_content);
//			commentView.addView(commentContentView);

			SpannableFixFocusTextView commentView = new SpannableFixFocusTextView(context);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.topMargin = 20;
			commentView.setLayoutParams(params);
			
			StringBuffer commentStringBuffer = new StringBuffer();
			commentStringBuffer.append(comment_from_name);

//			commentFromNameView.setOnClickListener(new OnClickListener(){
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					if (comment_from_id.equals(userPreferences.getString("userId", ""))) {
////						Intent intent = new Intent(context, TreeActivity.class);
////						context.startActivity(intent);
//						Intent intent = new Intent(
//								FeedDetailActivity.this,
//								PersonalDetailActivity.class);
//						intent.putExtra(
//								"followeeId",
//								comment_from_id);
//						intent.putExtra(
//								"followeeNickName",
//								userPreferences.getString("nickName", ""));
//						intent.putExtra(
//								"followeeAvatarUrl",
//								userPreferences.getString("avatarUrl", ""));
//
//						FeedDetailActivity.this
//								.startActivity(intent);
//					} else {
//						
//						Intent intent = new Intent(context, PersonalDetailActivity.class);
//						intent.putExtra("followeeId", comment_from_id);
//						
//						context.startActivity(intent);
//					}
//				}
//				
//			});

			if(!comment_to_name.equals(""))
			{
				commentStringBuffer.append("回复");
				
				commentStringBuffer.append(comment_to_name);
//				commentToNameView.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						if (comment_to_id.equals(userPreferences.getString("userId", ""))) {
////							Intent intent = new Intent(context, TreeActivity.class);
////							context.startActivity(intent);
//							Intent intent = new Intent(
//									FeedDetailActivity.this,
//									PersonalDetailActivity.class);
//							intent.putExtra(
//									"followeeId",
//									comment_from_id);
//							intent.putExtra(
//									"followeeNickName",
//									userPreferences.getString("nickName", ""));
//							intent.putExtra(
//									"followeeAvatarUrl",
//									userPreferences.getString("avatarUrl", ""));
//
//							FeedDetailActivity.this
//									.startActivity(intent);
//						} else {
//							
//							Intent intent = new Intent(context, PersonalDetailActivity.class);
//							intent.putExtra("followeeId", comment_to_id);
//							
//							context.startActivity(intent);
//						}
//					}
//					
//				});
			}
			
			commentStringBuffer.append(":" + comment_content);
			
			SpannableString sp = new SpannableString(commentStringBuffer);
			
			int spanStart = 0;
			sp.setSpan(
					new com.bebeanan.perfectbaby.common.NameClickableSpan(
							context, comment_from_id, comment_from_name), spanStart,
							spanStart + comment_from_name.length(),
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			spanStart = spanStart + comment_from_name.length();
			
			if(!comment_to_name.equals(""))
			{
				sp.setSpan(
						new com.bebeanan.perfectbaby.common.NameClickableSpan(
								context, comment_to_id, comment_to_name), spanStart+2,
								spanStart + 2 + comment_to_name.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				
				spanStart = spanStart + 2 + comment_to_name.length();
			}
			
			sp.setSpan(
					new MyClickableSpan(comment_from_name, index), spanStart,
							spanStart + comment_content.length() + 1,
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			commentView.setText(sp);
			commentView
					.setMovementMethod(SpannableFixFocusTextView.LocalLinkMovementMethod
							.getInstance());
			
			feed_detail_comment_layout.addView(commentView);
		}
		
		int comment_num = (Integer) Utils.feedDetail.get(MainListAdapter.COMMENT_NUMBER);
		if (comment_num != 0) {
			feed_detail_comment_number.setText("(" + comment_num + ")");
		} else {
			feed_detail_comment_number.setText("");
		}
	}
	
	private void showCommentEditLayout()
	{
		feed_detail_comment_edit.setCatchBack(true);
		feed_detail_comment_edit_layout.setVisibility(View.VISIBLE);
	
		feed_detail_comment_edit.requestFocus();
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(feed_detail_comment_edit, InputMethodManager.SHOW_FORCED);
		
		feed_detail_comment_edit.postDelayed(imeRunnable, 1500);
	}
	private void hideCommentEditLayout()
	{
		feed_detail_root_layout.setIMEChangeListener(null);
		
		feed_detail_comment_edit.setCatchBack(false);
		feed_detail_comment_edit_layout.setVisibility(View.GONE);
		
		feed_detail_comment_edit.requestFocus();
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(feed_detail_comment_edit.getWindowToken(), 0);
		
		feed_detail_comment_edit.setText("");
		
		commentIsToOthers = false;
	}


	private void updateTime(TextView recordTimeView, int recordTime) {
		int minute = recordTime / 60;
		int second = recordTime % 60;
		recordTimeView.setText(String.format("%02d:%02d", minute, second));
	}

//	private void downloadVoice(String fileUrl) {
//
//		if (fileUrl == null) {
//			Toast toast = Toast.makeText(context, "播放失败", Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//		}
//
//		NetHandler handler = new NetHandler(FeedDetailActivity.this, NetHandler.METHOD_DOWNLOAD_FILE,
//				fileUrl, null, null) {
//			@Override
//			public void handleRsp(Message msg) {
//				Bundle bundle = msg.getData();
//				int code = bundle.getInt("code");
//				if (code == 200) {
//					String data = bundle.getString("data");
//					String fileName = bundle.getString("uri");
//					fileName = fileName
//							.substring(fileName.lastIndexOf('/') + 1);
//
//					try {
//						File file = getOutputMediaFile(fileName);
//						FileOutputStream fop = new FileOutputStream(file);
//						fop.write(data.getBytes("ISO-8859-1"), 0,
//								data.getBytes("ISO-8859-1").length);
//						fop.flush();
//						fop.close();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//					Log.v("Kite", "file path is "
//							+ getOutputMediaFile(fileName).getAbsolutePath());
//
//					final DetailVoiceCheckbox voiceButton = feed_detail_voice_button;
//					final SeekBar voiceSeekBar = feed_detail_voice_seekbar;
//					final TextView voiceTime = feed_detail_voice_time;
//					updateTime(voiceTime, 0);
//					
//					voiceButton.sourceFile = fileName;
//					{
//						voiceButton.mediaPlayer = new MediaPlayer();
//						try {
//							voiceButton.mediaPlayer
//									.setDataSource(getOutputMediaFile(
//											voiceButton.sourceFile).getPath());
//							voiceButton.mediaPlayer.prepare();
//							voiceSeekBar.setMax(voiceButton.mediaPlayer
//									.getDuration());
//							final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
//							voiceHandler = new Handler();
//							voiceRunnable = new Runnable() {
//
//								int count = 10;
//								int currentTime = 0;
//								
//								@Override
//								public void run() {
//									// TODO Auto-generated method stub
//									count--;
//									if(count == 0)
//									{
//										currentTime++;
//										updateTime(voiceTime, currentTime);
//										
//										count = 10;
//									}
//									
//									voiceSeekBar.setProgress(voiceMediaPlayer
//											.getCurrentPosition());
//									voiceHandler.postDelayed(this, 100);
//								}
//							};
//
//							voiceHandler.removeCallbacks(voiceRunnable);
//							voiceHandler.postDelayed(voiceRunnable, 100);
//
//							voiceButton.mediaPlayer.start();
//							voiceButton.mediaPlayer
//									.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//										@Override
//										public void onCompletion(
//												MediaPlayer mediaPlayer) {
//
//											voiceSeekBar.setProgress(0);
//											voiceHandler
//													.removeCallbacks(voiceRunnable);
//											updateTime(voiceTime, 0);
//											voiceButton.setChecked(true);
//
//											mediaPlayer.release();
//											mediaPlayer = null;
//										}
//									});
//						} catch (IOException e) {
//							e.printStackTrace();
//							Log.e("DBG", "播放失败");
//						}
//						voiceButton.setChecked(false);
//
//					}
//
//					voiceButton.setOnClickListener(new View.OnClickListener() {
//						@Override
//						public void onClick(View view) {
//							
//
//							
//							if(isTriangleListShown())
//							{
//								hideTriangleLayout();
//								
//								return;
//							}
//							
//							if (!voiceButton.isChecked()) {
//								voiceButton.mediaPlayer = new MediaPlayer();
//								Runnable r = new Runnable() {
//									@Override
//									public void run() {
//										try {
//												voiceButton.mediaPlayer.setDataSource(FeedDetailActivity.this, Uri.parse(fileUrl));
//												voiceButton.mediaPlayer.prepare();
//												voiceSeekBar.setMax(voiceButton.mediaPlayer
//														.getDuration());
//											Log.v("Kite", "Duration: " + voiceButton.mediaPlayer.getDuration());
//											
//											final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;
//											
//											voiceRunnable = new Runnable() {
//
//												int count = 10;
//												int currentTime = 0;
//												
//												@Override
//												public void run() {
//													// TODO Auto-generated method stub
//													count--;
//													if(count == 0)
//													{
//														currentTime++;
//														updateTime(voiceTime, currentTime);
//														
//														count = 10;
//													}
//													
//													voiceSeekBar.setProgress(voiceMediaPlayer
//															.getCurrentPosition());
//													voiceHandler.postDelayed(this, 100);
//												}
//											};
//											
//											voiceHandler.removeCallbacks(voiceRunnable);
//											voiceHandler.postDelayed(voiceRunnable, 100);
//											
//											voiceButton.mediaPlayer.start();
//										} catch (Exception e) {
//											Log.v("Kite", e.getMessage(), e);
//										}
//									}
//								};
//								new Thread(r).start();
//							} else {
//								
//								voiceHandler
//								.removeCallbacks(voiceRunnable);
//								voiceSeekBar.setProgress(0);
//								updateTime(voiceTime, 0);
//								
//								if (voiceButton.mediaPlayer != null) {
//									voiceButton.mediaPlayer.release();
//								}
//								voiceButton.mediaPlayer = null;
//								voiceButton.setChecked(true);
//							}
//						}
//					});
//
//				} else {
//					Log.v("DBG", "Download " + bundle.getString("uri")
//							+ " failed");
//				}
//
//			}
//		};
//		handler.start();
//	}

	private void downloadVoice(final String fileUrl)
	{
		if (fileUrl == null) {
			Toast toast = Toast.makeText(context, "播放失败", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
		
		final DetailVoiceCheckbox voiceButton = feed_detail_voice_button;
		final SeekBar voiceSeekBar = feed_detail_voice_seekbar;
		final TextView voiceTime = feed_detail_voice_time;
		updateTime(voiceTime, 0);
		
		if (voiceButton.mediaPlayer != null) {
			voiceButton.mediaPlayer.start();
			return;
		}
		voiceButton.mediaPlayer = new CustomMediaPlayer();
		voiceButton.mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
		// 监听错误事件
		voiceButton.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.v("Kite", "Error on Listener, what: " + what + "extra: " + extra);
				return false;
			}
		});
		// 监听缓冲事件
		voiceButton.mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				Log.v("Kite", "MediaPlayer Update buffer: " + Integer.toString(percent) + "%");
			}
		});
		// 监听播放完毕事件
		voiceButton.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Listener Completed");
				
				voiceSeekBar.setProgress(0);
				voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
				updateTime(voiceTime, 0);
				voiceButton.setChecked(true);

				mp.pause();
			}
		});
		// 监听准备事件
		voiceButton.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				Log.v("Kite", "MediaPlayer Prepared Listener");
			}
		});
		
		voiceRunnable = new Runnable() {
			
			@Override
			public void run() {
				if(!voiceRunnableStop)
				{

					try {
						voiceButton.mediaPlayer.setDataSource(
								FeedDetailActivity.this, Uri.parse(fileUrl));
						voiceButton.mediaPlayer.prepare();
						voiceSeekBar.setMax(voiceButton.mediaPlayer
								.getDuration());
						Log.v("Kite",
								"Duration: "
										+ voiceButton.mediaPlayer.getDuration());

						final MediaPlayer voiceMediaPlayer = voiceButton.mediaPlayer;

						voiceTimmerRunnable = new Runnable() {

							int count = 10;

							@Override
							public void run() {
								// TODO Auto-generated method stub
								count--;
								if (count == 0) {
									updateTime(
											voiceTime,
											voiceMediaPlayer
													.getCurrentPosition() / 1000);

									count = 10;
								}

								voiceSeekBar.setProgress(voiceMediaPlayer
										.getCurrentPosition());
								voiceHandler.postDelayed(this, 100);
							}
						};

						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);

						voiceButton.mediaPlayer.start();
					} catch (Exception e) {
					}
				
				}
			}
		};
		
		voiceRunnableStop = false;
		new Thread(voiceRunnable).start();
		
		voiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				if(isTriangleListShown())
				{
					hideTriangleLayout();
					
					return;
				}
				
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
					
					return;
				}
				
				if(voiceButton.mediaPlayer != null)
				{
					if (!voiceButton.isChecked()) 
					{
						voiceButton.mediaPlayer.start();
						voiceHandler.removeCallbacks(voiceTimmerRunnable);
						voiceHandler.postDelayed(voiceTimmerRunnable, 100);
					} 
					else {
						
						voiceButton.mediaPlayer.pause();
						voiceHandler
						.removeCallbacks(voiceTimmerRunnable);
					}
				}
			}
		});
	}
	
	private static File getOutputMediaFile(String filename) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ filename);

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}
	
	public boolean isTriangleListShown()
	{
		return triangleListShown;
	}
	
	public void showTriangleLayout()
	{
		ImageView feed_detail_triangle = (ImageView) feed_detail_triangle_layout
				.findViewById(R.id.feed_detail_triangle);
		feed_detail_triangle.setImageResource(R.drawable.main_list_triangle_down);

		Animation anim = new RotateAnimation(90, 0,
				feed_detail_triangle.getWidth() / 2,
				feed_detail_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		feed_detail_triangle.startAnimation(anim);

		((LinearLayout) feed_detail_triangle_option_layout).setVisibility(View.VISIBLE);
		anim = new AlphaAnimation(0, 100);
		anim.setDuration(5000);
		anim.setFillAfter(false);
		((LinearLayout) feed_detail_triangle_option_layout).startAnimation(anim);

		triangleListShown = true;
	}
	
	public void hideTriangleLayout()
	{
		ImageView feed_detail_triangle = (ImageView) feed_detail_triangle_layout
				.findViewById(R.id.feed_detail_triangle);
		feed_detail_triangle.setImageResource(R.drawable.main_list_triangle_left);

		Animation anim = new RotateAnimation(-90, 0,
				feed_detail_triangle.getWidth() / 2,
				feed_detail_triangle.getHeight() / 2);
		anim.setDuration(500);
		anim.setFillAfter(true);
		feed_detail_triangle.startAnimation(anim);

		((LinearLayout) feed_detail_triangle_option_layout).setVisibility(View.GONE);
		anim = new AlphaAnimation(100, 0);
		anim.setDuration(500);
		anim.setFillAfter(false);
		((LinearLayout) feed_detail_triangle_option_layout).startAnimation(anim);

		triangleListShown = false;
	}
	
	private void hideShareMethodLayout() {
		share_method_shown = false;

		feed_detail_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) share_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			share_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		share_method_layout.startAnimation(animation);
	}

	private void showShareMethodLayout(final String feedId, final String title,
			final String text, final String imageUrl) {
		share_method_shown = true;

		feed_detail_mask_layout.setVisibility(View.VISIBLE);
		share_method_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) share_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			share_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		share_method_layout.startAnimation(animation);
		
		share_wechat_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				Platform platform = ShareSDK.getPlatform(Wechat.NAME);
				
				share(platform, feedId, title, text, imageUrl);
			}
			
		});
		
		share_friend_circle_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				Platform platform = ShareSDK.getPlatform(WechatMoments.NAME);
				
				share(platform, feedId, title, text, imageUrl);
			}
			
		});
		
		share_qzone_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				Platform platform = ShareSDK
						.getPlatform(
								FeedDetailActivity.this,
								QZone.NAME);
				
				share(platform, feedId, title, text, imageUrl);
			}
			
		});
		
		share_qq_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				Platform platform = ShareSDK
						.getPlatform(
								FeedDetailActivity.this,
								QQ.NAME);
				
				share(platform, feedId, title, text, imageUrl);
			}
			
		});
		
		share_sina_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				Platform platform = ShareSDK
						.getPlatform(
								FeedDetailActivity.this,
								SinaWeibo.NAME);
				
				share(platform, feedId, title, text, imageUrl);
			}
			
		});
	}
	
	private void share(final Platform platform, String feedId, final String title,
			final String text, final String imageUrl)
	{
		if (thirdShareHandler == null) {
			thirdShareHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {

					int id = msg.arg1;
					if (id == 1) {
						Toast toast = Toast.makeText(FeedDetailActivity.this, "分享成功",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						
					} else if (id == 2) {
						Toast toast = Toast.makeText(FeedDetailActivity.this, "分享失败",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				}
			};
		}
		
		builder = new AlertDialog.Builder(FeedDetailActivity.this);
		builder.setMessage("分享中");
		dialog = builder.show();
		
		NetHandler netHandler = new NetHandler(FeedDetailActivity.this, 
				NetHandler.METHOD_GET,
				"/share/generate?feedId="
						+ feedId,
				new LinkedList<BasicNameValuePair>(),
				null) {

			@Override
			public void handleRsp(
					Message msg) {
				// TODO Auto-generated
				// method stub
				Bundle bundle = msg
						.getData();
				int code = bundle
						.getInt("code");
				Log.v("Kite",
						"share data is "
								+ bundle.getString("data"));

				dialog.dismiss();
				
				if (code == 200) {
					try {
						JSONObject shareObject = new JSONObject(
								bundle.getString("data"));
						String shareUrl = shareObject
								.getString("url");

						ShareParams sp = new ShareParams();
						sp.setText(text);
						sp.setTitle(title);
						
						if(imageUrl != null)
						{
							sp.setImageUrl(imageUrl);
						}
						
						if(platform.getName().equals(WechatMoments.NAME) || platform.getName().equals(Wechat.NAME))
						{
							sp.setUrl(shareUrl);
							sp.setShareType(Platform.SHARE_WEBPAGE);
						}
						else
						{
							sp.setTitleUrl(shareUrl);
						}
						
						platform.setPlatformActionListener(new PlatformActionListener() {
							@Override
							public void onComplete(
									Platform platform,
									int i,
									HashMap<String, Object> stringObjectHashMap) {
								Message msg = new Message();
								msg.arg1 = 1;
								msg.arg2 = i;
								msg.obj = platform;
								thirdShareHandler
										.sendMessage(msg);
								Log.v("DBG",
										"SHARE DONE");
							}

							@Override
							public void onError(
									Platform platform,
									int i,
									Throwable throwable) {
								throwable
										.printStackTrace();
								Message msg = new Message();
								msg.arg1 = 2;
								msg.arg2 = i;
								msg.obj = platform;
								thirdShareHandler
										.sendMessage(msg);
								Log.v("DBG",
										"SHARE ERROR");
							}

							@Override
							public void onCancel(
									Platform platform,
									int i) {
								Log.v("DBG",
										"SHARE CANCEL");
							}
						});
						platform.share(sp);

					} catch (JSONException e) {
						// TODO
						// Auto-generated
						// catch block
						e.printStackTrace();
					}
				} else {
					Toast toast = Toast.makeText(
							FeedDetailActivity.this,
							"分享失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		};
		netHandler.start();
	}
	
	@Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
	
  	
  	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(feed_detail_voice_button.mediaPlayer != null)
		{
			voiceRunnableStop = true;
			
			voiceHandler.removeCallbacks(voiceTimmerRunnable);
			
			feed_detail_voice_button.mediaPlayer.stop();
			
			feed_detail_voice_button.mediaPlayer.release();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed_detail);
		
		if(Utils.application == null)
		{
			Utils.init(FeedDetailActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		context = FeedDetailActivity.this;
		feedsOperations = new FeedsOperations(context);
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/handwriting.fon");
		feedId = FeedDetailActivity.this.getIntent().getStringExtra("feedId");
		
		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setVisibility(View.VISIBLE);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FeedDetailActivity.this.finish();
			}
			
		});
		
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("详情");
		
		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);
		
		feed_detail_root_layout = (IMERelativeLayout) findViewById(R.id.feed_detail_root_layout);
		
		imeStateListener = new IMEStateListener(){

			@Override
			public void onChange() {
				// TODO Auto-generated method stub
				if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
				{
					hideCommentEditLayout();
				}
			}

		};
		
		imeRunnable = new Runnable()
		{

			@Override
			public void run() {
				// TODO Auto-generated method stub
				feed_detail_root_layout.setIMEChangeListener(imeStateListener);
			}
			
		};
		
		feed_detail_content_layout = (RelativeLayout) findViewById(R.id.feed_detail_root);
		
		feed_detail_avatar = (ImageView) 
				findViewById(R.id.feed_detail_avatar);
		feed_detail_nickname = (TextView) findViewById(R.id.feed_detail_nickname);
		feed_detail_nickname.setTypeface(typeFace);
		feed_detail_nickname.getPaint().setFakeBoldText(true);

		feed_detail_time = (TextView) findViewById(R.id.feed_detail_time);
		feed_detail_time.setTypeface(typeFace);
		feed_detail_time.getPaint().setFakeBoldText(true);

		feed_detail_locator_image = (ImageView) findViewById(R.id.feed_detail_locator_image);
		feed_detail_location = (TextView) findViewById(R.id.feed_detail_location);
		feed_detail_location.setTypeface(typeFace);
		feed_detail_location.getPaint().setFakeBoldText(true);

		feed_detail_triangle_layout = (LinearLayout) findViewById(R.id.feed_detail_triangle_layout);
		feed_detail_triangle_option_layout = (LinearLayout) findViewById(R.id.feed_detail_triangle_option_layout);
		feed_detail_triangle_option_layout.setVisibility(View.GONE);

		feed_detail_event = (ImageView) findViewById(R.id.feed_detail_event);
		feed_detail_content = (TextView) findViewById(R.id.feed_detail_content);
//		feed_detail_content.setTypeface(typeFace);
//		feed_detail_content.getPaint().setFakeBoldText(true);

		feed_detail_share_image = (ImageView) findViewById(R.id.feed_detail_share_image);
		feed_detail_up_image = (ImageView) findViewById(R.id.feed_detail_up_image);
		feed_detail_comment_image = (ImageView) findViewById(R.id.feed_detail_comment_image);
		feed_detail_share_number = (TextView) findViewById(R.id.feed_detail_share_number);
		feed_detail_share_number.setTypeface(typeFace);
		feed_detail_share_number.getPaint().setFakeBoldText(true);

		feed_detail_up_number = (TextView) findViewById(R.id.feed_detail_up_number);
		feed_detail_up_number.setTypeface(typeFace);
		feed_detail_up_number.getPaint().setFakeBoldText(true);

		feed_detail_comment_number = (TextView) findViewById(R.id.feed_detail_comment_number);
		feed_detail_comment_number.setTypeface(typeFace);
		feed_detail_comment_number.getPaint()
				.setFakeBoldText(true);

		feed_detail_up_filled = (ImageView) findViewById(R.id.feed_detail_up_filled);
		
		feed_detail_up_names = (SpannableFixFocusTextView) findViewById(R.id.feed_detail_up_names);
//		feed_detail_up_names.setTypeface(typeFace);
//		feed_detail_up_names.getPaint().setFakeBoldText(true);

		feed_detail_image_layout = (LinearLayout) findViewById(R.id.feed_detail_image_layout);

		feed_detail_video_layout = (RelativeLayout) findViewById(R.id.feed_detail_video_layout);

		feed_detail_video_thumb = (ImageView) feed_detail_video_layout
				.findViewById(R.id.feed_detail_video_thumb);
		// holder.feed_detail_video.setZOrderOnTop(true);

		feed_detail_voice_layout = (RelativeLayout) findViewById(R.id.feed_detail_voice_layout);

		feed_detail_voice_button = (DetailVoiceCheckbox) feed_detail_voice_layout
				.findViewById(R.id.feed_detail_voice_button);

		feed_detail_voice_background = (ImageView) feed_detail_voice_layout
				.findViewById(R.id.feed_detail_voice_background);

		feed_detail_voice_seekbar = (SeekBar) feed_detail_voice_layout
				.findViewById(R.id.feed_detail_voice_seekbar);
		feed_detail_voice_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

	        int originalProgress;

	        @Override
	        public void onStopTrackingTouch(SeekBar seekBar) {
	        }

	        @Override
	        public void onStartTrackingTouch(SeekBar seekBar) {
	            originalProgress = seekBar.getProgress();
	        }

	        @Override
	        public void onProgressChanged(SeekBar seekBar, int arg1, boolean fromUser) {
	            if(fromUser == true){
	                seekBar.setProgress(originalProgress);
	            }               
	        }
	    });

		feed_detail_voice_time = (TextView) feed_detail_voice_layout
				.findViewById(R.id.feed_detail_voice_time);

		feed_detail_comment_layout = (LinearLayout) findViewById(R.id.feed_detail_comment_layout);
		
		feed_detail_comment_edit_layout = (RelativeLayout) findViewById(R.id.feed_detail_comment_edit_layout);
		feed_detail_comment_edit = (DetectKeyDownActionEditText) findViewById(R.id.feed_detail_comment_edit);
		feed_detail_comment_send = (Button) findViewById(R.id.feed_detail_comment_send);

		feed_detail_mask_layout = (RelativeLayout) findViewById(R.id.feed_detail_mask_layout);
		
		share_method_layout = (RelativeLayout) findViewById(R.id.feed_detail_share_method_layout);

		share_wechat_layout = (LinearLayout) findViewById(R.id.feed_detail_share_wechat_layout);
		share_friend_circle_layout = (LinearLayout) findViewById(R.id.feed_detail_share_friend_circle_layout);
		share_qzone_layout = (LinearLayout) findViewById(R.id.feed_detail_share_qzone_layout);
		share_qq_layout = (LinearLayout) findViewById(R.id.feed_detail_share_qq_layout);
		share_sina_layout = (LinearLayout) findViewById(R.id.feed_detail_share_sina_layout);

		share_method_cancel = (TextView) findViewById(R.id.feed_detail_share_method_cancel);
		share_method_cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
			}
			
		});
		
		feed_detail_progress = (ProgressBar) findViewById(R.id.feed_detail_progress);
		feed_detail_progress.setVisibility(View.GONE);
		
		getFeedDetail();
	}

	private void deleteFeed(String feedId)
	{
		NetHandler handler = new NetHandler(FeedDetailActivity.this, NetHandler.METHOD_DELETE, "/feeds/"+feedId, new LinkedList<BasicNameValuePair>(), null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				Bundle bundle = msg.getData();
				
				String data = bundle.getString("data");
				int code = bundle.getInt("code");
				if(code ==200)
				{
					Log.v("Kite", "删除成功");
					
					Toast toast = Toast.makeText(FeedDetailActivity.this, "删除成功", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					
					Intent intent = new Intent();
					int position = FeedDetailActivity.this.getIntent().getIntExtra("position", -1);
					intent.putExtra("position", position);
					FeedDetailActivity.this.setResult(MainActivity.FEED_DETAIL_DELETE_CODE, intent);
					FeedDetailActivity.this.finish();
				}
				else
				{
					Log.v("Kite", "删除失败");
					Log.v("Kite", "delete fail because " + data);
					
					Toast toast = Toast.makeText(FeedDetailActivity.this, "删除失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
			
		};
		
		handler.start();
	}
	
	private void incNum(TextView view)
	{
		String original_number_string = (String) view.getText();
		if(original_number_string.equals(""))
		{
			view.setText("(" + 1 + ")");
		}
		else
		{
			int curNum = Integer.parseInt((String)original_number_string.subSequence(1, original_number_string.length()-1)) + 1;
			view.setText("(" + curNum + ")");
		}
	}
	
	private void decNum(TextView view)
	{
		String original_number_string = (String) view.getText();
		if(original_number_string.equals(""))
		{
			return;
		}
		else
		{
			int curNum = Integer.parseInt((String)original_number_string.subSequence(1, original_number_string.length()-1)) - 1;
			
			if(curNum != 0)
			{
				view.setText("(" + curNum + ")");
			}
			else
			{
				view.setText("");
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			if(isTriangleListShown())
			{
				hideTriangleLayout();
				
				return true;
			}
			
			if(feed_detail_comment_edit_layout.getVisibility() == View.VISIBLE)
			{
				hideCommentEditLayout();
				
				return true;
			}
			
			FeedDetailActivity.this.finish();
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
