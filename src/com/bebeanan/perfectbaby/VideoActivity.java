package com.bebeanan.perfectbaby;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class VideoActivity extends Activity {

	private VideoView video_video;
	private ProgressBar video_progress;

	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);

		video_video = (VideoView) findViewById(R.id.video_video);
		video_progress = (ProgressBar) findViewById(R.id.video_progress);

		String videoUrl = getIntent().getStringExtra("videoUrl");
		
		downloadVideo(video_video, videoUrl);
	}

	private void downloadVideo(final VideoView videoView, String fileUrl) {
		
		videoView.setVideoURI(Uri.parse(fileUrl));

		MediaController controller = new MediaController(VideoActivity.this){

			@Override
		    public void show(int timeout) {
		        super.show(0);
		    }
			
		};
		videoView.setMediaController(controller);

		videoView.requestFocus();
		videoView.seekTo(1);
		videoView.start();

		videoView.setOnPreparedListener(new OnPreparedListener(){

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				video_progress.setVisibility(View.GONE);
				videoView.start();
			}
			
		});
		videoView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				((VideoView) view).start();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			
			VideoActivity.this.finish();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	
}
