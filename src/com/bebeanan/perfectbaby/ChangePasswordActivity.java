package com.bebeanan.perfectbaby;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout.IMEStateListener;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class ChangePasswordActivity extends Activity {

	private IMELinearLayout changePasswordLayout;
    private EditText originalEditText;
    private EditText newPasswordEditText;
    private EditText repeatEditText;
    private ImageButton registerButton;

    private Toast toast;
    private AlertDialog.Builder builder;
    private boolean showBuilder = false;
    private AlertDialog dialog;
    
    private boolean firstSetPassword;

    private void CheckPass() {
        String original = originalEditText.getText().toString();
        String newpassword = newPasswordEditText.getText().toString();
        String repeat = repeatEditText.getText().toString();
        
        if(!firstSetPassword)
        {
        	if (!original.isEmpty() && !newpassword.isEmpty()
                    && !repeat.isEmpty()) {
                registerButton.setImageResource(R.drawable.change_password_complete_enable);
                registerButton.setClickable(true);
            } else {
                registerButton.setImageResource(R.drawable.change_password_complete_disable);
                registerButton.setClickable(false);
            }
        }
        else
        {
        	if (!newpassword.isEmpty()
                    && !repeat.isEmpty()) {
                registerButton.setImageResource(R.drawable.change_password_complete_enable);
                registerButton.setClickable(true);
            } else {
                registerButton.setImageResource(R.drawable.change_password_complete_disable);
                registerButton.setClickable(false);
            }
        }

        /* 这里需要添加每一个edittext的判断，这里还需要判断软键盘消失的时候 */
    }

    private boolean checkPassword(String password) {
        for(int i = 0; i != password.length(); i++) {
            if (!((password.charAt(i) >= 'A' && password.charAt(i) <= 'Z') ||
                    (password.charAt(i) >= 'a' && password.charAt(i) <= 'z') ||
                    (password.charAt(i) >= '0' && password.charAt(i) <= '9') ||
                    (password.charAt(i) == '_'))) {
                return false;
            }
        }
        return true;
    }

    @Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        toast = Toast.makeText(getApplicationContext(), "",
                Toast.LENGTH_SHORT);
        builder = new AlertDialog.Builder(ChangePasswordActivity.this);
        builder.setCancelable(false);

        changePasswordLayout = (IMELinearLayout) findViewById(R.id.change_password_layout);
        changePasswordLayout.setIMEChangeListener(new IMEStateListener() {
			
			@Override
			public void onChange() {
				// TODO Auto-generated method stub
				CheckPass();
			}
		});
        
        firstSetPassword = ChangePasswordActivity.this.getIntent().getBooleanExtra("firstSetPassword", false);
        
        if(firstSetPassword)
        {
        	TextView change_password_original_text = (TextView) findViewById(R.id.change_password_original_text);
        	change_password_original_text.setVisibility(View.GONE);
        }
        
        originalEditText = (EditText) findViewById(R.id.change_password_original_edittext);
//        originalEditText.setTypeface(Utils.typeface);
        if(firstSetPassword)
        {
        	originalEditText.setVisibility(View.GONE);
        }
        
        newPasswordEditText = (EditText) findViewById(R.id.change_password_new_edittext);
//        newPasswordEditText.setTypeface(Utils.typeface);
        repeatEditText = (EditText) findViewById(R.id.change_password_repeat_edittext);
//        repeatEditText.setTypeface(Utils.typeface);
        registerButton = (ImageButton) findViewById(R.id.register_add_button);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String original = originalEditText.getText().toString();
                String newpassword = newPasswordEditText.getText().toString();
                String repeat = repeatEditText.getText().toString();
                if (!firstSetPassword && original.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "原密码不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if (newpassword.isEmpty()) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "新密码不能为空",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if (newpassword.length() < 6 || newpassword.length() > 15) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "密码长度不能小于6个字符或大于15个字符",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if (!checkPassword(newpassword)) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "密码不能包含非法字符",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if (!newpassword.equals(repeat)) {
                    toast.cancel();
                    toast = Toast.makeText(getApplicationContext(), "两次密码不相同",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                JSONObject data = new JSONObject();
                try {
                	
                	if(!firstSetPassword)
                	{
                		data.put("oldpassword", original);
                    }
                	
                    data.put("password", newpassword);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                builder.setMessage("修改中");
                dialog = builder.show();
                showBuilder = true;

                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

                NetHandler handler = new NetHandler(ChangePasswordActivity.this, NetHandler.METHOD_POST,
                        "/resetpassword/modify", param, data) {
                    @Override
                    public void handleRsp(Message msg) {
                        dialog.dismiss();
                        showBuilder = false;
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        if (code == 200) {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "修改成功",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            String storeFile = StoreHandler.GetStoreFile(ChangePasswordActivity.this);
                            try {
                                JSONObject storeObject = new JSONObject(storeFile);
                                storeObject.put("password", newPasswordEditText.getText().toString());
                                StoreHandler.SetStoreFile(ChangePasswordActivity.this, storeObject.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                    overridePendingTransition(R.anim.activity_close_in_anim,
                                            R.anim.activity_close_out_anim);
                                }
                            }.start();
                            return;
                        } else if (code == 400) {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "原密码错误",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        } else {
                            toast.cancel();
                            toast = Toast.makeText(getApplicationContext(), "修改失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }
                };
                handler.start();
            }
        });
        registerButton.setClickable(false);

        originalEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    CheckPass();
                }
            }
        });
        originalEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CheckPass();
                    return true;
                }
                return false;
            }
        });


        newPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    CheckPass();
                }
            }
        });
        newPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CheckPass();
                    return true;
                }
                return false;
            }
        });

        repeatEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    CheckPass();
                }
            }
        });
        repeatEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CheckPass();
                    return true;
                }
                return false;
            }
        });

        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.change_password_title);
        titleView.setTypeface(Utils.typeface);

        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.drawable.title_back_pic);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);
        
        TextView change_password_original_text = (TextView) findViewById(R.id.change_password_original_text);
        change_password_original_text.setTypeface(Utils.typeface);
        
        TextView change_password_new_text = (TextView) findViewById(R.id.change_password_new_text);
        change_password_new_text.setTypeface(Utils.typeface);
        
        TextView change_password_repeat_text = (TextView) findViewById(R.id.change_password_repeat_text);
        change_password_repeat_text.setTypeface(Utils.typeface);

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (showBuilder) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        } else {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    this.finish();
                    overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        originalEditText.clearFocus();
        newPasswordEditText.clearFocus();
        repeatEditText.clearFocus();
        return super.onTouchEvent(event);
    }
}