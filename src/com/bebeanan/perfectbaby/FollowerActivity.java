package com.bebeanan.perfectbaby;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.PinyinUtil;
import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.InviteLayoutView;
import com.bebeanan.perfectbaby.zxing.view.InviteLayoutView.OnLoadingListener;
import com.perfectbaby.adapters.FollowerExpandableListAdapter;
import com.perfectbaby.adapters.FollowerExpandableListAdapter.OnInviteClickListener;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class FollowerActivity extends Activity{
	


	private ExpandableListView follower_list;
	private ProgressBar follower_progress;
	private InviteLayoutView follower_invite_layout;
	
	private ImageView follower_guide;
	
	private ArrayList<Map<String, String>> mainData;
	private ArrayList<ArrayList<Map<String, Object>>> childData;
	private FollowerExpandableListAdapter mAdapter;
	
	private String fansCacheDataString;
	
	private static SharedPreferences userPreferences;
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(fansCacheDataString != null)
		{
			StoreHandler.storeFansCahce(fansCacheDataString);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follower);
		
		if(Utils.application == null)
		{
			Utils.init(FollowerActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		follower_list = (ExpandableListView) findViewById(R.id.follower_list);
		follower_list.setDivider(null);
		follower_progress = (ProgressBar) findViewById(R.id.follower_progress);
		follower_invite_layout = (InviteLayoutView) findViewById(R.id.follower_invite_layout);
		follower_invite_layout.setOnLoadingListener(new OnLoadingListener(){

			@Override
			public void onLoading() {
				// TODO Auto-generated method stub
				follower_progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadComplete() {
				// TODO Auto-generated method stub
				follower_progress.setVisibility(View.GONE);
			}
			
		});
		
		follower_guide = (ImageView) findViewById(R.id.follower_guide);
		
		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				FollowerActivity.this.finish();
				overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
			}
			
		});
		
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("家人粉丝团");
		
		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);
		
		getData();
	}

	private void getData()
	{
		follower_progress.setVisibility(View.VISIBLE);
		
		mainData = new ArrayList<Map<String, String>>();
		Map<String, String> groupTemp = new HashMap<String, String>();
		groupTemp.put("group_title", "家人");
		groupTemp.put("group_describe", "（可以一起更新宝宝状态）");
		mainData.add(groupTemp);
		
		groupTemp = new HashMap<String, String>();
		groupTemp.put("group_title", "粉丝团");
		groupTemp.put("group_describe", "（可以查看非隐私状态）");
		mainData.add(groupTemp);
		
		fansCacheDataString = StoreHandler.getFansCache();
		if(fansCacheDataString != null)
		{
			follower_progress.setVisibility(View.GONE);
			
			setData(fansCacheDataString);
			
			getFans();
		}
		else
		{
			getFans();
		}
	}
	
	private void getFans()
	{
		NetHandler handler = new NetHandler(FollowerActivity.this, NetHandler.METHOD_GET, "/follower?userId=" + userPreferences.getString("userId", "")+"&$limit=0", new LinkedList<BasicNameValuePair>(), null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				follower_progress.setVisibility(View.GONE);
				
				Bundle bundle = msg.getData();
				
				int code = bundle.getInt("code");
				String data = bundle.getString("data");
				Log.v("Kite", "followers data is " + data);
				if(code == 200)
				{
					fansCacheDataString = data;
					
					setData(data);
				}
				else
				{
					Log.v("Kite", "get followers fail");
				}
			}
			
		};
		
		handler.start();
	}
	
	private void setData(String fansString)
	{

		Log.v("Kite", "get followers success");
		
		showGuide();
		
		childData = new ArrayList<ArrayList<Map<String, Object>>>();
		
		try {
			JSONArray followerArray = new JSONArray(fansString);
			
			ArrayList<Map<String, Object>> fansList = new ArrayList<Map<String, Object>>();
			ArrayList<Map<String, Object>> familyList = new ArrayList<Map<String, Object>>();
			
			for(int i=0; i<followerArray.length(); i++)
			{
				int group = followerArray.getJSONObject(i).getInt("group");
				
				JSONObject followerObject = followerArray.getJSONObject(i).getJSONObject("follower");
				Map<String, Object> temp = new HashMap<String, Object>();
				
				temp.put("id", followerArray.getJSONObject(i).getString("id"));
				
				if(followerObject.has("nickname"))
				{
					temp.put("nickName", followerObject.getString("nickname"));
				}
				else
				{
					continue;
				}
				
				temp.put("followerId", followerObject.getString("id"));
				
				if(followerObject.has("gender"))
				{
					temp.put("gender", followerObject.getInt("gender"));
				}
				else
				{
					temp.put("gender", 1);
				}
				
				if(followerObject.has("avatar"))
				{
					temp.put("avatarUrl", followerObject.getString("avatar"));
				}
				else
				{
					temp.put("avatarUrl", "");
				}
				
				switch(group)
				{
				case 0:
					
					fansList.add(temp);
					
					break;
					
				case 2:
					
					familyList.add(temp);
					
					break;
				}
			}
			
			Collections.sort(familyList, comparator);
			Collections.sort(fansList, comparator);
			
			childData.add(familyList);
			childData.add(fansList);
			
			mAdapter = new FollowerExpandableListAdapter(FollowerActivity.this, mainData, childData);
//			follower_list.setGroupIndicator(FollowerActivity.this.getResources().getDrawable(R.drawable.expandablelistviewselector)); 
			mAdapter.setOnInviteClickListener(new OnInviteClickListener(){

				@Override
				public void onInviteClick(int groupPosition) {
					// TODO Auto-generated method stub
					switch(groupPosition)
					{
					case 0:
						
						follower_invite_layout.setInviteType(follower_invite_layout.INVITE_TYPE_FAMILY);
						
						break;
						
					case 1:
						
						follower_invite_layout.setInviteType(follower_invite_layout.INVITE_TYPE_FANS);
						
						break;
					}
					
					follower_invite_layout.showInviteMethodLayout();
				}
			});
			follower_list.setAdapter(mAdapter);
			follower_list.expandGroup(0);
			follower_list.expandGroup(1);
			follower_list.setOnChildClickListener(new OnChildClickListener(){

				@Override
				public boolean onChildClick(
						ExpandableListView parent, View v,
						int groupPosition, int childPosition,
						long id) {
					// TODO Auto-generated method stub
					
					if(childData.get(groupPosition).size() == 0)
					{
						return false;
					}
					
					Intent intent = new Intent(FollowerActivity.this, PersonalDetailActivity.class);
					intent.putExtra("followeeId", (String)childData.get(groupPosition).get(childPosition).get("followerId"));
					intent.putExtra("followeeNickName", (String)childData.get(groupPosition).get(childPosition).get("nickName"));
					intent.putExtra("followeeAvatarUrl", (String)childData.get(groupPosition).get(childPosition).get("avatarUrl"));
					
					FollowerActivity.this.startActivity(intent);
					
					return false;
				}
				
			});
			
			follower_list.setOnItemLongClickListener(new OnItemLongClickListener(){

				@Override
				public boolean onItemLongClick(
						AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					final int groupPosition = (Integer)view.getTag(R.id.group_position);
					final int childPosition = (Integer)view.getTag(R.id.child_position);
					
					if(groupPosition!=-1 && childPosition!=-1)
					{
						AlertDialog.Builder builder = new Builder(FollowerActivity.this);

							
								builder.setMessage("删除之后，TA不能再关注宝宝的成长，确定删除吗？");
								builder.setTitle("提示");

								builder.setPositiveButton(
										"确定",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {

												deleteFollower(groupPosition, childPosition);
											}
										});

								builder.setNegativeButton(
										"取消",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {

											}
										});

								builder.create().show();
						
					}
					
					return true;
				}
				
			});
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	private void showGuide()
	{
		boolean followerGuideShown = StoreHandler.getGuide(StoreHandler.followerGuide);
		if(!followerGuideShown)
		{
			follower_guide.setVisibility(View.VISIBLE);
			follower_guide.setImageResource(R.drawable.followers_guide);
			follower_guide.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					follower_guide.setVisibility(View.GONE);

					StoreHandler.setGuide(StoreHandler.followerGuide, true);

					return;
				}
				
			});
		}
	}
	
	private void deleteFollower(final int groupPosition, final int childPosition)
	{
		final String deleteId = (String)childData.get(groupPosition).get(childPosition).get("id");
		NetHandler handler = new NetHandler(FollowerActivity.this, NetHandler.METHOD_DELETE, "/follower?id=" + deleteId, new LinkedList<BasicNameValuePair>(), null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				follower_progress.setVisibility(View.GONE);
				
				Bundle bundle = msg.getData();
				
				int code = bundle.getInt("code");
				String data = bundle.getString("data");

				if(code == 200)
				{
					Toast toast = Toast.makeText(getApplicationContext(),
							"删除成功", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					
					ArrayList<Map<String, Object>> newData = childData.get(groupPosition);
					newData.remove(childPosition);
					childData.remove(groupPosition);
					childData.add(groupPosition, newData);
					
					mAdapter.notifyDataSetChanged();
					
					if(fansCacheDataString != null)
					{
						try {
							
							JSONArray fansCacheDataArray = new JSONArray(fansCacheDataString);
							
							JSONArray tempfansCacheDataArray = new JSONArray();
							
							for (int i = 0; i < fansCacheDataArray.length(); i++) 
							{
								JSONObject fansObject = fansCacheDataArray
										.getJSONObject(i);
								
								if(!deleteId.equals(fansObject.getString("id")))
								{
									tempfansCacheDataArray.put(fansObject);
								}
							}
							
							fansCacheDataString = tempfansCacheDataArray.toString();
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				else
				{
					Log.v("Kite", "get followers fail");
					
					Toast toast = Toast.makeText(getApplicationContext(), "删除失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
			
		};
		
		handler.start();
	}
	
	Comparator<Map<String, Object>> comparator = new Comparator<Map<String, Object>>(){

//		RuleBasedCollator collator = (RuleBasedCollator) Collator.getInstance(Locale.CHINA);
		
		@Override
		public int compare(Map<String, Object> lhs, Map<String, Object> rhs) {
			// TODO Auto-generated method stub
			String lName = PinyinUtil.getPingYin((String)lhs.get("nickName"));
			String rName = PinyinUtil.getPingYin((String)rhs.get("nickName"));
			
			return lName.compareToIgnoreCase(rName);
		}
		
	};
}
