//package com.bebeanan.perfectbaby;
//
//import java.security.MessageDigest;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Random;
//
//import org.apache.http.message.BasicNameValuePair;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.inputmethod.EditorInfo;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.bebeanan.perfectbaby.common.Utils;
//import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout;
//import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout.IMEStateListener;
//
//public class RegisterAddActivity extends Activity{
//
//	private IMELinearLayout rootLayout;
//	
//	private TextView register_add_phone_text;
//	private EditText register_add_phone_edit;
//	private ImageView register_add_phone_valid;
//	private ImageButton register_add_phone_send;
//	
//	private TextView register_add_second_text;
//	
//	private TextView register_add_verify_text;
//	private EditText register_add_verify_edit;
//	private ImageView register_add_verify_valid;
//	
//	private TextView register_add_password_text;
//	private EditText register_add_password_edit;
//	private ImageView register_add_password_valid;
//	
//	private TextView register_add_password_repeat_text;
//	private EditText register_add_password_repeat_edit;
//	private ImageView register_add_password_repeat_valid;
//	
//	private ImageButton register_add_button;
//	
//	private AlertDialog.Builder builder;
//    private boolean showBuilder = false;
//    private AlertDialog dialog;
//    private Toast toast;
//    
//    private boolean phoneAvailable;
//    
//    private String code;
//    
//    private final int SEND_SECOND = 60;
//    private int second;
//    
//    private Handler secondHandler = new Handler();  
//    private Runnable secondRunnable = new Runnable() {  
//        @Override  
//        public void run() {  
//        	
//        	if(second == 0)
//        	{
//        		if(isPhone(register_add_phone_edit.getText().toString()))
//        		{
//        			register_add_second_text.setText("");  
//            		
//            		register_add_phone_send.setClickable(true);
//        			register_add_phone_send.setImageResource(R.drawable.bind_send_selector);
//        		}
//        	}
//        	else
//        	{
//        		second--;  
//                register_add_second_text.setText("还有" + second + "秒重新发送");  
//                secondHandler.postDelayed(this, 1000); 
//        	}
//        }  
//    };  
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_register_add);
//		
//		builder = new AlertDialog.Builder(RegisterAddActivity.this);
//        builder.setCancelable(false);
//        
//        toast = Toast.makeText(getApplicationContext(), "",
//                Toast.LENGTH_SHORT);
//		
//		TextView titleView = (TextView) findViewById(R.id.title_text);
//        titleView.setText("用户注册");
//        titleView.setTypeface(Utils.typeface);
//
//        ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
//        backButton.setImageResource(R.drawable.title_back_pic);
//        backButton.setVisibility(View.VISIBLE);
//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
//            }
//        });
//        
//        ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
//        rightButton.setVisibility(View.GONE);
//        
//        rootLayout = (IMELinearLayout)findViewById(R.id.register_root_layout);
//		
//		register_add_phone_text = (TextView) findViewById(R.id.register_add_phone_text);
//		register_add_phone_text.setTypeface(Utils.typeface);
//		register_add_phone_edit = (EditText) findViewById(R.id.register_add_phone_edit);
//		register_add_phone_edit.setTypeface(Utils.typeface);
//		register_add_phone_valid = (ImageView) findViewById(R.id.register_add_phone_valid);
//		
//		register_add_phone_send = (ImageButton) findViewById(R.id.register_add_phone_send);
//		
//		register_add_second_text = (TextView) findViewById(R.id.register_add_second_text);
//		register_add_second_text.setTypeface(Utils.typeface);
//		register_add_second_text.setText("");
//		
//		register_add_verify_text = (TextView) findViewById(R.id.register_add_verify_text);
//		register_add_verify_text.setTypeface(Utils.typeface);
//		register_add_verify_edit = (EditText) findViewById(R.id.register_add_verify_edit);
//		register_add_verify_edit.setTypeface(Utils.typeface);
//		register_add_verify_valid = (ImageView) findViewById(R.id.register_add_verify_valid);
//		
//		register_add_password_text = (TextView) findViewById(R.id.register_add_password_text);
//		register_add_password_text.setTypeface(Utils.typeface);
//		register_add_password_edit = (EditText) findViewById(R.id.register_add_password_edit);
//		register_add_password_edit.setTypeface(Utils.typeface);
//		register_add_password_valid = (ImageView) findViewById(R.id.register_add_password_valid);
//		
//		register_add_password_repeat_text = (TextView) findViewById(R.id.register_add_password_repeat_text);
//		register_add_password_repeat_text.setTypeface(Utils.typeface);
//		register_add_password_repeat_edit = (EditText) findViewById(R.id.register_add_password_repeat_edit);
//		register_add_password_repeat_edit.setTypeface(Utils.typeface);
//		register_add_password_repeat_valid = (ImageView) findViewById(R.id.register_add_password_repeat_valid);
//		
//		register_add_button = (ImageButton) findViewById(R.id.register_add_button);
//		register_add_button.setClickable(false);
//		
//		initView();
//	}
//
//	private void initView()
//	{
//		rootLayout.setIMEChangeListener(new IMEStateListener(){
//
//    		@Override
//    		public void onChange() {
//    			// TODO Auto-generated method stub
//    			CheckPass();
//    		}
//            	
//        });
//		
//		register_add_phone_edit.addTextChangedListener(new TextWatcher(){
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				
//				if(s.length() == 0)
//				{
//					register_add_phone_valid.setVisibility(View.INVISIBLE);
//				}
//				
//				if(s.length() == 11)
//				{
//					register_add_phone_send.setClickable(true);
//					register_add_phone_send.setImageResource(R.drawable.bind_send_selector);
//				}
//				else
//				{
//					register_add_phone_send.setClickable(false);
//					register_add_phone_send.setImageResource(R.drawable.bind_send_disable);
//				}
//				
//				register_add_verify_edit.setText("");
//			}
//			
//		});
//		
//		register_add_phone_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                	String currentPhone = register_add_phone_edit.getText().toString();
//                	if(isPhone(currentPhone))
//                	{
//                		isPhoneAvailable(currentPhone);
//                	}
//                	else
//                	{
//                		phoneAvailable = false;
//                	}
//                }
//            }
//        });
//
//		register_add_phone_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId,
//                                          KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEND
//                        || actionId == EditorInfo.IME_ACTION_DONE
//                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//                	String currentPhone = register_add_phone_edit.getText().toString();
//                	if(isPhone(currentPhone))
//                	{
//                		isPhoneAvailable(currentPhone);
//                	}
//                	else
//                	{
//                		phoneAvailable = false;
//                	}
//                    return true;
//                }
//                return false;
//            }
//        });
//		
//		register_add_verify_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                	
//                	CheckPass();
//                }
//            }
//        });
//		
////		register_add_verify_edit
////				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
////					public boolean onEditorAction(TextView v, int actionId,
////							KeyEvent event) {
////						if (actionId == EditorInfo.IME_ACTION_SEND
////								|| actionId == EditorInfo.IME_ACTION_DONE
////								|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
////		                	
////		                	CheckPass();
////		                	
////							return true;
////						}
////						return false;
////					}
////				});
//		
//		register_add_password_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                	
//                	CheckPass();
//                }
//            }
//        });
//		
////		register_add_password_edit
////				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
////					public boolean onEditorAction(TextView v, int actionId,
////							KeyEvent event) {
////						if (actionId == EditorInfo.IME_ACTION_SEND
////								|| actionId == EditorInfo.IME_ACTION_DONE
////								|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
////		                	
////		                	CheckPass();
////		                	
////							return true;
////						}
////						return false;
////					}
////				});
//		
//		register_add_password_repeat_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (!b) {
//                	
//                	CheckPass();
//                }
//            }
//        });
//		
////		register_add_password_repeat_edit
////				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
////					public boolean onEditorAction(TextView v, int actionId,
////							KeyEvent event) {
////						if (actionId == EditorInfo.IME_ACTION_SEND
////								|| actionId == EditorInfo.IME_ACTION_DONE
////								|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
////		                	
////		                	CheckPass();
////		                	
////							return true;
////						}
////						return false;
////					}
////				});
//		
//		register_add_phone_send.setClickable(false);
//		register_add_phone_send.setImageResource(R.drawable.bind_send_disable);
//		register_add_phone_send.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phone = register_add_phone_edit.getEditableText().toString();
//                if (phone.isEmpty()) {
//                    Toast toast = Toast.makeText(getApplicationContext(), "手机号为空",
//                            Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                } else {
//
//                    builder.setMessage("发送中");
//                    dialog = builder.show();
//                    showBuilder = true;
//
//                    JSONObject data = new JSONObject();
//                    try {
//                        data.put("phone", phone);
//                        Random randGen = new Random();
//                        char[]numbersAndLetters = ("0123456789").toCharArray();
//                        char [] randBuffer = new char[5];
//                        for (int i=0; i<randBuffer.length; i++) {
//                            randBuffer[i] = numbersAndLetters[randGen.nextInt(9)];
//                        }
//                        code = new String(randBuffer);
//                        data.put("code", code);
//                        Log.v("DBG", "code:" + code);
//                        data.put("check", MD5(phone+code+".bebeanan"));
//                        
//                        Log.v("Kite", "data is " + data.toString());
//
//                        List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//
//                        NetHandler handler = new NetHandler(RegisterAddActivity.this, NetHandler.METHOD_POST,
//                                "/phoneverify/", param, data) {
//                            @Override
//                            public void handleRsp(Message msg) {
//                                dialog.dismiss();
//                                showBuilder = false;
//                                Bundle bundle = msg.getData();
//                                int code = bundle.getInt("code");
//                                if (code == 200) {
//                                    Toast toast = Toast.makeText(getApplicationContext(), "发送成功",
//                                            Toast.LENGTH_SHORT);
//                                    toast.setGravity(Gravity.CENTER, 0, 0);
//                                    toast.show();
//                                    
//                                    register_add_phone_send.setClickable(false);
//                        			register_add_phone_send.setImageResource(R.drawable.bind_send_disable);
//                                    
//                                    second = SEND_SECOND;
//                                    secondHandler.postDelayed(secondRunnable, 1000);
//                                    
//                                    return;
//                                } else if (code == -100) {
//                                	Toast toast = Toast.makeText(RegisterAddActivity.this, "网络不给力哟~", Toast.LENGTH_SHORT);
//                                    toast.setGravity(Gravity.CENTER, 0, 0);
//                                    toast.show();
//                                    return;
//                                } else {
//                                	Toast toast = Toast.makeText(getApplicationContext(), "其他异常",
//                                            Toast.LENGTH_SHORT);
//                                    toast.setGravity(Gravity.CENTER, 0, 0);
//                                    toast.show();
//                                    
//                                    Log.v("Kite", "phoneBind fail because " + bundle.getString("data"));
//                                    
//                                    return;
//                                }
//                            }
//                        };
//                        handler.start();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//		
//		register_add_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phoneNumber = register_add_phone_edit.getText().toString();
//                String password = register_add_password_edit.getText().toString();
//
//                JSONObject data = new JSONObject();
//                try {
//                    data.put("phone", phoneNumber);
//                    data.put("password", password);
//                    Log.v("DBG", "register data:" + data.toString());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                builder.setMessage("创建中");
//                dialog = builder.show();
//                showBuilder = true;
//
//                List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//
//                NetHandler handler = new NetHandler(RegisterAddActivity.this, NetHandler.METHOD_POST,
//                        "/users", param, data) {
//                    @Override
//                    public void handleRsp(Message msg) {
//                        Bundle bundle = msg.getData();
//                        int code = bundle.getInt("code");
//                        if (code == 200) {
//                            toast = Toast.makeText(getApplicationContext(), "创建成功",
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//
//                            JSONObject data = new JSONObject();
//                            try {
//                                data.put("username", register_add_phone_edit.getText().toString());
//                                data.put("password", register_add_password_edit.getText().toString());
//                                Log.v("DBG", "login data:" + data.toString());
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//                            NetHandler handler = new NetHandler(RegisterAddActivity.this, NetHandler.METHOD_POST,
//                                    "/users/login", param, data) {
//                                @Override
//                                public void handleRsp(Message msg) {
//                                    dialog.dismiss();
//                                    showBuilder = false;
//                                    Bundle bundle = msg.getData();
//                                    int code = bundle.getInt("code");
//                                    Log.v("DBG", "code:" + code);
//                                    Log.v("DBG", bundle.getString("data"));
//                                    if (code == 200) {
//                                        MemoryHandler.getInstance().setKey("users", bundle.getString("data"));
//                                        new Thread() {
//                                            @Override
//                                            public void run() {
//                                                try {
//                                                    Thread.sleep(1000);
//                                                } catch (InterruptedException e) {
//                                                    e.printStackTrace();
//                                                }
//                                                Intent intent = new Intent();
//                                                intent.setClass(RegisterAddActivity.this, RegisterFullActivity.class);
//                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                startActivity(intent);
//                                                overridePendingTransition(R.anim.activity_close_in_anim,
//                                                        R.anim.activity_close_out_anim);
//                                            }
//                                        }.start();
//                                        return;
//                                    } else if (code == -100) {
//                                        toast.cancel();
//                                        toast = Toast.makeText(getApplicationContext(), "系统异常",
//                                                Toast.LENGTH_SHORT);
//                                        toast.setGravity(Gravity.CENTER, 0, 0);
//                                        toast.show();
//                                        return;
//                                    } else if (code == 401) {
//                                        toast.cancel();
//                                        toast = Toast.makeText(getApplicationContext(), "登录失败",
//                                                Toast.LENGTH_SHORT);
//                                        toast.setGravity(Gravity.CENTER, 0, 0);
//                                        toast.show();
//                                        return;
//                                    } else {
//                                        toast.cancel();
//                                        toast = Toast.makeText(getApplicationContext(), "其他异常",
//                                                Toast.LENGTH_SHORT);
//                                        toast.setGravity(Gravity.CENTER, 0, 0);
//                                        toast.show();
//                                        return;
//                                    }
//                                }
//                            };
//                            handler.start();
//                            return;
//                        } 
////                        else if (code == 400) {
////                        	dialog.dismiss();
////                            showBuilder = false;
////                            toast.cancel();
////                            Log.v("Kite", "register error: " + bundle.getString("data"));
////                            try {
////								JSONObject registerError = new JSONObject(bundle.getString("data"));
////								JSONObject errors = registerError.getJSONObject("errors");
////								if(errors.has("username") && errors.getString("username").contains("already in use"))
////								{
////									toast = Toast.makeText(getApplicationContext(), "邮箱已占用",
////		                                    Toast.LENGTH_SHORT);
////									toast.setGravity(Gravity.CENTER, 0, 0);
////		                            toast.show();
////		                            return;
////								}
////							} catch (JSONException e) {
////								// TODO Auto-generated catch block
////								e.printStackTrace();
////							} 
////                            toast = Toast.makeText(getApplicationContext(), "用户异常",
////                                    Toast.LENGTH_SHORT);
////                            toast.setGravity(Gravity.CENTER, 0, 0);
////                            toast.show();
////                            return;
////                        } 
//                        else if (code == -100) {
//                            dialog.dismiss();
//                            showBuilder = false;
//                            toast.cancel();
//                            toast = Toast.makeText(getApplicationContext(), "系统异常",
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//                            return;
//                        } else {
//                            dialog.dismiss();
//                            showBuilder = false;
//                            toast.cancel();
//                            toast = Toast.makeText(getApplicationContext(), "其他异常",
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//                            return;
//                        }
//                    }
//                };
//                handler.start();
//            }
//        });
//	}
//	
//	private void CheckPass() {
//    	String phoneNumber = register_add_phone_edit.getText().toString();
//        String password = register_add_password_edit.getText().toString();
//        String verify = register_add_verify_edit.getText().toString();
//        String repeat = register_add_password_repeat_edit.getText().toString();
//        if (!verify.isEmpty() && !phoneNumber.isEmpty() && !password.isEmpty()
//                && !repeat.isEmpty()) {
//        	
//        	if(phoneAvailable && verify.equals(code) && checkPassword(password) && password.equals(repeat))
//        	{
//        		register_add_button.setImageResource(R.drawable.register_add_complete_enable);
//        		register_add_button.setClickable(true);
//        	}
//        	else {
//        		register_add_button.setImageResource(R.drawable.register_add_complete_disable);
//        		register_add_button.setClickable(false);
//            }
//        } else {
//        	register_add_button.setImageResource(R.drawable.register_add_complete_disable);
//        	register_add_button.setClickable(false);
//        }
//
//        /* 这里需要添加每一个edittext的判断，这里还需要判断软键盘消失的时候 */
//        
//        if(!phoneNumber.isEmpty() && isPhone(phoneNumber))
//    	{
//    		isPhoneAvailable(phoneNumber);
//    	}
//    	else
//    	{
//    		phoneAvailable = false;
//    	}
//        
//        if(!verify.isEmpty())
//    	{
//        	if(verify.equals(code))
//        	{
//        		register_add_verify_valid.setVisibility(View.VISIBLE);
//        		register_add_verify_valid.setImageResource(R.drawable.register_email_valid);
//        	}
//        	else
//        	{
//        		register_add_verify_valid.setVisibility(View.VISIBLE);
//        		register_add_verify_valid.setImageResource(R.drawable.register_email_invalid);
//        	}
//    	}
//    	else
//    	{
//    		register_add_verify_valid.setVisibility(View.INVISIBLE);
//    	}
//        
//        if(!password.isEmpty())
//    	{
//        	if(checkPassword(password))
//        	{
//        		register_add_password_valid.setVisibility(View.VISIBLE);
//        		register_add_password_valid.setImageResource(R.drawable.register_email_valid);
//        	}
//        	else
//        	{
//        		register_add_password_valid.setVisibility(View.VISIBLE);
//        		register_add_password_valid.setImageResource(R.drawable.register_email_invalid);
//        	}
//    	}
//    	else
//    	{
//    		register_add_password_valid.setVisibility(View.INVISIBLE);
//    	}
//        
//        if(!repeat.isEmpty())
//    	{
//        	if(repeat.equals(register_add_password_edit.getText().toString()))
//        	{
//        		register_add_password_repeat_valid.setVisibility(View.VISIBLE);
//        		register_add_password_repeat_valid.setImageResource(R.drawable.register_email_valid);
//        	}
//        	else
//        	{
//        		register_add_password_repeat_valid.setVisibility(View.VISIBLE);
//        		register_add_password_repeat_valid.setImageResource(R.drawable.register_email_invalid);
//        	}
//    	}
//    	else
//    	{
//    		register_add_password_repeat_valid.setVisibility(View.INVISIBLE);
//    	}
//    }
//	
//	private boolean isPhone(String input)
//    {
//    	if(!input.matches("1[3|5|7|8|][0-9]{9}")){
//
//    		   return false;
//
//    		}else{
//
//    		    return true;
//
//    		}
//    }
//	
//	private void isPhoneAvailable(String input)
//    {
//		JSONObject data = new JSONObject();
//        try {
//            data.put("phone", input);
//            
//            phoneAvailable = false;
//        	String uri = "/functions/phoneAvailable";
//        	NetHandler handler = new NetHandler(RegisterAddActivity.this, NetHandler.METHOD_POST, uri, new LinkedList<BasicNameValuePair>(), data)
//        	{
//
//    			@Override
//    			public void handleRsp(Message msg) {
//    				// TODO Auto-generated method stub
//    				Bundle bundle = msg.getData();
//    				int code = bundle.getInt("code");
//    				
//    				if(code == 200)
//    				{
//    					String data = bundle.getString("data");
//    					try {
//    						JSONObject object = new JSONObject(data);
//    						boolean available = object.getBoolean("available");
//    						phoneAvailable = available;
//    						
//    						if(phoneAvailable)
//    						{
//    							register_add_phone_valid.setVisibility(View.VISIBLE);
//    							register_add_phone_valid.setImageResource(R.drawable.register_email_valid);
//    						}
//    						else
//    						{
//    							register_add_phone_valid.setVisibility(View.VISIBLE);
//    							register_add_phone_valid.setImageResource(R.drawable.register_email_invalid);
//    						}
//    					} catch (JSONException e) {
//    						// TODO Auto-generated catch block
//    						e.printStackTrace();
//    					}
//    				}
//    				else
//    				{
//    					phoneAvailable = false;
//    					register_add_phone_valid.setVisibility(View.GONE);
//    				}
////    				CheckPass();
//    			}
//        		
//        	};
//        	handler.start();
//        	
//            }catch (Exception e) {
//                e.printStackTrace();
//            }
//    }
//	
//	private boolean checkPassword(String password) {
//        for(int i = 0; i != password.length(); i++) {
//            if (!((password.charAt(i) >= 'A' && password.charAt(i) <= 'Z') ||
//                    (password.charAt(i) >= 'a' && password.charAt(i) <= 'z') ||
//                    (password.charAt(i) >= '0' && password.charAt(i) <= '9') ||
//                    (password.charAt(i) == '_'))) {
//                return false;
//            }
//        }
//        return true;
//    }
//	
//	private String MD5(String s) {
//		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
//				'a', 'b', 'c', 'd', 'e', 'f' };
////		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
////				'A', 'B', 'C', 'D', 'E', 'F' };
//		try {
//			byte[] strTemp = s.getBytes();
//			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
//			mdTemp.update(strTemp);
//			byte[] md = mdTemp.digest();
//			int j = md.length;
//			char str[] = new char[j * 2];
//			int k = 0;
//			for (int i = 0; i < j; i++) {
//				byte byte0 = md[i];
//				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
//				str[k++] = hexDigits[byte0 & 0xf];
//			}
//			return new String(str);
//		} catch (Exception e) {
//			return null;
//		}
//	}
//	
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//        switch (keyCode) {
//            case KeyEvent.KEYCODE_BACK:
//            	
//            	if(!showBuilder)
//            	{
//            		this.finish();
//                    overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
//                    return true;
//            	}
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//}

package com.bebeanan.perfectbaby;
/**
 * Created by KiteXu.
 */
import java.security.MessageDigest;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout;
import com.bebeanan.perfectbaby.zxing.view.IMELinearLayout.IMEStateListener;
import com.umeng.analytics.MobclickAgent;

public class RegisterAddActivity extends Activity {

	private IMELinearLayout rootLayout;

	private TextView register_add_phone_text;
	private EditText register_add_phone_edit;
	private ImageView register_add_phone_valid;
	private ImageButton register_add_phone_send;

	private TextView register_add_second_text;

	private TextView register_add_verify_text;
	private EditText register_add_verify_edit;
	private ImageView register_add_verify_valid;

	private TextView register_add_password_text;
	private EditText register_add_password_edit;
	private ImageView register_add_password_valid;

//	private TextView register_add_password_repeat_text;
//	private EditText register_add_password_repeat_edit;
//	private ImageView register_add_password_repeat_valid;

	private ImageButton register_add_button;

	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;
	private Toast toast;

	private String phoneGetVerifyCode;
	private boolean phoneChanged = false;
	private boolean phoneAvailable;

	private String code;

	private final int SEND_SECOND = 60;
	private int second;

	private Handler secondHandler = new Handler();
	private Runnable secondRunnable = new Runnable() {
		@Override
		public void run() {

			if (second == 0) {
				if (isPhone(register_add_phone_edit.getText().toString())) {
					register_add_second_text.setText("");

					register_add_phone_send.setClickable(true);
					register_add_phone_send
							.setImageResource(R.drawable.bind_send_selector);
				}
			} else {
				second--;
				register_add_second_text.setText("还有" + second + "秒重新发送");
				secondHandler.postDelayed(this, 1000);
			}
		}
	};

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_add);

		builder = new AlertDialog.Builder(RegisterAddActivity.this);
		builder.setCancelable(false);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

		TextView titleView = (TextView) findViewById(R.id.title_text);
		titleView.setText("用户注册");
		titleView.setTypeface(Utils.typeface);

		ImageView backButton = (ImageView) findViewById(R.id.title_left_button);
		backButton.setImageResource(R.drawable.title_back_pic);
		backButton.setVisibility(View.VISIBLE);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}
		});

		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);

		rootLayout = (IMELinearLayout) findViewById(R.id.register_root_layout);

		register_add_phone_text = (TextView) findViewById(R.id.register_add_phone_text);
		register_add_phone_text.setTypeface(Utils.typeface);
		register_add_phone_edit = (EditText) findViewById(R.id.register_add_phone_edit);
//		register_add_phone_edit.setTypeface(Utils.typeface);
		register_add_phone_valid = (ImageView) findViewById(R.id.register_add_phone_valid);

		register_add_phone_send = (ImageButton) findViewById(R.id.register_add_phone_send);

		register_add_second_text = (TextView) findViewById(R.id.register_add_second_text);
		register_add_second_text.setTypeface(Utils.typeface);
		register_add_second_text.setText("");

		register_add_verify_text = (TextView) findViewById(R.id.register_add_verify_text);
		register_add_verify_text.setTypeface(Utils.typeface);
		register_add_verify_edit = (EditText) findViewById(R.id.register_add_verify_edit);
//		register_add_verify_edit.setTypeface(Utils.typeface);
		register_add_verify_valid = (ImageView) findViewById(R.id.register_add_verify_valid);

		register_add_password_text = (TextView) findViewById(R.id.register_add_password_text);
		register_add_password_text.setTypeface(Utils.typeface);
		register_add_password_edit = (EditText) findViewById(R.id.register_add_password_edit);
//		register_add_password_edit.setTypeface(Utils.typeface);
		register_add_password_valid = (ImageView) findViewById(R.id.register_add_password_valid);

//		register_add_password_repeat_text = (TextView) findViewById(R.id.register_add_password_repeat_text);
//		register_add_password_repeat_text.setTypeface(Utils.typeface);
//		register_add_password_repeat_edit = (EditText) findViewById(R.id.register_add_password_repeat_edit);
//		register_add_password_repeat_edit.setTypeface(Utils.typeface);
//		register_add_password_repeat_valid = (ImageView) findViewById(R.id.register_add_password_repeat_valid);

		register_add_button = (ImageButton) findViewById(R.id.register_add_button);
		register_add_button.setClickable(false);

		initView();
	}

	private void initView() {
		rootLayout.setIMEChangeListener(new IMEStateListener() {

			@Override
			public void onChange() {
				// TODO Auto-generated method stub
				CheckPass();
			}

		});

		register_add_phone_edit.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				phoneChanged = true;

				if (s.length() == 0) {
					register_add_phone_valid.setVisibility(View.INVISIBLE);
				}

				if (s.length() == 11 && phoneAvailable) {
					register_add_phone_send.setClickable(true);
					register_add_phone_send
							.setImageResource(R.drawable.bind_send_selector);
				} else {
					register_add_phone_send.setClickable(false);
					register_add_phone_send
							.setImageResource(R.drawable.bind_send_disable);
				}

				register_add_verify_edit.setText("");
			}

		});

		register_add_phone_edit
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View view, boolean b) {
						if (!b) {
							String currentPhone = register_add_phone_edit
									.getText().toString();
							if (phoneChanged && isPhone(currentPhone)) {
								isPhoneAvailable(currentPhone);
							} else if (currentPhone.length() != 0
									&& !isPhone(currentPhone)) {
								phoneAvailable = false;

								register_add_phone_valid
										.setVisibility(View.VISIBLE);
								register_add_phone_valid
										.setImageResource(R.drawable.register_email_invalid);
							}
						}
					}
				});

		register_add_phone_edit
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEND
								|| actionId == EditorInfo.IME_ACTION_DONE
								|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
							String currentPhone = register_add_phone_edit
									.getText().toString();
							if (phoneChanged && isPhone(currentPhone)) {
								isPhoneAvailable(currentPhone);
							} else if (currentPhone.length() != 0
									&& !isPhone(currentPhone)) {
								phoneAvailable = false;

								register_add_phone_valid
										.setVisibility(View.VISIBLE);
								register_add_phone_valid
										.setImageResource(R.drawable.register_email_invalid);
							}
							return true;
						}
						return false;
					}
				});

		register_add_verify_edit
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View view, boolean b) {
						if (!b) {

							CheckPass();
						}
					}
				});

		// register_add_verify_edit
		// .setOnEditorActionListener(new TextView.OnEditorActionListener() {
		// public boolean onEditorAction(TextView v, int actionId,
		// KeyEvent event) {
		// if (actionId == EditorInfo.IME_ACTION_SEND
		// || actionId == EditorInfo.IME_ACTION_DONE
		// || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
		//
		// CheckPass();
		//
		// return true;
		// }
		// return false;
		// }
		// });

		register_add_password_edit
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					@Override
					public void onFocusChange(View view, boolean b) {
						if (!b) {

							CheckPass();
						}
					}
				});

		// register_add_password_edit
		// .setOnEditorActionListener(new TextView.OnEditorActionListener() {
		// public boolean onEditorAction(TextView v, int actionId,
		// KeyEvent event) {
		// if (actionId == EditorInfo.IME_ACTION_SEND
		// || actionId == EditorInfo.IME_ACTION_DONE
		// || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
		//
		// CheckPass();
		//
		// return true;
		// }
		// return false;
		// }
		// });

//		register_add_password_repeat_edit
//				.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//					@Override
//					public void onFocusChange(View view, boolean b) {
//						if (!b) {
//
//							CheckPass();
//						}
//					}
//				});

		// register_add_password_repeat_edit
		// .setOnEditorActionListener(new TextView.OnEditorActionListener() {
		// public boolean onEditorAction(TextView v, int actionId,
		// KeyEvent event) {
		// if (actionId == EditorInfo.IME_ACTION_SEND
		// || actionId == EditorInfo.IME_ACTION_DONE
		// || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
		//
		// CheckPass();
		//
		// return true;
		// }
		// return false;
		// }
		// });

		register_add_phone_send.setClickable(false);
		register_add_phone_send.setImageResource(R.drawable.bind_send_disable);
		register_add_phone_send.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final String phone = register_add_phone_edit.getEditableText()
						.toString();
				if (phone.isEmpty()) {
					Toast toast = Toast.makeText(getApplicationContext(),
							"手机号为空", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else {

					builder.setMessage("发送中");
					dialog = builder.show();
					showBuilder = true;

					JSONObject data = new JSONObject();
					try {
						data.put("phone", phone);
						Random randGen = new Random();
						char[] numbersAndLetters = ("0123456789").toCharArray();
						char[] randBuffer = new char[5];
						for (int i = 0; i < randBuffer.length; i++) {
							randBuffer[i] = numbersAndLetters[randGen
									.nextInt(9)];
						}
						code = new String(randBuffer);
						data.put("code", code);
						Log.v("DBG", "code:" + code);
						data.put("check", MD5(phone + code + ".bebeanan"));

						Log.v("Kite", "data is " + data.toString());

						List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

						NetHandler handler = new NetHandler(
								RegisterAddActivity.this,
								NetHandler.METHOD_POST, "/phoneverify/", param,
								data) {
							@Override
							public void handleRsp(Message msg) {
								dialog.dismiss();
								showBuilder = false;
								Bundle bundle = msg.getData();
								int code = bundle.getInt("code");
								if (code == 200) {

									phoneGetVerifyCode = phone;

									Toast toast = Toast.makeText(
											getApplicationContext(), "发送成功",
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();

									register_add_phone_send.setClickable(false);
									register_add_phone_send
											.setImageResource(R.drawable.bind_send_disable);

									second = SEND_SECOND;
									secondHandler.postDelayed(secondRunnable,
											1000);

									return;
								} else if (code == -100) {
									Toast toast = Toast.makeText(
											RegisterAddActivity.this,
											"网络不给力哟~", Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									return;
								} else {
									Toast toast = Toast.makeText(
											getApplicationContext(), "其他异常",
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();

									Log.v("Kite", "phoneBind fail because "
											+ bundle.getString("data"));

									return;
								}
							}
						};
						handler.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		register_add_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String phoneNumber = register_add_phone_edit.getText()
						.toString();
				String password = register_add_password_edit.getText()
						.toString();

				JSONObject data = new JSONObject();
				try {
					data.put("phone", phoneNumber);
					data.put("password", password);
					Log.v("DBG", "register data:" + data.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}

				builder.setMessage("创建中");
				dialog = builder.show();
				showBuilder = true;

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();

				NetHandler handler = new NetHandler(RegisterAddActivity.this,
						NetHandler.METHOD_POST, "/users", param, data) {
					@Override
					public void handleRsp(Message msg) {
						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						if (code == 200) {
							toast = Toast.makeText(getApplicationContext(),
									"创建成功", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

							JSONObject data = new JSONObject();
							try {
								data.put("username", register_add_phone_edit
										.getText().toString());
								data.put("password", register_add_password_edit
										.getText().toString());
								Log.v("DBG", "login data:" + data.toString());
							} catch (Exception e) {
								e.printStackTrace();
							}

							List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
							NetHandler handler = new NetHandler(
									RegisterAddActivity.this,
									NetHandler.METHOD_POST, "/users/login",
									param, data) {
								@Override
								public void handleRsp(Message msg) {
									dialog.dismiss();
									showBuilder = false;
									Bundle bundle = msg.getData();
									int code = bundle.getInt("code");
									Log.v("DBG", "code:" + code);
									Log.v("DBG", bundle.getString("data"));
									if (code == 200) {
										MemoryHandler.getInstance().setKey(
												"users",
												bundle.getString("data"));

										try {
											JSONObject object = new JSONObject(
													bundle.getString("data"));

											String userId = object
													.getString("uid");
											JPushInterface.setAlias(
													RegisterAddActivity.this,
													userId,
													new TagAliasCallback() {

														@Override
														public void gotResult(
																int arg0,
																String arg1,
																Set<String> arg2) {
															// TODO
															// Auto-generated
															// method stub
															Log.v("Kite",
																	"JPUSH "
																			+ arg0
																			+ " "
																			+ arg1
																			+ " "
																			+ arg2);
														}

													});
										} catch (JSONException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}

										new Thread() {
											@Override
											public void run() {
												try {
													Thread.sleep(1000);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}
												Intent intent = new Intent();
												intent.setClass(
														RegisterAddActivity.this,
														RegisterFullActivity.class);
												intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
														| Intent.FLAG_ACTIVITY_CLEAR_TASK);
												startActivity(intent);
												overridePendingTransition(
														R.anim.activity_close_in_anim,
														R.anim.activity_close_out_anim);
											}
										}.start();
										return;
									} else if (code == -100) {
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"系统异常", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
										return;
									} else if (code == 401) {
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"登录失败", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
										return;
									} else {
										toast.cancel();
										toast = Toast.makeText(
												getApplicationContext(),
												"其他异常", Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
										return;
									}
								}
							};
							handler.start();
							return;
						}
						// else if (code == 400) {
						// dialog.dismiss();
						// showBuilder = false;
						// toast.cancel();
						// Log.v("Kite", "register error: " +
						// bundle.getString("data"));
						// try {
						// JSONObject registerError = new
						// JSONObject(bundle.getString("data"));
						// JSONObject errors =
						// registerError.getJSONObject("errors");
						// if(errors.has("username") &&
						// errors.getString("username").contains("already in use"))
						// {
						// toast = Toast.makeText(getApplicationContext(),
						// "邮箱已占用",
						// Toast.LENGTH_SHORT);
						// toast.setGravity(Gravity.CENTER, 0, 0);
						// toast.show();
						// return;
						// }
						// } catch (JSONException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						// toast = Toast.makeText(getApplicationContext(),
						// "用户异常",
						// Toast.LENGTH_SHORT);
						// toast.setGravity(Gravity.CENTER, 0, 0);
						// toast.show();
						// return;
						// }
						else if (code == -100) {
							dialog.dismiss();
							showBuilder = false;
							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"系统异常", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;
						} else {
							dialog.dismiss();
							showBuilder = false;
							toast.cancel();
							toast = Toast.makeText(getApplicationContext(),
									"其他异常", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							return;
						}
					}
				};
				handler.start();
			}
		});
	}

	private void CheckPass() {
		String phoneNumber = register_add_phone_edit.getText().toString();
		String password = register_add_password_edit.getText().toString();
		String verify = register_add_verify_edit.getText().toString();
//		String repeat = register_add_password_repeat_edit.getText().toString();
		if (!verify.isEmpty() && !phoneNumber.isEmpty() && !password.isEmpty()) {

			if (phoneAvailable && phoneNumber.equals(phoneGetVerifyCode)
					&& verify.equals(code) && checkPassword(password)) {
				register_add_button
						.setImageResource(R.drawable.register_add_complete_enable);
				register_add_button.setClickable(true);
			} else {
				register_add_button
						.setImageResource(R.drawable.register_add_complete_disable);
				register_add_button.setClickable(false);
			}
		} else {
			register_add_button
					.setImageResource(R.drawable.register_add_complete_disable);
			register_add_button.setClickable(false);
		}

		/* 这里需要添加每一个edittext的判断，这里还需要判断软键盘消失的时候 */

		if (phoneChanged && !phoneNumber.isEmpty() && isPhone(phoneNumber)) {
			isPhoneAvailable(phoneNumber);
		} else if (phoneNumber.isEmpty() || !isPhone(phoneNumber)) {
			phoneAvailable = false;
		}

		if (!verify.isEmpty()) {
			if (phoneNumber.equals(phoneGetVerifyCode) && verify.equals(code)) {
				register_add_verify_valid.setVisibility(View.VISIBLE);
				register_add_verify_valid
						.setImageResource(R.drawable.register_email_valid);
			} else {
				register_add_verify_valid.setVisibility(View.VISIBLE);
				register_add_verify_valid
						.setImageResource(R.drawable.register_email_invalid);
			}
		} else {
			register_add_verify_valid.setVisibility(View.INVISIBLE);
		}

		if (!password.isEmpty()) {
			if (checkPassword(password)) {
				register_add_password_valid.setVisibility(View.VISIBLE);
				register_add_password_valid
						.setImageResource(R.drawable.register_email_valid);
			} else {
				register_add_password_valid.setVisibility(View.VISIBLE);
				register_add_password_valid
						.setImageResource(R.drawable.register_email_invalid);
			}
		} else {
			register_add_password_valid.setVisibility(View.INVISIBLE);
		}

//		if (!repeat.isEmpty()) {
//			if (repeat.equals(register_add_password_edit.getText().toString())) {
//				register_add_password_repeat_valid.setVisibility(View.VISIBLE);
//				register_add_password_repeat_valid
//						.setImageResource(R.drawable.register_email_valid);
//			} else {
//				register_add_password_repeat_valid.setVisibility(View.VISIBLE);
//				register_add_password_repeat_valid
//						.setImageResource(R.drawable.register_email_invalid);
//			}
//		} else {
//			register_add_password_repeat_valid.setVisibility(View.INVISIBLE);
//		}
	}

	private boolean isPhone(String input) {
		if (!input.matches("1[3|5|7|8|][0-9]{9}")) {

			return false;

		} else {

			return true;

		}
	}

	private void isPhoneAvailable(String input) {
		JSONObject data = new JSONObject();
		try {
			data.put("phone", input);

			phoneAvailable = false;
			String uri = "/functions/phoneAvailable";
			NetHandler handler = new NetHandler(RegisterAddActivity.this,
					NetHandler.METHOD_POST, uri,
					new LinkedList<BasicNameValuePair>(), data) {

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					Bundle bundle = msg.getData();
					int code = bundle.getInt("code");

					Log.v("Kite",
							"available data is " + bundle.getString("data"));

					if (code == 200) {
						phoneChanged = false;
						String data = bundle.getString("data");
						try {
							JSONObject object = new JSONObject(data);
							boolean available = object.getBoolean("available");
							phoneAvailable = available;

							if (phoneAvailable) {
								register_add_phone_valid
										.setVisibility(View.VISIBLE);
								register_add_phone_valid
										.setImageResource(R.drawable.register_email_valid);
								
								register_add_phone_send.setClickable(true);
								register_add_phone_send
										.setImageResource(R.drawable.bind_send_selector);
							} else {
								register_add_phone_valid
										.setVisibility(View.VISIBLE);
								register_add_phone_valid
										.setImageResource(R.drawable.register_email_invalid);
								
								register_add_phone_send.setClickable(false);
								register_add_phone_send
										.setImageResource(R.drawable.bind_send_disable);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						phoneAvailable = false;
						register_add_phone_valid.setVisibility(View.GONE);
					}
					// CheckPass();
				}

			};
			handler.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPassword(String password) {
		for (int i = 0; i != password.length(); i++) {
			if (!((password.charAt(i) >= 'A' && password.charAt(i) <= 'Z')
					|| (password.charAt(i) >= 'a' && password.charAt(i) <= 'z')
					|| (password.charAt(i) >= '0' && password.charAt(i) <= '9') || (password
						.charAt(i) == '_'))) {
				return false;
			}
		}
		return true;
	}

	private String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		// char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
		// '9',
		// 'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			byte[] strTemp = s.getBytes();
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:

			if (!showBuilder) {
				this.finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
