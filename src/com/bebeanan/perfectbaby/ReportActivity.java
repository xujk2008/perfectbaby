package com.bebeanan.perfectbaby;

import java.util.LinkedList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class ReportActivity extends Activity {

	private TextView report_text;
	
	private ImageView report_image_1, report_image_2, report_image_3,
			report_image_4, report_image_5, report_image_6, report_image_7,
			report_image_8;

	private String feedId;
	
	private SharedPreferences userPreferences;
	
	@Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		
		if(Utils.application == null)
		{
			Utils.init(ReportActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		report_text = (TextView) findViewById(R.id.report_text);
		report_text.setTypeface(Utils.typeface);
		
		report_image_1 = (ImageView) findViewById(R.id.report_image_1);
		report_image_2 = (ImageView) findViewById(R.id.report_image_2);
		report_image_3 = (ImageView) findViewById(R.id.report_image_3);
		report_image_4 = (ImageView) findViewById(R.id.report_image_4);
		report_image_5 = (ImageView) findViewById(R.id.report_image_5);
		report_image_6 = (ImageView) findViewById(R.id.report_image_6);
		report_image_7 = (ImageView) findViewById(R.id.report_image_7);
		report_image_8 = (ImageView) findViewById(R.id.report_image_8);
		
		feedId = ReportActivity.this.getIntent().getStringExtra("feedId");
		
		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ReportActivity.this.finish();
			}
			
		});
		
		TextView title_text = (TextView) findViewById(R.id.title_text);
		title_text.setTypeface(Utils.typeface);
		title_text.setText("举报");
		
		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);
		
		report_image_1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("欺诈");
			}
			
		});
		
		report_image_2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("色情");
			}
			
		});
		
		report_image_3.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("暴力");
			}
			
		});
		
		report_image_4.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("垃圾广告");
			}
			
		});
		
		report_image_5.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("人身攻击");
			}
			
		});
		
		report_image_6.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("泄露我的隐私");
			}
			
		});
		
		report_image_7.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("反动政治");
			}
			
		});
		
		report_image_8.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				report("其他");
			}
			
		});
	}

	private void report(String reportContent)
	{
		
		try {
			JSONObject reportObject = new JSONObject();
			
			JSONObject userObject = new JSONObject();
			userObject.put("id", userPreferences.getString("userId", ""));
			userObject.put("nickname",
					userPreferences.getString("nickName", ""));
			
			JSONObject object = new JSONObject();
			object.put("type", "feed");
			object.put("id", feedId);
			
			reportObject.put("user", userObject);
			reportObject.put("object", object);
			reportObject.put("description", reportContent);
			
			NetHandler reportHandler = new NetHandler(ReportActivity.this, NetHandler.METHOD_POST, "/reporting", new LinkedList<BasicNameValuePair>(), reportObject)
			{

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					Bundle bundle = msg.getData();
					int code = bundle.getInt("code");
					String data = bundle.getString("data");
					
					if(code == 200)
					{
						Log.v("Kite", "report success " + data);
						
						Toast toast = Toast.makeText(ReportActivity.this, "举报成功", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
					else
					{
						Log.v("Kite", "report fail " + data);
						
						Toast toast = Toast.makeText(ReportActivity.this, "举报失败", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				}
				
			};
			
			reportHandler.start();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
