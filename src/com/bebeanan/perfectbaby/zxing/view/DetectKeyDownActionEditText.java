package com.bebeanan.perfectbaby.zxing.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;
/**
 * Created by KiteXu.
 */
//This is an EditText which can detect the KEYCODE_BACK action when inputMethod is active
public class DetectKeyDownActionEditText extends EditText{

	private OnBackPressedListener onBackPressedListener;
	
	private boolean catchBack = false;
	
	public DetectKeyDownActionEditText(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public DetectKeyDownActionEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public DetectKeyDownActionEditText(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean dispatchKeyEventPreIme(KeyEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getKeyCode() == KeyEvent.KEYCODE_BACK)
		{
			if(onBackPressedListener != null && catchBack)
			{
				onBackPressedListener.onBackPressed();
				
				return true;
			}
		}
		
		return super.dispatchKeyEventPreIme(event);
	}

	public void setCatchBack(boolean catchBack)
	{
		this.catchBack = catchBack;
	}
	
	public void setOnBackPressedListener(OnBackPressedListener listener)
	{
		this.onBackPressedListener = listener;
	}
	
	public interface OnBackPressedListener
	{
		public void onBackPressed();
	};
}
