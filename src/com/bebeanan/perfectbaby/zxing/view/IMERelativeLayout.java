package com.bebeanan.perfectbaby.zxing.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
/**
 * Created by KiteXu.
 */
public class IMERelativeLayout extends RelativeLayout{

	private IMEStateListener listener;
	
	public interface IMEStateListener{
		void onChange();
	};
	
	public IMERelativeLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public IMERelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public void setIMEChangeListener(IMEStateListener listener)
	{
		this.listener = listener;
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// TODO Auto-generated method stub
		super.onLayout(changed, l, t, r, b);
		
		if(this.listener != null)
		{
			this.listener.onChange();
		}
	}
	
}
