package com.bebeanan.perfectbaby.zxing.view;

import java.util.HashMap;
import java.util.LinkedList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.tencent.qq.QQ;

import com.bebeanan.perfectbaby.MainActivity;
import com.bebeanan.perfectbaby.NetHandler;
import com.bebeanan.perfectbaby.R;
import com.bebeanan.perfectbaby.common.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * Created by KiteXu.
 */
public class InviteLayoutView extends RelativeLayout {

	private Context context;
	
	private RelativeLayout invite_mask_layout, invite_method_layout,
			invite_type_layout;
	private LinearLayout invite_wechat_layout, invite_mail_layout,
			invite_message_layout, invite_qq_layout;
	private TextView invite_method_cancel;
	private TextView invite_type_fans, invite_type_family, invite_type_cancel;
	
	private int inviteMethod, inviteType;
	
	private boolean invite_method_shown = false;
	private boolean invite_type_shown = false;
	
	private final static int INVITE_METHOD_HEIGHT_DP = 220,
			INVITE_TYPE_HEIGHT_DP = 230;
	private final static int INVITE_LAYOUT_OFFSET_DP = 20;

	private final static int INVITE_METHOD_WECHAT = 0, INVITE_METHOD_MAIL = 1,
			INVITE_METHOD_MESSAGE = 2, INVITE_METHOD_QQ = 3;
	public final static int INVITE_TYPE_FANS = 0, INVITE_TYPE_FRIENDS = 1,
			INVITE_TYPE_FAMILY = 2;
	
	private Handler inviteHandler;
	
	private OnLoadingListener onLoadingListener;
	
	public InviteLayoutView(Context context) {
		super(context);
		
		init(context);
	}

	public InviteLayoutView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init(context);
	}
	
	public InviteLayoutView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		
		init(context);
	}
	
	private void init(Context context)
 {
		this.context = context;

		LayoutInflater.from(context).inflate(R.layout.view_invite_layout, this);

		invite_mask_layout = (RelativeLayout) findViewById(R.id.invite_mask_layout);

		invite_method_layout = (RelativeLayout) findViewById(R.id.invite_method_layout);
		invite_wechat_layout = (LinearLayout) findViewById(R.id.invite_wechat_layout);
		invite_mail_layout = (LinearLayout) findViewById(R.id.invite_mail_layout);
		invite_message_layout = (LinearLayout) findViewById(R.id.invite_message_layout);
		invite_qq_layout = (LinearLayout) findViewById(R.id.invite_qq_layout);
		invite_method_cancel = (TextView) findViewById(R.id.invite_method_cancel);

		invite_type_layout = (RelativeLayout) findViewById(R.id.invite_type_layout);
		invite_type_fans = (TextView) findViewById(R.id.invite_type_fans);
		// invite_type_friends = (TextView)
		// findViewById(R.id.invite_type_friends);
		invite_type_family = (TextView) findViewById(R.id.invite_type_family);
		invite_type_cancel = (TextView) findViewById(R.id.invite_type_cancel);

		invite_mask_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (invite_method_shown) {
					hideInviteMethodLayout();

					return;
				}

				if (invite_type_shown) {
					hideInviteTypeLayout();

					return;
				}

			}

		});

		invite_wechat_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_WECHAT;
				hideInviteMethodLayout();

				invite();
			}

		});
		invite_mail_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_MAIL;
				hideInviteMethodLayout();

				invite();
			}

		});
		invite_message_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_MESSAGE;
				hideInviteMethodLayout();

				invite();
			}

		});
		invite_qq_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_QQ;
				hideInviteMethodLayout();

				invite();
			}

		});
		invite_method_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideInviteMethodLayout();
			}

		});

		invite_type_fans.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteType = INVITE_TYPE_FANS;
				showInviteMethodLayout();
			}

		});
		// invite_type_friends.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// inviteType = INVITE_TYPE_FRIENDS;
		// hideInviteTypeLayout();
		//
		// invite();
		// }
		//
		// });
		invite_type_family.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteType = INVITE_TYPE_FAMILY;
				showInviteMethodLayout();
			}

		});
		invite_type_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideInviteTypeLayout();
			}

		});
	}

	public void setInviteType(int inviteType)
	{
		this.inviteType = inviteType;
	}
	
	public void setHanlder(Handler inviteHandler)
	{
		this.inviteHandler = inviteHandler;
	}
	
	public boolean isMethodShown()
	{
		return invite_method_shown;
	}
	
	public boolean isTypeShown()
	{
		return invite_type_shown;
	}
	
	public void showInviteTypeLayout() {
		invite_type_shown = true;

		invite_mask_layout.setVisibility(View.VISIBLE);
		invite_type_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_type_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			invite_type_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_type_layout.startAnimation(animation);
	}
	
	public void hideInviteTypeLayout() {
		invite_type_shown = false;

		invite_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_type_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			invite_type_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_type_layout.startAnimation(animation);
	}
	
	private void hideInviteMethodLayout() {
		invite_method_shown = false;

		invite_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			invite_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_method_layout.startAnimation(animation);
	}

	public void showInviteMethodLayout() {
		
		hideInviteTypeLayout();
		
		invite_method_shown = true;

		invite_mask_layout.setVisibility(View.VISIBLE);
		invite_method_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			invite_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_method_layout.startAnimation(animation);
	}
	
	private void invite()
	{
		if(onLoadingListener != null)
		{
			onLoadingListener.onLoading();
		}
		
		JSONObject inviteObject = new JSONObject();
		try {
			inviteObject.put("userType", inviteType);
			NetHandler handler = new NetHandler(context, NetHandler.METHOD_POST, "/invitation/generate", new LinkedList<BasicNameValuePair>(), inviteObject){

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					
					if(onLoadingListener != null)
					{
						onLoadingListener.onLoadComplete();
					}
					
					Bundle bundle = msg.getData();
					
					int code = bundle.getInt("code");
					String data = bundle.getString("data");
					
					if(code == 200)
					{
						if(inviteHandler == null)
						{
							inviteHandler = new Handler() {
								@Override
								public void handleMessage(Message msg) {

									int id = msg.arg1;
									if (id == 1) {
//										Toast.makeText(MainActivity.this, "邀请成功",
//												Toast.LENGTH_LONG).show();
									} else if (id == 2) {
//										Toast.makeText(MainActivity.this, "邀请失败",
//												Toast.LENGTH_LONG).show();
									}
								}
							};
						}
						
						JSONObject resultObject;
						try {
							resultObject = new JSONObject(data);
							
							String inviteUrl = resultObject.getString("url");
							String inviteText = "我家宝宝在”完美宝贝“有窝啦，记录点滴成长，独家新生儿筛查系统！快来瞅瞅：" + inviteUrl + " 萌哒哒的手绘界面噢O(∩_∩)O~";
							
							switch(inviteMethod)
							{
							case INVITE_METHOD_WECHAT:
								
								Platform wechatPlat = ShareSDK.getPlatform("Wechat");
				                
								wechatPlat.setPlatformActionListener(new PlatformActionListener() {
									@Override
									public void onComplete(
											Platform platform,
											int i,
											HashMap<String, Object> stringObjectHashMap) {
										Message msg = new Message();
										msg.arg1 = 1;
										msg.arg2 = i;
										msg.obj = platform;
										if(inviteHandler != null)
										{
											inviteHandler
											.sendMessage(msg);
										}
										
										Log.v("DBG",
												"WECHAT SHARE DONE");
									}

									@Override
									public void onError(
											Platform platform,
											int i,
											Throwable throwable) {
										throwable
												.printStackTrace();
										Message msg = new Message();
										msg.arg1 = 2;
										msg.arg2 = i;
										msg.obj = platform;
										if(inviteHandler != null)
										{
											inviteHandler
											.sendMessage(msg);
										}
										Log.v("DBG",
												"WECHAT SHARE ERROR");
									}

									@Override
									public void onCancel(
											Platform platform,
											int i) {
										Log.v("DBG",
												"WECHAT SHARE CANCEL");
									}
								});
								ShareParams wechatSp = new ShareParams();
								wechatSp.setText(inviteText);
								wechatSp.setTitle("完美宝贝邀请");
								wechatSp.setShareType(Platform.SHARE_TEXT);
								wechatPlat.share(wechatSp);
								
								break;
								
							case INVITE_METHOD_MAIL:
								
								Intent mailIntent =new Intent(Intent.ACTION_SENDTO); 
								mailIntent.setType("*/*");
								mailIntent.putExtra(Intent.EXTRA_SUBJECT, "完美宝贝邀请"); 
								mailIntent.putExtra(Intent.EXTRA_TEXT, inviteText); 
								context.startActivity(Intent.createChooser(mailIntent,
				                        "请选择用于发送邮件的程序："));
								
								break;
								
							case INVITE_METHOD_MESSAGE:
								
								Intent messageIntent = new Intent(Intent.ACTION_VIEW);
								messageIntent.putExtra("sms_body", inviteText);
								messageIntent.setType("vnd.android-dir/mms-sms");
							    context.startActivity(messageIntent);
								
								break;
								
							case INVITE_METHOD_QQ:
								Platform QQPlat = ShareSDK.getPlatform(QQ.NAME);
				                
								QQPlat.setPlatformActionListener(new PlatformActionListener() {
									@Override
									public void onComplete(
											Platform platform,
											int i,
											HashMap<String, Object> stringObjectHashMap) {
										Message msg = new Message();
										msg.arg1 = 1;
										msg.arg2 = i;
										msg.obj = platform;
										if(inviteHandler != null)
										{
											inviteHandler
											.sendMessage(msg);
										}
										Log.v("DBG",
												"QQ SHARE DONE");
									}

									@Override
									public void onError(
											Platform platform,
											int i,
											Throwable throwable) {
										throwable
												.printStackTrace();
										Message msg = new Message();
										msg.arg1 = 2;
										msg.arg2 = i;
										msg.obj = platform;
										if(inviteHandler != null)
										{
											inviteHandler
											.sendMessage(msg);
										}
										Log.v("DBG",
												"QQ SHARE ERROR");
									}

									@Override
									public void onCancel(
											Platform platform,
											int i) {
										Log.v("DBG",
												"QQ SHARE CANCEL");
									}
								});
								ShareParams QQSp = new ShareParams();
								QQSp.setText(inviteText);
								QQSp.setTitle("完美宝贝邀请");
								QQSp.setShareType(Platform.SHARE_TEXT);
								QQPlat.share(QQSp);
								
								break;
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					else
					{
						Log.v("Kite", "invite fail " + data);
					}
				}
				
			};
			
			handler.start();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public interface OnLoadingListener{
		
		public void onLoading();
		
		public void onLoadComplete();
	}
	
	public void setOnLoadingListener(OnLoadingListener onLoadingListener)
	{
		this.onLoadingListener = onLoadingListener;
	}
}
