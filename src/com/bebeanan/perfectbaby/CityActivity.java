package com.bebeanan.perfectbaby;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by KiteXu.
 */
public class CityActivity extends Activity {

	private ImageButton clearButton;
	
	private TextView city_hot_city_text;
	
	private TextView city_spinner_province_text, city_spinner_city_text;
	
	private Spinner city_spinner_province, city_spinner_city;
	
	private boolean firstSet = true;
	
	private TextView city_gps_city_text;
	private LinearLayout city_gps_locator_layout;
	private ImageView city_gps_locator;
	private TextView city_gps_locator_text;
	
	private SharedPreferences userPreferences;
	
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
    private AlertDialog dialog;
    
    private LocationManager locationManager = null;
    private LocationListener locationListener;
    
    private static final int OPEN_GPS = 3000;
    
	public static String[] hotCityString = { "北京", "上海", "广州市", "杭州市", "宁波市",
			"温州市", "嘉兴市", "湖州市", "绍兴市", "金华市", "衢州市", "台州市", "丽水市", "舟山市" };
    
    public static String[] CityString = {
            "北京",
            "上海",
            "广州市",
            "杭州市",
            "宁波市",
            "温州市", 
            "嘉兴市", 
            "湖州市",  
            "绍兴市", 
            "金华市",  
            "衢州市", 
            "台州市",  
            "丽水市", 
            "舟山市",
            "天津",
            "重庆",
            "石家庄市",
            "邯郸市",
            "邢台市",
            "保定市",
            "张家口市",
            "承德市",
            "秦皇岛市",
            "唐山市",
            "沧州市",
            "廊坊市",
            "衡水市",
            "太原市",
            "大同市",
            "阳泉市",
            "晋城市",
            "朔州市",
            "晋中市",
            "忻州市",
            "吕梁市",
            "临汾市",
            "运城市",
            "郑州市",
            "开封市",
            "洛阳市",
            "平顶山市",
            "焦作市",
            "鹤壁市",
            "新乡市",
            "安阳市",
            "濮阳市",
            "许昌市",
            "漯河市",
            "三门峡市",
            "南阳市",
            "商丘市",
            "周口市",
            "驻马店市",
            "信阳市",
            "沈阳市",
            "大连市",
            "鞍山市",
            "抚顺市",
            "本溪市",
            "丹东市",
            "锦州市",
            "葫芦岛市",
            "营口市",
            "盘锦市",
            "阜新市",
            "辽阳市",
            "朝阳市",
            "长春市",
            "吉林市",
            "四平市",
            "通化市",
            "白山市",
            "松原市",
            "白城市",
            "延边州",
            "哈尔滨市",
            "齐齐哈尔市",
            "鹤岗市",
            "双鸭山市",
            "鸡西市",
            "大庆市",
            "伊春市",
            "牡丹江市",
            "佳木斯市",
            "七台河市",
            "黑河市",
            "绥化市",
            "大兴安岭地区",
            "呼和浩特市",
            "包头市",
            "乌海市",
            "赤峰市",
            "乌兰察布市",
            "锡林郭勒盟",
            "呼伦贝尔市",
            "鄂尔多斯市",
            "巴彦淖尔市",
            "阿拉善盟",
            "兴安盟",
            "通辽市",
            "南京市",
            "徐州市",
            "连云港市",
            "淮安市",
            "宿迁市",
            "盐城市",
            "扬州市",
            "泰州市",
            "南通市",
            "镇江市",
            "常州市",
            "无锡市",
            "苏州市",
            "济南市",
            "青岛市",
            "淄博市",
            "枣庄市",
            "东营市",
            "潍坊市",
            "烟台市",
            "威海市",
            "莱芜市",
            "德州市",
            "临沂市",
            "聊城市",
            "滨州市",
            "菏泽市",
            "日照市",
            "泰安市",
            "铜陵市",
            "合肥市",
            "淮南市",
            "淮北市",
            "芜湖市",
            "蚌埠市",
            "马鞍山市",
            "安庆市",
            "黄山市",
            "滁州市",
            "阜阳市",
            "亳州市",
            "宿州市",
            "池州市",
            "六安市",
            "福州市",
            "钓鱼岛",
            "厦门市",
            "三明市",
            "莆田市",
            "泉州市",
            "漳州市",
            "南平市",
            "龙岩市",
            "宁德市",
            "武汉市",
            "黄石市",
            "襄阳市",
            "十堰市",
            "荆州市",
            "宜昌市",
            "孝感市",
            "黄冈市",
            "咸宁市",
            "恩施州",
            "鄂州市",
            "荆门市",
            "随州市",
            "长沙市",
            "株洲市",
            "湘潭市",
            "韶山市",
            "衡阳市",
            "邵阳市",
            "岳阳市",
            "常德市",
            "张家界市",
            "郴州市",
            "益阳市",
            "永州市",
            "怀化市",
            "娄底市",
            "湘西州",
            "深圳市",
            "珠海市",
            "汕头市",
            "韶关市",
            "河源市",
            "梅州市",
            "惠州市",
            "汕尾市",
            "东莞市",
            "中山市",
            "江门市",
            "佛山市",
            "阳江市",
            "湛江市",
            "茂名市",
            "肇庆市",
            "云浮市",
            "清远市",
            "潮州市",
            "揭阳市",
            "南宁市",
            "柳州市",
            "桂林市",
            "梧州市",
            "北海市",
            "防城港市",
            "钦州市",
            "贵港市",
            "玉林市",
            "贺州市",
            "百色市",
            "河池市",
            "南昌市",
            "景德镇市",
            "萍乡市",
            "新余市",
            "九江市",
            "鹰潭市",
            "上饶市",
            "宜春市",
            "抚州市",
            "吉安市",
            "赣州市",
            "成都市",
            "自贡市",
            "攀枝花市",
            "泸州市",
            "绵阳市",
            "德阳市",
            "广元市",
            "遂宁市",
            "内江市",
            "乐山市",
            "宜宾市",
            "广安市",
            "南充市",
            "达州市",
            "巴中市",
            "雅安市",
            "眉山市",
            "资阳市",
            "阿坝州",
            "甘孜州",
            "凉山州",
            "海口市",
            "贵阳市",
            "六盘水市",
            "遵义市",
            "铜仁市",
            "毕节市",
            "安顺市",
            "黔西南州",
            "黔东南州",
            "黔南州",
            "昆明市",
            "曲靖市",
            "玉溪市",
            "昭通市",
            "普洱市",
            "临沧市",
            "保山市",
            "丽江市",
            "文山州",
            "红河州",
            "西双版纳州",
            "楚雄州",
            "大理州",
            "德宏州",
            "怒江州",
            "西安市",
            "铜川市",
            "宝鸡市",
            "咸阳市",
            "渭南市",
            "延安市",
            "汉中市",
            "榆林市",
            "商洛市",
            "安康市",
            "兰州市",
            "金昌市",
            "白银市",
            "天水市",
            "嘉峪关市",
            "平凉市",
            "庆阳市",
            "陇南市",
            "武威市",
            "张掖市",
            "酒泉市",
            "甘南州",
            "临夏州",
            "西宁市",
            "海东地区",
            "海北州",
            "黄南州",
            "海南州",
            "果洛州",
            "玉树州",
            "海西州",
            "银川市",
            "石嘴山市",
            "吴忠市",
            "固原市",
            "乌鲁木齐市",
            "克拉玛依市",
            "石河子市",
            "吐鲁番地区",
            "哈密地区",
            "和田地区",
            "阿克苏地区",
            "喀什地区",
            "克孜勒苏州",
            "巴音郭楞州",
            "昌吉州",
            "博尔塔拉州",
            "伊犁州",
            "塔城地区",
            "阿勒泰地区",
            "香港特别行政区",
            "台湾",
            "澳门市",
            "济源市",
            "三环以内",
            "三环到四环之间",
            "四环到五环之间",
            "五环到六环之间",
            "济宁市",
            "潜江市",
            "拉萨市",
            "宣城市",
            "天门市",
            "仙桃市",
            "辽源市",
            "儋州市",
            "来宾市",
            "中卫市",
            "长治市",
            "定西市",
            "那曲地区",
            "琼海市",
            "山南地区",
            "万宁市",
            "昌都地区",
            "日喀则地区",
            "神农架林区",
            "崇左市",
            "东方市",
            "三亚市",
            "文昌市",
            "五指山市",
            "临高县",
            "澄迈县",
            "定安县",
            "屯昌县",
            "昌江县",
            "白沙县",
            "琼中县",
            "陵水县",
            "保亭县",
            "乐东县",
            "三沙市",
            "阿里地区",
            "林芝地区",
            "迪庆州",
            "五家渠市",
            "管庄",
            "北苑",
            "博尔塔拉蒙古自治州阿拉山口口岸",
            "定福庄",
            "铁岭市",
            "阿拉尔市",
            "图木舒克市",
    };
    
    private String cityResult = "";
    
    private ArrayAdapter<String> adapter;
    
    public static final String PROVINCES[] = {"北京", "新疆", "重庆", "广东", "浙江", "天津", "澳门", "广西", "内蒙古", "宁夏", "江西", "台湾", "贵州", "安徽", "陕西", "辽宁", "山西", "青海", "香港", "四川", "江苏", "河北", "西藏", "福建", "吉林", "云南", "上海", "海南", "湖北", "甘肃", "湖南", "河南", "山东", "黑龙江" };
	public static final String CITIES[][] = {
        { "北京"},
        { "乌鲁木齐", "阿勒泰", "和田", "喀什", "吐鲁番", "塔城", "巴音郭楞", "克孜勒苏", "阿克苏", "昌吉", "克拉玛依", "博尔塔拉", "哈密", "伊犁" },
        { "重庆" },
        { "揭阳", "江门", "云浮", "广州", "阳江", "清远", "中山", "深圳", "惠州", "珠海", "茂名", "汕尾", "湛江", "梅州", "韶关", "佛山", "东莞", "潮州", "河源", "汕头", "肇庆" },
        { "丽水", "温州", "宁波", "衢州", "嘉兴", "杭州", "台州", "金华", "舟山", "绍兴", "湖州" },
        { "天津" },
        { "澳门" },
        { "北海", "柳州", "贵港", "钦州", "贺州", "南宁", "河池", "来宾", "崇左", "百色", "防城港", "梧州", "玉林", "桂林" },
        { "鄂尔多斯", "赤峰", "呼和浩特", "包头", "兴安", "锡林郭勒", "乌海", "乌兰察布", "通辽", "阿拉善", "巴彦淖尔", "呼伦贝尔" },
        { "石嘴山", "中卫", "银川", "吴忠", "固原" },
        { "上饶", "鹰潭", "景德镇", "宜春", "新余", "吉安", "抚州", "赣州", "萍乡", "南昌", "九江" },
        { "台湾" },
        { "黔东南", "贵阳", "遵义", "黔南", "安顺", "铜仁", "黔西南", "六盘水", "毕节" },
        { "六安", "蚌埠", "芜湖", "池州", "安庆", "铜陵", "黄山", "淮南", "宿州", "亳州", "巢湖", "滁州", "阜阳", "淮北", "马鞍山", "合肥", "宣城" },
        { "西安", "渭南", "延安", "铜川", "汉中", "咸阳", "商洛", "宝鸡", "榆林", "安康" },
        { "锦州", "沈阳", "营口", "朝阳", "抚顺", "大连", "辽阳", "盘锦", "丹东", "本溪", "葫芦岛", "铁岭", "鞍山", "阜新" },
        { "吕梁", "晋中", "阳泉", "忻州", "临汾", "太原", "长治", "运城", "朔州", "大同", "晋城" },
        { "玉树", "黄南", "海南", "海西", "果洛", "海北", "海东", "西宁" },
        { "香港" },
        { "遂宁", "宜宾", "甘孜", "乐山", "成都", "攀枝花", "广安", "眉山", "雅安", "资阳", "泸州", "内江", "自贡", "绵阳", "阿坝", "达州", "德阳", "凉山", "广元", "南充", "巴中" },
        { "无锡", "盐城", "扬州", "泰州", "镇江", "淮安", "常州", "南京", "南通", "苏州", "连云港", "宿迁", "徐州" },
        { "张家口", "唐山", "邢台", "邯郸", "廊坊", "石家庄", "秦皇岛", "承德", "保定", "衡水", "沧州" },
        { "阿里", "日喀则", "拉萨", "那曲", "昌都", "山南", "林芝" },
        { "漳州", "三明", "南平", "泉州", "宁德", "龙岩", "厦门", "莆田", "福州" },
        { "白山", "长春", "四平", "辽源", "松原", "延边", "白城", "通化", "吉林" },
        { "大理", "红河", "普洱", "德宏", "丽江", "临沧", "昆明", "楚雄", "昭通", "保山", "西双版纳", "曲靖", "玉溪", "怒江", "迪庆", "文山" },
        { "上海" },
        { "海口", "三亚" },
        { "襄阳", "荆门", "随州", "武汉", "十堰", "宜昌", "孝感", "恩施", "鄂州", "咸宁", "荆州", "黄石", "黄冈" },
        { "天水", "武威", "嘉峪关", "白银", "定西", "甘南", "金昌", "兰州", "张掖", "酒泉", "庆阳", "平凉", "临夏", "陇南" },
        { "长沙", "株洲", "郴州", "永州", "张家界", "益阳", "怀化", "娄底", "邵阳", "岳阳", "湘西", "湘潭", "衡阳", "常德" },
        { "濮阳", "新乡", "许昌", "漯河", "周口", "商丘", "三门峡", "安阳", "焦作", "平顶山", "鹤壁", "驻马店", "洛阳", "开封", "郑州", "南阳", "信阳" },
        { "枣庄", "莱芜", "德州", "菏泽", "滨州", "烟台", "日照", "东营", "青岛", "潍坊", "济宁", "威海", "聊城", "临沂", "泰安", "淄博", "济南" },
        { "大庆", "双鸭山", "鹤岗", "哈尔滨", "齐齐哈尔", "大兴安岭", "绥化", "七台河", "牡丹江", "黑河", "伊春", "佳木斯", "鸡西" },
};

    private EditText cityEditText;
    private LinearLayout cityLayout;
    
//    String category;

    void SetCityLayout(List<String> cities) {
        cityLayout.removeAllViews();
        for(int i = 0; i < (cities.size() + 2) / 3; i++) {
            LayoutInflater inflater = getLayoutInflater();
            RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.city_line, null);
            if (i * 3 < cities.size()) {
                final TextView textView = (TextView) layout.findViewById(R.id.left_textview);
                textView.setText(cities.get(i * 3));
                textView.setTypeface(Utils.typeface);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    	String cityName = ((TextView)view).getText().toString();
//                    	if(category.equals("don't upload."))
//                    	{
//                    		Intent intent = new Intent();
//                    		intent.putExtra("city", cityName);
//                    		CityActivity.this.setResult(RESULT_OK, intent);
//                    		CityActivity.this.finish();
//                    	}
//                    	else
//                    	{
                    		updateCity(cityName);
//                    	}
                    }
                });
            } else {
                TextView textView = (TextView) layout.findViewById(R.id.left_textview);
                textView.setVisibility(View.GONE);
            }

            if (i * 3 + 1 < cities.size()) {
                TextView textView = (TextView) layout.findViewById(R.id.middle_textview);
                textView.setText(cities.get(i * 3 + 1));
                textView.setTypeface(Utils.typeface);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    	String cityName = ((TextView)view).getText().toString();
//                    	if(category.equals("don't upload."))
//                    	{
//                    		Intent intent = new Intent();
//                    		intent.putExtra("city", cityName);
//                    		CityActivity.this.setResult(RESULT_OK, intent);
//                    		CityActivity.this.finish();
//                    	}
//                    	else
//                    	{
                    		updateCity(cityName);
//                    	}
                    }
                });
            } else {
                TextView textView = (TextView) layout.findViewById(R.id.middle_textview);
                textView.setVisibility(View.GONE);
            }

            if (i * 3 + 2 < cities.size()) {
                TextView textView = (TextView) layout.findViewById(R.id.right_textview);
                textView.setText(cities.get(i * 3 + 2));
                textView.setTypeface(Utils.typeface);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    	String cityName = ((TextView)view).getText().toString();
//                    	if(category.equals("don't upload."))
//                    	{
//                    		Intent intent = new Intent();
//                    		intent.putExtra("city", cityName);
//                    		CityActivity.this.setResult(RESULT_OK, intent);
//                    		CityActivity.this.finish();
//                    	}
//                    	else
//                    	{
                    		updateCity(cityName);
//                    	}
                    }
                });
            } else {
                TextView textView = (TextView) layout.findViewById(R.id.right_textview);
                textView.setVisibility(View.GONE);
            }
            cityLayout.addView(layout);
        }
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String city = cityEditText.getText().toString();
            
            if(city.length() == 0)
            {
            	clearButton.setVisibility(View.GONE);
            }
            else
            {
            	clearButton.setVisibility(View.VISIBLE);
            }
            List<String> result = new LinkedList<String>();
            if (city.isEmpty()) {
            	city_hot_city_text.setText("热门城市");
                for(int i = 0; i < hotCityString.length; i++) {
                    result.add(hotCityString[i]);
                }
            } else {
            	city_hot_city_text.setText("选择城市");
                for(int i = 0; i < CityString.length; i++) {
                    if (CityString[i].contains(city)) {
                        result.add(CityString[i]);
                    }
                }
            }
            SetCityLayout(result);
        }
    };

    
    
    @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
    	InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(cityEditText.getWindowToken(), 0);
    	
		super.onDestroy();
		
		if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
	}

	@Override
  	protected void onPause() {
  		// TODO Auto-generated method stub
  		super.onPause();
  		MobclickAgent.onPause(this);
  	}

  	@Override
  	protected void onResume() {
  		// TODO Auto-generated method stub
  		super.onResume();
  		MobclickAgent.onResume(this);
  	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

//        category = CityActivity.this.getIntent().getStringExtra("category");
        
        if(Utils.application == null)
		{
			Utils.init(CityActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		cityResult = CITIES[0][0];
        
        cityEditText = (EditText) findViewById(R.id.city_edittext);
        cityEditText.addTextChangedListener(watcher);

        city_hot_city_text = (TextView) findViewById(R.id.city_hot_city_text);
        city_hot_city_text.setTypeface(Utils.typeface);
        city_hot_city_text.setText("热门城市");
        
        cityLayout = (LinearLayout) findViewById(R.id.city_layout);
        
        city_spinner_province_text = (TextView) findViewById(R.id.city_spinner_province_text);
        city_spinner_province_text.setTypeface(Utils.typeface);
		city_spinner_city_text = (TextView) findViewById(R.id.city_spinner_city_text);
		city_spinner_city_text.setTypeface(Utils.typeface);
		
		city_spinner_province = (Spinner) findViewById(R.id.city_spinner_province);
		city_spinner_city = (Spinner) findViewById(R.id.city_spinner_city);
		
		initSpinners();
		
        ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
        title_left.setImageResource(R.drawable.title_back_pic);
        title_left.setVisibility(View.VISIBLE);
        title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	setResult(1);
                finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
            }
        });
        
        TextView titleView = (TextView) findViewById(R.id.title_text);
        titleView.setText(R.string.city_title);
        titleView.setTypeface(Utils.typeface);
        
        ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
        title_right.setVisibility(View.GONE);

        clearButton = (ImageButton) findViewById(R.id.city_clear);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityEditText.setText("");
            }
        });

        List<String> result = new LinkedList<String>();
        for(int i = 0; i < hotCityString.length; i++) {
            result.add(hotCityString[i]);
        }
        SetCityLayout(result);
        

        locationListener = new LocationListener(){
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                // TODO Auto-generatedmethod stub
            }

            @Override
            public void onProviderDisabled(String provider) {
                // TODO Auto-generatedmethod stub
            }

            @Override
            public void onLocationChanged(Location location) {
                return;
            }
        };
        
        city_gps_city_text = (TextView) findViewById(R.id.city_gps_city_text);
        city_gps_city_text.setTypeface(Utils.typeface);
        
        city_gps_locator_text = (TextView) findViewById(R.id.city_gps_location_text);
        city_gps_locator_text.setTypeface(Utils.typeface);
        city_gps_locator_text.setText("点击定位");
        
    	city_gps_locator_layout = (LinearLayout) findViewById(R.id.city_gps_locator_layout);
    	city_gps_locator_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(cityEditText.getWindowToken(), 0);
				
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
		            Toast toast = Toast.makeText(getApplicationContext(), "请开启GPS",
		                    Toast.LENGTH_SHORT);
		            toast.setGravity(Gravity.CENTER, 0, 0);
		            toast.show();
		            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		            startActivityForResult(intent, OPEN_GPS); //此为设置完成后返回到获取界面
		            return ;
		        }

		        // 查找到服务信息
		        Criteria criteria = new Criteria();
		        criteria.setAccuracy(Criteria.ACCURACY_FINE); // 高精度
		        criteria.setAltitudeRequired(false);
		        criteria.setBearingRequired(false);
		        criteria.setCostAllowed(true);
		        criteria.setPowerRequirement(Criteria.POWER_LOW); // 低功耗

		        String provider = locationManager.getBestProvider(criteria, true); // 获取GPS信息
		        Location location = locationManager.getLastKnownLocation(provider); // 通过GPS获取位置
		        updateToNewLocation(location);
			}
    		
    	});
    	
        // 设置监听器，自动更新的最小时间为间隔N秒(1秒为1*1000，这样写主要为了方便)或最小位移变化超过N米
//        locationManager.requestLocationUpdates(provider, 100 * 1000, 500,
//                locationListener);
    }

    private void initSpinners() {
		adapter = new ArrayAdapter<String>(CityActivity.this,
				android.R.layout.simple_spinner_item, PROVINCES);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		city_spinner_province.setAdapter(adapter);
		city_spinner_province
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {

						adapter = new ArrayAdapter<String>(
								CityActivity.this,
								android.R.layout.simple_spinner_item,
								CITIES[position]);
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						city_spinner_city.setAdapter(adapter);

						city_spinner_city
								.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
									@Override
									public void onItemSelected(
											AdapterView<?> parent, View view,
											int position, long id) {

										if(firstSet)
										{
											cityEditText.setText("");
											
											firstSet = false;
										}
										else
										{
											cityResult = CITIES[city_spinner_province.getSelectedItemPosition()][position];
											cityEditText.setText(cityResult);
										}
									}

									@Override
									public void onNothingSelected(
											AdapterView<?> parent) {
										
										cityEditText.setText(cityResult);
									}
								});
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						cityEditText.setText(cityResult);
					}
				});
	}
    
    private void updateToNewLocation(Location location) {

    	city_gps_locator_text.setText("定位中...");
    	
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            // positionText.setText("纬度：" + latitude + " 经度：" + longitude);
            // positionText.setVisibility(View.VISIBLE);
            Log.v("DBG", "纬度：" + latitude + " 经度：" + longitude);
            List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
            param.add(new BasicNameValuePair("ak", "0E1f6db56b29795a5f5939cd83a35a54"));
            param.add(new BasicNameValuePair("output", "json"));
            param.add(new BasicNameValuePair("location", latitude + "," + longitude));
            NetHandler handler = new NetHandler(CityActivity.this, NetHandler.METHOD_GET_LOCATION, "http://api.map.baidu.com/geocoder/v2/",
                    param, null) {
                @Override
                public void handleRsp(Message msg) {
                    Bundle bundle = msg.getData();
                    int code = bundle.getInt("code");
                    if (code == 200) {
                        try {
                            JSONObject object = new JSONObject(bundle.getString("data"));
                            Log.v("Kite", "GPS data is " + object);
                            if (object.getInt("status") == 0) {
                            	JSONObject addressComponent = object.getJSONObject("result").getJSONObject("addressComponent");
                            	city_gps_locator_text.setText(addressComponent.getString("city"));
                            	cityEditText.setText(addressComponent.getString("city"));
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
            		            toast.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
        		            toast.show();
                        }
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "获取地理信息失败",
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
    		            toast.show();
                        
                        city_gps_locator_text.setText("点击定位");
                    }
                }
            };
            handler.start();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "无法获取地理信息",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            
            city_gps_locator_text.setText("点击定位");
        }

    }
    
    private void updateCity(final String cityName)
    {
    	String category = CityActivity.this.getIntent().getStringExtra("category");
    	if(category != null && category.equals("don't upload."))
    	{
    		Intent intent = new Intent();
            intent.putExtra("city", cityName);
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
    		
    		return;
    	}
    	
    	builder = new AlertDialog.Builder(CityActivity.this);
    	builder.setMessage("上传中");
        dialog = builder.show();
        showBuilder = true;
        
    	try {

            JSONObject feeds = new JSONObject();
            feeds.put("city", cityName);

            String userId = userPreferences.getString("userId", null);
            if(userId != null)
            {
            	NetHandler handler = new NetHandler(CityActivity.this, NetHandler.METHOD_PUT,
                        "/users/"+userId, new LinkedList<BasicNameValuePair>(), feeds) {
                    @Override
                    public void handleRsp(Message msg) {
                    	
                    	dialog.dismiss();
                        showBuilder = false;
                        
                        Bundle bundle = msg.getData();
                        int code = bundle.getInt("code");
                        Log.v("Kite", "post city data is " + bundle.getString("data"));
                        if (code == 200) {
                        	
                        	Toast toast = Toast.makeText(CityActivity.this, "保存成功", Toast.LENGTH_SHORT);
                        	toast.setGravity(Gravity.CENTER, 0, 0);
        		            toast.show();
                        	
                        	Intent intent = new Intent();
                            intent.putExtra("city", cityName);
                            setResult(0, intent);
                            finish();
                            overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                            Editor editor = userPreferences.edit();
                            editor.putString("city", cityName);
                            editor.commit();
                        } else {
                        	Log.v("Kite", "city code is: " + code + " message is " + bundle.getString("data"));
                            Toast toast = Toast.makeText(CityActivity.this, "保存失败", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
        		            toast.show();
                        }
                    }
                };
                handler.start();
            }
    	}catch (Exception e) {
            e.printStackTrace();
        }
    }

	public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
            	
            	if(showBuilder)
            	{
            		return true;
            	}
                
                setResult(1);
                this.finish();
                overridePendingTransition(R.anim.activity_close_in_anim, R.anim.activity_close_out_anim);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}