package com.bebeanan.perfectbaby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class PostActivity extends Activity {

	private LinearLayout post_list_layout;
	private ProgressBar post_list_progress;
	
	private List<Map<String, String>> mData = new ArrayList<Map<String, String>>();
	private List<TextView> messageViews = new ArrayList<TextView>();
	
	private static SharedPreferences userPreferences;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);

		if(Utils.application == null)
		{
			Utils.init(PostActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}
		
		post_list_layout = (LinearLayout) findViewById(R.id.post_list_layout);
		post_list_progress = (ProgressBar) findViewById(R.id.post_list_progress);

		ImageView title_left = (ImageView) findViewById(R.id.title_left_button);
		title_left.setImageResource(R.drawable.title_back_pic);
		title_left.setVisibility(View.VISIBLE);
		title_left.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				overridePendingTransition(R.anim.activity_close_in_anim,
						R.anim.activity_close_out_anim);
			}
		});

		TextView titleView = (TextView) findViewById(R.id.title_text);
		titleView.setText(R.string.my_message_title);
		titleView.setTypeface(Utils.typeface);

		ImageView title_right = (ImageView) findViewById(R.id.title_right_button);
		title_right.setVisibility(View.GONE);

		initView();
	}

	private void initView() {
		
		post_list_progress.setVisibility(View.VISIBLE);
		
		NetHandler handler = new NetHandler(PostActivity.this, NetHandler.METHOD_GET,
				"/notification?owner=" + userPreferences.getString("userId", "") + "&hasRead=false", new LinkedList<BasicNameValuePair>(), null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				
				post_list_progress.setVisibility(View.GONE);
				
				Bundle bundle = msg.getData();
				
				String data = bundle.getString("data");
				int code = bundle.getInt("code");
				
				Log.v("Kite", "post data is " + data);
				if(code == 200)
				{
					try {
						JSONArray postArray = new JSONArray(data);
						
						if(postArray.length() == 0)
						{
							Toast toast = Toast.makeText(PostActivity.this, "没有新消息", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}
						for(int i=0; i<postArray.length(); i++)
						{
							JSONObject postObject = postArray.getJSONObject(i);
							final String messageId = postObject.getString("id");
							Map<String, String> temp = new HashMap<String, String>();
							temp.put("messageId", messageId);
							
							JSONObject sourceObject = postObject.getJSONObject("source");
							String type = sourceObject.getString("type");
							
							if(type.equalsIgnoreCase("feed"))
							{
								final String feedId = sourceObject.getString("id");
								temp.put("feedId", feedId);
								
								String content = postObject.getString("content");
								final TextView postText = new TextView(PostActivity.this);
								postText.setTypeface(Utils.typeface);
								postText.setSingleLine(true);
								postText.setEllipsize(TruncateAt.MIDDLE);
								postText.setText(content);
								postText.setClickable(true);
								postText.setOnClickListener(new OnClickListener(){

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										Intent intent = new Intent(PostActivity.this,
												FeedDetailActivity.class);
										intent.putExtra("feedId", feedId);

										PostActivity.this.startActivity(intent);
										
										setRead(messageId);
										
										post_list_layout.removeView(postText);
										
										scanMessages(feedId);
									}
									
								});
								postText.setBackgroundResource(R.drawable.post_item_selector);
								
								post_list_layout.addView(postText);
								mData.add(temp);
								messageViews.add(postText);
								
								postText.setGravity(Gravity.CENTER_VERTICAL);
								postText.setPadding((int)(10*Utils.density), 0, (int)(10*Utils.density), 0);
//								LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)postText.getLayoutParams();
//								params.topMargin = (int) (20 * Utils.density);
//								postText.setLayoutParams(params);
								
							}
							else if(type.equalsIgnoreCase("follow"))
							{
								JSONObject fromUserObject = postObject.getJSONObject("fromUser");
								final String fromUserId = fromUserObject.getString("id");
								
								String content = postObject.getString("content");
								
								final View postView = LayoutInflater.from(PostActivity.this).inflate(R.layout.item_post_follow, null);
								postView.setClickable(false);
								
								TextView item_post_follow_content = (TextView) postView.findViewById(R.id.item_post_follow_content);
								item_post_follow_content.setTypeface(Utils.typeface);
								item_post_follow_content.setSingleLine(true);
								item_post_follow_content.setMaxWidth((int)(180*Utils.density));
								item_post_follow_content.setEllipsize(TruncateAt.MARQUEE);
								item_post_follow_content.setMarqueeRepeatLimit(6);
								item_post_follow_content.setFocusable(true);
								item_post_follow_content.setFocusableInTouchMode(true);
								item_post_follow_content.setText(content);
								item_post_follow_content.setOnClickListener(new OnClickListener(){

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										((TextView) v).requestFocus();
										((TextView) v).setSelected(!v.isSelected());
									}
									
								});
								
								TextView item_post_follow_accept = (TextView) postView.findViewById(R.id.item_post_follow_accept);
								item_post_follow_accept.setTypeface(Utils.typeface);
								item_post_follow_accept.setOnClickListener(new OnClickListener(){

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										addFollower(fromUserId, messageId);
										
										post_list_layout.removeView(postView);
									}
									
								});
								
								TextView item_post_follow_ignore = (TextView) postView.findViewById(R.id.item_post_follow_ignore);
								item_post_follow_ignore.setTypeface(Utils.typeface);
								item_post_follow_ignore.setOnClickListener(new OnClickListener(){

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										setRead(messageId);
										
										post_list_layout.removeView(postView);
									}
									
								});
								
								post_list_layout.addView(postView);
								
//								LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)postView.getLayoutParams();
//								params.topMargin = (int) (20 * Utils.density);
//								postView.setLayoutParams(params);
								
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					Log.v("Kite", "post fail");
				}
			}

		};
		handler.start();
	}

	private void scanMessages(String curFeedId)
	{
		for(int i=0; i<mData.size(); i++)
		{
			String messageId = mData.get(i).get("messageId");
			String feedId = mData.get(i).get("feedId");
			
			if(curFeedId.equals(feedId))
			{
				setRead(messageId);
				
				post_list_layout.removeView(messageViews.get(i));
			}
		}
	}
	
	private void setRead(String messageId)
	{
		JSONObject messageObject = new JSONObject();
		try {
			messageObject.put("hasRead", true);
			NetHandler handler = new NetHandler(PostActivity.this, NetHandler.METHOD_PUT,
					"/notification?id=" + messageId, new LinkedList<BasicNameValuePair>(), messageObject)
			{

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					Bundle bundle = msg.getData();
					
					int code = bundle.getInt("code");
				}
				
			};
			
			handler.start();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void addFollower(String followerId, final String messageId)
	{
		JSONObject followerObject = new JSONObject();
		
		try {
			followerObject.put("userId", userPreferences.getString("userId", ""));
			followerObject.put("follower", followerId);
			
			NetHandler handler = new NetHandler(PostActivity.this, NetHandler.METHOD_POST,
					"/follower", new LinkedList<BasicNameValuePair>(),
					followerObject) {

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub

					Bundle bundle = msg.getData();

					int code = bundle.getInt("code");
					String data = bundle.getString("data");
					Log.v("Kite", "follower data is " + data);
					if (code == 200) {
						
						setRead(messageId);
						
						Toast toast = Toast.makeText(PostActivity.this, "接受成功",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();

						Log.v("Kite", "follow success");
					} else {
						
						Toast toast = Toast.makeText(PostActivity.this, "接受失败",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();

						Log.v("Kite", "follow fail because" + data);
					}
				}

			};
			
			handler.start();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
