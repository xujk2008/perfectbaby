package com.bebeanan.perfectbaby;

import com.bebeanan.perfectbaby.common.Utils;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Created by KiteXu.
 */
public class BrowserActivity extends Activity{

	WebView browser_webview;
	
	  @Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			MobclickAgent.onPause(this);
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			MobclickAgent.onResume(this);
		}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);
		
		browser_webview = (WebView) findViewById(R.id.browser_webview);
		
		ImageView leftButton = (ImageView) findViewById(R.id.title_left_button);
		leftButton.setImageResource(R.drawable.title_back_pic);
		leftButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				BrowserActivity.this.finish();
			}
		});
		
		TextView titleText = (TextView) findViewById(R.id.title_text);
		titleText.setTypeface(Utils.typeface);
		titleText.setText(BrowserActivity.this.getIntent().getStringExtra("title"));
		
		ImageView rightButton = (ImageView) findViewById(R.id.title_right_button);
		rightButton.setVisibility(View.GONE);
		
		setView();
	}

	private void setView()
	{
		String url = BrowserActivity.this.getIntent().getStringExtra("url");
		
		if(url != null)
		{
			browser_webview.getSettings().setJavaScriptEnabled(true);
			browser_webview.setWebViewClient(new WebViewClient(){

				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					// TODO Auto-generated method stub
					view.loadUrl(url);
					
					return true;
				}
				
			});
			browser_webview.loadUrl(url);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			
			if(browser_webview.canGoBack())
			{
				browser_webview.goBack();
			}
			else
			{
				BrowserActivity.this.finish();
			}
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	
}
