package com.bebeanan.perfectbaby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

import com.bebeanan.perfectbaby.common.Rotate3DAnimation;
import com.bebeanan.perfectbaby.common.RotatePhoto;
import com.bebeanan.perfectbaby.common.Utils;
import com.bebeanan.perfectbaby.zxing.view.DetectKeyDownActionEditText;
import com.bebeanan.perfectbaby.zxing.view.DetectKeyDownActionEditText.OnBackPressedListener;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.luminous.pick.Action;
import com.perfectbaby.adapters.MainListAdapter;
import com.perfectbaby.adapters.MainListAdapter.OnCommentImageClickListener;
import com.perfectbaby.adapters.MainListAdapter.OnDeleteImageClickListener;
import com.perfectbaby.adapters.MainListAdapter.OnShareImageClickListener;
import com.perfectbaby.adapters.MainListAdapter.OnTouchingZoomInListener;
import com.perfectbaby.adapters.MainListAdapter.OnUpImageClickListener;
import com.perfectbaby.adapters.MainListAdapter.OnVideoClickListener;
import com.umeng.analytics.MobclickAgent;
/**
 * Created by KiteXu.
 */
public class MainActivity extends Activity {
	private ImageView title_left_button, title_unread, title_right_button;
	private TextView title_text;

	private ImageView main_guide;
	
	private ImageView user_info_avatar;
	private ImageView user_info_gender;
	private TextView user_info_name;

	private DrawerLayout main_drawer;

	private Button user_info_city, user_info_rada, user_info_babies,
			user_info_fans, user_info_message, user_info_favorite,
			user_info_invite, user_info_group;
	private Button more_function_post, more_function_guide, more_function_newborn,
			more_function_modify, more_function_bind,
			more_function_feedback, more_function_about, more_function_logout;

	private TextView dialog_name_text;
	private EditText dialog_name_edit;
	private ImageView dialog_name_cancel, dialog_name_complete;

	private PullToRefreshListView main_list;
	private List<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
	private MainListAdapter mAdapter;
	private JSONArray cacheDataArray = new JSONArray();
	
	private int[] main_guide_src = {R.drawable.main_guide_0, R.drawable.main_guide_1, R.drawable.main_guide_2, R.drawable.main_guide_3, R.drawable.main_guide_4};
	private int guideIndex;

	private String headMessage;
	
	private boolean hasMorePages;
	private boolean scrollingUp;
	private int firstItemIndex;
	private int lastItemIndex;
	private String since = null;

	private float lastSensitiveY = 0;
	private float lastY = 0;

	private RelativeLayout main_bottom_layout;
	private ImageView main_bottom_sound, main_bottom_text, main_bottom_photo,
			main_bottom_video, main_bottom_search;

	private LinearLayout main_bottom_select_layout;
	private ImageView main_bottom_gallery, main_bottom_take,
			main_bottom_cancel;

	private RelativeLayout main_comment_edit_layout;
	private DetectKeyDownActionEditText main_comment_edit;
	private ImageButton main_comment_send;

	private ProgressBar main_progress;

	private RelativeLayout main_mask_layout, invite_method_layout,
			invite_type_layout;
	private LinearLayout invite_wechat_layout, invite_mail_layout,
			invite_message_layout, invite_qq_layout;
	private TextView invite_method_cancel;
	private TextView invite_type_fans, invite_type_family,
			invite_type_cancel;
//	private TextView invite_type_friends;
	
	private RelativeLayout share_method_layout;
	private LinearLayout share_wechat_layout, share_friend_circle_layout, share_qzone_layout, share_qq_layout, share_sina_layout;
	private TextView share_method_cancel;
	
	private int inviteMethod, inviteType;

	private boolean leftDrawerShown = false;
	private boolean rightDrawerShown = false;

	private boolean invite_method_shown = false;
	private boolean invite_type_shown = false;
	private boolean share_method_shown = false;

	private Uri fileUri;

	// private String babyId;
	private int currentBaby = 0;

	private Toast toast;
	private AlertDialog.Builder builder;
	private boolean showBuilder = false;
	private AlertDialog dialog;

	private long exitTime = 0;

	private InputMethodManager imm;

	private static SharedPreferences userPreferences;

	private static final int ANIMATION_LARGE_MARGIN_DP = 73;
	private static final int ANIMATION_MARGIN_DP = 58;

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public static final int EVENT_SYSTEM = 1;
	public static final int EVENT_VOICE = 8;
	public static final int EVENT_NOTE = 16;
	public static final int EVENT_PHOTO = 2;
	public static final int EVENT_VIDEO = 4;
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 101;
	private static final int CAPTURE_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE = 102;
	private static final int GET_IMAGE_ACTIVITY_REQUEST_CODE = 2000;
	private static final int GET_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE = 2001;
	private static final int GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE = 2002;
	private static final int EVENTACTIVITY_REQUEST_CODE = 1001;
	private static final int CITYACTIVITY_REQUEST_CODE = 1002;
	private static final int FEED_DETAIL_REQUEST_CODE = 1003;
	public static final int FEED_DETAIL_DELETE_CODE = 10031;
	public static final int FEED_DETAIL_UP_COMMENT_CODE = 10032;

	private final static int EventTypeSystem = 0x01;
	private final static int EventTypePhoto = 0x02;
	private final static int EventTypeVideo = 0x04;
	private final static int EventTypeVoice = 0x08;
	private final static int EventTypeNote = 0x10;

	private final static int INVITE_METHOD_HEIGHT_DP = 220,
			INVITE_TYPE_HEIGHT_DP = 230;
	private final static int INVITE_LAYOUT_OFFSET_DP = 20;

	private final static int INVITE_METHOD_WECHAT = 0, INVITE_METHOD_MAIL = 1,
			INVITE_METHOD_MESSAGE = 2, INVITE_METHOD_QQ = 3;
	private final static int INVITE_TYPE_FANS = 0, INVITE_TYPE_FRIENDS = 1,
			INVITE_TYPE_FAMILY = 2;
	
	private Handler thirdLoginHandler;
	
	private Handler thirdShareHandler;
	
	private static Platform sharePlatform;

	private boolean isLoading = false;
	
	String allowString = "";

	/**
	 * Create a file Uri for saving an image or video
	 */
	private static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type, 0));
	}

	/**
	 * Create a File for saving an image or video
	 */
	private static File getOutputMediaFile(int type, int number) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PerfectBaby");

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("PerfectBaby", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + String.valueOf(number) + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + String.valueOf(number) + ".mp4");
		} else {
			return null;
		}

		Log.v("DBG", mediaFile.getPath());

		return mediaFile;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
		
		if(mAdapter != null)
		{
			mAdapter.stopVoice();
		}
	}

	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		initView();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StartActivity.initShareSDK(MainActivity.this);
		
		if(Utils.application == null)
		{
			Utils.init(MainActivity.this);
		}
		if(userPreferences == null)
		{
			userPreferences = Utils.application
					.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
		}

		StoreHandler.getAndSetBabies();
		
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		title_left_button = (ImageView) findViewById(R.id.title_left_button);
		title_unread = (ImageView) findViewById(R.id.title_unread);
		title_right_button = (ImageView) findViewById(R.id.title_right_button);
		title_text = (TextView) findViewById(R.id.title_text);

		main_guide = (ImageView) findViewById(R.id.main_guide);
		
		user_info_name = (TextView) findViewById(R.id.user_info_name);
		user_info_avatar = (ImageView) findViewById(R.id.user_info_avatar);
		user_info_gender = (ImageView) findViewById(R.id.user_info_gender);

		user_info_city = (Button) findViewById(R.id.user_info_city);
		user_info_city.setTypeface(Utils.typeface);
		user_info_rada = (Button) findViewById(R.id.user_info_rada);
		user_info_babies = (Button) findViewById(R.id.user_info_babies);
		user_info_fans = (Button) findViewById(R.id.user_info_fans);
		user_info_message = (Button) findViewById(R.id.user_info_message);
		user_info_favorite = (Button) findViewById(R.id.user_info_favorite);
		user_info_invite = (Button) findViewById(R.id.user_info_invite);
		user_info_group = (Button) findViewById(R.id.user_info_group);

		more_function_post = (Button) findViewById(R.id.more_function_post);
		more_function_guide = (Button) findViewById(R.id.more_function_guide);
		more_function_newborn = (Button) findViewById(R.id.more_function_newborn);
		more_function_modify = (Button) findViewById(R.id.more_function_modify);
		more_function_bind = (Button) findViewById(R.id.more_function_bind);
		more_function_feedback = (Button) findViewById(R.id.more_function_feedback);
		more_function_about = (Button) findViewById(R.id.more_function_about);
		more_function_logout = (Button) findViewById(R.id.more_function_logout);

		main_drawer = (DrawerLayout) findViewById(R.id.main_drawer);
		// main_drawer.setFocusableInTouchMode(false);

		main_list = (PullToRefreshListView) findViewById(R.id.main_list);

		main_bottom_layout = (RelativeLayout) findViewById(R.id.main_bottom_layout);
		main_bottom_sound = (ImageView) findViewById(R.id.main_bottom_sound);
		main_bottom_text = (ImageView) findViewById(R.id.main_bottom_text);
		main_bottom_photo = (ImageView) findViewById(R.id.main_bottom_photo);
		main_bottom_video = (ImageView) findViewById(R.id.main_bottom_video);
		main_bottom_search = (ImageView) findViewById(R.id.main_bottom_search);

		main_bottom_select_layout = (LinearLayout) findViewById(R.id.main_bottom_select_layout);
		main_bottom_select_layout.setVisibility(View.INVISIBLE);
		main_bottom_gallery = (ImageView) findViewById(R.id.main_bottom_gallery);
		main_bottom_take = (ImageView) findViewById(R.id.main_bottom_take);
		main_bottom_cancel = (ImageView) findViewById(R.id.main_bottom_cancel);

		main_comment_edit_layout = (RelativeLayout) findViewById(R.id.main_comment_edit_layout);
		main_comment_edit = (DetectKeyDownActionEditText) findViewById(R.id.main_comment_edit);
		main_comment_send = (ImageButton) findViewById(R.id.main_comment_send);

		main_progress = (ProgressBar) findViewById(R.id.main_progress);
		main_progress.setVisibility(View.VISIBLE);

		main_mask_layout = (RelativeLayout) findViewById(R.id.main_mask_layout);

		invite_method_layout = (RelativeLayout) findViewById(R.id.invite_method_layout);
		invite_wechat_layout = (LinearLayout) findViewById(R.id.invite_wechat_layout);
		invite_mail_layout = (LinearLayout) findViewById(R.id.invite_mail_layout);
		invite_message_layout = (LinearLayout) findViewById(R.id.invite_message_layout);
		invite_qq_layout = (LinearLayout) findViewById(R.id.invite_qq_layout);
		invite_method_cancel = (TextView) findViewById(R.id.invite_method_cancel);

		invite_type_layout = (RelativeLayout) findViewById(R.id.invite_type_layout);
		invite_type_fans = (TextView) findViewById(R.id.invite_type_fans);
//		invite_type_friends = (TextView) findViewById(R.id.invite_type_friends);
		invite_type_family = (TextView) findViewById(R.id.invite_type_family);
		invite_type_cancel = (TextView) findViewById(R.id.invite_type_cancel);

		share_method_layout = (RelativeLayout) findViewById(R.id.share_method_layout);
		
		share_wechat_layout = (LinearLayout) findViewById(R.id.share_wechat_layout);
		share_friend_circle_layout = (LinearLayout) findViewById(R.id.share_friend_circle_layout);
		share_qzone_layout = (LinearLayout) findViewById(R.id.share_qzone_layout);
		share_qq_layout = (LinearLayout) findViewById(R.id.share_qq_layout);
		share_sina_layout = (LinearLayout) findViewById(R.id.share_sina_layout);
		
		share_method_cancel = (TextView) findViewById(R.id.share_method_cancel);
		
		initTitle();

		initView();

		// builder = new AlertDialog.Builder(MainActivity.this);
		// builder.setMessage("加载中");
		// dialog = builder.show();
		// showBuilder = true;

		hasMorePages = true;
		since = null;

		String cacheDataString = StoreHandler.getCache();
		if(cacheDataString != null)
		{
			main_progress.setVisibility(View.GONE);
			
			Map<String, Object> banner = new HashMap<String, Object>();
			banner.put(MainListAdapter.HEAD_BACKGROUND,
					userPreferences.getString("headBackground", ""));
			banner.put(MainListAdapter.HEAD_BACKGROUND_URL,
					userPreferences.getString("headBackgroundUrl", ""));
			banner.put(MainListAdapter.HEAD_NICKNAME,
					userPreferences.getString("nickName",
							""));
			banner.put(MainListAdapter.HEAD_AVATAR,
					userPreferences.getString("avatarUrl",
							null));
			setData(StoreHandler.getCache(), false, 0,
					banner );
			
			hasMorePages = true;
			since = null;

			refresh(false, 0);
		}
		else
		{
			refresh(false, 0);
		}
		
	}

	private void showGuide()
	{
		boolean mainGuideShown = StoreHandler.getGuide(StoreHandler.mainGuide);
		if(!mainGuideShown)
		{
			main_guide.setVisibility(View.VISIBLE);
			guideIndex = 0;
			main_guide.setImageResource(main_guide_src[guideIndex++]);
			main_guide.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(guideIndex == main_guide_src.length)
					{
						main_guide.setVisibility(View.GONE);
						
						StoreHandler.setGuide(StoreHandler.mainGuide, true);
						
						return;
					}
					
					main_guide.setImageResource(main_guide_src[guideIndex++]);
				}
				
			});
		}
	}
	
	private void initView() {

		// String baby = MemoryHandler.getInstance().getKey("baby");
		//
		// babyId = "";
		// try {
		// JSONArray babyArray = new JSONArray(baby);
		// babyId = babyArray.getJSONObject(currentBaby).getString("id");
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// Log.v("DBG", "babyId " + babyId);

		user_info_name.setTypeface(Utils.typeface);
		String nickName = userPreferences.getString("nickName", "");
		user_info_name.setText(nickName);

		user_info_city.setText(userPreferences.getString("city", "所在城市"));

		String user_info_avatar_url = userPreferences
				.getString("avatarUrl", "");
		if (!user_info_avatar_url.equals("")) {
			new UrlImageViewHelper().setUrlDrawable(user_info_avatar,
					userPreferences.getString("avatarUrl", ""));
		} else {
			user_info_avatar
					.setImageResource(R.drawable.user_info_default_avatar);
		}

		user_info_avatar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				final CharSequence[] items = { "相册", "拍照", "取消" };
				builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("选择图片");
				builder.setItems(items, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if (i == 1) {
							Intent intent = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0); // create
																					// a
																					// file
																					// to
																					// save
																					// the
																					// image
							intent.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(file)); // set the
															// image
															// file
															// name

							// start the image capture Intent
							startActivityForResult(intent,
									CAPTURE_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE);
						} else if (i == 0) {
							//
							Intent intent = new Intent(
									Intent.ACTION_PICK,
									android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(intent,
									GET_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE);
						} else {
							dialog.dismiss();
						}
					}
				});
				dialog = builder.show();

			}

		});

		user_info_name.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog user_name_edit_dialog = new Dialog(
						MainActivity.this);
				user_name_edit_dialog.show();

				Window window = user_name_edit_dialog.getWindow();
				window.setBackgroundDrawable(new ColorDrawable(0));
				window.setContentView(R.layout.dialog_edit_user_info);

				dialog_name_text = (TextView) window
						.findViewById(R.id.dialog_edit_user_info_text);
				dialog_name_text.setTypeface(Utils.typeface);
				
				TextView dialog_name_hint = (TextView) window.findViewById(R.id.dialog_edit_user_info_hint);
				dialog_name_hint.setTypeface(Utils.typeface);

				dialog_name_cancel = (ImageView) window
						.findViewById(R.id.dialog_edit_user_info_cacel);
				dialog_name_cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						user_name_edit_dialog.cancel();
					}

				});
				dialog_name_complete = (ImageView) window
						.findViewById(R.id.dialog_edit_user_info_complete);
				dialog_name_complete
						.setImageResource(R.drawable.dialog_edit_user_info_complete_enable);
				dialog_name_complete.setClickable(true);
				dialog_name_complete.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.hideSoftInputFromWindow(dialog_name_edit.getWindowToken(), 0);
						
						String currentName = dialog_name_edit.getText()
								.toString();
						
						int bytesNumber = 0;
						try {
							bytesNumber = currentName.getBytes("GBK").length;
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(bytesNumber < 4)
						{
							toast = Toast.makeText(getApplicationContext(), "亲，昵称长度不够噢～",
		                            Toast.LENGTH_SHORT);
		                    toast.setGravity(Gravity.CENTER, 0, 0);
		                    toast.show();
		                    
		                    return;
						}
						
						if (!currentName.equals(userPreferences.getString(
								"nickName", ""))) {
							updateNickName(currentName);
						}
						user_name_edit_dialog.cancel();
					}

				});
				dialog_name_edit = (EditText) window
						.findViewById(R.id.dialog_edit_user_info_edit);
				dialog_name_edit.setTypeface(Utils.typeface);
				dialog_name_edit.setText(userPreferences.getString("nickName",
						""));
				dialog_name_edit.addTextChangedListener(new TextWatcher() {

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						
						if (s.length() != 0) {
							dialog_name_complete
									.setImageResource(R.drawable.dialog_edit_user_info_complete_enable);
							dialog_name_complete.setClickable(true);
							
							int bytesNumber = 0;
							try {
								bytesNumber = s.toString().getBytes("GBK").length;
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							Log.v("Kite", "bytesNumber is " + bytesNumber);
							
							if(bytesNumber == 32)
							{
								allowString = s.toString();
							}
							
							if(bytesNumber>32)
							{
								dialog_name_edit.setText(allowString);
								
								dialog_name_edit.setSelection(allowString.length());  
								
								Toast toast = Toast.makeText(MainActivity.this, "亲，昵称太长了～", Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
							}
						} else {
							dialog_name_complete
									.setImageResource(R.drawable.dialog_edit_user_info_complete_disable);
							dialog_name_complete.setClickable(false);
						}
					}

				});
				showBuilder = true;
			}

		});

		int gender = userPreferences.getInt("gender", -1);
		if (gender == -1) {
			user_info_gender.setVisibility(View.GONE);
		} else if (gender == 1) {
			user_info_gender.setVisibility(View.VISIBLE);
			user_info_gender.setImageResource(R.drawable.user_info_male);
		} else {
			user_info_gender.setVisibility(View.VISIBLE);
			user_info_gender.setImageResource(R.drawable.user_info_female);
		}

		user_info_city.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						CityActivity.class);

				MainActivity.this.startActivityForResult(intent,
						CITYACTIVITY_REQUEST_CODE);
			}

		});

		user_info_invite.setVisibility(View.VISIBLE);
		user_info_invite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				showInviteMethodLayout();
				
				showInviteTypeLayout();
			}

		});
		
		user_info_group.setVisibility(View.VISIBLE);
		user_info_group.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				showInviteMethodLayout();
				
				joinQQGroup("yjzSxhKRepxIwRHwo2yxYJeruAx0BIWq");
			}

		});
		
		main_mask_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (invite_method_shown) {
					hideInviteMethodLayout();

					return;
				}

				if (invite_type_shown) {
					hideInviteTypeLayout();

					return;
				}

				if(share_method_shown)
				{
					hideShareMethodLayout();
					
					return;
				}
			}
			
		});

		invite_wechat_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_WECHAT;
				hideInviteMethodLayout();
				
				invite();
			}

		});
		invite_mail_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_MAIL;
				hideInviteMethodLayout();
				
				invite();
			}

		});
		invite_message_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_MESSAGE;
				hideInviteMethodLayout();
				
				invite();
			}

		});
		invite_qq_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteMethod = INVITE_METHOD_QQ;
				hideInviteMethodLayout();
				
				invite();
			}
			
		});
		invite_method_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideInviteMethodLayout();
			}

		});

		invite_type_fans.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteType = INVITE_TYPE_FANS;
				showInviteMethodLayout();
			}

		});
//		invite_type_friends.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				inviteType = INVITE_TYPE_FRIENDS;
//				hideInviteTypeLayout();
//
//				invite();
//			}
//
//		});
		invite_type_family.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteType = INVITE_TYPE_FAMILY;
				showInviteMethodLayout();
			}

		});
		invite_type_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideInviteTypeLayout();
			}

		});
		
		share_method_cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
			}
			
		});

		user_info_rada.setVisibility(View.GONE);
		// user_info_rada.setOnClickListener(new OnClickListener(){
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// });

		// user_info_follow.setVisibility(View.GONE);
		user_info_babies.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(MainActivity.this,
//						MyFolloweeActivity.class);
				
				Intent intent = new Intent(MainActivity.this,
						FollowBabiesActivity.class);
				MainActivity.this.startActivity(intent);
			}

		});

		user_info_fans.setVisibility(View.VISIBLE);
		user_info_fans.setOnClickListener(new OnClickListener(){
		
		 @Override
		 public void onClick(View v) {
		 // TODO Auto-generated method stub

				Intent intent = new Intent(MainActivity.this,
						FollowerActivity.class);
				MainActivity.this.startActivity(intent);
		 }
		
		 });

		user_info_message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						PostActivity.class);

				MainActivity.this.startActivity(intent);
			}

		});

		more_function_post.setVisibility(View.VISIBLE);
		 more_function_post.setOnClickListener(new OnClickListener(){
		
		 @Override
		 public void onClick(View v) {
		 // TODO Auto-generated method stub

				Intent intent = new Intent(MainActivity.this,
						MyMessageActivity.class);

				MainActivity.this.startActivity(intent);
		 }
		
		 });

		more_function_guide.setVisibility(View.VISIBLE);
		more_function_guide.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						BrowserActivity.class);
				intent.putExtra("url", "http://share.bebeanan.com/babysearch/table.html");
				intent.putExtra("title", "育儿指南");

				MainActivity.this.startActivity(intent);
			}

		});
		
		more_function_newborn.setVisibility(View.VISIBLE);
		more_function_newborn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						BrowserActivity.class);
				intent.putExtra("url", "https://share.bebeanan.com/babysearch/scinfo.html");
				intent.putExtra("title", "新生儿筛查");

				MainActivity.this.startActivity(intent);
			}

		});

		user_info_favorite.setVisibility(View.VISIBLE);
		user_info_favorite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						FavActivity.class);

				MainActivity.this.startActivity(intent);
			}

		});

		more_function_modify.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						ChangePasswordActivity.class);

				MainActivity.this.startActivity(intent);
			}

		});

		more_function_bind.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String users = MemoryHandler.getInstance().getKey("users");
				Log.v("Kite", "users data in MainActivity is " + users);
		        try {
		            JSONObject object = new JSONObject(users);
		            if(object.has("phone") && !object.getString("phone").equals(""))
		            {
		            	Intent intent = new Intent(MainActivity.this,
								PhoneChangeActivity.class);
		            	intent.putExtra("phone", object.getString("phone"));

						MainActivity.this.startActivity(intent);
		            }
		            else
		            {
		            	Intent intent = new Intent(MainActivity.this,
								PhoneBindActivity.class);

						MainActivity.this.startActivity(intent);
		            }
		            
		        } catch (Exception e) {
		            e.printStackTrace();
		            
		            Intent intent = new Intent(MainActivity.this,
							PhoneBindActivity.class);

					MainActivity.this.startActivity(intent);
		        }
			}

		});

		more_function_feedback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						FeedbackActivity.class);

				MainActivity.this.startActivity(intent);
			}

		});

		more_function_about.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(MainActivity.this,
						AboutActivity.class);

				MainActivity.this.startActivity(intent);
			}

		});

		more_function_logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				builder = new AlertDialog.Builder(MainActivity.this);
				builder.setMessage("正在退出");
				dialog = builder.show();
				showBuilder = true;

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
				NetHandler logoutHandler = new NetHandler(MainActivity.this, 
						NetHandler.METHOD_POST, "/users/logout", param, null) {

					@Override
					public void handleRsp(Message msg) {
						// TODO Auto-generated method stub

						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");

						if (code == 200) {
							Intent intent = new Intent(MainActivity.this,
									LoginActivity.class);
							MainActivity.this.startActivity(intent);
							MainActivity.this.finish();

							Editor edit = userPreferences.edit();
							edit.clear();
							edit.commit();

							StoreHandler.clearBabies();
							StoreHandler.clearCookies();
							StoreHandler.clearCache();
							StoreHandler.clearFolloweeCache();
							StoreHandler.clearFansCache();
						}
					}

				};

				logoutHandler.start();
			}

		});

		main_list.setMode(Mode.BOTH);
		main_list.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				hasMorePages = true;
				since = null;

				refresh(false, 0);
			}

			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub

				if (hasMorePages) {
					refresh(true, mData.size() + 1);
				} else {
					Toast toast = Toast.makeText(MainActivity.this, "没有更多了",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					Runnable runnable = new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							main_list.onRefreshComplete();
						}

					};

					Handler handler = new Handler();
					handler.postDelayed(runnable, 1000);
				}
			}

		});
		
		ILoadingLayout headLabels = main_list.getLoadingLayoutProxy(true, false);
		headLabels.setTextTypeface(Utils.typeface);
		ILoadingLayout endLabels = main_list.getLoadingLayoutProxy(false, true);
		endLabels.setPullLabel("上拉刷新...");
		endLabels.setReleaseLabel("放开以刷新...");
		endLabels.setRefreshingLabel("正在载入...");
		endLabels.setTextTypeface(Utils.typeface);

		main_list.getRefreshableView().setOnTouchListener(
				new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub

						if (event.getAction() == MotionEvent.ACTION_MOVE) {

							hideCommentEditLayout();

							float y = event.getRawY();

							if (Math.abs(y - lastY) > 50) {
								if (lastY != 0 && y < lastY) {
									// main_bottom_layout.setVisibility(View.GONE);
									hideBottomLayout();
								} else if (lastY != 0 && y > lastY) {
									// main_bottom_layout.setVisibility(View.VISIBLE);
									showBottomLayout();
								}

								if (mAdapter != null && mAdapter.isTriangleListShown()) {
									
									mAdapter.hideTriangleLayout();

								}
								
								lastY = y;
							}

							if (lastSensitiveY != 0 && y < lastSensitiveY) {
								scrollingUp = false;
								Log.v("Kite", "Scrolling down.");
							} else if (lastSensitiveY != 0
									&& y > lastSensitiveY) {
								scrollingUp = true;
								Log.v("Kite", "Scrolling up.");
							}

							lastSensitiveY = y;
							
						}

						else if (event.getAction() == MotionEvent.ACTION_UP) {
							lastY = 0;
							lastSensitiveY = 0;
						}

						return false;
					}
				});

		main_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				if(position == 0)
				{
					return;
				}
				
				if (mAdapter != null && mAdapter.isTriangleListShown()) {
					
					mAdapter.hideTriangleLayout();

					return;
				}
				
				Utils.feedDetail = mData.get(position-1);
				Intent intent = new Intent(MainActivity.this,
						FeedDetailActivity.class);
				intent.putExtra("feedId", (String)mData.get(position - 1).get("feedId"));
				intent.putExtra("position", position);

				MainActivity.this.startActivityForResult(intent, FEED_DETAIL_REQUEST_CODE);
			}

		});
		
		main_bottom_sound.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String baby = MemoryHandler.getInstance().getKey("baby");
				if (baby == null) {
					Intent intent = new Intent(MainActivity.this,
							BabyActivity.class);
					intent.putExtra("next", true);
					intent.putExtra("addBaby", true);

					MainActivity.this.startActivity(intent);

					return;
				}
				JSONArray babyArray;
				try {
					babyArray = new JSONArray(baby);
					if (babyArray.length() == 0) {
						Intent intent = new Intent(MainActivity.this,
								BabyActivity.class);
						intent.putExtra("next", true);
						intent.putExtra("addBaby", true);

						MainActivity.this.startActivity(intent);

						return;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent();
				intent.putExtra("EventType", EventActivity.EVENT_VOICE);
				currentBaby = userPreferences.getInt("currentBaby", 0);
				intent.putExtra("currentBaby", currentBaby);
				intent.setClass(MainActivity.this, EventActivity.class);
				startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);
			}

		});

		main_bottom_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String baby = MemoryHandler.getInstance().getKey("baby");
				if (baby == null) {
					Intent intent = new Intent(MainActivity.this,
							BabyActivity.class);
					intent.putExtra("next", true);
					intent.putExtra("addBaby", true);

					MainActivity.this.startActivity(intent);

					return;
				}
				JSONArray babyArray;
				try {
					babyArray = new JSONArray(baby);
					if (babyArray.length() == 0) {
						Intent intent = new Intent(MainActivity.this,
								BabyActivity.class);
						intent.putExtra("next", true);
						intent.putExtra("addBaby", true);

						MainActivity.this.startActivity(intent);

						return;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent();
				intent.putExtra("EventType", EventActivity.EVENT_NOTE);
				currentBaby = userPreferences.getInt("currentBaby", 0);
				intent.putExtra("currentBaby", currentBaby);
				intent.setClass(MainActivity.this, EventActivity.class);
				startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);
			}

		});

		main_bottom_photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String baby = MemoryHandler.getInstance().getKey("baby");
				if (baby == null) {
					Intent intent = new Intent(MainActivity.this,
							BabyActivity.class);
					intent.putExtra("next", true);
					intent.putExtra("addBaby", true);

					MainActivity.this.startActivity(intent);

					return;
				}
				JSONArray babyArray;
				try {
					babyArray = new JSONArray(baby);
					if (babyArray.length() == 0) {
						Intent intent = new Intent(MainActivity.this,
								BabyActivity.class);
						intent.putExtra("next", true);
						intent.putExtra("addBaby", true);

						MainActivity.this.startActivity(intent);

						return;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				rotateHide();

				rotateSelectShow();
			}

		});

		main_bottom_gallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent getImage = new Intent(Intent.ACTION_GET_CONTENT);
				// getImage.addCategory(Intent.CATEGORY_OPENABLE);
				// getImage.setType("image/jpeg");
				// startActivityForResult(getImage,
				// GET_IMAGE_ACTIVITY_REQUEST_CODE);

				// Intent intent = new Intent(Intent.ACTION_PICK,
				// android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				// startActivityForResult(intent,
				// GET_IMAGE_ACTIVITY_REQUEST_CODE);
				Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
				i.putExtra("maxSelection", 9);
				startActivityForResult(i, GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		});
		
		main_bottom_take.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a
																	// file to
																	// save the
																	// image

				intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the
																	// image
																	// file name

				// start the image capture Intent
				startActivityForResult(intent,
						CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		});

		main_bottom_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				rotateSelectHide();

				rotateShow();
			}
		});

		main_bottom_video.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String baby = MemoryHandler.getInstance().getKey("baby");
				if (baby == null) {
					Intent intent = new Intent(MainActivity.this,
							BabyActivity.class);
					intent.putExtra("next", true);
					intent.putExtra("addBaby", true);

					MainActivity.this.startActivity(intent);

					return;
				}
				JSONArray babyArray;
				try {
					babyArray = new JSONArray(baby);
					if (babyArray.length() == 0) {
						Intent intent = new Intent(MainActivity.this,
								BabyActivity.class);
						intent.putExtra("next", true);
						intent.putExtra("addBaby", true);

						MainActivity.this.startActivity(intent);

						return;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

				fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO); // create a
																	// file to
																	// save the
																	// video
				intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the
																	// image
																	// file name
				intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 1 * 30);
				intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0.8); // set the
																	// video
																	// image
																	// quality
																	// to high

				// start the Video Capture Intent
				startActivityForResult(intent,
						CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

				final Toast toast = Toast.makeText(MainActivity.this, "单次最多可录制 30秒^_^",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
//				final Handler toastHandelr = new Handler()
//				{
//
//					@Override
//					public void handleMessage(Message msg) {
//						// TODO Auto-generated method stub
//						super.handleMessage(msg);
//						
//						switch(msg.what)
//						{
//						case 0:
//							
//							toast.setText("单次最多可录制 30秒^_^");
//							toast.show();
//							
//							break;
//						}
//					}
//					
//				};
//				
//				new Thread()
//				{
//					public void run()
//					{
//						try {
//							this.sleep(3000);
//							toastHandelr.sendEmptyMessage(0);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//				}.start();
			}

		});

		main_bottom_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				// TODO Auto-generated method stub
				
				String users = MemoryHandler.getInstance().getKey("users");
		        try {
		            JSONObject object = new JSONObject(users);
		            if(object.has("phone") && !object.getString("phone").equals(""))
		            {
		            	Intent intent = new Intent();
						intent.setClass(MainActivity.this, SearchActivity.class);
						startActivity(intent);
		            }
		            else
		            {
		            	AlertDialog.Builder builder = new Builder(
								MainActivity.this);
		            	
		            	builder.setMessage("为了安全起见，采血条查询需要您绑定手机。现在绑定吗？");
						builder.setTitle("未绑定手机");

						builder.setPositiveButton(
								"确定",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {
										Intent intent = new Intent(MainActivity.this,
												PhoneBindActivity.class);

										MainActivity.this.startActivity(intent);
									}
								});

						builder.setNegativeButton(
								"取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {

									}
								});

						builder.create().show();
		            }
		            
		        } catch (Exception e) {
		            e.printStackTrace();
		            
		            AlertDialog.Builder builder = new Builder(
							MainActivity.this);
	            	
	            	builder.setMessage("为了安全起见，采血条查询需要您绑定手机。现在绑定吗？");
					builder.setTitle("未绑定手机");

					builder.setPositiveButton(
							"确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									Intent intent = new Intent(MainActivity.this,
											PhoneBindActivity.class);

									MainActivity.this.startActivity(intent);
								}
							});

					builder.setNegativeButton(
							"取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {

								}
							});

					builder.create().show();
		        }
			
			}

		});

		main_list.setSelected(true);

		main_comment_edit.setTypeface(Utils.typeface);
		main_comment_edit.setOnBackPressedListener(new OnBackPressedListener() {

			@Override
			public void onBackPressed() {
				// TODO Auto-generated method stub
				if (main_comment_edit_layout.getVisibility() == View.VISIBLE) {
					hideCommentEditLayout();
				}
			}

		});

		main_comment_send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final String commentContent = main_comment_edit.getText()
						.toString();
				hideCommentEditLayout();

				final int itemIndex = mAdapter.getItemIndex();
				JSONObject commentObject = new JSONObject();
				String feedId = (String) mData.get(itemIndex).get("feedId");
				try {
					commentObject.put("feedId", feedId);
					JSONObject user = new JSONObject();
					user.put("id", userPreferences.getString("userId", ""));
					user.put("nickname",
							userPreferences.getString("nickName", ""));
					commentObject.put("user", user);
					commentObject.put("content", commentContent);
					commentObject.put("type", "text");

					NetHandler handlerCommet = new NetHandler(MainActivity.this, 
							NetHandler.METHOD_POST, "/comments",
							new LinkedList<BasicNameValuePair>(), commentObject) {

						@Override
						public void handleRsp(Message msg) {
							// TODO Auto-generated method stub
							Bundle bundleComment = msg.getData();
							int codeComment = bundleComment.getInt("code");

							if (codeComment == 200) {
								Log.v("Kite", "评论发布成功");

								hasMorePages = true;
								since = null;

								// refresh(false, mAdapter.getItemIndex()+1);
								HashMap<String, String> tempComment = new HashMap<String, String>();
								tempComment.put(MainListAdapter.COMMENT_FROM_USER_NAME,
										userPreferences.getString("nickName",
												""));
								tempComment.put(
										MainListAdapter.COMMENT_CONTENT,
										commentContent);

								Map<String, Object> cur = mData.get(itemIndex);
								List<Map<String, String>> comments = (List) cur
										.get(MainListAdapter.COMMENTS);
								comments.add(0, tempComment);

								cur.put(MainListAdapter.COMMENT_NUMBER,
										(Integer) cur
												.get(MainListAdapter.COMMENT_NUMBER) + 1);

								mAdapter.notifyDataSetChanged();
							} else {
								Log.v("Kite", "comment fail because: "
										+ bundleComment.getString("data"));
							}
						}

					};

					handlerCommet.start();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
	}
	
	/****************
	*
	* 发起添加群流程。群号：完美宝贝用户群(384083314) 的 key 为： yjzSxhKRepxIwRHwo2yxYJeruAx0BIWq
	* 调用 joinQQGroup(yjzSxhKRepxIwRHwo2yxYJeruAx0BIWq) 即可发起手Q客户端申请加群 完美宝贝用户群(384083314)
	*
	* @param key 由官网生成的key
	* @return 返回true表示呼起手Q成功，返回fals表示呼起失败
	******************/
	public boolean joinQQGroup(String key) {
	    Intent intent = new Intent();
	    intent.setData(Uri.parse("mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D" + key));
	   // 此Flag可根据具体产品需要自定义，如设置，则在加群界面按返回，返回手Q主界面，不设置，按返回会返回到呼起产品界面    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
	    try {
	        startActivity(intent);
	        return true;
	    } catch (Exception e) {
	        // 未安装手Q或安装的版本不支持
	        return false;
	    }
	}

	private void hideInviteMethodLayout() {
		invite_method_shown = false;

		main_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			invite_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_method_layout.startAnimation(animation);
	}

	private void showInviteMethodLayout() {
		
		hideInviteTypeLayout();
		
		invite_method_shown = true;

		main_mask_layout.setVisibility(View.VISIBLE);
		invite_method_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			invite_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_method_layout.startAnimation(animation);
	}

	private void hideInviteTypeLayout() {
		invite_type_shown = false;

		main_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_type_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			invite_type_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_type_layout.startAnimation(animation);
	}

	private void showInviteTypeLayout() {
		invite_type_shown = true;

		main_mask_layout.setVisibility(View.VISIBLE);
		invite_type_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) invite_type_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			invite_type_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_TYPE_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		invite_type_layout.startAnimation(animation);
	}

	private void invite()
	{
		JSONObject inviteObject = new JSONObject();
		try {
			inviteObject.put("userType", inviteType);
			NetHandler handler = new NetHandler(MainActivity.this, NetHandler.METHOD_POST, "/invitation/generate", new LinkedList<BasicNameValuePair>(), inviteObject){

				@Override
				public void handleRsp(Message msg) {
					// TODO Auto-generated method stub
					Bundle bundle = msg.getData();
					
					int code = bundle.getInt("code");
					String data = bundle.getString("data");
					if(code == 200)
					{
						if(thirdLoginHandler == null)
						{
							thirdLoginHandler = new Handler() {
								@Override
								public void handleMessage(Message msg) {

									int id = msg.arg1;
									if (id == 1) {
//										Toast.makeText(MainActivity.this, "邀请成功",
//												Toast.LENGTH_SHORT).show();
									} else if (id == 2) {
//										Toast.makeText(MainActivity.this, "邀请失败",
//												Toast.LENGTH_SHORT).show();
									}
								}
							};
						}
						
						JSONObject resultObject;
						try {
							resultObject = new JSONObject(data);
							
							String inviteUrl = resultObject.getString("url");
							String inviteText = "我家宝宝在”完美宝贝“有窝啦，记录点滴成长，独家新生儿筛查系统！快来瞅瞅：" + inviteUrl + " 萌哒哒的手绘界面噢O(∩_∩)O~";
							
							switch(inviteMethod)
							{
							case INVITE_METHOD_WECHAT:
								
								Platform wechatPlat = ShareSDK.getPlatform("Wechat");
				                
								wechatPlat.setPlatformActionListener(new PlatformActionListener() {
									@Override
									public void onComplete(
											Platform platform,
											int i,
											HashMap<String, Object> stringObjectHashMap) {
										Message msg = new Message();
										msg.arg1 = 1;
										msg.arg2 = i;
										msg.obj = platform;
										thirdLoginHandler
												.sendMessage(msg);
										Log.v("DBG",
												"WECHAT SHARE DONE");
									}

									@Override
									public void onError(
											Platform platform,
											int i,
											Throwable throwable) {
										throwable
												.printStackTrace();
										Message msg = new Message();
										msg.arg1 = 2;
										msg.arg2 = i;
										msg.obj = platform;
										thirdLoginHandler
												.sendMessage(msg);
										Log.v("DBG",
												"WECHAT SHARE ERROR");
									}

									@Override
									public void onCancel(
											Platform platform,
											int i) {
										Log.v("DBG",
												"WECHAT SHARE CANCEL");
									}
								});
								ShareParams wechatSp = new ShareParams();
								wechatSp.setText(inviteText);
								wechatSp.setTitle("完美宝贝邀请");
								wechatSp.setShareType(Platform.SHARE_TEXT);
								wechatPlat.share(wechatSp);
								
								break;
								
							case INVITE_METHOD_MAIL:
								
								Intent mailIntent =new Intent(Intent.ACTION_SENDTO); 
								mailIntent.setType("*/*");
								mailIntent.putExtra(Intent.EXTRA_SUBJECT, "完美宝贝邀请"); 
								mailIntent.putExtra(Intent.EXTRA_TEXT, inviteText); 
								MainActivity.this.startActivity(Intent.createChooser(mailIntent,
				                        "请选择用于发送邮件的程序："));
								
								break;
								
							case INVITE_METHOD_MESSAGE:
								
								Intent messageIntent = new Intent(Intent.ACTION_VIEW);
								messageIntent.putExtra("sms_body", inviteText);
								messageIntent.setType("vnd.android-dir/mms-sms");
							    MainActivity.this.startActivity(messageIntent);
								
								break;
								
							case INVITE_METHOD_QQ:
								Platform QQPlat = ShareSDK.getPlatform(QQ.NAME);
				                
								QQPlat.setPlatformActionListener(new PlatformActionListener() {
									@Override
									public void onComplete(
											Platform platform,
											int i,
											HashMap<String, Object> stringObjectHashMap) {
										Message msg = new Message();
										msg.arg1 = 1;
										msg.arg2 = i;
										msg.obj = platform;
										thirdLoginHandler
												.sendMessage(msg);
										Log.v("DBG",
												"QQ SHARE DONE");
									}

									@Override
									public void onError(
											Platform platform,
											int i,
											Throwable throwable) {
										throwable
												.printStackTrace();
										Message msg = new Message();
										msg.arg1 = 2;
										msg.arg2 = i;
										msg.obj = platform;
										thirdLoginHandler
												.sendMessage(msg);
										Log.v("DBG",
												"QQ SHARE ERROR");
									}

									@Override
									public void onCancel(
											Platform platform,
											int i) {
										Log.v("DBG",
												"QQ SHARE CANCEL");
									}
								});
								ShareParams QQSp = new ShareParams();
								QQSp.setText(inviteText);
								QQSp.setTitle("完美宝贝邀请");
								QQSp.setShareType(Platform.SHARE_TEXT);
								QQPlat.share(QQSp);
								
								break;
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					else
					{
						Log.v("Kite", "invite fail " + data);
					}
				}
				
			};
			
			handler.start();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void hideShareMethodLayout() {
		share_method_shown = false;

		main_mask_layout.setVisibility(View.GONE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) share_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight;
			share_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				-(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		share_method_layout.startAnimation(animation);
	}

	private void showShareMethodLayout(final String feedId, final String title,
			final String text, final String imageUrl) {
		share_method_shown = true;

		main_mask_layout.setVisibility(View.VISIBLE);
		share_method_layout.setVisibility(View.VISIBLE);

		{
			RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) share_method_layout
					.getLayoutParams();
			params1.topMargin = Utils.screenHeight
					- (int) ((INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP) * Utils.density);
			share_method_layout.setLayoutParams(params1);
		}

		Animation animation = new TranslateAnimation(0, 0,
				(INVITE_METHOD_HEIGHT_DP + INVITE_LAYOUT_OFFSET_DP)
						* Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		share_method_layout.startAnimation(animation);
		
		share_wechat_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				sharePlatform = ShareSDK.getPlatform(Wechat.NAME);
				
				share(sharePlatform, feedId, text, imageUrl);
			}
			
		});
		
		share_friend_circle_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				sharePlatform = ShareSDK.getPlatform(WechatMoments.NAME);
				
				share(sharePlatform, feedId, text, imageUrl);
			}
			
		});
		
		share_qzone_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				sharePlatform = ShareSDK
						.getPlatform(
								MainActivity.this,
								QZone.NAME);
				
				share(sharePlatform, feedId, text, imageUrl);
			}
			
		});
		
		share_qq_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				sharePlatform = ShareSDK
						.getPlatform(
								MainActivity.this,
								QQ.NAME);
				
				share(sharePlatform, feedId, text, imageUrl);
			}
			
		});
		
		share_sina_layout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideShareMethodLayout();
				
				sharePlatform = ShareSDK
						.getPlatform(
								MainActivity.this,
								SinaWeibo.NAME);
				
				share(sharePlatform, feedId, text, imageUrl);
			}
			
		});
	}
	
	private void share(final Platform platform, final String feedId,
			final String text, final String imageUrl)
	{
		if (thirdShareHandler == null) {
			thirdShareHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {

					int id = msg.arg1;
					if (id == 1) {
						Toast toast = Toast.makeText(MainActivity.this, "分享成功",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						
					} else if (id == 2) {
						Toast toast = Toast.makeText(MainActivity.this, "分享失败",
								Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}
				}
			};
		}
		
		builder = new AlertDialog.Builder(MainActivity.this);
		builder.setMessage("分享中");
		dialog = builder.show();
		showBuilder = true;
		
		NetHandler netHandler = new NetHandler(MainActivity.this, 
				NetHandler.METHOD_GET,
				"/share/generate?feedId="
						+ feedId,
				new LinkedList<BasicNameValuePair>(),
				null) {

			@Override
			public void handleRsp(
					Message msg) {
				// TODO Auto-generated
				// method stub
				Bundle bundle = msg
						.getData();
				int code = bundle
						.getInt("code");
				Log.v("Kite",
						"share data is "
								+ bundle.getString("data"));

				dialog.dismiss();
				showBuilder = false;
				
				if (code == 200) {
					try {
						JSONObject shareObject = new JSONObject(
								bundle.getString("data"));
						String shareUrl = shareObject
								.getString("url");

						ShareParams sp = new ShareParams();
						sp.setText(text);
						
						String title = "";
						if(text.length()>=20)
						{
							title = text.substring(0, 20);
						}
						else
						{
							title = text + "……";
						}
						
						sp.setTitle(title);
						
						if(imageUrl != null)
						{
							sp.setImageUrl(imageUrl);
						}
						
						if(platform.getName().equals(WechatMoments.NAME) || platform.getName().equals(Wechat.NAME))
						{
							sp.setUrl(shareUrl);
							sp.setShareType(Platform.SHARE_WEBPAGE);
						}
						else
						{
							sp.setTitleUrl(shareUrl);
						}
						
						platform.setPlatformActionListener(new PlatformActionListener() {
							@Override
							public void onComplete(
									Platform platform,
									int i,
									HashMap<String, Object> stringObjectHashMap) {
								Message msg = new Message();
								msg.arg1 = 1;
								msg.arg2 = i;
								msg.obj = platform;
								thirdShareHandler
										.sendMessage(msg);
								Log.v("DBG",
										"SHARE DONE");
								
								JSONObject updateObject = new JSONObject();
								JSONObject incObject = new JSONObject();
								try {
									incObject.put("$inc", 1);
									updateObject.put("shareCount", incObject);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								final int itemIndex = mAdapter.getItemIndex();
								NetHandler incHandler = new NetHandler(MainActivity.this, NetHandler.METHOD_POST, "/feeds/"+feedId, new LinkedList<BasicNameValuePair>(), updateObject)
								{

									@Override
									public void handleRsp(Message msg) {
										// TODO Auto-generated method stub
										Bundle bundle = msg.getData();
										String data = bundle.getString("data");
										Log.v("Kite", "inc data is " + data);
										int code = bundle.getInt("code");
										
										if(code == 200)
										{
											Map<String, Object> cur = mData.get(itemIndex);

											cur.put(MainListAdapter.SHARE_NUMBER,
													(Integer) cur
															.get(MainListAdapter.SHARE_NUMBER) + 1);

											mAdapter.notifyDataSetChanged();
										}
									}
									
								};
								
								incHandler.start();
							}

							@Override
							public void onError(
									Platform platform,
									int i,
									Throwable throwable) {
								throwable
										.printStackTrace();
								Message msg = new Message();
								msg.arg1 = 2;
								msg.arg2 = i;
								msg.obj = platform;
								thirdShareHandler
										.sendMessage(msg);
								Log.v("DBG",
										"SHARE ERROR");
							}

							@Override
							public void onCancel(
									Platform platform,
									int i) {
								Log.v("DBG",
										"SHARE CANCEL");
							}
						});
						platform.share(sp);

					} catch (JSONException e) {
						// TODO
						// Auto-generated
						// catch block
						e.printStackTrace();
					}
				} else {
					Toast toast = Toast.makeText(
							MainActivity.this,
							"分享失败",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		};
		netHandler.start();
	}
	
	private void showCommentEditLayout() {
		main_comment_edit.setCatchBack(true);
		main_comment_edit_layout.setVisibility(View.VISIBLE);
		main_bottom_layout.setVisibility(View.GONE);
		main_bottom_select_layout.setVisibility(View.INVISIBLE);

		main_comment_edit.requestFocus();
		imm.showSoftInput(main_comment_edit, InputMethodManager.SHOW_FORCED);
	}

	private void hideCommentEditLayout() {
		main_comment_edit.setCatchBack(false);
		main_comment_edit_layout.setVisibility(View.GONE);
		main_bottom_layout.setVisibility(View.VISIBLE);
		main_bottom_select_layout.setVisibility(View.INVISIBLE);

		main_comment_edit.requestFocus();
		imm.hideSoftInputFromWindow(main_comment_edit.getWindowToken(), 0);
		main_comment_edit.setText("");
	}

	private void hideBottomLayout() {
		RelativeLayout.LayoutParams selectParams = (RelativeLayout.LayoutParams) main_bottom_select_layout
				.getLayoutParams();
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_layout
				.getLayoutParams();

		if (selectParams.bottomMargin != (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density)) {
			hideBottomSelectLayout();

			params.bottomMargin = (int) -(ANIMATION_MARGIN_DP * Utils.density);
			main_bottom_layout.setLayoutParams(params);
		} else if (params.bottomMargin == 0) {
			params.bottomMargin = (int) -(ANIMATION_MARGIN_DP * Utils.density);
			main_bottom_layout.setLayoutParams(params);

			TranslateAnimation animation = new TranslateAnimation(0, 0,
					-ANIMATION_MARGIN_DP * Utils.density, 0);
			animation.setDuration(500);
			animation.setFillAfter(false);

			main_bottom_layout.startAnimation(animation);
		}
	}

	private void showBottomLayout() {
		RelativeLayout.LayoutParams selectParams = (RelativeLayout.LayoutParams) main_bottom_select_layout
				.getLayoutParams();
		if (selectParams.bottomMargin != (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density)) {
			selectParams.bottomMargin = (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density);
			main_bottom_select_layout.setLayoutParams(selectParams);
		}

		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_layout
				.getLayoutParams();
		if (params.bottomMargin == 0) {
			return;
		}

		params.bottomMargin = 0;
		main_bottom_layout.setLayoutParams(params);

		TranslateAnimation animation = new TranslateAnimation(0, 0,
				ANIMATION_MARGIN_DP * Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		main_bottom_layout.startAnimation(animation);
	}

	private void hideBottomSelectLayout() {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_select_layout
				.getLayoutParams();

		params.bottomMargin = (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density);
		main_bottom_select_layout.setLayoutParams(params);

		TranslateAnimation animation = new TranslateAnimation(0, 0,
				-ANIMATION_LARGE_MARGIN_DP * Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		main_bottom_select_layout.startAnimation(animation);
	}

	private void showBottomSelectLayout() {
		hideBottomLayout();
		main_bottom_select_layout.setVisibility(View.VISIBLE);

		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_select_layout
				.getLayoutParams();

		params.bottomMargin = 0;
		main_bottom_select_layout.setLayoutParams(params);

		TranslateAnimation animation = new TranslateAnimation(0, 0,
				ANIMATION_LARGE_MARGIN_DP * Utils.density, 0);
		animation.setDuration(500);
		animation.setFillAfter(false);

		main_bottom_select_layout.startAnimation(animation);
	}

	private void rotateSelectShow() {
//		final Rotate3DAnimation rotation = new Rotate3DAnimation(90, 0, 0,
//				main_bottom_select_layout.getHeight(), 310.0f, true);
		final Rotate3DAnimation rotation = new Rotate3DAnimation(90, 0, main_bottom_select_layout.getWidth()/2,
				main_bottom_select_layout.getHeight(), 310.0f, true);
		rotation.setDuration(500);
		rotation.setFillAfter(false);
		rotation.setInterpolator(new AccelerateInterpolator());
		// 设置监听

		rotation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams selectParams = (RelativeLayout.LayoutParams) main_bottom_select_layout
						.getLayoutParams();
				if (selectParams.bottomMargin != 0) {
					selectParams.bottomMargin = 0;
					main_bottom_select_layout.setLayoutParams(selectParams);
				}
				main_bottom_select_layout.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub

			}
		});

		main_bottom_select_layout.startAnimation(rotation);
	}

	private void rotateSelectHide() {
//		final Rotate3DAnimation rotation = new Rotate3DAnimation(0, 90, 0,
//				main_bottom_select_layout.getHeight(), 310.0f, true);
		final Rotate3DAnimation rotation = new Rotate3DAnimation(0, 90, main_bottom_select_layout.getWidth()/2,
				main_bottom_select_layout.getHeight(), 310.0f, true);
		rotation.setDuration(500);
		rotation.setFillAfter(false);
		rotation.setInterpolator(new AccelerateInterpolator());
		// 设置监听

		rotation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_select_layout
						.getLayoutParams();

				params.bottomMargin = (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density);
				main_bottom_select_layout.setLayoutParams(params);
			}
		});

		main_bottom_select_layout.startAnimation(rotation);
	}

	private void rotateShow() {
//		final Rotate3DAnimation rotation = new Rotate3DAnimation(-90, 0, 0,
//				main_bottom_layout.getHeight(), 310.0f, true);
		final Rotate3DAnimation rotation = new Rotate3DAnimation(-90, 0, main_bottom_layout.getWidth()/2,
				main_bottom_layout.getHeight(), 310.0f, true);
		rotation.setDuration(500);
		rotation.setFillAfter(false);
		rotation.setInterpolator(new AccelerateInterpolator());
		// 设置监听

		rotation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_layout
						.getLayoutParams();
				if (params.bottomMargin != 0) {
					params.bottomMargin = 0;
					main_bottom_layout.setLayoutParams(params);
				}
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub

			}
		});

		main_bottom_layout.startAnimation(rotation);
	}

	private void rotateHide() {
//		final Rotate3DAnimation rotation = new Rotate3DAnimation(0, -90, 0,
//				main_bottom_layout.getHeight(), 310.0f, true);
		final Rotate3DAnimation rotation = new Rotate3DAnimation(0, -90, main_bottom_layout.getWidth()/2,
				main_bottom_layout.getHeight(), 310.0f, true);
		rotation.setDuration(500);
		rotation.setFillAfter(false);
		rotation.setInterpolator(new AccelerateInterpolator());
		// 设置监听

		rotation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) main_bottom_layout
						.getLayoutParams();

				params.bottomMargin = (int) -(ANIMATION_LARGE_MARGIN_DP * Utils.density);
				main_bottom_layout.setLayoutParams(params);
			}
		});

		main_bottom_layout.startAnimation(rotation);
	}

	private void refresh(boolean getMore, final int selectionAfterLoad) {
		// temp = new HashMap<String, Object>();
		// temp.put(MainListAdapter.AVATAR,
		// R.drawable.user_info_default_avatar);
		// temp.put(MainListAdapter.NICKNAME, "你谁呀");
		// temp.put(MainListAdapter.TIME, "2小时前");
		// temp.put(MainListAdapter.LOCATION, "浙大路");
		// temp.put(MainListAdapter.CONTENT,
		// "我请您吃，烧花鸭，烧雏鸡，烧子鹅，蒸羊羔，蒸熊掌，蒸鹿尾，卤鸡卤鸭酱鸡腊肉，松花小肚晾肉香肠。");
		// temp.put(MainListAdapter.SHARE_NUMBER, "0");
		// temp.put(MainListAdapter.UP_NUMBER, "1");
		// temp.put(MainListAdapter.COMMENT_NUMBER, "1");
		// temp.put(MainListAdapter.UP_NAMES, "那谁，老谁，小谁");
		//
		// List<Map<String, String>> comments;
		// Map<String, String> tempComment;
		// {
		// comments = new ArrayList<Map<String, String>>();
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "你猜我是谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "说得真溜儿，再来段京剧？");
		// comments.add(tempComment);
		// }
		// temp.put(MainListAdapter.COMMENTS, comments);
		//
		// List<Integer> imageList = new ArrayList<Integer>();
		// imageList.add(R.drawable.main_head_img);
		// imageList.add(R.drawable.main_head_img);
		// temp.put(MainListAdapter.IMAGE_LIST, imageList);
		//
		// mData.add(temp);
		//
		// temp = new HashMap<String, Object>();
		// temp.put(MainListAdapter.AVATAR, R.drawable.black_bear);
		// temp.put(MainListAdapter.NICKNAME, "我是谁");
		// temp.put(MainListAdapter.TIME, "2小时前");
		// temp.put(MainListAdapter.LOCATION, "浙大路");
		// temp.put(MainListAdapter.CONTENT,
		// "我正在城楼观山景，耳听得城外乱纷纷，旌旗招展空翻影，却原来是司马发来的兵。");
		// temp.put(MainListAdapter.SHARE_NUMBER, "0");
		// temp.put(MainListAdapter.UP_NUMBER, "1");
		// temp.put(MainListAdapter.COMMENT_NUMBER, "1");
		// temp.put(MainListAdapter.UP_NAMES, "那谁，老谁，小谁");
		//
		// {
		// comments = new ArrayList<Map<String, String>>();
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "那谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "好，一分钱听七段！");
		// comments.add(tempComment);
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "小谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "瞎说，一个煎饼听一下午。");
		// comments.add(tempComment);
		// }
		// temp.put(MainListAdapter.COMMENTS, comments);
		//
		// imageList = new ArrayList<Integer>();
		// imageList.add(R.drawable.main_head_img);
		// imageList.add(R.drawable.main_head_img);
		// imageList.add(R.drawable.main_head_img);
		// temp.put(MainListAdapter.IMAGE_LIST, imageList);
		//
		// mData.add(temp);
		//
		// temp = new HashMap<String, Object>();
		// temp.put(MainListAdapter.AVATAR, R.drawable.black_bear);
		// temp.put(MainListAdapter.NICKNAME, "我是谁");
		// temp.put(MainListAdapter.TIME, "2小时前");
		// temp.put(MainListAdapter.LOCATION, "浙大路");
		// temp.put(MainListAdapter.CONTENT,
		// "说点儿啥呢，报菜名和京剧都说过了，于是来段评书呗，但是我还不会，所以不说了吧。");
		// temp.put(MainListAdapter.SHARE_NUMBER, "0");
		// temp.put(MainListAdapter.UP_NUMBER, "1");
		// temp.put(MainListAdapter.COMMENT_NUMBER, "1");
		// temp.put(MainListAdapter.UP_NAMES, "那谁，老谁，小谁");
		//
		// {
		// comments = new ArrayList<Map<String, String>>();
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "那谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "好，一分钱听七段！");
		// comments.add(tempComment);
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "小谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "瞎说，一个煎饼听一下午。");
		// comments.add(tempComment);
		// }
		// temp.put(MainListAdapter.COMMENTS, comments);
		//
		// imageList = new ArrayList<Integer>();
		// // imageList.add(R.drawable.main_background);
		// temp.put(MainListAdapter.IMAGE_LIST, imageList);
		//
		// mData.add(temp);
		//
		// temp = new HashMap<String, Object>();
		// temp.put(MainListAdapter.AVATAR, R.drawable.black_bear);
		// temp.put(MainListAdapter.NICKNAME, "我是谁");
		// temp.put(MainListAdapter.TIME, "2小时前");
		// temp.put(MainListAdapter.LOCATION, "浙大路");
		// temp.put(MainListAdapter.CONTENT,
		// "我就是测试一下横版图片的显示效果。");
		// temp.put(MainListAdapter.SHARE_NUMBER, "0");
		// temp.put(MainListAdapter.UP_NUMBER, "1");
		// temp.put(MainListAdapter.COMMENT_NUMBER, "1");
		// temp.put(MainListAdapter.UP_NAMES, "那谁，老谁，小谁");
		//
		// {
		// comments = new ArrayList<Map<String, String>>();
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "那谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "好，一分钱听七段！");
		// comments.add(tempComment);
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "小谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "瞎说，一个煎饼听一下午。");
		// comments.add(tempComment);
		// }
		// temp.put(MainListAdapter.COMMENTS, comments);
		//
		// imageList = new ArrayList<Integer>();
		// // imageList.add(R.drawable.main_background);
		// temp.put(MainListAdapter.IMAGE_LIST, imageList);
		//
		// mData.add(temp);
		//
		// temp = new HashMap<String, Object>();
		// temp.put(MainListAdapter.AVATAR, R.drawable.black_bear);
		// temp.put(MainListAdapter.NICKNAME, "我是谁");
		// temp.put(MainListAdapter.TIME, "2小时前");
		// temp.put(MainListAdapter.LOCATION, "浙大路");
		// temp.put(MainListAdapter.CONTENT,
		// "我就是测试一下没有图片的显示效果。");
		// temp.put(MainListAdapter.SHARE_NUMBER, "0");
		// temp.put(MainListAdapter.UP_NUMBER, "1");
		// temp.put(MainListAdapter.COMMENT_NUMBER, "1");
		// temp.put(MainListAdapter.UP_NAMES, "那谁，老谁，小谁");
		//
		// {
		// comments = new ArrayList<Map<String, String>>();
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "那谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "好，一分钱听七段！");
		// comments.add(tempComment);
		//
		// tempComment = new HashMap<String, String>();
		// tempComment.put(MainListAdapter.COMMENT_NAME, "小谁");
		// tempComment.put(MainListAdapter.COMMENT_CONTENT, "瞎说，一个煎饼听一下午。");
		// comments.add(tempComment);
		// }
		// temp.put(MainListAdapter.COMMENTS, comments);
		//
		// imageList = new ArrayList<Integer>();
		// temp.put(MainListAdapter.IMAGE_LIST, imageList);
		//
		// mData.add(temp);

		if (!isLoading) {
			isLoading = true;

			if (!getMore) {
				
				NetHandler headMessageHandler = new NetHandler(MainActivity.this, NetHandler.METHOD_GET,
						"/notification?owner=" + userPreferences.getString("userId", "") + "&hasRead=false", new LinkedList<BasicNameValuePair>(), null) {

					@Override
					public void handleRsp(Message msg) {
						// TODO Auto-generated method stub
						Bundle bundle = msg.getData();
						
						String data = bundle.getString("data");
						int code = bundle.getInt("code");
						
						Log.v("Kite", "post data is " + data);
						if(code == 200)
						{
							headMessage = null;
							try {
								JSONArray postArray = new JSONArray(data);
								
								if(postArray.length() > 0)
								{
									JSONObject postObject = postArray.getJSONObject(postArray.length()-1);
									
									headMessage = postObject
											.getString("content");
								}
								
								List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
								NetHandler bannerHandler = new NetHandler(MainActivity.this, 
										NetHandler.METHOD_GET, "/banner", param, null) {

									@Override
									public void handleRsp(Message msg) {
										// TODO Auto-generated method stub
										Bundle bundle = msg.getData();
										int code = bundle.getInt("code");
										if (code == 200) {
											String data = bundle.getString("data");
											Log.v("Kite", "banner is " + data);
											try {
												JSONArray bannerArray = new JSONArray(data);
												
												if(bannerArray.length() > 0)
												{
													JSONObject banner = bannerArray
															.getJSONObject(0);
													Map<String, Object> temp = new HashMap<String, Object>();
													temp.put(MainListAdapter.HEAD_MESSAGE,
															headMessage);
													
													temp.put(MainListAdapter.HEAD_BACKGROUND,
															banner.getString("image"));
													temp.put(MainListAdapter.HEAD_BACKGROUND_URL,
															banner.getString("url"));
													temp.put(MainListAdapter.HEAD_NICKNAME,
															userPreferences.getString("nickName",
																	""));
													temp.put(MainListAdapter.HEAD_AVATAR,
															userPreferences.getString("avatarUrl",
																	null));
													mData.add(temp);

													Editor editor = userPreferences.edit();
													editor.putString("headBackground",
															banner.getString("image"));
													editor.putString("headBackgroundUrl",
															banner.getString("url"));
													editor.commit();
													
													getFeeds(false, selectionAfterLoad, temp);
												}
												else
												{
													Intent intent = new Intent(MainActivity.this, LoginActivity.class);
								            		intent.putExtra("loginRequired", true);
								            		MainActivity.this.startActivity(intent);
								            		
								            		main_list.onRefreshComplete();
								            		isLoading = false;
												}

											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										} else {
											main_list.onRefreshComplete();
											// dialog.dismiss();
											// showBuilder = false;
											main_progress.setVisibility(View.GONE);

											Toast toast = Toast.makeText(MainActivity.this, "加载失败",
													Toast.LENGTH_SHORT);
											toast.setGravity(Gravity.CENTER, 0, 0);
											toast.show();

											return;
										}
									}
								};

								bannerHandler.start();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else
						{
							Log.v("Kite", "post fail");
						}
					}

				};
				
				headMessageHandler.start();
			
			} else {
				getFeeds(true, selectionAfterLoad, null);
			}
		}
	}

	private void getFeeds(final boolean getMore, final int selectionAfterLoad,
			final Map<String, Object> banner) {

		List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
		param.add(new BasicNameValuePair("userId", userPreferences.getString(
				"userId", "")));
		param.add(new BasicNameValuePair("size", "10"));
		Log.v("Kite", "since is " + since);
		if (since != null) {
			param.add(new BasicNameValuePair("since", since));
		}

		NetHandler hanlder = new NetHandler(MainActivity.this, NetHandler.METHOD_GET,
				"/feeds/timeline", param, null) {

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				// dialog.dismiss();
				// showBuilder = false;
				isLoading = false;
				main_progress.setVisibility(View.GONE);

				Bundle bundle = msg.getData();
				int code = bundle.getInt("code");
				Log.v("Kite", "feeds code is " + code);

				main_list.onRefreshComplete();

				if (code == 200) {
					
					showGuide();
					
					String data = bundle.getString("data");
					Log.v("Kite", "feeds data is " + data);

//					try {
//						JSONArray feedsArray = new JSONArray(data);
//
//						if (feedsArray.length() < 10) {
//							hasMorePages = false;
//						}
//						ArrayList<Map<String, Object>> refreshData = null;
//						for (int i = 0; i < feedsArray.length(); i++) {
//							JSONObject feed = feedsArray.getJSONObject(i);
//
//							Map<String, Object> temp = new HashMap<String, Object>();
//
//							temp.put("feedId", feed.getString("id"));
//
//							JSONObject owner = feed.getJSONObject("owner");
//							if (owner.has("avatar")) {
//								temp.put(MainListAdapter.AVATAR,
//										owner.getString("avatar"));
//							} else {
//								temp.put(MainListAdapter.AVATAR, "");
//							}
//							temp.put(MainListAdapter.NICKNAME,
//									owner.getString("nickname"));
//							temp.put(MainListAdapter.FEEDS_OWNER_ID,
//									owner.getString("id"));
//							temp.put(MainListAdapter.USER_TYPE,
//									owner.getInt("usertype"));
//
//							double createdAt = feed.getDouble("createdAt");
//							DecimalFormat df = new DecimalFormat("0.000");
//							since = df.format(createdAt);
//
//							String time = getTime(createdAt);
//							temp.put(MainListAdapter.TIME, time);
//
//							if (feed.has("location")) {
//								temp.put(MainListAdapter.LOCATION,
//										feed.getString("location"));
//							} else {
//								temp.put(MainListAdapter.LOCATION, "");
//							}
//
//							if (feed.has("tag")) {
//								temp.put(MainListAdapter.EVENT_TAG,
//										feed.getInt("tag"));
//								Log.v("Kite", "has tag: " + feed.getInt("tag")
//										+ " pos is " + i);
//							} else {
//								temp.put(MainListAdapter.EVENT_TAG, 0);
//								Log.v("Kite", "no tag pos is " + i);
//							}
//
//							temp.put(MainListAdapter.CONTENT,
//									feed.getString("text"));
//							if (feed.has("shareCount")) {
//								temp.put(MainListAdapter.SHARE_NUMBER,
//										feed.getInt("shareCount"));
//							} else {
//								temp.put(MainListAdapter.SHARE_NUMBER, 0);
//							}
//
//							temp.put(MainListAdapter.UP_NUMBER,
//									feed.getInt("upCount"));
//							temp.put(MainListAdapter.COMMENT_NUMBER,
//									feed.getInt("commentCount"));
//
//							List<Map<String, String>> upList = new ArrayList<Map<String, String>>();
//							Map<String, String> upTemp;
//							if(feed.has("likes")){
//								JSONArray upNamesArray = feed
//										.getJSONArray("likes");
//								for (int j = 0; j < upNamesArray.length(); j++) {
//									JSONObject upObject = upNamesArray
//											.getJSONObject(j);
//									upTemp = new HashMap<String, String>();
//									upTemp.put(MainListAdapter.UP_ID,
//											upObject.getString("id"));
//									JSONObject userObject = upObject
//											.getJSONObject("user");
//									upTemp.put(MainListAdapter.UP_USER_NAME,
//											userObject.getString("nickname"));
//									upTemp.put(MainListAdapter.UP_USER_ID,
//											userObject.getString("id"));
//
//									upList.add(upTemp);
//								}
//							}
//							temp.put(MainListAdapter.UP_LIST, upList);
//
//							List<Map<String, String>> comments = new ArrayList<Map<String, String>>();
//							Map<String, String> tempComment;
//							if(feed.has("comments")){
//								JSONArray commentsArray = feed
//										.getJSONArray("comments");
//								for (int j = 0; j < commentsArray.length(); j++) {
//									JSONObject commentObject = commentsArray
//											.getJSONObject(j);
//									JSONObject userObject = commentObject
//											.getJSONObject("user");
//
//									tempComment = new HashMap<String, String>();
//									tempComment.put(
//											MainListAdapter.COMMENT_NAME,
//											userObject.getString("nickname"));
//									tempComment.put(
//											MainListAdapter.COMMENT_CONTENT,
//											commentObject.getString("content"));
//									comments.add(tempComment);
//								}
//							}
//							temp.put(MainListAdapter.COMMENTS, comments);
//
//							int type = feed.getInt("type");
//							temp.put(MainListAdapter.TYPE, type);
//							Log.v("Kite", "i is " + i + " type is " + type);
//
//							if (feed.has("urls")) {
//								JSONArray urls = feed.getJSONArray("urls");
//
//								switch (type) {
//								case EVENT_PHOTO:
//
//									List<String> imageList = new ArrayList<String>();
//									for (int j = 0; j < urls.length(); j++) {
//										String url = urls.getString(j);
//										imageList.add(url);
//										// imageList.add("http://ww1.sinaimg.cn/thumbnail/61e36371jw1elizp53a5rg206o04wb29.gif");
//									}
//									temp.put(MainListAdapter.IMAGE_LIST,
//											imageList);
//
//									break;
//
//								case EVENT_VIDEO:
//
//									String video_url = urls.getString(0);
//									temp.put(MainListAdapter.VIDEO_URL,
//											video_url);
//
//									break;
//
//								case EVENT_VOICE:
//
//									String voice_url = urls.getString(0);
//									temp.put(MainListAdapter.VOICE_URL,
//											voice_url);
//
//									break;
//								}
//
//							}
//
//							if (!getMore) {
//								if (refreshData == null) {
//									refreshData = new ArrayList<Map<String, Object>>();
//								}
//								refreshData.add(temp);
//							} else {
//								mData.add(temp);
//							}
//						}
//
//						if (!getMore && refreshData != null) {
//							mData.clear();
//							mData.add(banner);
//
//							for (int i = 0; i < refreshData.size(); i++) {
//								mData.add(refreshData.get(i));
//							}
//						}
//
//						mAdapter = new MainListAdapter(MainActivity.this, mData);
//						mAdapter.setOnTouchingZoomInListener(new OnTouchingZoomInListener() {
//
//							@Override
//							public void onTouchingZoomIn() {
//								// TODO Auto-generated method stub
//								int itemIndex = mAdapter.getItemIndex();
//								ArrayList<String> imageResourceList = (ArrayList) mData
//										.get(itemIndex).get(
//												MainListAdapter.IMAGE_LIST);
//
//								int imageIndex = mAdapter.getImageIndex();
//
//								Intent intent = new Intent(MainActivity.this,
//										GalleryActivity.class);
//								intent.putStringArrayListExtra(
//										"imageResourceList", imageResourceList);
//								intent.putExtra("imageIndex", imageIndex);
//
//								MainActivity.this.startActivity(intent);
//								MainActivity.this.overridePendingTransition(
//										R.anim.zoom_in, 0);
//							}
//						});
//
//						mAdapter.setOnVideoClickListener(new OnVideoClickListener() {
//
//							@Override
//							public void onVideoClickListener(String videoUrl) {
//								// TODO Auto-generated method stub
//								Intent intent = new Intent(MainActivity.this,
//										VideoActivity.class);
//								intent.putExtra("videoUrl", videoUrl);
//								MainActivity.this.startActivity(intent);
//							}
//						});
//
//						mAdapter.setOnShareImageClickListener(new OnShareImageClickListener() {
//
//							@Override
//							public void onShareImageClick() {
//								// TODO Auto-generated method stub
//								hasMorePages = true;
//								since = null;
//
//								refresh(false, mAdapter.getItemIndex() + 1);
//							}
//
//						});
//
//						mAdapter.setOnCommentImageClickListener(new OnCommentImageClickListener() {
//
//							@Override
//							public void onCommentImageClick() {
//								// TODO Auto-generated method stub
//								switch (main_comment_edit_layout
//										.getVisibility()) {
//								case View.GONE:
//
//									showCommentEditLayout();
//
//									break;
//
//								case View.VISIBLE:
//
//									hideCommentEditLayout();
//
//									break;
//								}
//							}
//
//						});
//
//						mAdapter.setOnUpImageClickListener(new OnUpImageClickListener() {
//
//							@Override
//							public void onUpImageClick(final int upListIndex) {
//								// TODO Auto-generated method stub
//								final int itemIndex = mAdapter.getItemIndex();
//								JSONObject upObject = new JSONObject();
//								String feedId = (String) mData.get(itemIndex)
//										.get("feedId");
//								try {
//									upObject.put("feedId", feedId);
//									JSONObject user = new JSONObject();
//									user.put("id", userPreferences.getString(
//											"userId", ""));
//									user.put("nickname", userPreferences
//											.getString("nickName", ""));
////									upObject.put("user", user);
//
//									String method = new String();
//									String uri = new String();
//									if (upListIndex != -1) {
//										method = NetHandler.METHOD_DELETE;
//										List<Map<String, String>> upList = (List) mData
//												.get(itemIndex)
//												.get(MainListAdapter.UP_LIST);
//										uri = "/feedup/"
//												+ upList.get(upListIndex).get(
//														MainListAdapter.UP_ID);
//										
//										Log.v("Kite", "upId is " + upList.get(upListIndex).get(
//														MainListAdapter.UP_ID));
//									} else {
//										method = NetHandler.METHOD_POST;
//										uri = "/feedup";
//									}
//
//									NetHandler handlerCommet = new NetHandler(
//											method,
//											uri,
//											new LinkedList<BasicNameValuePair>(),
//											upObject) {
//
//										@Override
//										public void handleRsp(Message msg) {
//											// TODO Auto-generated method stub
//											Bundle bundleUp = msg.getData();
//											int codeUp = bundleUp
//													.getInt("code");
//
//											String upData = bundleUp.getString("data");
//											if (codeUp == 200) {
//												if (upListIndex == -1) {
//													Log.v("Kite", "点赞成功");
//
//													Map<String, String> upTemp = new HashMap<String, String>();
//													upTemp.put(
//															MainListAdapter.UP_USER_NAME,
//															userPreferences
//																	.getString(
//																			"nickName",
//																			""));
//													upTemp.put(
//															MainListAdapter.UP_USER_ID,
//															userPreferences
//																	.getString(
//																			"userId",
//																			""));
//													
//													try {
//														JSONObject upObject = new JSONObject(upData);
//														
//														upTemp.put(
//																MainListAdapter.UP_ID,
//																upObject.getString("id"));
//													} catch (JSONException e) {
//														// TODO Auto-generated catch block
//														e.printStackTrace();
//													}
//
//													Map<String, Object> cur = mData
//															.get(itemIndex);
//													List<Map<String, String>> upList = (List) cur
//															.get(MainListAdapter.UP_LIST);
//													upList.add(upTemp);
//
//													cur.put(MainListAdapter.UP_NUMBER,
//															(Integer) cur
//																	.get(MainListAdapter.UP_NUMBER) + 1);
//
//													mAdapter.notifyDataSetChanged();
//												} else {
//													Log.v("Kite", "取消点赞成功");
//
//													Map<String, Object> cur = mData
//															.get(itemIndex);
//													List<Map<String, String>> upList = (List) cur
//															.get(MainListAdapter.UP_LIST);
//													upList.remove(upListIndex);
//
//													cur.put(MainListAdapter.UP_NUMBER,
//															(Integer) cur
//																	.get(MainListAdapter.UP_NUMBER) - 1);
//
//													mAdapter.notifyDataSetChanged();
//												}
//											} else {
//												Log.v("Kite",
//														"up fail because: "
//																+ bundleUp
//																		.getString("data"));
//											}
//										}
//
//									};
//
//									handlerCommet.start();
//								} catch (JSONException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//
//						});
//
//						main_list.setAdapter(mAdapter);
//
//						main_list.getRefreshableView().setSelection(
//								selectionAfterLoad);
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					
					setData(data, getMore, selectionAfterLoad, banner);

				} else {
					Toast toast = Toast.makeText(MainActivity.this, "加载失败", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					return;
				}
			}

		};

		hanlder.start();
	}

	private void setData(String data, final boolean getMore, final int selectionAfterLoad,
			final Map<String, Object> banner)
	{

		Log.v("Kite", "feeds data is " + data);

		try {
			JSONArray feedsArray = new JSONArray(data);

			if (feedsArray.length() < 10) {
				hasMorePages = false;
			}
			ArrayList<Map<String, Object>> refreshData = null;
			for (int i = 0; i < feedsArray.length(); i++) {
				JSONObject feed = feedsArray.getJSONObject(i);

				Map<String, Object> temp = new HashMap<String, Object>();

				temp.put("feedId", feed.getString("id"));

				JSONObject owner = feed.getJSONObject("owner");
				if (owner.has("avatar")) {
					temp.put(MainListAdapter.AVATAR,
							owner.getString("avatar"));
				} else {
					temp.put(MainListAdapter.AVATAR, "");
				}
				temp.put(MainListAdapter.NICKNAME,
						owner.getString("nickname"));
				temp.put(MainListAdapter.FEEDS_OWNER_ID,
						owner.getString("id"));
				temp.put(MainListAdapter.USER_TYPE,
						owner.getInt("usertype"));

				double createdAt = feed.getDouble("createdAt");
				DecimalFormat df = new DecimalFormat("0.000");
				since = df.format(createdAt);

				String time = getTime(createdAt);
				temp.put(MainListAdapter.TIME, time);

				if (feed.has("location")) {
					temp.put(MainListAdapter.LOCATION,
							feed.getString("location"));
				} else {
					temp.put(MainListAdapter.LOCATION, "");
				}

				if (feed.has("tag")) {
					temp.put(MainListAdapter.EVENT_TAG,
							feed.getInt("tag"));
					Log.v("Kite", "has tag: " + feed.getInt("tag")
							+ " pos is " + i);
				} else {
					temp.put(MainListAdapter.EVENT_TAG, 0);
					Log.v("Kite", "no tag pos is " + i);
				}

				temp.put(MainListAdapter.CONTENT,
						feed.getString("text"));
				if (feed.has("shareCount")) {
					temp.put(MainListAdapter.SHARE_NUMBER,
							feed.getInt("shareCount"));
				} else {
					temp.put(MainListAdapter.SHARE_NUMBER, 0);
				}

				temp.put(MainListAdapter.UP_NUMBER,
						feed.getInt("upCount"));
				temp.put(MainListAdapter.COMMENT_NUMBER,
						feed.getInt("commentCount"));

				List<Map<String, String>> upList = new ArrayList<Map<String, String>>();
				Map<String, String> upTemp;
				if(feed.has("likes")){
					JSONArray upNamesArray = feed
							.getJSONArray("likes");
					for (int j = 0; j < upNamesArray.length(); j++) {
						JSONObject upObject = upNamesArray
								.getJSONObject(j);
						upTemp = new HashMap<String, String>();
						upTemp.put(MainListAdapter.UP_ID,
								upObject.getString("id"));
						
						if(upObject.has("user") && !upObject.getString("user").equals("null"))
						{
							JSONObject userObject = upObject
									.getJSONObject("user");
							upTemp.put(MainListAdapter.UP_USER_NAME,
									userObject.getString("nickname"));
							upTemp.put(MainListAdapter.UP_USER_ID,
									userObject.getString("id"));

							upList.add(upTemp);
						}
					}
				}
				temp.put(MainListAdapter.UP_LIST, upList);

				List<Map<String, String>> comments = new ArrayList<Map<String, String>>();
				Map<String, String> tempComment;
				if(feed.has("comments")){
					JSONArray commentsArray = feed
							.getJSONArray("comments");
					
					for (int j = 0; j < commentsArray.length(); j++) {
						JSONObject commentObject = commentsArray
								.getJSONObject(j);
						
						if(commentObject.has("user") && !commentObject.getString("user").equals("null"))
						{
							JSONObject userObject = commentObject
									.getJSONObject("user");

							tempComment = new HashMap<String, String>();
							tempComment.put(
									MainListAdapter.COMMENT_FROM_USER_NAME,
									userObject.getString("nickname"));
							tempComment.put(
									MainListAdapter.COMMENT_FROM_USER_ID,
									userObject.getString("id"));
							
							tempComment.put(
									MainListAdapter.COMMENT_CONTENT,
									commentObject.getString("content"));
							comments.add(tempComment);
						}
					}
				}
				temp.put(MainListAdapter.COMMENTS, comments);

				int type = feed.getInt("type");
				temp.put(MainListAdapter.TYPE, type);
				Log.v("Kite", "i is " + i + " type is " + type);
				
//				temp.put(MainListAdapter.IS_FAVOR,
//						feed.getBoolean("isFavor"));
				temp.put(MainListAdapter.IS_FAVOR,
						false);

				if (feed.has("urls")) {
					JSONArray urls = feed.getJSONArray("urls");

					switch (type) {
					case EVENT_PHOTO:

						List<String> imageList = new ArrayList<String>();
						for (int j = 0; j < urls.length(); j++) {
							String url = urls.getString(j);
							imageList.add(url);
							// imageList.add("http://ww1.sinaimg.cn/thumbnail/61e36371jw1elizp53a5rg206o04wb29.gif");
						}
						temp.put(MainListAdapter.IMAGE_LIST,
								imageList);

						break;

					case EVENT_VIDEO:

						String video_url = urls.getString(0);
						temp.put(MainListAdapter.VIDEO_URL,
								video_url);

						break;

					case EVENT_VOICE:

						String voice_url = urls.getString(0);
						temp.put(MainListAdapter.VOICE_URL,
								voice_url);

						break;
					}

				}

				if (!getMore) {
					if (refreshData == null) {
						refreshData = new ArrayList<Map<String, Object>>();
					}
					refreshData.add(temp);
				} else {
					mData.add(temp);
					cacheDataArray.put(feedsArray.getJSONObject(i));
				}
			}

			if (!getMore && refreshData != null) {
				mData.clear();
				mData.add(banner);
				cacheDataArray = new JSONArray();

				for (int i = 0; i < refreshData.size(); i++) {
					mData.add(refreshData.get(i));
					cacheDataArray.put(feedsArray.getJSONObject(i));
				}
			}

			mAdapter = new MainListAdapter(MainActivity.this, mData);
			mAdapter.setOnTouchingZoomInListener(new OnTouchingZoomInListener() {

				@Override
				public void onTouchingZoomIn() {
					// TODO Auto-generated method stub
					int itemIndex = mAdapter.getItemIndex();
					ArrayList<String> imageResourceList = (ArrayList) mData
							.get(itemIndex).get(
									MainListAdapter.IMAGE_LIST);

					int imageIndex = mAdapter.getImageIndex();

					Intent intent = new Intent(MainActivity.this,
							GalleryActivity.class);
					intent.putStringArrayListExtra(
							"imageResourceList", imageResourceList);
					intent.putExtra("imageIndex", imageIndex);

					MainActivity.this.startActivity(intent);
					MainActivity.this.overridePendingTransition(
							R.anim.zoom_in, 0);
				}
			});

			mAdapter.setOnVideoClickListener(new OnVideoClickListener() {

				@Override
				public void onVideoClickListener(String videoUrl) {
					// TODO Auto-generated method stub
					
					Intent intent = new Intent(MainActivity.this,
							VideoActivity.class);
//					Intent intent = new Intent(MainActivity.this,
//							VideoDemoActivity.class);
					intent.putExtra("videoUrl", videoUrl);
					MainActivity.this.startActivity(intent);
				}
			});

			mAdapter.setOnShareImageClickListener(new OnShareImageClickListener() {

				@Override
				public void onShareImageClick(String feedId, String title,
						String text, String imageUrl) {
					// TODO Auto-generated method stub
					
					showShareMethodLayout(feedId, title,
							text, imageUrl);
					
				}

//				@Override
//				public void onShareImageClick(String feedId) {
//					// TODO Auto-generated method stub
//					LinkedList<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
//					param.add(new BasicNameValuePair("shareCount", "{$inc: 1}"));
//					NetHandler handler = new NetHandler(NetHandler.METHOD_PUT, "/feeds/" + feedId, param, null){
//
//						@Override
//						public void handleRsp(Message msg) {
//							// TODO Auto-generated method stub
//							Bundle bundle = msg.getData();
//							
//							String data = bundle.getString("data");
//							int code = bundle.getInt("code");
//							Log.v("Kite", "share data is " + data);
//							if(code == 200)
//							{
//								Log.v("Kite", "share inc success.");
//							}
//							else
//							{
//								Log.v("Kite", "share inc fail.");
//							}
//						}
//						
//					};
//					
//					handler.start();
//				}

			});

			mAdapter.setOnCommentImageClickListener(new OnCommentImageClickListener() {

				@Override
				public void onCommentImageClick() {
					// TODO Auto-generated method stub
					switch (main_comment_edit_layout
							.getVisibility()) {
					case View.GONE:

						showCommentEditLayout();

						break;

					case View.VISIBLE:

						hideCommentEditLayout();

						break;
					}
				}

			});

			mAdapter.setOnUpImageClickListener(new OnUpImageClickListener() {

				@Override
				public void onUpImageClick(final int upListIndex) {
					// TODO Auto-generated method stub
					mAdapter.setUpStatus(true);
					final int itemIndex = mAdapter.getItemIndex();
					JSONObject upObject = new JSONObject();
					String feedId = (String) mData.get(itemIndex)
							.get("feedId");
					try {
						upObject.put("feedId", feedId);
						JSONObject user = new JSONObject();
						user.put("id", userPreferences.getString(
								"userId", ""));
						user.put("nickname", userPreferences
								.getString("nickName", ""));
//						upObject.put("user", user);

						String method = new String();
						String uri = new String();
						if (upListIndex != -1) {
							method = NetHandler.METHOD_DELETE;
							List<Map<String, String>> upList = (List) mData
									.get(itemIndex)
									.get(MainListAdapter.UP_LIST);
							uri = "/feedup/"
									+ upList.get(upListIndex).get(
											MainListAdapter.UP_ID);
							
							Log.v("Kite", "upId is " + upList.get(upListIndex).get(
											MainListAdapter.UP_ID));
						} else {
							method = NetHandler.METHOD_POST;
							uri = "/feedup";
						}

						NetHandler handlerCommet = new NetHandler(MainActivity.this, 
								method,
								uri,
								new LinkedList<BasicNameValuePair>(),
								upObject) {

							@Override
							public void handleRsp(Message msg) {
								// TODO Auto-generated method stub
								Bundle bundleUp = msg.getData();
								int codeUp = bundleUp
										.getInt("code");

								mAdapter.setUpStatus(false);
								
								String upData = bundleUp.getString("data");
								if (codeUp == 200) {
									if (upListIndex == -1) {
										Log.v("Kite", "点赞成功");

										Map<String, String> upTemp = new HashMap<String, String>();
										upTemp.put(
												MainListAdapter.UP_USER_NAME,
												userPreferences
														.getString(
																"nickName",
																""));
										upTemp.put(
												MainListAdapter.UP_USER_ID,
												userPreferences
														.getString(
																"userId",
																""));
										
										try {
											JSONObject upObject = new JSONObject(upData);
											
											upTemp.put(
													MainListAdapter.UP_ID,
													upObject.getString("id"));
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										Map<String, Object> cur = mData
												.get(itemIndex);
										List<Map<String, String>> upList = (List) cur
												.get(MainListAdapter.UP_LIST);
										upList.add(upTemp);

										cur.put(MainListAdapter.UP_NUMBER,
												(Integer) cur
														.get(MainListAdapter.UP_NUMBER) + 1);

										mAdapter.notifyDataSetChanged();
									} else {
										Log.v("Kite", "取消点赞成功");

										Map<String, Object> cur = mData
												.get(itemIndex);
										List<Map<String, String>> upList = (List) cur
												.get(MainListAdapter.UP_LIST);
										upList.remove(upListIndex);

										cur.put(MainListAdapter.UP_NUMBER,
												(Integer) cur
														.get(MainListAdapter.UP_NUMBER) - 1);

										mAdapter.notifyDataSetChanged();
									}
								} else {
									Log.v("Kite",
											"up fail because: "
													+ bundleUp
															.getString("data"));
									
									mAdapter.notifyDataSetChanged();
								}
							}

						};

						handlerCommet.start();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});

			mAdapter.setOnDeleteImageClickListener(new OnDeleteImageClickListener(){

				@Override
				public void onDeleteImageClick(final int position) {
					// TODO Auto-generated method stub
					AlertDialog.Builder builder = new Builder(MainActivity.this);

					builder.setMessage("确定删除?");
					builder.setTitle("提示");

					builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							deleteFeed(position);
						}
					});

					builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

					builder.create().show();
				}
				
			});
			
			main_list.setAdapter(mAdapter);

			main_list.getRefreshableView().setSelection(
					selectionAfterLoad);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	}
	
	private void deleteFeed(final int position)
	{
		NetHandler handler = new NetHandler(MainActivity.this, NetHandler.METHOD_DELETE, "/feeds/"+mData.get(position).get("feedId"), new LinkedList<BasicNameValuePair>(), null)
		{

			@Override
			public void handleRsp(Message msg) {
				// TODO Auto-generated method stub
				Bundle bundle = msg.getData();
				
				String data = bundle.getString("data");
				int code = bundle.getInt("code");
				if(code ==200)
				{
					Log.v("Kite", "删除成功");
					mData.remove(position);
					mAdapter.notifyDataSetChanged();
					
					
					try {
						JSONArray tempCacheDataArray = new JSONArray();
						for (int i = 0; i < cacheDataArray.length(); i++) 
						{
							if(i != (position-1))
							{
								tempCacheDataArray.put(cacheDataArray
										.getJSONObject(i));
							}
						}
						cacheDataArray = tempCacheDataArray;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					Log.v("Kite", "删除失败");
					Log.v("Kite", "delete fail because " + data);
				}
			}
			
		};
		
		handler.start();
	}
	
	private void initTitle() {
		title_text.setVisibility(View.GONE);

		title_left_button.setVisibility(View.VISIBLE);
		title_left_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (!leftDrawerShown) {
					if (rightDrawerShown) {
						main_drawer.closeDrawer(Gravity.RIGHT);
						rightDrawerShown = false;
					}
					main_drawer.openDrawer(Gravity.LEFT);
					leftDrawerShown = true;
				}

				else {
					main_drawer.closeDrawer(Gravity.LEFT);
					leftDrawerShown = false;
				}
			}

		});

		title_right_button.setVisibility(View.VISIBLE);
		title_right_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (!rightDrawerShown) {
					if (leftDrawerShown) {
						main_drawer.closeDrawer(Gravity.LEFT);
						leftDrawerShown = false;
					}

					main_drawer.openDrawer(Gravity.RIGHT);
					rightDrawerShown = true;
				} else {
					main_drawer.closeDrawer(Gravity.RIGHT);
					rightDrawerShown = false;
				}
			}

		});

	}

	private void updateAvatar(final String avatarUrl) {
		try {

			JSONObject feeds = new JSONObject();
			feeds.put("avatar", avatarUrl);

			String userId = userPreferences.getString("userId", null);
			if (userId != null) {
				NetHandler handler = new NetHandler(MainActivity.this, NetHandler.METHOD_PUT,
						"/users/" + userId,
						new LinkedList<BasicNameValuePair>(), feeds) {
					@Override
					public void handleRsp(Message msg) {

						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						Log.v("Kite", "post feed code is " + code);
						if (code == 200) {

							Editor editor = userPreferences.edit();
							editor.putString("avatarUrl", avatarUrl);
							editor.commit();

							new UrlImageViewHelper().setUrlDrawable(
									user_info_avatar,
									userPreferences.getString("avatarUrl", ""));
							mData.get(0).put(
									MainListAdapter.HEAD_AVATAR,
									userPreferences
											.getString("avatarUrl", null));
							mAdapter.notifyDataSetChanged();

							Toast toast = Toast.makeText(MainActivity.this, "保存成功",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

						} else {
							Toast toast = Toast.makeText(MainActivity.this, "保存失败",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}
					}
				};
				handler.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateNickName(final String nickName) {
		try {

			JSONObject feeds = new JSONObject();
			feeds.put("nickname", nickName);
			Log.v("Kite", "nickName is " + nickName);

			String userId = userPreferences.getString("userId", null);
			if (userId != null) {
				NetHandler handler = new NetHandler(MainActivity.this, NetHandler.METHOD_PUT,
						"/users/" + userId,
						new LinkedList<BasicNameValuePair>(), feeds) {
					@Override
					public void handleRsp(Message msg) {

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						Log.v("Kite", "post feed code is " + code);
						if (code == 200) {

							Editor editor = userPreferences.edit();
							editor.putString("nickName", nickName);
							editor.commit();

							user_info_name.setText(userPreferences.getString(
									"nickName", ""));

							mData.get(0)
									.put(MainListAdapter.HEAD_NICKNAME,
											userPreferences.getString(
													"nickName", null));
							mAdapter.notifyDataSetChanged();

							Toast toast = Toast.makeText(MainActivity.this, "保存成功",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

						} else {
							Toast toast = Toast.makeText(MainActivity.this, "保存失败",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}
					}
				};
				handler.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode == FEED_DETAIL_REQUEST_CODE)
		{
			if(resultCode == FEED_DETAIL_DELETE_CODE)
			{
				int position = data.getIntExtra("position", -1);
				if(position != -1)
				{
					mData.remove(position - 1);
					mAdapter.notifyDataSetChanged();
				}
			}
			
			if(resultCode == FEED_DETAIL_UP_COMMENT_CODE)
			{
				int position = data.getIntExtra("position", -1);
				if(position != -1)
				{
					mData.remove(position - 1);
					mData.add(position-1, Utils.feedDetail);
					mAdapter.notifyDataSetChanged();
				}
			}
		}
		
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

			rotateSelectHide();

			rotateShow();

			if (resultCode == RESULT_OK) {

				String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
						.getAbsolutePath();
				Bitmap bitmap = null;
				try {
					BitmapFactory.Options opts = new BitmapFactory.Options();
					opts.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(filename, opts);
					int srcWidth = opts.outWidth;
					int srcHeight = opts.outHeight;
					int desWidth = 0;
					int desHeight = 0;
					int more = 0;
					// 缩放比例
					double ratio = 0.0;
					if (srcWidth > srcHeight) {
						ratio = srcWidth / 640;
						desWidth = 640;
						desHeight = (int) (srcHeight / ratio);
						more = srcWidth % 640 == 0 ? 0 : 1;
					} else {
						ratio = srcHeight / 640;
						desHeight = 640;
						desWidth = (int) (srcWidth / ratio);
						more = srcHeight % 640 == 0 ? 0 : 1;
					}
					// 设置输出宽度、高度
					BitmapFactory.Options newOpts = new BitmapFactory.Options();
					newOpts.inSampleSize = (int) (ratio) + more;
					newOpts.inJustDecodeBounds = false;
					newOpts.outWidth = desWidth;
					newOpts.outHeight = desHeight;
					// bitmap = BitmapFactory.decodeFile(filename, newOpts);
					bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
					FileOutputStream out = new FileOutputStream(filename);
					if (bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out)) {
						out.flush();
						out.close();

						if (!bitmap.isRecycled()) {
							bitmap.recycle();
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				// Image captured and saved to fileUri specified in the Intent

				Intent intent = new Intent();
				intent.putExtra("EventType", EventActivity.EVENT_PHOTO);
				intent.setData(fileUri);
				currentBaby = userPreferences.getInt("currentBaby", 0);
				intent.putExtra("currentBaby", currentBaby);
				intent.setClass(MainActivity.this, EventActivity.class);
				startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the image capture
			} else {
				// Image capture failed, advise user
			}
		}

		if (requestCode == GET_IMAGE_ACTIVITY_REQUEST_CODE) {

			if (resultCode == RESULT_OK) {
				rotateSelectHide();

				rotateShow();

				if (data != null) {
					Log.v("DBG", "Data: " + data.toString());
					Uri originalUri = data.getData();
					String[] proj = { MediaStore.Images.Media.DATA };
					Cursor actualimagecursor = managedQuery(originalUri, proj,
							null, null, null);
					int actual_image_column_index = actualimagecursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					actualimagecursor.moveToFirst();
					String img_path = actualimagecursor
							.getString(actual_image_column_index);
					File file = new File(img_path);
					Log.v("DBG", "Get File Path " + file.getAbsolutePath());
					String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
							.getAbsolutePath();
					Bitmap bitmap = null;
					try {
						BitmapFactory.Options opts = new BitmapFactory.Options();
						opts.inJustDecodeBounds = true;
						BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
						int srcWidth = opts.outWidth;
						int srcHeight = opts.outHeight;
						int desWidth = 0;
						int desHeight = 0;
						int more = 0;
						// 缩放比例
						double ratio = 0.0;
						if (srcWidth > srcHeight) {
							ratio = srcWidth / 640;
							desWidth = 640;
							desHeight = (int) (srcHeight / ratio);
							more = srcWidth % 640 == 0 ? 0 : 1;
						} else {
							ratio = srcHeight / 640;
							desHeight = 640;
							desWidth = (int) (srcWidth / ratio);
							more = srcHeight % 640 == 0 ? 0 : 1;
						}
						// 设置输出宽度、高度
						BitmapFactory.Options newOpts = new BitmapFactory.Options();
						newOpts.inSampleSize = (int) (ratio) + more;
						newOpts.inJustDecodeBounds = false;
						newOpts.outWidth = desWidth;
						newOpts.outHeight = desHeight;
						// bitmap =
						// BitmapFactory.decodeFile(file.getAbsolutePath(),
						// newOpts);
						bitmap = RotatePhoto.getPhotoRotated(
								file.getAbsolutePath(), newOpts);
						FileOutputStream out = new FileOutputStream(filename);
						if (bitmap
								.compress(Bitmap.CompressFormat.JPEG, 50, out)) {
							out.flush();
							out.close();

							if (!bitmap.isRecycled()) {
								bitmap.recycle();
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					Intent intent = new Intent();
					intent.putExtra("EventType", EventActivity.EVENT_PHOTO);
					intent.setData(fileUri);
					currentBaby = userPreferences.getInt("currentBaby", 0);
					intent.putExtra("currentBaby", currentBaby);
					intent.setClass(MainActivity.this, EventActivity.class);
					startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);

				}
			}

		}

		if (requestCode == GET_MULTI_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				rotateSelectHide();

				rotateShow();

				if (data != null) {
					Log.v("DBG", "Data: " + data.toString());
					String[] all_path = data.getStringArrayExtra("all_path");

					for (int i = 0; i < all_path.length; i++) {
						String img_path = all_path[i];
						File file = new File(img_path);
						Log.v("DBG", "Get File Path " + file.getAbsolutePath());
						String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE,
								i).getAbsolutePath();
						Bitmap bitmap = null;
						try {
							BitmapFactory.Options opts = new BitmapFactory.Options();
							opts.inJustDecodeBounds = true;
							BitmapFactory.decodeFile(file.getAbsolutePath(),
									opts);
							int srcWidth = opts.outWidth;
							int srcHeight = opts.outHeight;
							int desWidth = 0;
							int desHeight = 0;
							int more = 0;
							// 缩放比例
							double ratio = 0.0;
							if (srcWidth > srcHeight) {
								ratio = srcWidth / 640;
								desWidth = 640;
								desHeight = (int) (srcHeight / ratio);
								more = srcWidth % 640 == 0 ? 0 : 1;
							} else {
								ratio = srcHeight / 640;
								desHeight = 640;
								desWidth = (int) (srcWidth / ratio);
								more = srcHeight % 640 == 0 ? 0 : 1;
							}
							// 设置输出宽度、高度
							BitmapFactory.Options newOpts = new BitmapFactory.Options();
							newOpts.inSampleSize = (int) (ratio) + more;
							newOpts.inJustDecodeBounds = false;
							newOpts.outWidth = desWidth;
							newOpts.outHeight = desHeight;
							// bitmap =
							// BitmapFactory.decodeFile(file.getAbsolutePath(),
							// newOpts);
							bitmap = RotatePhoto.getPhotoRotated(
									file.getAbsolutePath(), newOpts);
							FileOutputStream out = new FileOutputStream(
									filename);
							if (bitmap.compress(Bitmap.CompressFormat.JPEG, 50,
									out)) {
								out.flush();
								out.close();

								if (!bitmap.isRecycled()) {
									bitmap.recycle();
								}
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					Intent intent = new Intent();
					intent.putExtra("EventType", EventActivity.EVENT_PHOTO);
					intent.putExtra("miNumber", all_path.length);
					intent.setData(fileUri);
					currentBaby = userPreferences.getInt("currentBaby", 0);
					intent.putExtra("currentBaby", currentBaby);
					intent.setClass(MainActivity.this, EventActivity.class);
					startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);

				}
			}

		}

		if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {

				Intent intent = new Intent();
				intent.putExtra("EventType", EventActivity.EVENT_VIDEO);
				intent.setData(fileUri);
				currentBaby = userPreferences.getInt("currentBaby", 0);
				intent.putExtra("currentBaby", currentBaby);
				intent.setClass(MainActivity.this, EventActivity.class);
				startActivityForResult(intent, EVENTACTIVITY_REQUEST_CODE);

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the video capture
			} else {
				// Video capture failed, advise user
			}
		}

		if (requestCode == EVENTACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				hasMorePages = true;
				since = null;

				mData.clear();

				refresh(false, 0);
			}
		}

		if (requestCode == CITYACTIVITY_REQUEST_CODE) {
			if (resultCode == 0) {
				user_info_city.setText(data.getStringExtra("city"));
			}
		}

		if (requestCode == GET_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				if (data != null) {
					Log.v("DBG", "Data: " + data.toString());
					Uri originalUri = data.getData();
					String[] proj = { MediaStore.Images.Media.DATA };
					Cursor actualimagecursor = managedQuery(originalUri, proj,
							null, null, null);
					int actual_image_column_index = actualimagecursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					actualimagecursor.moveToFirst();
					String img_path = actualimagecursor
							.getString(actual_image_column_index);
					File file = new File(img_path);
					Log.v("DBG", "Get File Path " + file.getAbsolutePath());

					String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
							.getAbsolutePath();
					Bitmap bitmap = null;
					try {
						BitmapFactory.Options opts = new BitmapFactory.Options();
						BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
						int srcWidth = opts.outWidth;
						int srcHeight = opts.outHeight;
						int desWidth = 0;
						int desHeight = 0;
						int more = 0;
						// 缩放比例
						double ratio = 0.0;
						if (srcWidth > srcHeight) {
							ratio = srcWidth / 640;
							desWidth = 640;
							desHeight = (int) (srcHeight / ratio);
							more = srcWidth % 640 == 0 ? 0 : 1;
						} else {
							ratio = srcHeight / 640;
							desHeight = 640;
							desWidth = (int) (srcWidth / ratio);
							more = srcHeight % 640 == 0 ? 0 : 1;
						}
						// 设置输出宽度、高度
						BitmapFactory.Options newOpts = new BitmapFactory.Options();
						newOpts.inSampleSize = (int) (ratio) + more;
						newOpts.inJustDecodeBounds = false;
						newOpts.outWidth = desWidth;
						newOpts.outHeight = desHeight;
						// bitmap =
						// BitmapFactory.decodeFile(file.getAbsolutePath(),
						// newOpts);
						bitmap = RotatePhoto.getPhotoRotated(
								file.getAbsolutePath(), newOpts);
						FileOutputStream out = new FileOutputStream(filename);
						if (bitmap
								.compress(Bitmap.CompressFormat.JPEG, 50, out)) {
							out.flush();
							out.close();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					builder = new AlertDialog.Builder(MainActivity.this);
					builder.setMessage("上传中");
					dialog = builder.show();
					showBuilder = true;
					List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
					param.add(new BasicNameValuePair("filename",
							getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
									.getAbsolutePath()));
					param.add(new BasicNameValuePair("filetype", "image"));
					NetHandler handler = new NetHandler(MainActivity.this, 
							NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
						@Override
						public void handleRsp(Message msg) {
							dialog.dismiss();
							showBuilder = false;

							Bundle bundle = msg.getData();
							int code = bundle.getInt("code");
							if (code == 200) {

								String data = bundle.getString("data");
								try {
									JSONArray array = new JSONArray(data);

									String avatarUrl = array.getJSONObject(0)
											.getString("url");
									updateAvatar(avatarUrl);

								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {
								Toast toast = Toast.makeText(getApplicationContext(), "上传失败",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								
								return;
							}
						}
					};
					handler.start();
				}
			}
		}

		if (requestCode == CAPTURE_IMAGE_AVATAR_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {

				builder = new AlertDialog.Builder(MainActivity.this);
				builder.setMessage("上传中");
				dialog = builder.show();
				showBuilder = true;

				String filename = getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
						.getAbsolutePath();
				Bitmap bitmap = null;
				try {
					BitmapFactory.Options opts = new BitmapFactory.Options();
					opts.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(filename, opts);
					int srcWidth = opts.outWidth;
					int srcHeight = opts.outHeight;
					int desWidth = 0;
					int desHeight = 0;
					// 缩放比例
					double ratio = 0.0;
					int more = 0;
					if (srcWidth > srcHeight) {
						ratio = srcWidth / 640;
						desWidth = 640;
						desHeight = (int) (srcHeight / ratio);
						more = srcWidth % 640 == 0 ? 0 : 1;
					} else {
						ratio = srcHeight / 640;
						desHeight = 640;
						desWidth = (int) (srcWidth / ratio);
						more = srcHeight % 640 == 0 ? 0 : 1;
					}
					// 设置输出宽度、高度
					BitmapFactory.Options newOpts = new BitmapFactory.Options();
					newOpts.inSampleSize = (int) (ratio) + more;
					newOpts.inJustDecodeBounds = false;
					// newOpts.outWidth = desWidth;
					// newOpts.outHeight = desHeight;
					// bitmap = BitmapFactory.decodeFile(filename, newOpts);
					bitmap = RotatePhoto.getPhotoRotated(filename, newOpts);
					FileOutputStream out = new FileOutputStream(filename);
					if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
						out.flush();
						out.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				List<BasicNameValuePair> param = new LinkedList<BasicNameValuePair>();
				param.add(new BasicNameValuePair("filename",
						getOutputMediaFile(MEDIA_TYPE_IMAGE, 0)
								.getAbsolutePath()));
				param.add(new BasicNameValuePair("filetype", "image"));
				NetHandler handler = new NetHandler(MainActivity.this, 
						NetHandler.METHOD_UPLOAD_FILE, "/file", param, null) {
					@Override
					public void handleRsp(Message msg) {
						dialog.dismiss();
						showBuilder = false;

						Bundle bundle = msg.getData();
						int code = bundle.getInt("code");
						if (code == 200) {

							String data = bundle.getString("data");
							try {

								JSONArray array = new JSONArray(data);
								updateAvatar(array.getJSONObject(0).getString(
										"url"));

							} catch (Exception e) {
								e.printStackTrace();
							}

							return;

						} else {

							Toast toast = Toast.makeText(getApplicationContext(), "上传失败",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							
							return;

						}
					}
				};
				handler.start();

			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the image capture
			} else {
				// Image capture failed, advise user
			}
		}
	}

	private Date getDateFromSeconds(double seconds) {
		long millis = (long) (seconds * 1000);
		Date date = new Date(millis);

		return date;
	}

	private String getTime(double seconds) {
		Date curDate = getDateFromSeconds(System.currentTimeMillis());
		int curYear = curDate.getYear();
		int curMonth = curDate.getMonth();
		int curDay = curDate.getDay();
		int curHour = curDate.getHours();
		int curMinute = curDate.getMinutes();
		int curSecond = curDate.getSeconds();

		Date date = getDateFromSeconds(seconds);
		int year = date.getYear();
		int month = date.getMonth();
		int day = date.getDay();
		int hour = date.getHours();
		int minute = date.getMinutes();
		int second = date.getSeconds();

		if (curYear == year) {
			if (curMonth == month) {
				if (curDay == day) {
					if (curHour == hour) {
						if (curMinute == minute) {
							return "刚刚";
						}

						return (curMinute - minute) + "分钟前";
					}

					return (curHour - hour) + "小时前";
				}
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
		String formattedDate = sdf.format(date);

		return formattedDate;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (invite_method_shown) {
				hideInviteMethodLayout();

				return true;
			}

			if (invite_type_shown) {
				hideInviteTypeLayout();

				return true;
			}

			if(share_method_shown)
			{
				hideShareMethodLayout();
				
				return true;
			}
			
			if (rightDrawerShown) {
				main_drawer.closeDrawer(Gravity.RIGHT);
				rightDrawerShown = false;

				return true;
			}

			if (leftDrawerShown) {
				main_drawer.closeDrawer(Gravity.LEFT);
				leftDrawerShown = false;

				return true;
			}

			if (mAdapter != null && mAdapter.isTriangleListShown()) {
				mAdapter.hideTriangleLayout();

				return true;
			}
			
			long curTime = System.currentTimeMillis();
			if ((curTime - exitTime) > 2000) {
				
				toast = Toast.makeText(MainActivity.this, "再按一次退出", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

				exitTime = curTime;
			} else {
				MainActivity.this.finish();

				if(cacheDataArray.length() != 0)
				{
					StoreHandler.storeCahce(cacheDataArray.toString());
				}
				
				System.exit(0);
			}

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

}